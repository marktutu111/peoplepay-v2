/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, StyleSheet, Image,Platform} from 'react-native';
import Colors from '../constants/colors';
import {Icon} from 'react-native-elements';

const Header = props => {
  return (
    <View style={styles.HeaderRow}>
      <View style={styles.LogoContainer}>
        <Image
          style={styles.Logo}
          source={require('../assets/images/PeoplePay2_white.png')}
          resizeMode="contain"
        />
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
          bottom: 5,
        }}>
        <Icon
          name="menu"
          type="feather"
          color="white"
          size={30}
          onPress={props.pressTo}
          containerStyle={styles.navIcon}
        />
        <Text style={{color: Colors.accent}}>{props.text}</Text>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  HeaderRow: {...Platform.select(
    {
        ios:{
            height:10,
            paddingTop:40
        },
        android:{
          height:100,
          paddingTop:25
        }
    }
  ),
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignContent: 'center',
    width: '100%',
    backgroundColor: Colors.secondary,
  },
  HeaderText: {
    textAlign: 'center',
    fontFamily: 'Roboto-Bold',
    fontSize: 18,
    textTransform: 'uppercase',
    color: Colors.accent,
    height:50,
    paddingHorizontal: 20,
  },
  navIcon: {
    padding: 20,
  },
  LogoContainer: {
    paddingHorizontal: 30,
  },
  Logo: {
    width: 100,
    height: 100,
    // paddingHorizontal: 30,
  },
});
export default Header;
