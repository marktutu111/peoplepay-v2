/* eslint-disable react-native/no-inline-styles */
/*This is an example of React Native App Intro Slider */
import React from 'react';
//import react in project
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image as ImageRn,
} from 'react-native';
//import all the required component
import AppIntroSlider from 'react-native-app-intro-slider';
import LoginPrimaryButton from './LoginPrimaryButton';
import SecondaryButton from './SecondaryButton';
import Colors from '../constants/colors';
// import Svg, {Circle, Image, ClipPath, Pattern, Mask} from 'react-native-svg';
import {SCREEN_HEIGHT, SCREEN_WIDTH} from '../constants/dimensions';


import TxtInput from './TxtInput';
//import AppIntroSlider to use it
export default class AppIntro extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showRealApp: false,
      //To show the main page of the app
    };
  }
  _onDone = () => {
    // this.props.login;
    this.setState({showRealApp: true});
  };
  _onSkip = () => {
    this.setState({showRealApp: true});
  };
  _renderItem = ({item}) => {
    return (
      <View
        style={{
          flex: 1,
          // backgroundColor: item.backgroundColor,
          alignItems: 'center',
          justifyContent: 'space-around',
          paddingBottom: 50,
        }}>
        <Text style={styles.title}>{item.title}</Text>
        <Svg height={SCREEN_HEIGHT} width={SCREEN_WIDTH}>
          <Image
            style={styles.image}
            href={item.image}
            resizeMode="contain"
            width="50%"
          />
        </Svg>
        <Text style={styles.text}>{item.text}</Text>
      </View>
    );
  };
  render() {
    //If false show the Intro Slides
    if (this.state.showRealApp) {
      //Real Application
      return (
        <View
          style={{
            flex: 1,
            minWidth: '100%',
            backgroundColor: Colors.accent,
            justifyContent: 'flex-start',
          }}>
          <View
            style={{
              width: '100%',
              justifyContent: 'flex-end',

              // paddingHorizontal: '10%',
            }}>
            <View
              style={styles.linearGradient}>
              <ImageRn
                source={require('../assets/images/peoplepay-icon-white.png')}
                style={styles.Logo}
                resizeMode="contain"
              />
            </View>
            <View style={styles.formContainer}>
              {/* Screen Title */}
              <View style={styles.HeaderTextMain}>
                <Text style={styles.HeaderText}>Welcome</Text>
                <Text style={styles.HeaderText1}>Sign in to continue</Text>
              </View>
              <View>
                <TxtInput
                  placeholder="Email or phone number"
                  IconName="user"
                  IconType="antdesign"
                  onChangeText={v => this.setState({email: v.trim()})}
                />
                <TxtInput
                  placeholder="Password"
                  IconName="lock"
                  IconType="feather"
                  onChangeText={v =>
                    this.setState({
                      password: v.trim(),
                    })
                  }
                />
                {/* <TxtInput placeholder="Repeat Password" /> */}
              </View>
              <TouchableOpacity
                style={styles.textForgotContainer}
                onPress={() => this.props.navigation.navigate('ForgotPassword')}
                activeOpacity={0.7}>
                <Text style={styles.textForgot}>Forgot password?</Text>
              </TouchableOpacity>
              <View style={styles.btn}>
                <LoginPrimaryButton
                  title="Login"
                  loading={this.state.loading}
                  pressTo={() => {
                    const {email, password} = this.state;
                    if (email === '' || password === '') {
                      return Alert.alert(
                        'Oops!',
                        'Email or password is required',
                      );
                    }
                    return this.login({
                      email: email,
                      password: password,
                    });
                  }}
                />
              </View>
              {/* End of container */}
            </View>
          </View>
        </View>
      );
    } else {
      //Intro slides
      return (
        <AppIntroSlider
          slides={slides}
          renderItem={this._renderItem}
          onDone={this.props.login}
          showSkipButton={false}
          // showNextButton={false}
          // showDoneButton={false}
          onSkip={this._onSkip}
        />
      );
    }
  }
}

const styles = StyleSheet.create({
  image: {
    alignSelf: 'center',
    alignContent: 'center',
    justifyContent: 'center',
  },
  formContainer: {
    backgroundColor: Colors.accent,
    width: '90%',
    height: '70%',
    alignSelf: 'center',
    position: 'absolute',
    borderRadius: 20,
    paddingHorizontal: 30,
    shadowColor: Colors.tetiary,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.21,
    shadowRadius: 2,
    elevation: 4,
  },
  textForgotContainer: {
    // paddingHorizontal: 30,
    marginVertical: 5,
  },
  textForgot: {
    color: Colors.tertiary,
    // textAlign: 'center',
    fontSize: 14,
    opacity: 0.8,
    // marginBottom: 40,
  },
  text: {
    fontSize: 16,
    color: Colors.tetiary,
    textAlign: 'center',
    // padding: 30,
    // paddingBottom: 10,
    paddingHorizontal: 65,
    opacity: 0.7,
  },
  HeaderTextMain: {
    color: Colors.secondary,
    // textAlign: 'left',
    paddingBottom: 20,
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    marginVertical: 10,
  },
  HeaderText: {
    fontSize: 35,
    color: Colors.secondary,
    textTransform: 'uppercase',
    // opacity: 0.7,
  },
  HeaderText1: {
    fontSize: 14,
    color: Colors.tertiary,
    // textAlign: 'center',
    opacity: 0.8,
    top: 3,
  },
  title: {
    fontSize: 25,
    color: 'black',
    textAlign: 'center',
    marginBottom: 16,
  },
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: 'rgba(0, 0, 0, .2)',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  LoginImage: {
    left: 200,
  },
  Logo: {
    width: 80,
    height: 80,
    alignSelf: 'center',
  },
  linearGradient: {
    position: 'absolute',
    top: '-20%',
    zIndex: 1,
    width: '60%',
    height: 100,
    borderTopRightRadius: 45,
    borderBottomRightRadius: 45,
    alignContent: 'center',
    justifyContent: 'center',
    // opacity: 0.9,
  },
});

const slides = [
  {
    key: 's1',
    text: 'Top-up your Mobile data and airtime on the go',
    // title: 'Mobile Recharge',
    image: require('../assets/images/undraw_transfer_money_rywa.png'),
    backgroundColor: '#ffffff',
  },
  {
    key: 's2',
    // title: 'Flight Booking',
    text: 'Skip Long queues, send and recieve money anywhere',
    image: require('../assets/images/undraw_mobile_pay_9abj.png'),
    backgroundColor: '#ffffff',
  },
  {
    key: 's3',
    // title: 'Great Offers',
    text: 'Enjoy Great offers on our all services',
    image: require('../assets/images/undraw_online_transactions_02ka.png'),
    backgroundColor: '#ffffff',
  },
];
