import React from 'react';
import {
  TouchableOpacity,
  Text,
  StyleSheet,
  Dimensions,
  View,
} from 'react-native';
import Card from './Card';
import {Icon} from 'react-native-elements';
import Colors from '../constants/colors';

const QuickserviceTile = (props) => {
  // console.log(props);
  let direction = props.direction === 'center' ? 'flex-end' : 'flex-start';
  let defaultSize = Dimensions.get('window').height > 700 ? 45 : 40;
  return (
    <TouchableOpacity onPress={props.pressTo} activeOpacity={0.4}>
      <Card
        style={{
          ...{
            width: Dimensions.get('window').height > 700 ? 140 : 120,
            height: Dimensions.get('window').height > 700 ? 140 : 120,
            margin: 10,
           
          },
          ...props.style,
        }}>
        <Icon
          name={props.iconname}
          type={props.icontype}
          containerStyle={styles.navIcon}
          color={Colors.secondary}
          size={props.iconsize !== undefined ? props.iconsize : defaultSize}
        />
        <View style={styles.textContainer}>
          <Text maxColumns={2} style={styles.text1}>
            {props.title}
          </Text>
          <Text maxColumns={2} style={styles.text2}>
            {props.title2}
          </Text>
        </View>
      </Card>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  textContainer: {
    // paddingVertical: 5,
    opacity: 0.8,
  },
  text2: {
    fontFamily: 'Roboto-Bold',
    color: Colors.tetiary,
    fontSize: 14,
    textAlign: 'center',
    bottom: 3,
  },

  text1: {
    fontFamily: 'Roboto-Bold',
    color: Colors.tetiary,
    fontSize: 14,
    textAlign: 'center',
  },
  navIcon: {
    alignItems: 'center',
    paddingVertical: 5,
    opacity: 0.8,
  },
});

export default QuickserviceTile;
