import React, {Component} from 'react';
import {Text, StyleSheet, View, Image, TouchableOpacity} from 'react-native';
import colors from '../constants/colors';
import {Icon} from 'react-native-elements';

export default class MyAccountCard extends Component {
  render() {
    return (
      <TouchableOpacity
        style={[styles.container, {backgroundColor: this.props.bgcolor}]}
        onPress={this.props.onPress}>
        <Image
          style={styles.background}
          source={require('../assets/images/bg1.png')}
          resizeMode="cover"
        />
        <Image
          style={styles.network}
          source={this.props.imageUri}
          resizeMode="cover"
        />
        <Text style={styles.MainTextLeft}>{this.props.title}</Text>
        <Text style={styles.number}>{this.props.issuer}</Text>
        <Text>{this.props.number}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    overflow: 'hidden',
    borderRadius: 10,
    borderWidth: 0.5,
  },
  network: {
    height: 50,
    width: 50,
    // top: 0,
    // bottom: 0,
  },
  background: {
    position:'absolute',
    width: '100%',
    opacity: 0.08,
    height: 100,
    marginTop: 18,
    borderRadius: 10,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    backgroundColor: 'white',
  },
  MainTextLeft: {
    color: '#000',
    fontFamily:'Roboto-Medium',
    fontSize: 16,
  },
  MainTextRight: {
    color: '#000',
    fontFamily: 'Roboto-Medium',
    fontSize: 16,
  },
  number: {
    color: '#6d6d6d',
    fontFamily: 'Roboto-Regular',
    fontSize: 16,
  },
  Accounttype: {
    position: 'absolute',
    color: '#6d6d6d',
    fontSize: 16,
  },
});
