import React, { Component } from 'react'
import { 
    Text, 
    View,
    FlatList,
    StyleSheet,
    Modal,
    TouchableOpacity,
    SafeAreaView,
    StatusBar,
    Platform,
    TextInput
} from 'react-native';

import Icon from "react-native-vector-icons/FontAwesome5";
import Contacts from 'react-native-contacts';
import colors from '../constants/colors';
import { PermissionsAndroid } from 'react-native';



export default class LazyContactPicker extends Component {

    state={
        numbers:[],
        contacts:[],
        _temp:[]
    }


    onsearch=(v)=>{
        try {
            const filter=this.state.contacts.filter(cont=>`${cont.givenName} ${cont.familyName}`.indexOf(v)>-1);
            this.setState({contacts:filter});
        } catch (err) {}
    }

    getContact(){
        if(Platform.OS==='android'){
            PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
                {
                  'title': 'Contacts',
                  'message': 'PeoplesPay would like to view your contacts.',
                  'buttonPositive': 'Please accept'
                }
              ).then((res)=>{
                Contacts.getAll().then(contacts=>{
                    this.setState(
                        {
                            contacts:contacts,
                            _temp:contacts,
                            numbers:[]
                        }
                    )
                })
              })
        }else{
            Contacts.getAll().then(contacts=>{
                this.setState(
                    {
                        contacts:contacts,
                        _temp:contacts,
                        numbers:[]
                    }
                )
            })
        }
    }

    render() {
        return (
            <Modal
                animationType="slide"
                onDismiss={()=>null}
                presentationStyle="fullScreen"
                visible={this.props.openModal}
                onShow={this.getContact.bind(this)}
            >
                <StatusBar barStyle="dark-content"/>
                <SafeAreaView>
                    <View style={{
                        paddingHorizontal:30,
                        paddingVertical:20,
                        flexDirection:'row',
                        alignItems:'center',
                        borderRadius:5,
                        borderColor:'#ddd',
                        borderBottomWidth:1
                    }}>
                        <TouchableOpacity onPress={()=>{
                            if(this.state.numbers.length>0){
                                this.setState(
                                    {
                                        numbers:[]
                                    }
                                )
                            }else{
                                this.props.closeModal();
                            }
                        }}>
                            <Icon
                                name="times"
                                size={30}
                                color={colors.secondary}
                            />
                        </TouchableOpacity>
                        <TextInput 
                            onChangeText={(text)=>{text===''?this.setState({contacts:this.state._temp}):this.onsearch(text)}} 
                            style={{ marginHorizontal:25,lineHeight:20}} 
                            placeholder='search name'
                        />
                    </View>
                    <FlatList
                        style={styles.list}
                        extraData={this.state}
                        data={this.state.numbers.length>0?this.state.numbers:this.state.contacts}
                        keyExtractor={(v,i)=>i.toString()}
                        renderItem={({item})=>{
                            switch (this.state.numbers.length>0) {
                                case true:
                                    return(
                                        <TouchableOpacity
                                            style={styles.listItem}
                                            onPress={()=>{
                                                this.setState(
                                                    {
                                                        numbers:[]
                                                    },()=>{
                                                        this.props.getContact(item.number)
                                                    }
                                                )
                                            }}
                                        >
                                            <Text style={styles.text}>
                                                {item.number}
                                            </Text>
                                        </TouchableOpacity>
                                    )
                                default:
                                    return (
                                        <TouchableOpacity
                                            style={styles.listItem}
                                            onPress={()=>{
                                                this.setState(
                                                    {
                                                        numbers:item.phoneNumbers
                                                    }
                                                )
                                            }}
                                        >
                                            <View style={styles.contItem}>
                                                <View style={styles.countCont}>
                                                    <Text style={{color:'#fff'}}>{item.phoneNumbers.length}</Text>
                                                </View>
                                                <Text style={styles.text}>
                                                    {`${item.givenName} ${item.familyName}`}
                                                </Text>
                                            </View>
                                            <Text style={styles.text1}>
                                                {item.phoneNumbers.map(num=>num.number).join('\n')}
                                            </Text>
                                        </TouchableOpacity>
                                    )
                            }
                        }}
                    />
                </SafeAreaView>
            </Modal>
        )
    }
}





const styles = StyleSheet.create({
    list:{
        padding:20
    },
    text:{
        fontSize:25,
        paddingHorizontal:20
    },
    text1:{
        fontSize:20,
        color:'#6d6d6d',
        paddingHorizontal:50
    },
    listItem:{
        borderBottomColor:'#eee',
        borderBottomWidth:0.5,
        paddingVertical:20
    },
    contItem:{
        flexDirection:'row',
        alignItems:'center',
        marginVertical:10
    },
    countCont:{
        borderRadius:50,
        width:30,
        height:30,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:colors.secondary
    }
})



// export default {
//     NumbersComponent,
//     ContactsPickerComponent
// }