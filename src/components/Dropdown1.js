import React from 'react';
import {View, StyleSheet, Picker} from 'react-native';
import Colors from '../constants/colors';

const Dropdown1 = props => {
  return (
    <View style={styles.container}>
      <View style={styles.textField}>
        <Picker
          selectedValue={props.selectedValue}
          style={styles.txtInput}
          onValueChange={props.onValueChange}>
          <Picker.item label={props.placeholder} value="" />
          {props.items &&
            props.items.map(
              (item = {}, i) =>
                item && (
                  <Picker.item
                    label={item.name}
                    value={item.value}
                    key={item.value}
                  />
                ),
            )}
        </Picker>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  dropdownContainer: {
    paddingVertical: 10,
    paddingHorizontal: 30,
  },
  container: {
    borderRadius: 50,
    height: 60,
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: Colors.tetiary,
    justifyContent: 'space-evenly',
    // backgroundColor: Colors.accent,
    alignItems: 'center',

    // shadowColor: '#000',
    // shadowOffset: {
    //   width: 0,
    //   height: 1,
    // },
    // shadowOpacity: 0.21,
    // shadowRadius: 2,
    // elevation: 4,
  },

  txtInput: {
    flexDirection: 'row',
    width: '100%',
    // backgroundColor: '#fff',
    // borderBottomWidth: 1,
    // borderColor: Colors.secondary,
    // justifyContent: 'center',
    // alignItems: 'center',
    paddingVertical: 10,
    fontFamily: 'Roboto-Bold',
    fontSize: 15,
    opacity: 0.7,
    color: Colors.tetiary,
    paddingHorizontal: 15,
  },
  navIcon: {
    paddingRight: 15,
  },
  userIcon: {
    backgroundColor: 'rgba(38,38,38,0.7)',
    height: 60,
    width: 60,
    justifyContent: 'center',
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
  },
  textField: {
    flexDirection: 'row',
    // justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  iconsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

export default Dropdown1;
