import React, {useState} from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Platform,
} from 'react-native';
import Colors from '../constants/colors';
import {Icon} from 'react-native-elements';
import Icons from 'react-native-vector-icons/FontAwesome5';
import { TouchableOpacity } from 'react-native';



const TxtInput=(props)=>{
  const [hideText,unHideText]=useState(true);
  return (
    <View style={{...styles.container, ...props.style}}>
      {!props.hideIcon && <TouchableOpacity
        onPress={props.onContact}
        hitSlop={{
          top:10,
          bottom:10,
          left:10,
          right:10
        }}
      >
        {props.fa ? <Icons name={props.IconName}
          type={props.IconType}
          size={15}
          color={Colors.secondary}
          style={{...styles.icon,marginHorizontal:10}}/>:<Icon
          name={props.IconName}
          type={props.IconType}
          size={25}
          color={Colors.secondary}
          style={styles.icon}
          containerStyle={styles.icon}
        />}
        </TouchableOpacity>}
      <TextInput
        multiline={props.multiline}
        maxLength={props.maxLength}
        keyboardType={props.keyboardType}
        secureTextEntry={props.password && hideText}
        placeholder={props.placeholder}
        style={{...styles.txtInput}}
        placeholderTextColor="#A0A0A0"
        onChangeText={props.onChangeText}
        autoCapitalize="none"
        defaultValue={props.defaultValue}
        editable={props.editable}
        enablesReturnKeyAutomatically={true}
      />
      {props.password && <Icon
        name={hideText ? 'eye' : 'eye-off'}
        type="feather"
        size={20}
        color={Colors.tetiary}
        style={styles.icon,{...Platform.select(
          {
            ios:{
              marginRight:5
            }
          }
        )}}
        containerStyle={styles.icon}
        onPress={() => {
          unHideText(!hideText);
        }}
      />}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    borderRadius:15,
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth:0.4,
    marginVertical: 4,
    overflow: 'hidden',
    borderColor: 'grey',
    paddingVertical:3
  },
  icon: {...Platform.select(
    {
      ios:{
        paddingLeft:10,
      },
      android:{
        paddingLeft:5,
      }
    }
  ),
    opacity: 0.7,
  },
  txtInput: {...Platform.select(
    {
      android:{
        paddingVertical:8,
        width:'75%'
      },
      ios:{
        paddingVertical:14,
        width:'70%'
      },
    }
  ),
    paddingLeft: 15,
    color: Colors.tetiary,
    paddingRight: 60,
    fontSize: 15,
    fontFamily: 'Roboto-Regular',
  },
});

export default TxtInput;
