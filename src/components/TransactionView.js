import React, {PureComponent} from 'react';
import {Text, Dimensions, StyleSheet, View} from 'react-native';

import SwiperFlatList from 'react-native-swiper-flatlist';
import Colors from '../constants/colors';

export default class App extends PureComponent {

  render() {
    const data=this.props.data;
    const getData=(type)=>{
      try {
        return this.props.toggle? 'X.00':this.props.data[type][0]['totalAmount'];
      } catch (err) {
        return 0;
      }
    }

    return (
      <View style={styles.container}>
        {/* <Text style={[styles.title, {color: 'white'}]}>
          Recent Transactions
        </Text> */}
        <SwiperFlatList autoplay autoplayDelay={5} autoplayLoop index={2}>
          <View style={styles.child}>
            {/* <Text style={styles.text}>1.</Text> */}
            <Text style={styles.text}>Send Money</Text>
            <Text style={styles.text2}>GHS{getData('SM')}</Text>
          </View>
          <View style={styles.child}>
            {/* <Text style={styles.text}>2.</Text> */}
            <Text style={styles.text}>Airtime Topup</Text>
            <Text style={styles.text2}>GHS{getData('AT')}</Text>
          </View>
          <View style={styles.child}>
            {/* <Text style={styles.text}>3.</Text> */}
            <Text style={styles.text}>Gift Cards</Text>
            <Text style={styles.text2}>GHS{getData('ECARDS')}</Text>
          </View>
          <View style={styles.child}>
            {/* <Text style={styles.text}>4.</Text> */}
            <Text style={styles.text}>Pay Bills</Text>
            <Text style={styles.text2}>GHS{getData('PB')}</Text>
          </View>
          <View style={styles.child}>
            {/* <Text style={styles.text}>5.</Text> */}
            <Text style={styles.text}>GHQ</Text>
            <Text style={styles.text2}>GHS{getData('QRP')}</Text>
          </View>
        </SwiperFlatList>
      </View>
    );
  }
}


export const {width, height}=Dimensions.get('window');

const styles=StyleSheet.create({
  container: {
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    height:40
  },
  child: {
    width:width/1.1,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal:12
  },
  title: {
    textAlign: 'center',
  },
  text: {
    textAlign:'center'
  },
  text2:{
    color:'green',
    marginRight:15
  }
});
