import React, { Component } from 'react';
import { View,StyleSheet,Text,TouchableOpacity } from 'react-native';
import DatePicker from 'react-native-date-picker';
import Icon from 'react-native-vector-icons/FontAwesome5';
import colors from '../constants/colors';
import moment from "moment";



class LazyDatePicker extends Component {

    state={
        toggle:false,
        date:new Date()
    }

    render (){
        return(
            <View style={[styles.container,this.props.style]}>
                <TouchableOpacity 
                    style={[styles.textContainer,this.state.toggle && { borderBottomEndRadius:0,borderBottomStartRadius:0 } ]} 
                    onPress={()=>this.setState({toggle:!this.state.toggle})}>
                        <View style={{flexDirection:'row',alignItems:'center'}}>
                            <Text style={{color:'#6d6d6d'}}>{this.props.placeholder}</Text>
                            <Text style={{fontSize:20,marginLeft:10}}>{moment(this.state.date).format('MM / DD / YYYY')}</Text>
                        </View>
                        <Icon 
                            name={this.state.toggle?'chevron-up':'chevron-down'}
                            color={colors.secondary}
                        />
                </TouchableOpacity>
                {this.state.toggle && <View style={{height:1,backgroundColor:'#ddd'}}></View>}
                {this.state.toggle && <View style={styles.pickerContainer}>
                    <DatePicker
                        mode={'date'}
                        date={this.state.date}
                        onDateChange={(d)=>{
                            this.setState(
                                {
                                    date:d
                                },()=>this.props.onDateChange && this.props.onDateChange(
                                    d
                                )
                            )
                        }}
                    />
                </View>}
            </View>
        )
    }
}

const styles=StyleSheet.create({
    container:{
        width:'100%',
        marginVertical:5,
        borderColor:'#ddd',
        borderWidth:1,
        borderRadius:10
    },
    textContainer:{
        borderRadius:10,
        flexDirection:'row',
        backgroundColor:'#fff',
        paddingVertical:15,
        paddingHorizontal:15,
        justifyContent:'space-between',
        alignItems:'center'
    },
    pickerContainer:{
        backgroundColor:'#fff',
        borderBottomEndRadius:10,
        borderBottomStartRadius:10
    }
});


export default LazyDatePicker;
