import React from 'react';
import { View,TouchableOpacity,StyleSheet,Text } from 'react-native';
import colors from '../constants/colors';
import {Icon} from 'react-native-elements';


const SupportButtons=(props)=>(
    <TouchableOpacity
        activeOpacity={0.8}
        style={styles.menuContainer}
        onPress={props.onPress}>
        <View style={styles.menuleftItem}>
            <Icon
                name={props.icon}
                type={props.type??'materialIcons'}
                size={25}
                color={colors.secondary}
            />
            <Text style={styles.menuText}>
                {props.text}
            </Text>
        </View>
        <Icon
            name="keyboard-arrow-right"
            size={20}
            color="#6d6d6d"
            style={styles.arrow}
        />
    </TouchableOpacity>
);


const styles = StyleSheet.create({
    menuContainer: {
        paddingVertical: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderColor: '#ccc',
        width: '100%',
    },
    menuContainerbottom: {
        paddingVertical: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderColor: '#ACACAC',
        width: '100%',
        alignItems:'center'
    },
    menuleftItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderColor: '#262626',
    },
    menuText: {
        fontSize: 16,
        color: '#262626',
        paddingLeft: 20,
    },
    helpContainer: {
        paddingVertical: 30,
    },
});


export default SupportButtons;
