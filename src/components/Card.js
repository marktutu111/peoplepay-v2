import React from 'react';
import {View, StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import colors from '../constants/colors';

const Card=props=>{
  return (
    <TouchableOpacity
      style={{...styles.card,...props.style}}
      activeOpacity={0.9}>
      {props.children}
    </TouchableOpacity>
  );
};

const styles=StyleSheet.create({
  card: {
    shadowColor: colors.tetiary,
    backgroundColor:colors.accent,
    padding: 20,
    borderRadius: 10,
    borderColor:'#ddd',
    borderWidth:1,
    marginHorizontal: 20,
    borderWidth:1
  },
  shadow:{
    shadowOffset: {width: 0, height: 1},
    shadowRadius: 6,
    shadowOpacity: 0.26,
    elevation: 3,
  }
});



export default Card;
