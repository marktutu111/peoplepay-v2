import React from 'react';
import { View,TouchableOpacity,ActivityIndicator } from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome5";
import colors from '../constants/colors';

const LazyArrowButton=(props)=>(
    <TouchableOpacity
        onPress={props.onPress}
        hitSlop={{top:10,left:10,right:10,bottom:10}}
        disabled={props.loading || props.disabled}
        style={[{
            width:50,
            height:50,
            justifyContent:'center',
            alignItems:'center',
            backgroundColor:colors.secondary,
            borderRadius:100,
            marginVertical:10,
            marginRight:'auto',
            marginLeft:'auto'
        },props.style]}
    >
        {
            props.loading?(
                <ActivityIndicator 
                    color="#fff"
                    size="small"
                />
            ):(
                <Icon
                    name={props.icon || "arrow-right"}
                    color="#fff"
                    size={props.size || 20}
                />
            )
        }
    </TouchableOpacity>
);

export default LazyArrowButton;
