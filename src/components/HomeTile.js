import React from 'react';
import {
  TouchableOpacity,
  Text,
  StyleSheet,
  Dimensions,
  View,
  Image,
} from 'react-native';
import Card from './Card';
import {Icon} from 'react-native-elements';
import Colors from '../constants/colors';

const HomeTile=(props)=>{
  let defaultSize=Dimensions.get('window').height * 0.06;
  return (
    <TouchableOpacity onPress={props.pressTo} activeOpacity={0.8}>
      <Card
        style={{
          width: Dimensions.get('window').width * 0.29,
          height: Dimensions.get('window').height * 0.15,
          // borderWidth:1,
          marginVertical:7,
          marginHorizontal:7,
          padding: 5,
          borderWidth:2,
        borderColor:'#eee',
          justifyContent:'center'
        }}>
        {/* <Image
          style={styles.Image}
          source={props.imageUri}
          resizeMode="contain"
        /> */}
        <Icon
          name={props.iconname}
          type={props.icontype}
          containerStyle={styles.navIcon}
          color={Colors.secondary}
          size={props.iconsize !== undefined ? props.iconsize:defaultSize}
        />
        <View style={styles.textContainer}>
          <Text maxColumns={1} style={styles.text1}>
            {props.title}
          </Text>
          <Text maxColumns={2} style={styles.text2}>
            {props.title2}
          </Text>
        </View>
      </Card>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  textContainer: {
    opacity:0.8,
  },
  ImageContainer: {
    justifyContent: 'center',
    alignContent: 'center',
    backgroundColor: Colors.accent,
    paddingTop:10,
    height:60,
    width:60,
    borderColor:Colors.secondary,
    shadowColor:'#000',
  },
  Image: {
    height:80,
    width:80,
    alignSelf: 'center',
    marginVertical: 10,
  },
  text2: {
    color: Colors.tetiary,
    fontSize: 14,
    textAlign: 'center',
    bottom: 3,
  },
  text1: {
    fontFamily:'Roboto-Bold',
    color:Colors.tetiary,
    fontSize:12,
    textAlign:'center'
  },
  navIcon: {
    alignItems:'center',
    paddingVertical:5,
    opacity:0.8,
  },
});

export default HomeTile;
