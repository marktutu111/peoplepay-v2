import React from 'react';
import {View, Text, StyleSheet, Image, Platform,SafeAreaView} from 'react-native';
import Colors from '../constants/colors';
// import {Icon} from 'react-native-elements';
import Icon from "react-native-vector-icons/FontAwesome";
import colors from '../constants/colors';
import { TouchableOpacity } from 'react-native';

const Header=props=>{
  return (
    <View style={{...styles.HeaderRow,backgroundColor:props.bg || '#fff'}}>
      <View style={{
          flexDirection:'row',
          justifyContent:'flex-start',
          alignItems:'center',
          marginHorizontal:20
        }}>
          <TouchableOpacity
            hitSlop={{top:20,left:20,right:20,bottom:20}}
            onPress={props.pressTo}
          >
            <Icon
              name={props.icon || 'long-arrow-left'}
              color={colors.secondary}
              size={props.size || 20}
            />
          </TouchableOpacity>
        <Text style={{color:props.titleColor || colors.secondary,marginLeft:10,fontWeight:'200'}}>{props.text}</Text>
      </View>
      {
        props.iconRight ? (
          <TouchableOpacity 
            onPress={props.iconRPress}
          hitSlop={{
            top:10,
            bottom:10,
            left:10,
            right:10
          }} style={{
            marginHorizontal:25,
          }}>
            <Icon name={props.iconRName} size={20} color={colors.secondary}/>
          </TouchableOpacity>
        ):(
          <Image
            style={styles.Logo}
            source={require('../assets/images/peoplepay-icon.png')}
            resizeMode="contain"
          />
        )
      }
    </View>
  );
};
const styles = StyleSheet.create({
  HeaderRow: {
    ...Platform.select({
      ios: {
        height: 100,
        paddingTop: 25,
      },
      android: {
        height: 50,
      },
    }),
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    alignContent: 'center',
    width: '100%',
    backgroundColor:Colors.secondary,
  },
  HeaderText: {
    textAlign: 'center',
    fontFamily: 'Roboto-Bold',
    fontSize: 18,
    textTransform: 'uppercase',
    color: Colors.accent,
    paddingHorizontal: 20,
  },
  LogoContainer: {
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row',
    marginHorizontal:30,
    width:25,
    height:25,
    overflow:'hidden'
  },
  Logo: {
    width:'20%',
    height:'35%'
  },
});
export default Header;
