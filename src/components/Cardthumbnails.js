import React from 'react';
import {View, Text, TouchableOpacity, Image,Dimensions} from 'react-native';
import Card from './Card';

const Cardthumbnails = props => {
  return (
    <TouchableOpacity>
      <Card style={{width: Dimensions.get('window').height > 700 ? 100 : 70, height:Dimensions.get('window').height > 700 ? 100 : 60, padding: 0,  }}>
        {props.children}
      </Card>
    </TouchableOpacity>
  );
};

export default Cardthumbnails;
