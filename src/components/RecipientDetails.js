import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import Colors from '../constants/colors';

const CardThumbnailImages = props => {
  return (
    <View style={styles.CardThumbnailContainer}>
      <View style={styles.Container1}>
        <View style={styles.ImageContainer}>
          <Image
            style={styles.Image}
            source={props.imageUri}
            resizeMode="contain"
      
          />
        </View>
      </View>
      <View style={styles.TextContainer}>
        <Text style={styles.TextName}>{props.name}</Text>
        <Text style={styles.TextNetwork}>{props.network}</Text>
        <Text style={styles.TextNumber}>{props.number}</Text>
        <Text style={styles.TextAmount}>{props.amount}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  CardThumbnailContainer: {
    // marginVertical: 30,
    width: '90%',
    justifyContent: 'center',
    backgroundColor: Colors.accent,
    height: 300,
    borderRadius: 10,
    alignItems: 'center',
    // shadowColor: '#000',
    // shadowOffset: {
    //   width: 0,
    //   height: 1,
    // },
    // shadowOpacity: 0.21,
    // shadowRadius: 2,
    // elevation: 1,
  },
  Container1: {
    flexDirection: 'row',
    // paddingVertical: 30,
  },
  ImageContainer: {
    justifyContent: 'center',
    alignContent: 'center',
    // backgroundColor: Colors.primary,
    padding: 10,
    height: 120,
    width: 120,
    borderColor: Colors.primary,
    borderWidth: 1,
    borderRadius: 60,
  },
  Image: {
    height: 90,
    width: 90,
    alignSelf: 'center',
    // padding: 50,
    overflow: "hidden",
  },
  TextContainer: {
    color: Colors.secondary,
    borderColor: Colors.secondary,
    borderWidth: 1,
    paddingHorizontal: 30,
    paddingVertical: 10,
    margin: 5,
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
  },
  TextName: {
    fontFamily: 'Roboto-Regular',
    fontSize: 20,
    opacity: 0.9,
    paddingVertical: 1,
    // color: Colors.primary,
    color: Colors.tetiary,
  },
  TextNetwork: {
    fontFamily: 'Roboto-Regular',
    fontSize: 18,
    opacity: 0.9,
    color: Colors.tetiary,
    paddingVertical: 1,
  },
  TextNumber: {
    fontFamily: 'Roboto-Regular',
    fontSize: 18,
    opacity: 0.9,
    color: Colors.tetiary,
    paddingVertical: 1,
  },
  TextAmount: {
    fontFamily: 'Roboto-Regular',
    fontSize: 18,
    opacity: 0.9,
    color: Colors.tetiary,
    paddingVertical: 1,
  },

  BottomIcon: {
    opacity: 0.9,
    justifyContent: 'flex-end',
    alignSelf: 'flex-end',
  },
  // BottomText: {
  //   fontFamily: 'Roboto-Regular',
  //   fontSize: 14,
  //   opacity: 0.8,
  //   color: colors.secondary,
  // },
});

export default CardThumbnailImages;
