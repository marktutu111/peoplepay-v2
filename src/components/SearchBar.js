import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import Colors from '../constants/colors';

const SearchBar = props => {
  return (
    <View style={styles.fieldContainer}>
      <Text style={styles.textContainer}>{props.title}</Text>
      <View style={styles.filterContainer}>
        <View style={styles.searchbarContainer}>
          <Icon
            name="search1"
            size={20}
            color={Colors.tertiary}
            style={styles.searchIcon}
          />
          <TextInput
            style={styles.searchbar}
            placeholder="Search by recipient name"
          />
        </View>
        {/* <TouchableOpacity activeOpacity={0.8} onPress={props.pressTo}>
          <Icon name="filter" size={30} color="#262626" style={styles.icon} />
        </TouchableOpacity> */}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  fieldContainer: {
    paddingVertical: 15,
  },
  filterContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textContainer: {
    fontSize: 16,
    fontFamily: 'Roboto-Bold',
    color: '#262626',
    marginBottom: 10,
    textTransform: 'uppercase',
  },
  searchbarContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.accent,
    borderColor: Colors.primary,
    borderWidth: 1,
    height: 40,
    borderRadius: 5,
    width: '100%',
  },
  searchbar: {
    width: '85%',
    fontSize: 16,
    fontFamily: 'Roboto-',
    color: Colors.primary,
    textAlign: 'left',
  },
  searchIcon: {
    marginRight: 5,
  },
  icon: {
    opacity: 0.7,
  },
});

export default SearchBar;
