import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import Colors from '../constants/colors';
import {Icon} from 'react-native-elements';
import colors from '../constants/colors';

const CardThumbnailImages = props => {
  return (
    <TouchableOpacity activeOpacity={0.7} onPress={props.pressTo}>
      <View style={styles.CardThumbnailContainer}>
        <View style={styles.Container1}>
          <View style={styles.ImageContainer}>
            <Image
              style={styles.Image}
              source={props.imageUri}
              resizeMode="contain"
            />
          </View>
          <View style={styles.TextContainer}>
            <Text style={styles.MainText}>{props.title}</Text>
            {/* <Text style={styles.SubText}>{props.subtitle}</Text> */}
          </View>
        </View>

        {/* <View style={styles.Container2}> */}
        {/* <Text style={styles.BottomText}>{props.navTitle}</Text> */}
        <Icon
          name="arrow-right"
          type="feather"
          size={18}
          color={Colors.tetiary}
          containerStyle={styles.BottomIcon}
        />
        {/* </View> */}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  CardThumbnailContainer: {
    paddingVertical: 10,
    paddingHorizontal: 30,
    marginBottom: 5,
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    height: 90,
    // borderRadius: 15,
    // opacity: 0.8,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.21,
    shadowRadius: 2,
    elevation: 8,
  },
  Container1: {
    flexDirection: 'row',
  },
  ImageContainer: {
    justifyContent: 'center',
    alignContent: 'center',
    backgroundColor: Colors.accent,
    paddingTop: 10,
    height: 60,
    width: 60,
    borderColor: Colors.secondary,
    shadowColor: '#000',
  },
  Image: {
    height: 60,
    width: 60,
    alignSelf: 'center',
  },
  TextContainer: {
    color: Colors.secondary,
    paddingHorizontal: 30,
    textAlign: 'center',
    alignSelf: 'center',
  },
  MainText: {
    fontSize: 18,
    opacity: 0.7,
    // color: Colors.primary,
    color: Colors.tetiary,
  },

  BottomIcon: {
    opacity: 0.7,
    justifyContent: 'flex-end',
    alignSelf: 'flex-end',
  },
  // BottomText: {
  //   fontFamily: 'Roboto-Bold',
  //   fontSize: 14,
  //   opacity: 0.8,
  //   color: colors.secondary,
  // },
});

export default CardThumbnailImages;
