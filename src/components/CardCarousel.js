import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Carousel from 'react-native-anchor-carousel';
const data = [
  {
    key: 's1',

    // title: 'Mobile Recharge',
    image: {
      uri: '../assets/images/undraw_online_payments_luau (1).svg',
    },
    backgroundColor: '#fff',
  },
  {
    key: 's2',
    // title: 'Flight Booking',

    image: {
      uri: '../assets/images/undraw_Queue_j6ij.svg',
    },
    backgroundColor: '#fff',
  },
  {
    key: 's3',
    // title: 'Great Offers',
    image: {
      uri: '../assets/images/undraw_pay_online_b1hk.svg',
    },
    backgroundColor: '#fff',
  },
];
const renderItem = ({item, index}) => {
  const {backgroundColor} = item;
  return (
    <TouchableOpacity
      style={[styles.item, {backgroundColor}]}
      onPress={() => {
        this._carousel.scrollToIndex(index);
      }}>
      .......
    </TouchableOpacity>
  );
};

const CardCarousel = props => {
  return (
    <View>
      <Carousel
        style={styles.carousel}
        data={data}
        renderItem={renderItem}
        itemWidth={200}
        containerWidth={250 - 20}
        separatorWidth={20}
        // ref={c => {
        //   _carousel = c;
        // }}
        //pagingEnable={false}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  carousel: {
    height: 150,
    width: 300,
  },
});

export default CardCarousel;
