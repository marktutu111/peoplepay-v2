import React, { Compon, Component } from 'react';
import { Alert } from "react-native";
import otpService from '../services/otp.service';


const WithOTP=(WrapperComponent)=>class extends Component {

  state={
    loading:false
  }

  sendOtp=async(number,resend=false)=>{
    try {
        if(!number || number.length !== 10)throw Error(
          'Invalid number'
        )
        if(resend) this.setState({loading:true});
        const response=await otpService.sendOTP(number);
        if (!response.success)throw this.props.navigation.goBack();
        if(resend){
            this.setState(
              {
                loading:false
              },()=>Alert.alert('OTP STATUS','your OTP has being sent to your phone')
            )
        }
    } catch (err) {
      this.setState({
        loading:false
      },()=>Alert.alert('OTP Not Sent',err.message))
    }
};

  render(){
    return <WrapperComponent loading={this.state.loading} sendOtp={this.sendOtp.bind(this)} {...this.props}/>
  }

}



export default WithOTP;