import React from 'react';
import {
  TouchableOpacity,
  Text,
  StyleSheet,
  Dimensions,
  View,
  Image
} from 'react-native';
import Card from './Card';
import {Icon} from 'react-native-elements';

const GiftCardTiles = (props) => {
  let direction = props.direction === 'center' ? 'flex-end' : 'flex-start';
  let defaultSize = Dimensions.get('window').height > 700 ? 50 : 50;
  return (
    <TouchableOpacity onPress={props.pressTo} activeOpacity={0.4}>
      <Card
        style={{
          ...{
            width:'auto',
            height:Dimensions.get('window').height > 700 ? 130 : 130,
            margin:5,
            flexDirection:'row',
            justifyContent: 'space-between',
            borderWidth:2,
          },
          ...props.style,
        }}>
        <View style={styles.textContainer}>
          <Icon
              name={props.iconname}
              type={props.icontype}
              containerStyle={styles.navIcon}
              color={props.bg}
              size={props.iconsize !== undefined ? props.iconsize : defaultSize}
            />
          <Text maxColumns={2} style={{...styles.text1,color:props.bg }}>
            {props.title}
          </Text>
          <Text maxColumns={2} style={styles.text2}>
            {props.title2}
          </Text>
        </View>
        <View style={{
            height:70,
            width:70,
            marginTop:10,
            justifyContent:'center',
            alignItems:'center',
            overflow: 'hidden',
            borderRadius:5,
            borderRadius:50
          }}>
            <Image 
              style={{
                width:'100%',
                height:'100%'
              }}
              source={props.image}
            />
          </View>
      </Card>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  textContainer: {
    // paddingVertical: 5,
    // opacity: 0.8,
  },
  text2: {
    fontFamily: 'Roboto-Bold',
    color: 'white',
    // fontSize: 30,
    textAlign: 'right',
    bottom: 3,
    marginTop:5
  },

  text1: {
    fontWeight:'500',
    color: 'white',
    fontSize: 25,
    textAlign: 'right',
    marginTop:10
  },
  navIcon: {
    alignItems: 'flex-start',
    // paddingVertical: 5,
    // opacity: 0.8,
  },
});

export default GiftCardTiles;
