import React, {Component} from 'react';
import {Text, View, StyleSheet,TouchableOpacity,Dimensions} from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";
import colors from '../constants/colors';
const {width,height}=Dimensions.get('screen');

const HistoryDetails=props=>{

  const getStatus=()=>{
    switch (props.data?.status) {
      case 'paid':
        return 'green';
      case 'pp':
        return 'orange';
      default:
        return 'red';
    }
  }


  return (
    <TouchableOpacity 
      style={[styles.historydetailsContainer,{ borderLeftColor:getStatus() }]}
      onPress={props.onPress}
    >
      <View style={styles.detailsBoldContainer}>
        <Text style={styles.detailsBold}>{props.title}</Text>
        <Text style={[styles.detailsBold,{ color:getStatus() }]}>{props.amount}</Text>
      </View>
      <View style={styles.detailslightContainer}>
        {props.data?.transaction_type === 'SM' && <Text style={styles.detailslight}>To: {props.from}</Text>}
        {props.data?.transaction_type === 'ECARDS' && <Text style={styles.detailslight}>{props.data?.gift_card?.ProductName}</Text>}
        {props.data?.transaction_type === 'QRP' && <Text style={styles.detailslight}>{props.data?.recipient_account_name}</Text>}
      </View>
        {props.description && <Text style={{marginHorizontal:10,color:colors.primary}}>{props.description}</Text>}
      <View style={{padding:10,flexDirection:'row',justifyContent:'space-between'}}>
        {props.showArrow && <Text style={{color:'#262626'}}>{'account: '+props.data?.payment_account_type}</Text>}
        {props.status && <Text style={{backgroundColor:'#000',paddingHorizontal:5,paddingVertical:5,color:'#fff'}}>{props.status}</Text>}
        <View style={{
          flexDirection:'row',
          alignItems:'center',
        }}>
          <Text>{props.date}</Text>
          {props.showArrow && <Icon
            style={{marginLeft:5}}
            name="chevron-right"
            color="#ccc"
          />}
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles=StyleSheet.create({
  historydetailsContainer: {
    width:width/1.1,
    backgroundColor:'#fff',
    borderColor: '#ddd',
    borderWidth: 0.6,
    borderLeftWidth: 1,
    marginVertical: 10,
    paddingVertical:10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.21,
    shadowRadius: 2,
    elevation: 4,
  },
  detailsBoldContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  detailsBold: {
    paddingHorizontal: 10,
    paddingVertical: 10,
    fontSize: 16,
    color: '#262626',
  },
  detailslightContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  detailslight: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    justifyContent: 'space-between',
    fontWeight: 'normal',
    fontSize: 12,
    color: '#262626',
  },
});

export default HistoryDetails;
