import React from 'react';
import Colors from '../constants/colors';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';
import {Icon} from 'react-native-elements';

const OtherButton=props=>{
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={styles.main}
      onPress={props.pressTo}>
      <View style={styles.ViewAllContainer}>
        <Icon
          name="plus"
          type="feather"
          size={16}
          color="#ffffff"
          containerStyle={styles.BottomIcon}
        />
        <Text style={styles.ViewAllText}>{props.title}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  main:{
    width:200
  },
  ViewAllContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
    backgroundColor: Colors.secondary,
    borderRadius: 30,
    alignSelf: 'center',
  },
  ViewAllText: {
    textAlign: 'center',
    paddingHorizontal: 2,
    paddingVertical: 10,
    color: 'white',
    fontSize: 12
  },
});
export default OtherButton;
