import React, { Component } from 'react';
import { Platform } from 'react-native';
import { View,Text,StyleSheet,ActivityIndicator,Dimensions,TouchableOpacity,Image } from 'react-native';
import Icons from "react-native-vector-icons/FontAwesome5";
const {width}=Dimensions.get('screen');



class CardContainer extends Component {

    render(){
        const props=this.props;
        const formatDate=()=>{
            try {
                const [
                    month,
                    year
                ]=props.date.split('-');
                return `${month}/${year}`;
            } catch (err) {return null};
        }
        
        return (
            <View style={[styles.card,props.style]} key={props._key}>
                <View style={styles.cardCont}>
                    <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                        <Image resizeMode="center" style={{ transform:[{scale:1.5}],height:50,width:50,marginLeft:20 }} source={require('../assets/images/thumbnail_high-1.png')}/>
                        <View style={{flexDirection:'row',alignItems:'center'}}>
                            <Text style={{marginVertical:10,fontWeight:'500',color:'#fff'}}>GHS{!props.loading_balance && <Text>{props.balance}</Text>}</Text>
                            {props.loading_balance && <ActivityIndicator size={'small'} color="#fff" style={{marginLeft:5}}/>}
                        </View>
                        {/* <TouchableOpacity
                            onPress={props.reload}
                            hitSlop={{top:10,bottom:10,left:10,right:10}}
                        >
                            <Icons 
                                name='sync' 
                                size={20}
                                color="#fff"
                            />
                        </TouchableOpacity> */}
                    </View>
                    <Text style={{
                        letterSpacing:5,
                        fontSize:17,
                        color:'#fff',
                    }}>{`0000 0000 0000 ${props.cardId || '0000'}`}</Text>
                    <Text style={{marginVertical:10,color:'#fff'}}>EXP <Text style={{fontWeight:'500'}}>{formatDate()}</Text></Text>
                    <View style={{
                        flexDirection:'row',
                        alignItems:'center',
                        justifyContent:'space-between'
                    }}>
                        <Text style={{fontWeight:'bold',color:'#fff'}}>{props.name}</Text>
                        <Image resizeMode="center" style={{transform:[{scale:1.7}]}} source={require('../assets/images/thumbnail_high.png')}/>
                    </View>
                </View>
                <View style={{
                    position:'absolute',
                    justifyContent:'center',
                    alignItems:'center',
                    overflow:'hidden',
                    width:'90%',
                    height:180,
                    borderRadius:10
                }}>
                    <Image resizeMode={Platform.OS==='ios'?'contain':'center'} source={require('../assets/images/ecobank.png')}/>
                </View>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    card:{
        width:width/1.1,
        justifyContent:'center',
        alignItems:'center',
        marginVertical:15,
        position:'relative',
        overflow:'hidden'
    },
    cardCont:{
        width:'90%',
        borderRadius:10,
        justifyContent:'center',
        borderWidth:2,
        borderColor:'#ddd',
        padding:20,
        height:180,
        zIndex:1000
    }
});

export default CardContainer;
