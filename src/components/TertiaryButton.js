import React from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
  View
} from 'react-native';
import Colors from '../constants/colors';
import Fontawesome from "react-native-vector-icons/FontAwesome5";
import {Icon} from 'react-native-elements';

const TertiaryButton=props=>{
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={styles.Container}
      disabled={props.loading || props.disabled}
      onPress={props.pressTo}>
        <View style={{flexDirection:'row',alignItems:'center'}}>
        {
          props.fas ? (<Fontawesome 
            name={props.IconName}
            size={20}
            color={Colors.secondary}
            containerStyle={styles.navIcon}
          />):(
            <Icon
              name={props.IconName}
              type={props.IconType}
              size={20}
              color={Colors.secondary}
              containerStyle={styles.navIcon}
            />
          )
        }
        <Text style={styles.text}>{props.title}</Text>
        </View>
      {props.loading && <ActivityIndicator />}
      {props.children}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  Container: {
    borderRadius:20,
    alignItems: 'center',
    marginVertical: 7,
    flexDirection: 'row',
    borderWidth: 1,
    borderColor:'#ddd',
    paddingVertical:15,
    paddingHorizontal: 20,
    width: '100%',
    height: 60,
    backgroundColor:Colors.accent,
    shadowColor:Colors.tetiary,
    // shadowOffset: {
    //   width: 0,
    //   height: 1,
    // },
    // shadowOpacity: 0.21,
    // shadowRadius: 10,
    // elevation: 3,
    justifyContent:'space-between'
  },
  text: {
    fontSize:15,
    paddingHorizontal: 20,
    textAlign: 'left',
    justifyContent: 'center',
    opacity: 0.8,
    fontWeight:'300',
    color: Colors.tetiary,
  },
});
export default TertiaryButton;
