import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image,Dimensions, Platform} from 'react-native';
import Colors from '../constants/colors';
import IconButton from './IconButton';
import {Icon} from 'react-native-elements';
const {height}=Dimensions.get('screen');

const BeneficiaryTiles=props=>{
  const renderIcon=()=>{
    switch (props.account_type) {
      case 'momo':
        return {
          name:'mobile',
          icon:'entypo'
        }
      case 'token':
      case 'card':
        return {
          name:'credit-card',
          icon:'font-awesome-5'
        }
      case 'bank':
        return {
          name:'bank',
          icon:'material-community'
        }
      case 'wallet':
        return {
          name:'wallet',
          icon:'font-awesome-5'
        }
      default:
        return {
          name:'',
          icon:''
        }
    }
  }
  return (
    <TouchableOpacity disabled={props.disabled} activeOpacity={0.8} onPress={props.pressTo}>
      <View style={styles.CardThumbnailContainer}>
        <View style={styles.Container1}>
          <View style={styles.ImageContainer}>
            {props.imageUri?.uri?<Image
              style={styles.Image}
              source={props.imageUri}
              resizeMode="contain"
            />:<Icon
            name={renderIcon().name}
            type={renderIcon().icon}
            size={25}
            color={Colors.secondary}
          />}
          </View>
          <View style={styles.TextContainer}>
            <Text style={styles.TitleText}>{props.title}</Text>
            <Text style={styles.MainText}>{props.issuer}</Text>
            <Text style={styles.MainText}>{"****" + props.number.substring(props.number.length - 4,props.number.length)}</Text>
          </View>
          {props.showDelete && <View style={styles.button}>
            <IconButton name={props.deleteIcon || "trash-alt"} onPress={props.delete}/>
          </View>}
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  CardThumbnailContainer: {
    marginHorizontal:28,
    paddingVertical:25,
    paddingHorizontal:20,
    marginVertical:5,
    justifyContent: 'center',
    backgroundColor: Colors.accent,
    height:height/10,
    borderRadius:25,
    borderColor:'#ddd',
    borderWidth:1,
    overflow:'hidden',
    ...Platform.select(
      {
        'android':{
          height:height/7.5
        }
      }
    )
  },
  Container1: {
    flexDirection: 'row',
    backgroundColor: Colors.accent,
  },
  ImageContainer: {
    justifyContent: 'center',
    alignContent: 'center',
    backgroundColor: Colors.accent,
    height:50,
    width:50,
    overflow:'hidden',
    borderColor:'#ddd',
    borderRadius:50,
    // borderWidth:1
  },
  Image: {
    height:'100%',
    width:'100%',
    alignSelf:'center',
  },
  TextContainer: {
    color:Colors.secondary,
    paddingHorizontal:10,
    textAlign: 'center',
    alignSelf: 'center',
  },
  MainText: {
    fontWeight:'300',
    opacity: 0.7,
    color:'#000',
  },
  TitleText: {
    fontFamily:'Roboto-Bold',
    fontSize:17,
    color: Colors.secondary,
    marginVertical:5,
    fontWeight:'200'
  },
  BottomIcon: {
    opacity: 0.7,
    justifyContent: 'flex-end',
    alignSelf: 'flex-end',
  },
  button:{
    position:'absolute',
    right:0,
    bottom:0
  }
});

export default BeneficiaryTiles;
