import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
const Link = props => {
  return (
    <View>
      <TouchableOpacity
        activeOpacity={0.8}
        style={styles.menuContainer}
        onPress={props.pressTo}>
        <Text style={styles.menuText}>{props.name}</Text>
        <Icon
          name="keyboard-arrow-right"
          size={20}
          color="#262626"
          style={styles.arrow}
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  menuContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderColor: '#262626',
    paddingVertical: 15,
  },
  menuText: {
    fontSize: 14,
    fontFamily: 'Roboto-Bold',
    color: '#262626',
    opacity: 0.7,
  },
  arrow: {
    paddingHorizontal: 20,
  },
});

export default Link;
