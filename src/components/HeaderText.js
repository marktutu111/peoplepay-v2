import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import Colors from '../constants/colors';
import {Icon} from 'react-native-elements';

const HeaderText = props => {
  return (
    <View style={styles.HeaderTextMain}>
      <Text style={styles.HeaderText}>{props.title1}</Text>
      <Text style={styles.HeaderText1}>{props.title2}</Text>
    </View>
  );
};
const styles = StyleSheet.create({
  HeaderTextMain: {
    textAlign: 'left',
    paddingHorizontal: 30,
    paddingTop: 10,
    paddingBottom: 10,
  },
  HeaderText: {
    // fontFamily: 'Roboto-Bold',
    fontSize: 28,
    color: Colors.tertiary,
    fontWeight:'200',
    opacity: 0.8,
  },
  HeaderText1: {
    // fontFamily: 'Roboto-Regular',
    fontSize: 28,
    color: Colors.secondary,
    fontWeight:'200',
    opacity: 0.7,
    paddingBottom: 10,
    // bottom: 15,
  },
});
export default HeaderText;
