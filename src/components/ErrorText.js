import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const ErrorText = props => {
  return (
    <View>
      <Text style={styles.text}>{props.message}</Text>
    </View>
  );
};

export default ErrorText;

const styles = StyleSheet.create({
  text: {
    color: 'red',
    fontSize: 12,
    fontFamily: 'Roboto-Regular',
    textAlign:"right",
    paddingRight:20,
  },
});
