import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Colors from '../constants/colors';

const HeaderTxt1 = props => {
  return (
    <View style={[styles.HeaderTextMain,{...props.style}]}>
      <Text style={styles.HeaderText}>{props.txt1}</Text>
      <Text style={styles.HeaderText1}>{props.txt2}</Text>
    </View>
  );
};
const styles = StyleSheet.create({
  HeaderTextMain: {
    textAlign: 'left',
    paddingHorizontal:35,
    paddingBottom: 10,
    flexDirection: 'row',
    marginTop:20
  },
  HeaderText: {
    fontFamily:'Roboto-Bold',
    fontSize: 35,
    color: Colors.tetiary,
    // textAlign: 'center',
    opacity: 0.8,
  },
  HeaderText1: {
    fontFamily: 'Roboto-Regular',
    fontSize: 35,
    color: Colors.secondary,
    // textAlign: 'center',
    opacity: 0.8,
    // bottom: 20,
  },
});
export default HeaderTxt1;
