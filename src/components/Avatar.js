import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import Colors from '../constants/colors';

const ProfileAvatar = props => {
  return (
    <>
      <TouchableOpacity activeOpacity={0.8}>
        <View style={styles.iconContainer}>
          <Icon name="user" size={60} color="#262626" style={styles.icon} />
        </View>
      </TouchableOpacity>
      <View>
        <Text style={styles.iconName}>{props.name}</Text>
        <Text style={styles.iconEmail}>{props.email}</Text>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  iconContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    bottom: 20,
  },
  icon: {
    borderRadius: 250,
    borderWidth: 1,
    borderColor: Colors.primary,
    paddingVertical: 30,
    paddingHorizontal: 30,
    backgroundColor: '#FAFAFA',
  },
  iconName: {
    fontSize: 14,
    marginTop: 10,
    textAlign: 'center',
    justifyContent: 'center',
  },
  iconEmail: {
    fontSize: 10,
    textAlign: 'center',
    justifyContent: 'center',
    opacity: 0.7,
  },
});

export default ProfileAvatar;
