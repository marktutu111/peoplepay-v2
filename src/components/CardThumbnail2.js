import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Colors from '../constants/colors';
import {Icon} from 'react-native-elements';
import colors from '../constants/colors';
import { SCREEN_HEIGHT } from '../constants/dimensions';

const CardThumbnail2 = props => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={styles.Container}
      onPress={props.pressTo}>
      <Icon
        name={props.iconname}
        type={props.icontype}
        size={35}
        color={Colors.secondary}
        containerStyle={styles.navIcon}
      />
      <View style={styles.textContainer}>
        <Text style={styles.text1}>{props.title}</Text>
        <Text style={styles.text2}>{props.subtitle}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  Container: {
    borderRadius:20,
    alignItems:'center',
    marginBottom:20,
    flexDirection:'row',
    borderWidth: 1,
    borderColor:'#ddd',
    paddingHorizontal:20,
    width:'100%',
    height:SCREEN_HEIGHT > 700 ? 80 : 60,
    backgroundColor:Colors.accent
  },

  text1: {
    fontFamily:'Roboto-Bold',
    fontSize: 16,
    paddingHorizontal:20,
    textAlign:'left',
    justifyContent:'center',
    opacity: 0.8,
    color:Colors.tetiary,
    fontWeight:'200'
  },
  text2: {
    fontFamily: 'Roboto-Medium',
    fontSize: 14,
    paddingHorizontal: 20,
    textAlign: 'left',
    justifyContent: 'center',
    opacity: 0.6,
    color: Colors.tetiary,
    fontWeight:'200'
  },
});

export default CardThumbnail2;
