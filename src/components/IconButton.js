import React from 'react';
import { StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import FontAwesome from 'react-native-vector-icons/FontAwesome5';
import colors from '../constants/colors';


const IconButton=(props)=>(
    <TouchableOpacity style={styles.button} onPress={props.onPress}>
        <FontAwesome 
            name={props.name}
            size={props.size || 10}
            color={'#fff' || props.color}
        />
    </TouchableOpacity>
);

const styles=StyleSheet.create({
    button:{
        width:30,
        height:30,
        justifyContent:'center',
        alignItems:'center',
        padding:5,
        backgroundColor:colors.secondary,
        borderRadius:50
    }
});

export default IconButton;
