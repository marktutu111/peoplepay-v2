import React from 'react';
import Colors from '../constants/colors';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import { Platform } from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome5";

const LoginPrimaryButton=props=>{
  return (
    <TouchableOpacity
      disabled={props.loading}
      onPress={props.pressTo}
      activeOpacity={0.8}
      style={[styles.ButtonContainer, props.style]}>
      {props.loading ? (
        <ActivityIndicator size="small" color="white" />
      ) : (
        <View style={{
          flexDirection:'row',
          alignItems:'center',
          justifyContent:'center'
        }}>
          <Icon name={props.icon} color="#fff"/>
          <Text 
            style={[styles.buttonText,props.textStyle]}
          >{props.title}</Text>
        </View>
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  ButtonContainer: {
    position: 'relative',
    justifyContent: 'center',
    ...Platform.select(
      {
        ios:{
          height:55,
          borderRadius:20
        },
        android:{
          height:45,
          borderRadius:15
        }
      }
    ),
    backgroundColor:Colors.secondary,
    // borderColor: Colors.secondary,
    width: '100%',
    alignSelf: 'center',
    // marginVertical: 10,
  },
  gradient: {
    flex: 1,
  },
  buttonText: {
    fontSize: 13,
    fontFamily: 'Roboto-Regular',
    textTransform: 'uppercase',
    textAlign: 'center',
    margin: 10,
    color: '#fff',
    backgroundColor: 'transparent',
    letterSpacing: 0.2,
  },
});
export default LoginPrimaryButton;
