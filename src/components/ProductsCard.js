import React from 'react';
import {
  TouchableOpacity,
  Text,
  StyleSheet,
  Dimensions,
  View,
  Image,
} from 'react-native';
import {Icon} from 'react-native-elements';
import Colors from '../constants/colors';

const ProductsCard = (props) => {
  //   let direction = props.direction === 'center' ? 'flex-end' : 'flex-start';
  let defaultSize = Dimensions.get('window').height > 700 ? 45 : 40;
  return (
    <TouchableOpacity
      onPress={props.pressTo}
      activeOpacity={0.4}
      style={styles.container}>
      <Icon
        name={props.iconname}
        type={props.icontype}
        containerStyle={styles.navIcon}
        color={props.bg}
        size={props.iconsize !== undefined ? props.iconsize : defaultSize}
      />
      <View style={{flex: 1, justifyContent: 'center', paddingLeft: 10}}>
        <Text maxColumns={1} style={styles.text1}>{props.title}</Text>
        <Text maxColumns={2} style={styles.text2}>{props.description}</Text>
      </View>
      <View
        style={{
          flex: 1,
          maxWidth: '30%',
         
        }}>
        <View
          style={{
            backgroundColor: `${props.bg}`,
            flex: 1,
            justifyContent: 'center',
            borderBottomLeftRadius: 30,
            borderTopLeftRadius: 60,
          }}>
          <Text maxColumns={1} style={styles.text3}>
            GHS {props.price}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    height: 90,
    overflow:'hidden',
    borderRadius:10,
    shadowColor: '#000',
    shadowOffset: {
      width: 1,
      height: 3,
    },
    shadowOpacity: 0.21,
    shadowRadius: 10,
    elevation: 5,
    justifyContent: 'space-between',
    backgroundColor:Colors.accent,
    marginVertical: 10,
    borderLeftWidth: 5,
    borderLeftColor: 'rgba(0,0,0,0.4)',
    borderColor:'#ccc',
    borderWidth:0.5
  },
  background: {
    width: '100%',
    height: 100,
    marginTop: 18,
    alignSelf: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  textContainer: {
    position: 'absolute',
    flexDirection: 'row',
    width: '100%',
    opacity: 1,
    top: 0,
    bottom: 0,
    left: 20,
    zIndex: 1,
    justifyContent: 'center',
    alignContent: 'space-between',
  },
  text2: {
    fontFamily: 'Roboto-Bold',
    color: Colors.tetiary,
    fontSize: 14,
    textAlign: 'left',
    // bottom: 3,
    // left: 60,
    // position: 'absolute',
  },

  text1: {
    
    color: '#000',
    fontFamily: 'Roboto-Medium',
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'left',
  },
  text3: {
    fontFamily: 'Roboto-Bold',
    color: Colors.accent,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  navIcon: {
    opacity: 0.8,
    // position: 'absolute',
    left: 0,
    top: 10,
  },
});

export default ProductsCard;
