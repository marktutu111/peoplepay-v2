import React from 'react';
import {Text, TouchableOpacity, StyleSheet} from 'react-native';
import Colors from '../constants/colors';

const SecondaryButton = props => {
  return (
    <TouchableOpacity
      style={styles.btn}
      onPress={props.pressTo}
      activeOpacity={0.8}>
      <Text style={styles.text}>{props.title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  btn: {
    // width: '80%',
    padding: 10,
    alignContent: 'center',
    justifyContent: 'center',

    borderRadius: 50,
    height: 60,
  },
  text: {
    fontFamily: 'Roboto-Bold',
    color: Colors.tetiary,
    textAlign: 'center',
    fontSize: 18,
  },
});
export default SecondaryButton;
