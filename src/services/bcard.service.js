
import API, { BVIRTUAL } from "../constants/api";


const getCities=()=>fetch(
    `${BVIRTUAL}/regions/country/Ghana`,
    {
        method: 'GET'
    }
).then(res=>res.json()).catch(err=>err);


const createAccount=(data)=>fetch(
    `${BVIRTUAL}/accounts`,
    {
        method: 'POST',
        body:JSON.stringify(
            {
                ...data,
                idExpiryDate:''
            }
        )
    }
).then(res=>res.json()).catch(err=>err);


const viewBalance=(data)=>fetch(
    `${BVIRTUAL}/accounts/${data['mobileNumber']}/cards/${data['customerId']}/balance`,
    {
        method: 'POST',
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);


const blockCard=(type,data)=>fetch(
    `${BVIRTUAL}/accounts/${data['mobileNumber']}/cards/11704680/${data['cardId']}/${type}`,
    {
        method: 'POST',
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);


const cardTransactions=(phone,data)=>fetch(
    `${BVIRTUAL}/accounts/${phone}/cards/12794133/transactions`,
    {
        method: 'POST',
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);


const verifyAccount=(data)=>fetch(
    `${BVIRTUAL}/accounts/verifyaccount`,
    {
        method: 'POST',
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);


const transferFunds=(data)=>fetch(
    `${BVIRTUAL}/accounts/${data['mobileNumber']}/cards/${data['fromCardId']}/transfers`,
    {
        method: 'POST',
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);


const loadCards=(phone)=>fetch(
    `${BVIRTUAL}/accounts/${phone}/cards/Ghana`,
    {
        method: 'GET'
    }
).then(res=>res.json()).catch(err=>err);


const getFees=()=>fetch(
    `${BVIRTUAL}/fees/Ghana`,
    {
        method: 'GET'
    }
).then(res=>res.json()).catch(err=>err);


const login=(phone)=>fetch(
    `${BVIRTUAL}/accounts/${phone}/authenticate`,
    {
        method: 'POST',
        body:JSON.stringify(
            {
                "mobilenumber":phone,
                "password":"0000",
                "country":"Ghana"
            }
        )
    }
).then(res=>res.json()).catch(err=>err);


export default {
    getCities,
    createAccount,
    viewBalance,
    blockCard,
    cardTransactions,
    login,
    transferFunds,
    loadCards,
    getFees,
    verifyAccount
};