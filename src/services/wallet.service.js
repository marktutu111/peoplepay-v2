import API from "../constants/api";


const addWallet=(data)=>fetch(
    `${API}/wallets/new`,
    {
        method: 'POST',
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);


const getWallets=(data)=>{
    return fetch(
        `${API}/wallets/get/customer`,
        {
            method: 'POST',
            body: JSON.stringify(data)
        }
    ).then(res=>res.json()).catch(err=>err);
}


const deleteWallet=(id)=>fetch(
    `${API}/wallets/delete/${id}`,
    {
        method: 'DELETE'
    }
).then(res=>res.json()).catch(err=>err);


export default {
    addWallet,
    getWallets,
    deleteWallet
}