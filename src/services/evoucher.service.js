import API from '../constants/api';

const getAllCategories = () =>
  fetch(
    `${API}/giftcards/categories/get`,
    {
      method: 'GET',
    },
  ).then((res)=>res.json()).catch((err) => err);

const getCatergoryProducts = (id) =>
  fetch(`${API}/giftcards/categories/get/${id}`, {
    method: 'GET',
  })
    .then((res) => res.json())
    .catch((err) => err);

export default {getAllCategories, getCatergoryProducts};
