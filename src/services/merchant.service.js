import API from '../constants/api';

const getMerchants=()=>fetch(`${API}/merchants/get`,
    {
        method: 'GET'
    }
).then((res)=>res.json()).catch((err)=>err);


const getByCategory=(id)=>fetch(`${API}/merchants/category/get/${id}`,
    {
        method: 'GET'
    }
).then((res)=>res.json()).catch((err)=>err);


const getMerchantCode=(id)=>fetch(`${API}/merchants/itc/get/${id}`,
    {
        method: 'GET'
    }
).then((res)=>res.json()).catch((err)=>err);


const lookup=(data)=>fetch(
    `${API}/merchants/itc/lookup`,
    {
        method:'POST',
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);


const checkMerchant=(id)=>fetch(`${API}/merchants/check/${id}`,
    {
        method: 'GET'
    }
).then((res)=>res.json()).catch((err)=>err);


const getMerchantTransactions=(id)=>fetch(`${API}/qrc/merchant/summay/${id}`,
    {
        method: 'GET'
    }
).then((res)=>res.json()).catch((err)=>err);



export default {
    getMerchants,
    getMerchantCode,
    lookup,
    getByCategory,
    checkMerchant,
    getMerchantTransactions
}