import API from "../constants/api";

const validateToken=(token)=>fetch(
    `${API}/jwt/validate/${token}`,
    {
        method: 'GET'
    }
).then(res=>res.json()).catch(err=>err);


export default validateToken;