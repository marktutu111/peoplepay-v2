import API from "../constants/api";

const addBeneficiary=(data)=>fetch(
    `${API}/beneficiaries/new`,
    {
        method:'POST',
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);

const getCustomerBeneficiaries=(id)=>fetch(
    `${API}/beneficiaries/get/customer/${id}`,
    {
        method: 'GET'
    }
).then(res=>res.json()).catch(err=>err);

const deleteBeneficiary=(id)=>fetch(
    `${API}/beneficiaries/delete/${id}`,
    {
        method:'DELETE'
    }
).then(res=>res.json()).catch(err=>err);



export default {
    addBeneficiary,
    getCustomerBeneficiaries,
    deleteBeneficiary
};