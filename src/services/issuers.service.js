import API from "../constants/api";
const getIssuers=()=>fetch(
    `${API}/issuers/get`,
    {
        method:'GET'
    }
).then(res=>res.json()).catch(err=>err);


export default {
    getIssuers
}