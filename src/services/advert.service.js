import API from "../constants/api";

const getAdverts=()=>fetch(
    `${API}/adverts/get`,
    {
        method:'GET'
    }
).then(res=>res.json()).catch(err=>err);


export default getAdverts;