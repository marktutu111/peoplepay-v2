import API from '../constants/api';

const getDataPackges=()=>fetch(`${API}/busy/bundles`,
    {
        method: 'GET'
    }
).then((res)=>res.json()).catch((err)=>err);

export default {
    getDataPackges
}