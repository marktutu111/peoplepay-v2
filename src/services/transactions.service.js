import baseUrl from "../constants/api";
import API from "../constants/api";

const getCustomerTransactions = (id) => fetch(
    `${API}/transactions/get/customer/${id}`,
    {
        method: 'GET'
    }
).then(res=>res.json()).catch(err=>err);

const getCustomerSummary=(id)=>fetch(
    `${API}/transactions/customer/summary/get/${id}`,
    {
        method: 'GET'
    }
).then(res=>res.json()).catch(err=>err);

const filterTransactions=(data)=>fetch(
    `${API}/transactions/get?customerId=${data.customerId}&transaction_type=${data.transaction_type}&status=paid`,
    {
        method:'GET',
        headers:{
            'Content-type':'Application/json'
        },
    }
).then(res=>res.json()).catch(err=>err);


const createTransaction=(data)=>fetch(
    `${API}/transactions/sendmoney`,
    {
        method: 'POST',
        body: JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);


const reversal=(data)=>fetch(
    `${API}/transactions/reversal`,
    {
        method: 'POST',
        body: JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);

const buycard = (data)=>fetch(
    `${API}/transactions/buycard`,
    {
        method: 'POST',
        body: JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);


const getNec=(data)=>fetch(
    `${API}/transactions/nec`,
    {
        method: 'POST',
        body: JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);


const getMerchantDetails=(id)=>fetch(
    `${API}/merchants/qre/${id}`,
    {
        method: 'GET'
    }
).then(res=>res.json()).catch(err=>err);


const getTransactionStatus=(id)=>fetch(
    `${API}/transactions/get/${id}`,
    {
        method: 'GET'
    }
).then(res=>res.json()).catch(err=>err);


const decodeQr=(qrstring)=>fetch(
    `${baseUrl}/merchants/qrcdata/decode`,
    {
        method: 'POST',
        body:JSON.stringify(
            {
                qrcode:qrstring
            }
        )
    }
).then(res=>res.json()).catch(err=>err);




export default {
    getCustomerTransactions,
    createTransaction,
    getNec,
    buycard,
    decodeQr,
    getCustomerSummary,
    getMerchantDetails,
    reversal,
    getTransactionStatus,
    filterTransactions
}