import 'intl';
import 'intl/locale-data/jsonp/en'; // or any other locale you need

const FormatAmount=(amount)=>{
    try {
        const formatter=new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'GHS',
        });
        return formatter.format(amount);
    } catch (err) {
        return 0;
    }
}

export default FormatAmount;