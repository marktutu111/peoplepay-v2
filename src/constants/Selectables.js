const BankNames = [
  {name: 'ABSA', value: 'Absa'},
  {name: 'Access', value: 'Access'},
  {name: 'ADB', value: 'ADB'},
  {name: 'CAL', value: 'cal'},
  {name: 'Ecobank', value: 'ecobank'},
  {name: 'Fidelity', value: 'fidelity'},
  {name: 'Fnb Bank', value: 'fnb'},
  {name: 'GCB Bank', value: 'gcb'},
  {name: 'GT Bank', value: 'Gt'},
  {name: 'HFC Bank', value: 'Hfc'},
  {name: 'GCB Bank', value: 'gcb'},
  {name: 'NIB Bank', value: 'nib'},
  {name: 'Standard Chartered', value: 'SCBank'},
  {name: 'Societee General Bank', value: 'SG bank'},
  {name: 'UBA', value: 'uba'},
  {name: 'UMB Bank', value: 'umb'},
];

const WalletTypes = [
  {name: 'Mobile Money', value: 'Momo'},
  {name: 'Bank Account', value: 'Bank Account'},
  {name: 'Proxy Wallet', value: 'ProxyWallet'},
];
const AccountTypes = [
  {name: 'Current', value: 'current'},
  {name: 'Savings', value: 'Savings'},
];
const NetworkProviders = [
  {name: 'MTN ', value: 'mtn'},
  {name: 'Vodafone', value: 'Voda'},
  {name: 'AirtelTigo', value: 'AirtelTigo'},
  {name: 'Glo', value: 'glo'},
  {name: 'Expresso', value: 'expresso'},
];
const NetworkProducts = [
  { name: 'Airtime', value: 'Airtime' },
  { name: 'Data', value: 'Data' },
];

const WalletProviders = [
  {name: 'MTN Mobile Money', value: 'mtn'},
  {name: 'Vodafone Cash', value: 'Voda'},
  {name: 'AirtelTigo Money', value: 'AirtelTigo'},
];

export default {
  BankNames,
  WalletTypes,
  AccountTypes,
  NetworkProviders,
  WalletProviders,
};
