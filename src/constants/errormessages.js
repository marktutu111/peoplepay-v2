

const messages={
    confirmPassword:'Please enter your password',
    phone:'Please enter a valid phone number',
    email:'Please enter a valid email',
    fullname:'Please enter your fullname',
    payment_account_type:'Please choose which account you want to make payment from',
    payment_account_issuer:'Please choose your network provider',
    payment_account_number:'Please enter your mobile money account number',
    payment_account_name:'Please choose which account you want to make payment from'
}

export default messages;