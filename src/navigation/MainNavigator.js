import React from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs';
import {Icon} from 'react-native-elements';
import CustomSidebarMenu from '../components/CustomSidebarMenu';
import {Dimensions, Easing, Animated} from 'react-native';
import Colors from '../constants/colors';

// Authentication Screens
import LoginScreen from '../screens/AuthenticationSreens/LoginScreen';
import SignUpScreen from '../screens/AuthenticationSreens/SignUpScreen';
import ForgotPasswordScreen from '../screens/AuthenticationSreens/ForgotPasswordScreen';
import VerifyCustomerScreen from '../screens/AuthenticationSreens/VerifyCustomerScreen';

// Welcome Screens
import SplashScreen from '../screens/WelcomeScreens/SplashScreen';

import HomeScreen from '../screens/HomeScreen';

// Help and Support Screens
import HelpAndSupportScreen from '../screens/HelpAndSupportScreens/HelpAndSupportScreen';
import FAQsScreen from '../screens/HelpAndSupportScreens/FAQsScreen';

// Airtime and Data Screens
import AirtimeScreen from '../screens/AirtimeAndDataScreens/AirtimeScreen';

// Account Setting Screens
import AccountSettings from '../screens/AccountSettingScreens/AccountSettingsScreen';
import ChangePassword from '../screens/AccountSettingScreens/ChangePasswordScreen';
import EditProfile from '../screens/AccountSettingScreens/EditProfileScreen';

// Transaction History Screns
import TransactionHistory from '../screens/TransactionHistoryScreens/TransactionHistoryScreen';
import HistoryDetailsScreen from '../screens/TransactionHistoryScreens/HistoryDetailsScreen';

// Send Money Screens
import SendMoneyScreen from '../screens/SendMoneyScreens/SendMoneyScreen';
import BankAccount from '../screens/SendMoneyScreens/BankAccountScreen';
import BankRecipientDetails from '../screens/SendMoneyScreens/BankAccountRecipientDetailsScreen';
import SenderDetailsMoMo from '../screens/SendMoneyScreens/SenderDetailsMoMoScreen';
import MobileVerificationScreen from '../screens/SendMoneyScreens/MobileVerificationScreen';
// Pay Bills Screens
import PayBillScreen from '../screens/PayBillsScreens/PayBillScreen';
import UtilityScreen from '../screens/PayBillsScreens/UtilityScreen';
// import PaymentFormScreen from '../screens/PayBillsScreens/PaymentFormScreen';
import CustomerDetailScreen from '../screens/PayBillsScreens/CustomerDetailsScreen';
import ResponseScreen from '../screens/PayBillsScreens/ResponseScreen';

// Gift Cards Screens
import CardCategoryScreen from '../screens/GiftCardsScreens/CardCategoryScreen';
import CardProductsScreen from '../screens/GiftCardsScreens/CardProductsScreen';
import CardSummaryScreen from '../screens/GiftCardsScreens/CardSummaryScreen';
 import VerifyCardPaymentScreen from '../screens/GiftCardsScreens/VerifyPaymentScreen';
import CardPaymentDetailScreen from '../screens/GiftCardsScreens/CardPaymentDetailScreen';
// import ResponseScreen from '../screens/GiftCardsScreens/ResponseScreen';

// Receive Money Screens

// import RecieveMoneyScreen from '../screens/RecieveMoneyScreen';
import QRPayScreen from '../screens/QRPay/QRPayScreen';
import QRVerifyScreen from '../screens/QRPay/QrVerifyScreen';
import ScanDetailsScreen from '../screens/QRPay/ScanDetailsScreen';

// Cards/Wallet Screens
import WalletScreen from '../screens/WalletScreens/WalletsScreen';
import CreateWalletScreen from '../screens/WalletScreens/CreateWalletScreen';
import CreateBankWalletScreen from '../screens/WalletScreens/CreateBankWalletScreen';

// Proxy Pay Screens
import ProxyPayScreen from '../screens/ProxyPayScreens/ProxyPayScreen';
import ProxyPaySubscribeScreen from '../screens/ProxyPayScreens/ProxyPaySubscribeScreen';
import ProxyTransferScreen from '../screens/ProxyPayScreens/ProxyTransferScreen';
import ProxyPaySuccessScreen from '../screens/ProxyPayScreens/ProxyPaySuccessScreen';
import ProxyUpdateScreen from '../screens/ProxyPayScreens/ProxyUpdateScreen';
import ProxyUnsubscribeScreen from '../screens/ProxyPayScreens/ProxyUnsubscribeScreen';
import ProxySendScreen from '../screens/ProxyPayScreens/ProxySendScreen';
import ProxyPayVerifyScreen from '../screens/ProxyPayScreens/ProxyPayVerifyScreen';
import ProxyResponseScreen from '../screens/ProxyPayScreens/ProxyResponseScreen';

// Beneficiary Screens
import BeneficiaryScreen from '../screens/BeneficiaryScreens/BeneficiaryScreen';
import BankBeneficiaryScreen from '../screens/BeneficiaryScreens/BankBeneficiaryScreen';
import AddBeneficiaryBankAccountScreen from '../screens/BeneficiaryScreens/AddBeneficiaryBankAccountScreen';
import FundPaymentScreen from '../screens/PayBillsScreens/FundScreen';
import VerifyPaymentScreen from '../screens/PayBillsScreens/VerifyPayment';
import PrivacyPolicyScreen from '../screens/PrivacyPolicyScreen';
import UpdateBeneficiary from '../screens/BeneficiaryScreens/UpdateBeneficiaryScreen';
import ChooseWalletScreen from '../screens/WalletScreens/ChooseWalletScreen';
import htmlViewScreen from '../screens/SendMoneyScreens/htmlViewScreen';
import AccountRef from "../screens/PayBillsScreens/AccountRef";
import ProductListScreen from '../screens/PayBillsScreens/ProductList';
import QrpOptionsScreen from '../screens/QRPay/QRPayOptions';
import PayWithTerminalIdScreen from '../screens/QRPay/PayWithTerminal_Id';
import QRPaymentScreen from '../screens/QRPay/QRPaymentSource';
import VerifyId from '../screens/virtualcard/VerifyIdFront';
import VerifyIdSelfie from '../screens/virtualcard/VerifyIdSelfie';
import VirtualCardScreen from '../screens/virtualcard/VirtualCardScreen';
import FundCard from '../screens/virtualcard/FundCard';
import CardTransactions from '../screens/virtualcard/cardTransactions';
import TransferMoneyToCard from '../screens/virtualcard/CardTransfer';
import VirtualSignup from '../screens/virtualcard/VirtualSignup';
import Getcard from "../screens/virtualcard/Getcard";
import TransferMoneyToDifferentCard from '../screens/virtualcard/CardTransferDifferent';
import MerchantLogin from "../screens/Merchants/MerchantsLogin";
import CreditCard from '../screens/SendMoneyScreens/CreditCardScreen';
import { CustomExample } from '../components/customPicker';
import ChooseSource from '../screens/SendMoneyScreens/ChooseSource';
import ConfirmTransaction from '../screens/ConfirmTransaction';
import { SavedAccountsPicker } from '../screens/SavedAccountsPicker';
import VerifyWalletAccount from '../screens/WalletScreens/VerifyWalletAccount';
import CreateCardWalletScreen from '../screens/WalletScreens/CreateCardWalletScreen';
import ReferalsComponent from '../screens/Referal/ReferalScreen';
import WalletTopupScreen from '../screens/WalletScreens/WalletTopupScreen';
import DataPackagesScreen from '../screens/BusyInternetScreens/PackagesScreen';
import EnterDataNumber from "../screens/BusyInternetScreens/DataNumberScreen";



const HelpAndSupportNavigator=createStackNavigator(
  {
    Support: {screen: HelpAndSupportScreen},
    FAQ: {screen: FAQsScreen},
    Privacy: {screen: PrivacyPolicyScreen},
  },
  {
    initialRouteName: 'Support',
    defaultNavigationOptions: {
      headerShown:false,
      headerTransparent:true,
      headerTitle:()=>null,
    },
  },
);


const ProxyNavigator=createStackNavigator(
  {
    ProxyPay: {screen: ProxyPayScreen},
    Subscribe: {screen: ProxyPaySubscribeScreen},
    ProxyTransfer: {screen: ProxyTransferScreen},
    ProxySuccess: {screen: ProxyPaySuccessScreen},
    ProxyUpdate: {screen: ProxyUpdateScreen},
    ProxyDelete: {screen: ProxyUnsubscribeScreen},
    ProxySend: {screen: ProxySendScreen},
    ProxyVerify: {screen: ProxyPayVerifyScreen},
    ProxyResponse: {screen: ProxyResponseScreen},
  },
  {
    initialRouteName: 'ProxyPay',
    defaultNavigationOptions: {
      headerShown: false,
      headerTransparent: true,
      headerTitle: () => null,
    },
  },
);

const PayBillsNavigator=createStackNavigator(
  {
    PayBills: {screen: PayBillScreen},
    Option: {screen: UtilityScreen},
    Response: {screen: ResponseScreen},
    FundPayment: {screen: FundPaymentScreen},
    CustomerDetail: {screen: CustomerDetailScreen},
    ProductList:{screen:ProductListScreen},
    VerifyPayment: {screen: VerifyPaymentScreen},
    AccountRef:{screen:AccountRef}
  },
  {
    initialRouteName:'PayBills',
    defaultNavigationOptions: {
      headerShown:false,
      headerTransparent:true,
      headerTitle:()=>null,
    },
  },
);


const GiftCardsNavigator=createStackNavigator(
  {
    GiftCards: {screen: CardCategoryScreen},
    Product: {screen: CardProductsScreen},
    CardSummary: {screen: CardSummaryScreen},
    CardPayment: {screen: CardPaymentDetailScreen},
    // CustomerDetail: {screen: CustomerDetailsScreen},
    VerifyPaymentCards: {screen: VerifyCardPaymentScreen},
  },
  {
    initialRouteName: 'GiftCards',
    defaultNavigationOptions: {
      headerShown: false,
      headerTransparent: true,
      headerTitle: () => null,
    },
  },
);




const QRNavigator = createStackNavigator(
  {
    QRP_Option:{screen:QrpOptionsScreen},
    QRPay: {screen: QRPayScreen},
    PaymentSource:{screen:QRPaymentScreen},
    TerminalPay:{screen:PayWithTerminalIdScreen},
    ScanDetails: {screen: ScanDetailsScreen},
    QrVerify: {screen: QRVerifyScreen},
  },
  {
    initialRouteName: 'QRP_Option',
    defaultNavigationOptions: {
      headerShown: false,
      headerTransparent: true,
      headerTitle: () => null,
    },
  },
);



const HomeStack=createStackNavigator(
  {
    Home: {screen: HomeScreen},
    QRPay: {screen: QRNavigator},
    Referals:{screen:ReferalsComponent},
    MerchantLogin:{ screen:MerchantLogin },
    PayBills: {screen: PayBillsNavigator},
    GiftCards: {screen: GiftCardsNavigator},
    Beneficiary: {screen: BeneficiaryScreen,},
    AddBeneficiaryBankAccount: {screen: AddBeneficiaryBankAccountScreen},
    BankBeneficiary: {screen: BankBeneficiaryScreen},
    UpdateBeneficiary: {screen: UpdateBeneficiary},
    Support: {screen: HelpAndSupportNavigator},
    History: {screen: TransactionHistory},
    HistoryDetails: {screen: HistoryDetailsScreen},
    Wallets: {screen: WalletScreen},
    VirtualSignup: {screen: VirtualSignup},
    GetCard: { screen:Getcard },
    VirtualCard:{screen:VirtualCardScreen},
    FundCard:{screen:FundCard},
    WalletTopup:{screen:WalletTopupScreen},
    CardTransactions:{screen:CardTransactions},
    CardTranferCustomer:{screen:TransferMoneyToDifferentCard},
    CardTranfer:{screen:TransferMoneyToCard},
    VerifyId:{screen:VerifyId},
    VerifyIdSelfie:{screen:VerifyIdSelfie},
    CreateWallet: {screen:CreateWalletScreen},
    ChooseWallet: {screen: ChooseWalletScreen},
    CreateBankWallet: {screen: CreateBankWalletScreen},
    VerifyWalletAccount:{ screen:VerifyWalletAccount },
    CustomPicker:{ screen:CustomExample },
    ChooseSource:{ screen:ChooseSource },
    SendMoney: {screen:SendMoneyScreen},
    BankAccount: {screen: BankAccount,},
    BankRecipientDetails: {screen: BankRecipientDetails},
    SenderDetailsMoMo: {screen: SenderDetailsMoMo},
    BankBeneficiary: {screen: BankBeneficiaryScreen},
    Verify: {screen: MobileVerificationScreen},
    WebView:{screen:htmlViewScreen},
    DebitCard:{ screen:CreditCard },
    ConfirmTransaction:{ screen:ConfirmTransaction },
    SavedAccounts:{ screen:SavedAccountsPicker },
    CreateCardWalletScreen:{ screen:CreateCardWalletScreen },
    BusyInternet : {screen:DataPackagesScreen},
    EnterBundleDataNumber : {screen:EnterDataNumber}
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      headerShown: false,
      headerTransparent: true,
      headerTitle: () => null,
    },
  }
);


const Drawer=createDrawerNavigator(
  {
    Home: {screen:HomeStack},
    // Account: {
    //   screen: AccountSettings,
    // },
    EditProfile: {screen: EditProfile},
    Password: {screen: ChangePassword},
    Help: {screen: HelpAndSupportScreen},
    // CreateMobileWallet: {screen: CreateMobileWalletScreen},
  },
  {
    initialRouteName: 'Home',
    drawerType: 'back',
    hideStatusBar: true,
    contentComponent: CustomSidebarMenu,
    disableGestures: false,
    drawerWidth: Dimensions.get('window').width * 0.65,
  }
);



// Authentication navigator
const AuthStack=createStackNavigator(
  {
    Login: {screen: LoginScreen},
    SignUp: {screen: SignUpScreen},
    VerifyCustomer: {screen: VerifyCustomerScreen},
    ForgotPassword: {screen: ForgotPasswordScreen},
  },
  {
    defaultNavigationOptions: {
      headerShown: false,
      headerTransparent: true,
      headerTitle: () => null,
    },
  },
);



// Root Navigator
const RootNavigator = createSwitchNavigator(
  {
    Splash: SplashScreen,
    Login: AuthStack,
  },
  {
    initialRouteName: 'Splash',
    navigationOptions: {
      headerShown: false,
      hideStatusBar: true,
      headerTransparent: true,
      headerTitle: () => null,
    },
  },
);

const AuthNavigator = createAppContainer(RootNavigator);
const MainNavigator = createAppContainer(Drawer);

export default {
  AuthNavigator,
  MainNavigator,
};
