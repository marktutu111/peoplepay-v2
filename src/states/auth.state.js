import React, { Component, createContext } from 'react';
import { Alert,AppState } from "react-native";
import fetchIntercept from "fetch-intercept";
import AsyncStorage from '@react-native-community/async-storage';
import _JService from "../services/jwt.service";


export const AuthContext=createContext();
export const AuthConsumer=AuthContext.Consumer;


export default class AuthState extends Component {

    unregister=null;
    subscriptionValidateToken$=null;


    state={
        loggedIn:false,
        loading:false,
        user:{},
        token:null,
        password:null,
        card:{}
    };



    validateToken(){
        this.subscriptionValidateToken$=AppState.addEventListener("change",nextAppState=>{
              if (nextAppState==="active"){
                    this.state.token && _JService(this.state.token).then(
                        res=>{
                            if(!res.success){
                                this.logout();
                            }
                        }
                    ).catch(err=>null)
              }
            }
          );
    }
    
    componentDidMount=()=>{
        this.validateToken();
        this.getUser();
    }


    fingerPrintLogin=async()=>{
      try {
          const [user,password]=await Promise.all(
              [
                AsyncStorage.getItem('peoplepay-user'),
                AsyncStorage.getItem('peoplepay-user-password')
              ]
          )
          if (typeof user==='string' && typeof password === 'string'){
              const _user=JSON.parse(user);
              return {
                email:_user.email,
                password:password
            }
          } throw Error('Please login with your username and password for the first time'); 
      } catch (err) {return err.message;}
  }


  getUser=async()=>{
      try {
        const password=await AsyncStorage.getItem(
            'peoplepay-user-password'
        );
        if (typeof password==='string'){
            return this.state.password=password;
        } throw Error('please login');
      } catch (err) {}
  }


  logout=async()=>{
    this.setState(
        {
            user:null,
            token:null,
            password:null
        },()=>{
            try {
                Alert.alert('Session Expired','You are successfully logged out to protect your account,you may login again to continue.');
                if (this.unregister) this.unregister();
                this.subscriptionValidateToken$.remove();
            } catch (err) {}
        }
    )
  }


  setHeaders=(token)=>{
      this.unregister=fetchIntercept.register({
        request:(url,config)=>{
            config.headers={
                'Content-type':'Application/json',
                'Authorization':'Bearer ' + token
            }
            return [url,config];
        },
        requestError:(err)=>{
            return Promise.reject(
                err
            );
        },
        response:(response)=>{
            switch (response.status) {
                case 400:
                    throw Error(
                        'Dear customer please check and fill in all the required details for the input fields. We cannot proceed with your request.'
                    )
                default:
                    if(response.status!==200){
                        throw Error(
                            'Dear customer we are not able to process your request at the moment, please check and try again.'
                        )
                    }
                    break;
            }

            return response;
        },
        responseError:(err)=>{
            if(err instanceof TypeError){
                return Promise.reject(
                    new Error(
                        'Internet connection is not available'
                    )
                )
            }
            return Promise.reject(
                err
            );
        }
    });
  }


  updateState=(data)=>{
      Object.keys(data).forEach(key=>{
          if (key) {
              this.setState(
                  {
                      [key]: data[key]
                  }
              )
          }
      })
  }

  render() {
    return (
        <AuthContext.Provider value={{
            state: this.state,
            updateState: this.updateState.bind(this),
            logout: this.logout.bind(this),
            setHeaders:this.setHeaders.bind(this),
            fingerPrintLogin:this.fingerPrintLogin.bind(this)
        }}>
            { this.props.children }
        </AuthContext.Provider>
    );
  }
}
