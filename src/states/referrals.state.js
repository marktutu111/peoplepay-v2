import React, { Component,createContext } from 'react';
import { Alert, Text, View } from 'react-native';
import service from "../services/referral.service";
import localStorage from "@react-native-community/async-storage";



export const ReferralContext=createContext();
export const ReferralConsumer=ReferralContext.Consumer;

class ReferralProvider extends Component {

    user=this.props.user;
    state={
        loading:false,
        referal:{},
        summary:{}
    };


    componentDidMount(){
        this.loadReferal();
    }


    async fetchSummary(){
        try {
            this.setState({loading:true});
            const response=await service.summary(this.state.referal?.code);
            if(!response.success){
                throw Error(
                    response.message
                )
            }
            this.setState(
                {
                    loading:false,
                    summary:response.data
                }
            )
        } catch (err) {
            this.setState(
                {
                    loading:false
                }
            )
        }
    }

    removeCode(){
        localStorage.removeItem(
            'ppay_referal'
        ).then(this.loadReferal.bind(this))
            .catch(err=>null)
    }


    async fetchReferal(){
        try {
            this.setState({loading:true});
            const response=await service.fetchCode(this.user._id);
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            const referal=Array.isArray(response.data)?response.data[0]:response.data;
            this.setState(
                {
                    loading:false,
                    referal:referal
                },this.saveReferal.bind(this)
            )
        } catch (err) {
            this.setState(
                {
                    loading:false
                }
            )
        }
    }

    async deleteCode(){
        try {
            this.setState({loading:true});
            const response=await service.deleteCode(this.state.referal._id);
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            Alert.alert('Successful',response.message);
            this.setState(
                {
                    loading:false
                },this.removeCode.bind(this)
            )
        } catch (err) {
            Alert.alert('Oops',err.message);
            this.setState(
                {
                    loading:false
                }
            )
        }
    }

    async loadReferal(){
        try {
            const data=await localStorage.getItem('ppay_referal');
            if(typeof data !== 'string'){
                throw Error(
                    'NOT FOUND'
                )
            };
            const referal=JSON.parse(data);
            if(!referal || !referal._id){
                throw Error(
                    'INVALID'
                )
            }
            this.setState(
                {
                    referal:referal
                },this.fetchSummary.bind(this)
            )
        } catch (err) {this.fetchReferal()}
    }


    saveReferal(){
        const {referal}=this.state;
        localStorage.setItem(
            'ppay_referal',
            JSON.stringify(referal)
        ).catch(err=>null)
    }


    generateCode(code){
        return new Promise(async(resolve,reject)=>{
            try {
                this.setState({loading:true});
                const response=await service.generateCode(
                    {
                        code:code,
                        customerId:this.user._id,
                        promotType:'referal',
                        customerType:'customers'
                    }
                );
                if(!response.success){
                    throw Error(
                        response.message
                    )
                }
                Alert.alert('Successful',response.message);
                this.setState(
                    {
                        loading:false,
                        referal:response.data
                    },()=>{
                        this.saveReferal.bind(this);
                        resolve();
                    }
                )
            } catch (err) {
                Alert.alert('Oops',err.message);
                this.setState(
                    {
                        loading:false
                    },reject
                )
            }
        })
    }

    render() {
        return (
            <ReferralContext.Provider value={{
                state:this.state,
                generateCode:this.generateCode.bind(this),
                fetchReferal:this.fetchReferal.bind(this),
                loadReferal:this.loadReferal.bind(this),
                deleteCode:this.deleteCode.bind(this)
            }}>
                {this.props.children}
            </ReferralContext.Provider>
        )
    }
}


export default ReferralProvider;
