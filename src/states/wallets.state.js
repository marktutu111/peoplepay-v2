import React, { Component, createContext } from 'react';
import service from "../services/wallet.service";
import localStorage from "@react-native-community/async-storage";


export const WalletContext=createContext();
export const WalletConsumer=WalletContext.Consumer;


export default class walletState extends Component {
  
  
  user=this.props.user;
  _cacheId=`${this.user._id}wallets`;

  state={
    loading:false,
    wallets:[]
  };


  loadFromCache=()=>localStorage.getItem(this._cacheId).then(res=>{
        if(typeof res==='string'){
            const _data=JSON.parse(res);
            this.setState(
              {
                wallets:_data
              }
            )
        }

        this.fetchWallets();

  }).catch(err=>null);


  fetchWallets=async()=>{
    try {
      const { _id }=this.user;
      if (!_id) return;
      this.setState({loading:true});
      const password=await localStorage.getItem('peoplepay-user-password');
      const response=await service.getWallets({ customerId:_id,password:password});
      if(!response.success){
        throw Error(
          response.message
        )
      };
      this.setState(
        {
          loading:false,
          wallets:response.data
        },()=>{localStorage.setItem(this._cacheId,JSON.stringify(response.data))}
      )
    } catch (err) {
        this.setState({loading:false});
    }
  }


  

  getWallets=()=>this.loadFromCache().catch(err=>err);




  updateState=(data)=>{
    Object.keys(data).forEach(key => {
        if (key) {
            this.setState(
                {
                  [key]:data[key]
                }
            )
        }
    })
}

  render() {
    return (
      <WalletContext.Provider value={{
        state:this.state,
        updateState:this.updateState.bind(this),
        getWallets:this.getWallets.bind(this)
      }}>
          { this.props.children }
      </WalletContext.Provider>
    );
  }
}
