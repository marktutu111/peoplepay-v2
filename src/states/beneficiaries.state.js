import React, { Component, createContext } from 'react';
import service from "../services/beneficiary.service";
import localStorage from "@react-native-community/async-storage";


export const BeneficiaryContext = createContext();
export const BeneficiariesConsumer = BeneficiaryContext.Consumer;


export default class beneficiariesState extends Component {


    user=this.props.user || {};
    _cacheId=`${this.user._id}beneficiaries`;

    state = {
        loading:false,
        beneficiaries:[]
    };



  loadFromCache=()=>localStorage.getItem(this._cacheId).then(res=>{
        if(typeof res==='string'){
            const _data=JSON.parse(res);
            this.setState(
              {
                  beneficiaries:_data
              }
            )
        };

        this.fetchBeneficiaries();

    });


  fetchBeneficiaries=async()=>{
    try {
        this.setState({ loading:true });
        const response = await service.getCustomerBeneficiaries(this.user['_id']);
        if(!response.success){
            throw Error(
                response.message
            )
        }
        this.setState(
            {
                loading:false,
                beneficiaries:response.data
            },()=>{
                localStorage.setItem(this._cacheId,JSON.stringify(response.data)).catch(err=>null)
            }
        )
    } catch (err) {
        this.setState({loading:false});
    }
  }


  getBeneficiaries=()=>this.loadFromCache()



  updateState = (data) => {
    Object.keys(data).forEach(key => {
        if (key) {
            this.setState(
                {
                    [key]: data[key]
                }
            )
        }
    })
}

  render() {
    return (
      <BeneficiaryContext.Provider value={{
          state: this.state,
          updateState: this.updateState.bind(this),
          getBeneficiaries: this.getBeneficiaries.bind(this)
      }}>
          {this.props.children}
      </BeneficiaryContext.Provider>
    );
  }
}
