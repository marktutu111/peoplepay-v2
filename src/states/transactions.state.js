import React, { Component, createContext } from 'react';
export const TransactionContext=createContext();
export const TransactionConsumer=TransactionContext.Consumer;

export default class TransactionState extends Component {

  state={
    transaction:{},
    transactions:[]
  }

  clearTransaction(callback){
    this.setState(
      {
        returnPage:'',
        transaction:{}
      },callback
    )
  }


  pushTransaction(data,callback){
    let _t={...this.state.transaction,...data}
    this.setState(
      {
        transaction:_t
      },callback
    )
  }

  
  updateState=(data,callback=null)=>{
    Object.keys(data).forEach(key=>{
        if (key) {
            this.setState(
              {
                [key]: data[key]
              }
            )
        }
    });
    if(callback)callback();
    
}


  render() {
    return (
      <TransactionContext.Provider value={{
        state: this.state,
        updateState: this.updateState.bind(this),
        pushTransaction:this.pushTransaction.bind(this),
        clearTransaction:this.clearTransaction.bind(this)
      }}>
        { this.props.children }
      </TransactionContext.Provider>
    );
  }
}
