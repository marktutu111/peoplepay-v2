import React, { Component,createContext } from 'react'
import { Text, View } from 'react-native';
import CardService from "../services/bcard.service";



export const VirtualContext=createContext();
export const VirtualConsumer=VirtualContext.Consumer;

export default class VirtualcardState extends Component {

    state={
        loading_balance:false,
        selected:{},
        balance:''
    }

    viewBalance=async()=>{
        try {
            this.setState(
                {
                    loading_balance:true
                }
            );
            const {customerId,cardId}=this.state.selected;
            const response=await CardService.viewBalance(
                {
                    "mobileNumber":this.getUser('phone'),
                    "customerId":customerId,
                    "cardId":cardId,
                    "country":"Ghana"
                }
            );
            if(!response.success){
                throw Error(
                    response[
                        'message'
                    ]
                )
            };
            const {recordId}=response;
            this.setState(
                {
                    balance:recordId,
                    loading_balance:false
                }
            )
        } catch (err) {
            this.setState(
                {
                    loading_balance:false
                }
            ) 
        }
    }

    render() {
        return (
            <VirtualContext.Provider value={this.state}>
                {this.props.children}
            </VirtualContext.Provider>
        )
    }
}
