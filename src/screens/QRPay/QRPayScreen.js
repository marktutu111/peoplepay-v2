import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
    Alert,
    ActivityIndicator
} from 'react-native';
import Colors from '../../constants/colors';
import Header from '../../components/Header';
import {Icon} from 'react-native-elements';
import HeaderTxt1 from '../../components/HeaderTxt1';
import {QRScannerView} from 'react-native-qrcode-scanner-view';
import service from "../../services/transactions.service";


// change cont declaration name and export default name
class QRPayScreen extends Component {

    isLoaded=false;

    state={
        open_scanner:false,
        loading:false
    };

    componentDidMount(){
        this.isLoaded=true;
    }

    renderTitleBar = () => (
        <View style={{flexDirection:'row',marginTop:30}}>
            <Icon
                name="arrow-left"
                type="feather"
                color="white"
                size={30}
                onPress={()=>this.setState({open_scanner:false})}
                containerStyle={{padding:16}}
            />
            <Text
                style={{
                    color: 'white',
                    textAlign: 'center',
                    padding: 16,
                    fontFamily: 'Roboto-Medium',
                    fontSize: 25,
                }}>
                Scan QR code
            </Text>
        </View>
    );


    getQrDetails=async(qr)=>{
        try {
            this.setState({loading:true});
            const response=await service.decodeQr(qr);
            if (!response.success){
                throw Error(
                    'QRCode is not valid'
                );
            }
            return this.isLoaded && this.setState({loading:false},()=>{
                this.props.navigation.navigate(
                    'ScanDetails',
                    {merchant:response.data,qr:qr}
                );
            });
        } catch (err) {
            Alert.alert('Oops!',err.message);
            if(this.isLoaded){
                this.setState(
                    {
                        loading:false
                    }
                )
            }
        }
    }



    barcodeReceived=(event)=>{
        if (event.data) {
            this.state.open_scanner = false;
            return this.getQrDetails(event.data);
        }
    };




    render() {
        return this.state.open_scanner ? (
            <View style={{flex: 1}}>
                <QRScannerView
                    hintText=""
                    renderHeaderView={this.renderTitleBar}
                    onScanResult={this.barcodeReceived.bind(this)}
                    renderFooterView={this.renderMenu}
                    scanBarAnimateReverse={true}
                />
            </View>
        ) : (
            <View style={styles.screen}>
                {/* Header of the Screen */}
                <Header
                    pressTo={()=>this.props.navigation.goBack()}
                />
                <ScrollView style={styles.scroll}>
                    {/* Screen Title */}
                    <HeaderTxt1 txt1="GhQR" txt2=" Pay" />
                    {/* Screen Main Content */}
                    <View style={styles.Body}>
                        <View style={styles.TextContainer}>
                            <Text style={styles.InfoText}>
                                Please make sure QR code is in the center of the
                                screen.
                            </Text>
                            <Text style={styles.ScanText}>Tap to Scan</Text>
                        </View>
                        <View style={styles.container}>
                            <View style={styles.iconsContainer}>
                                <TouchableOpacity>
                                    {
                                        this.state.loading ? (
                                            <ActivityIndicator 
                                                size={'large'}
                                                color="red"
                                            />
                                        ) : (
                                            <Icon
                                                name="qrcode-scan"
                                                type="material-community"
                                                size={115}
                                                color={Colors.secondary}
                                                disabled={this.state.loading}
                                                onPress={()=>{this.setState({open_scanner:true})}}
                                                containerStyle={styles.navIcon}
                                            />
                                        )
                                    }
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    HeaderTextMain: {
        color: Colors.secondary,
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingVertical: 15,
        paddingBottom: 20,
        flexDirection: 'row',
    },
    HeaderText: {
        fontFamily: 'Roboto-ExtraBold',
        fontSize: 35,
        color: Colors.secondary,
        textAlign: 'left',
        opacity: 0.8,
    },
    HeaderText1: {
        fontFamily: 'Roboto-ExtraBold',
        fontSize: 35,
        color: Colors.primary,
        textAlign: 'left',
        opacity: 0.8,
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
        paddingTop: 30,
    },
    InfoText: {
        fontFamily: 'Roboto-Medium',
        width: '80%',
        paddingVertical: 5,
        textAlign: 'left',
        opacity: 0.5,
        paddingHorizontal: 30,
        bottom: 30,
    },
    ScanText: {
        fontFamily: 'Roboto-Bold',
        width: '100%',
        paddingVertical: 15,
        textAlign: 'center',
        opacity: 0.5,
        fontSize: 15,
        marginBottom: 50,
        // paddingHorizontal: 30,
    },
    container: {
        // borderRadius: 5,
        // height: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: Colors.accent,
        alignItems: 'center',
        // marginBottom: 30,
        bottom: 10,
    },
    iconsContainer: {
        borderRadius: 10,
        // borderWidth: 1,
        // justifyContent: 'space-between',
        borderColor: Colors.primary,
        paddingVertical: 40,
        paddingHorizontal: 70,
        backgroundColor: '#FAFAFA',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.21,
        shadowRadius: 10,
        elevation: 3,
    },
    navIcon2: {
        paddingVertical: 5,
        opacity: 0.4,
    },
    iconContainer2: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 20,
    },
    scanning: {
        fontFamily: 'Roboto-Medium',
        opacity: 0.7,
        color: Colors.tertiary,
    },
});

export default QRPayScreen;
