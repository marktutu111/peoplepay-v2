import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Header from '../../components/Header';
import {ScrollView} from 'react-native-gesture-handler';
import TxtInput from '../../components/TxtInput';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import {AuthConsumer} from '../../states/auth.state';
import {CustomExample} from '../../components/customPicker';
import LazyContactPicker from '../../components/contacts-picker.component';
import {CustomPicker} from 'react-native-custom-picker';
import service from '../../services/wallet.service';




// change cont declaration name and export default name
class QRPaymentScreen extends Component {

    state={
        customerId:'',
        account_number:'',
        account_numberValid:true,
        account_issuer:'',
        account_issuerValid: true,
        account_type:'',
        recipient_account_typeValid: true,
        openModal:false,
        option: '',
        wallets:[]
    };

    validate=(text,type)=>{
        const {account_type} = this.state;
        if (account_type === 'momo' && type === 'accountnumber') {
            if (!/^[0]?\d{9}$/.test(text)) {
                this.setState({
                    recipient_account_numberValid: false,
                });
            } else {
                this.setState({
                    recipient_account_numberValid: true,
                });
            }
        }
        if (account_type === 'bank' && type === 'accountnumber') {
            if (!/^\d{10,17}$/.test(text)) {
                this.setState(
                    {
                        recipient_account_numberValid: false,
                    }
                );
            } else {
                this.setState(
                    {
                        recipient_account_numberValid: true,
                    }
                );
            }
        }
        if (type === 'accountname') {
            if (text === '' || !/^[a-zA-Z]+/.test(text)) {
                this.setState({
                    recipient_account_nameValid: false,
                });
            } else {
                this.setState({
                    recipient_account_nameValid: true,
                });
            }
        }
        if (type === 'issuer') {
            if (text === '') {
                this.setState({
                    recipient_account_issuerValid: false,
                });
            } else {
                this.setState({
                    recipent_account_issuerValid: true,
                });
            }
        }
    };


    componentDidMount=()=>{};


    getWallets=async()=>{
        try {
            const {_id}=this.user;
            if (!_id) {
                return;
            }
            this.setState({loading: true});
            const response=await service.getWallets(
                {
                    customerId:_id,
                    password:this.password,
                }
            );
            if (!response.success) {
                throw Error(
                    response.message
                );
            }
            this.setState({
                loading:false,
                wallets:response.data,
            });
        } catch (err) {this.getWallets().catch(err=>null)}
    };

    render=()=>{

        const setTitle = () => {
            let string = this.state.recipient_account_type;
            if (string === 'bank') {
                return 'Bank';
            }
            if (string === 'momo') {
                return 'Mobile Money';
            }
        };

        return (
            <View style={styles.screen}>
                {/* Header of the Screen */}
                <AuthConsumer>
                    {({state}) => {
                        const {user,password}=state;
                        this.user=user;
                        this.password=password;
                    }}
                </AuthConsumer>
                <Header pressTo={() => this.props.navigation.goBack()} />
                <ScrollView
                    style={styles.scroll}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag">
                    <View style={styles.Body}>

                        <View style={{
                                backgroundColor: 'rgba(255,0,0,0.3)',
                                borderRadius: 20,
                                marginHorizontal: 20,
                            }}>
                            <Text style={styles.heading}>Choose account you want to receive payments</Text>
                        </View>
                        <View style={styles.dropdownContainer}>
                            <CustomExample
                                    placeholder="Choose Account"
                                    selectedValue={this.state.option}
                                    onValueChange={({value})=>this.setState(
                                            {
                                                option:value,
                                            },
                                            ()=>{
                                                if(this.state.option==='1'){
                                                    this.setState(
                                                        {
                                                            selected_account: '',
                                                            payment_account_name: '',
                                                        },()=>this.getWallets()
                                                    );
                                                }
                                            }
                                        )
                                    }
                                    items={[
                                        {
                                            name: 'Select from my Accounts',
                                            value: '1',
                                        },
                                        {
                                            name: 'Enter Payment Details',
                                            value: '0',
                                        }
                                    ]}
                                />
                        </View>
                        <>
                            <View style={styles.dropdownContainer}>
                                <CustomExample
                                    placeholder={this.state.recipient_account_type ==='momo'? 'Select Network': 'Select bank account issuer'}
                                    selectedValue={this.state.recipient_account_issuer}
                                    type={this.state.recipient_account_type}
                                    onValueChange={({value})=>{
                                        this.setState(
                                            {
                                                recipient_account_issuer: value,
                                            },()=>{
                                                this.validate(value,'issuer');
                                            }
                                        );
                                    }}
                                />
                            </View>
                            <View style={styles.TxtInputContainer}>
                                    <TxtInput
                                        style={
                                            !this.state.account_numberValid
                                                ? {
                                                    borderColor:'red',
                                                    borderWidth:3,
                                                }
                                                : null
                                        }
                                        placeholder={'account number'}
                                        keyboardType="number-pad"
                                        IconName="hash"
                                        IconType="feather"
                                        contact={true}
                                        defaultValue={this.state.account_number}
                                        onChangeText={v=>{
                                            this.setState({account_number:v.trim()});
                                            this.validate(v,'accountnumber');
                                        }}
                                        onContact={()=>this.setState(
                                            {
                                                openModal:true
                                            }
                                        )}
                                    />
                            </View>
                        </>

                        {this.state.option === '1' && (
                            <View style={styles.dropdownContainer}>
                                <CustomExample
                                    placeholder="Select Account"
                                    selectedValue={this.state.selected_account}
                                    onValueChange={({value}) => {
                                        try {
                                            const wallet=this.state.wallets.find(wallet=>wallet._id === value);
                                            this.setState(
                                                {
                                                    account_type:wallet.account_type,
                                                    account_name:wallet.account_name,
                                                    account_issuer:wallet.account_issuer,
                                                    account_number:wallet.account_number,
                                                    selected_account:value
                                                }
                                            )
                                        } catch (err) {}
                                    }}
                                    items={this.state.wallets.map(
                                        ({
                                            account_name,
                                            _id,
                                            account_number,
                                        }) => {
                                            return {
                                                name:account_name,
                                                value:_id,
                                                account_number:account_number,
                                            };
                                        },
                                    )}
                                />
                            </View>
                        )}

                        <View style={styles.buttonContainer}>
                            <LoginPrimaryButton
                                title="Send"
                                pressTo={()=>{
                                    let invalid=null;
                                    let data={...this.state};
                                    delete data['openModal'];
                                    Object.keys(data).forEach(key => {
                                        if (!data[key] || data[key] === '') {
                                            invalid = key;
                                        }
                                    });
                                    if (typeof invalid === 'string') {
                                        return;
                                    }
                                    this.props.navigation.navigate(
                                        'BankRecipientDetails',
                                        {data: this.state},
                                    );
                                }}
                            />
                        </View>
                    </View>
                </ScrollView>
                <LazyContactPicker 
                    openModal={this.state.openModal}
                    getContact={(v)=>{
                        try {
                            this.setState(
                                {
                                    openModal:false,
                                    recipient_account_number:v.trim()
                                },()=>{
                                    this.validate(v, 'accountnumber');
                                }
                            )
                        } catch (err) {}
                    }}
                    closeModal={
                        ()=>this.setState(
                            {
                                openModal:false
                            }
                        )
                    }
                />
            </View>
        );
    };
}
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
        marginVertical:50
    },
    heading: {
        textAlign: 'left',
        color: 'white',
        opacity: 1,
        fontFamily: 'Roboto-Bold',
        fontSize:18,
        textAlign: 'left',
        textAlignVertical:'center',
        padding:15,
    },
    dropdownContainer: {
        paddingVertical: 10,
        marginHorizontal: 30,
    },
    TxtInputContainer: {
        paddingHorizontal: 30,
    },
    buttonContainer: {
        // alignItems: 'center',
        paddingVertical: 20,
        paddingHorizontal: 30,
    },
});

export default QRPaymentScreen;
