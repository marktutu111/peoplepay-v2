import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    TextInput
} from 'react-native';
import Colors from '../../constants/colors';
import Header from '../../components/Header';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import service from "../../services/transactions.service";
import { Alert } from 'react-native';



// change cont declaration name and export default name
class PayWithTerminalIdScreen extends Component {

    isLoaded=false;

    state={
        loading:false,
        terminalId:''
    };

    componentDidMount(){
        this.isLoaded=true;
    }


    getMerchat=async()=>{
        try {
            this.setState({loading:true});
            let response=await service.getMerchantDetails(
                this.state.terminalId
            );
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            return this.setState(
                {
                    loading:false
                },()=>{
                    this.props.navigation.navigate(
                        'ScanDetails',
                        response.data
                    );
                }
            )
        } catch (err) {
            this.setState(
                {
                    loading:false
                },()=>Alert.alert(
                    'Merchant Error',
                    err.message
                )
            )
        }
    }


    render() {
        return this.state.open_scanner ? (
            <View style={{flex: 1}}>
                <QRScannerView
                    hintText=""
                    renderHeaderView={this.renderTitleBar}
                    onScanResult={this.barcodeReceived.bind(this)}
                    renderFooterView={this.renderMenu}
                    scanBarAnimateReverse={true}
                />
            </View>
        ) : (
            <View style={styles.screen}>
                <Header
                    pressTo={()=>this.props.navigation.goBack()}
                />
                <ScrollView style={styles.scroll}>
                    <View style={styles.Body}>
                        <View style={styles.TextContainer}>
                            <Text style={{
                                textAlign:'center',
                                marginTop:10,
                                fontSize:50,
                                fontWeight:'bold'
                            }}>GHQR</Text>
                            <Text style={styles.ScanText}>
                                Enter Terminal Id
                            </Text>
                        </View>
                        <View style={styles.container}>
                            <View style={styles.iconsContainer}>
                                <TextInput
                                        style={styles.textfield}
                                        onChangeText={v=>this.state.terminalId=v.trim()}
                                        keyboardType="number-pad"
                                        maxLength={10}
                                    />
                            </View>
                            <View style={{
                                marginVertical:15,
                                width:150
                            }}>
                                <LoginPrimaryButton
                                        loading={this.state.loading}
                                        title="Continue"
                                        pressTo={this.getMerchat.bind(this)}
                                    />
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    HeaderTextMain: {
        color: Colors.secondary,
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingVertical: 15,
        paddingBottom: 20,
        flexDirection: 'row',
    },
    HeaderText: {
        fontFamily: 'Roboto-ExtraBold',
        fontSize: 35,
        color: Colors.secondary,
        textAlign: 'left',
        opacity: 0.8,
    },
    HeaderText1: {
        fontFamily: 'Roboto-ExtraBold',
        fontSize: 35,
        color: Colors.primary,
        textAlign: 'left',
        opacity: 0.8,
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
        marginVertical:100
    },
    ScanText: {
        fontFamily: 'Roboto-Bold',
        width: '100%',
        paddingVertical: 15,
        textAlign: 'center',
        opacity: 0.5,
        fontSize: 15,
        marginBottom: 50,
        color:Colors.secondary
    },
    container: {
        flexDirection:'column',
        justifyContent: 'center',
        backgroundColor: Colors.accent,
        alignItems: 'center',
        bottom: 10,
    },
    iconsContainer: {
        marginVertical:10,
        marginHorizontal:70,
        width:'100%',
        justifyContent:'center',
        alignItems:'center'
    },
    textfield: {
        borderBottomColor:Colors.tetiary,
        borderBottomWidth: 1,
        alignItems:'center',
        fontFamily:'Roboto-Regular',
        color:Colors.tetiary,
        textAlign: 'center',
        width: '80%',
        ...Platform.select(
            {
                ios:{
                    height:50
                }
            }
        ),
        letterSpacing:10,
        fontSize:30
    },
});

export default PayWithTerminalIdScreen;
