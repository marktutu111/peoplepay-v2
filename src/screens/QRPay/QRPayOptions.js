import React from 'react';
import {View,StyleSheet} from 'react-native';
import Colors from '../../constants/colors';
import Header from '../../components/Header';
import TertiaryButton from '../../components/TertiaryButton';
import {ScrollView} from 'react-native-gesture-handler';
import {SCREEN_HEIGHT} from '../../constants/dimensions';
import HeaderTxt1 from '../../components/HeaderTxt1';



const QrpOptionsScreen=(props)=>{
    return (
        <View style={styles.screen}>
            {/* Header of the Screen */}
            <Header pressTo={() => props.navigation.navigate('Home')} />
            <ScrollView
                style={styles.scroll}
                keyboardShouldPersistTaps="always"
                keyboardDismissMode="on-drag">
                <HeaderTxt1 txt1="GHQR"/>
                <View style={styles.Body}>
                    <View style={styles.cardrow}>
                        <TertiaryButton
                            IconName="user"
                            IconType="antdesign"
                            title="Enter Terminal Id"
                            pressTo={()=>props.navigation.navigate('TerminalPay')}
                        />
                        <TertiaryButton
                            fas
                            IconName="mobile"
                            title="Scan and Pay"
                            pressTo={()=>props.navigation.navigate('QRPay')}
                        />
                        {/* <TertiaryButton
                            fas
                            IconName="qrcode"
                            title="Get QRCode"
                            pressTo={()=>props.navigation.navigate('PaymentSource')}
                        />
                        <TertiaryButton
                            fas
                            IconName="qrcode"
                            title="View QRCode"
                            pressTo={null}
                        /> */}
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },

    Body: {
        justifyContent: 'center',
        flex: 1,
    },
    cardrow: {
        paddingHorizontal: 30,
        paddingVertical: SCREEN_HEIGHT > 700 ? 20 : 0,
        justifyContent: 'space-evenly',
    },
    heading: {
        textAlign: 'left',
        // paddingVertical: 10,
        // marginBottom: 10,
        // borderBottomWidth: 0.8,
        // borderColor: Colors.primary,
        color: 'white',
        opacity: 1,
        fontFamily: 'Roboto-Bold',
        fontSize: 18,
        textAlign: 'center',
        textAlignVertical: 'center',
        padding: 5,
    },
    // HeaderText: {
    //     textAlign: 'center',
    //     fontFamily: 'Roboto-Bold',
    //     fontSize: 18,
    //     textTransform: 'uppercase',
    //     color: Colors.accent,
    //     paddingHorizontal: 20,
    // },
    HeaderTextMain: {
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingTop: 30,
        paddingBottom: 20,
        flexDirection: 'row',
    },
    HeaderText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.secondary,
        // textAlign: 'center',
        opacity: 0.8,
    },
    HeaderText1: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.primary,
        // textAlign: 'center',
        opacity: 0.8,
        // bottom: 20,
    },
});

export default QrpOptionsScreen;
