import React, {Component} from 'react';
import {View, Text, StyleSheet, Alert, TextInput} from 'react-native';
import Colors from '../../constants/colors';
import Header from '../../components/Header';
import HeaderText from '../../components/HeaderText';
import {ScrollView} from 'react-native-gesture-handler';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import {AuthConsumer} from '../../states/auth.state';
import TxtInput from '../../components/TxtInput';
import {CustomExample} from '../../components/customPicker';
import service from '../../services/wallet.service';
import { TransactionContext } from '../../states/transactions.state';



export default class ScanDetailsScreen extends Component {

    static contextType=TransactionContext;

    state={
        amount: '',
        description: "",
    };


    render() {
        const {updateState}=this.context;
        const merchant=this.props.navigation.getParam('merchant') || '';
        return (
            <View style={styles.screen}>
                <AuthConsumer>
                    {({state})=>{
                        this.user = state.user;
                        this.password = state.password;
                    }}
                </AuthConsumer>
                <Header pressTo={()=>this.props.navigation.goBack()} />
                <ScrollView
                    style={styles.scroll}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag">
                    <HeaderText title1={`Pay Merchant`} title2={merchant} />
                    <View style={styles.Body}>

                        <View style={{paddingHorizontal:30}}>
                            <TxtInput
                                placeholder={'Amount'}
                                IconName="money"
                                IconType="font-awesome"
                                keyboardType="number-pad"
                                onChangeText={v=>this.state.amount=v.trim()}
                            />
                            <TxtInput
                                placeholder={'Description'}
                                IconName="description"
                                IconType="material"
                                onChangeText={v=>this.state.description=v}
                            />
                        </View>

                        <View style={styles.buttonContainer}>
                            <AuthConsumer>
                                {({state}) => {
                                    return (
                                        <LoginPrimaryButton
                                            loading={this.state.loading}
                                            title="Send"
                                            pressTo={()=>{
                                                const {user}=state;
                                                let data=this.props.navigation.getParam('qr');
                                                const {
                                                    amount,
                                                    description
                                                }=this.state;
                                                if(amount===''||description===''){
                                                    return;
                                                }
                                                let transaction={
                                                    amount:amount,
                                                    customerId:user._id,
                                                    description:description,
                                                    qrcode:data,
                                                    transaction_type:'QRP',
                                                    recipient_account_name:merchant
                                                };
                                                updateState(
                                                    {
                                                        returnPage:'QRP_Option',
                                                        transaction:transaction
                                                    },()=>{
                                                        this.props.navigation.navigate(
                                                            'ChooseSource',
                                                            {
                                                                data:transaction,
                                                                page:'SenderDetailsMoMo',
                                                                data:{ page:'ConfirmTransaction' }
                                                            }
                                                        )
                                                    }
                                                )
                                            }}
                                        />
                                    );
                                }}
                            </AuthConsumer>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
    },
    heading: {
        marginTop: 10,
        textAlign: 'left',
        paddingBottom: 10,
        marginBottom: 10,
        borderBottomWidth: 0.8,
        borderColor: Colors.primary,
        marginHorizontal: 30,
        fontFamily: 'Roboto-Bold',
        color: Colors.tetiary,
        opacity: 0.7,
    },
    dropdownContainer: {
        paddingVertical: 10,
        marginHorizontal: 30,
    },

    TxtInputContainer: {
        paddingHorizontal: 30,
    },
    buttonContainer: {
        // alignItems: 'center',
        paddingVertical: 20,
        paddingHorizontal: 30,
    },
});
