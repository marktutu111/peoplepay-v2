/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {View, StyleSheet, ScrollView, Alert} from 'react-native';
// component imports
import TxtInput from '../../components/TxtInput';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import Header from '../../components/Header';
import HeaderTxt1 from '../../components/HeaderTxt1';
import {AuthConsumer} from '../../states/auth.state';
import service from '../../services/users.service';

class EditProfileScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            loading: false,
        };
    }

    updateUser = async () => {
        try {
            let data = {};
            Object.keys(this.state).forEach(key => {
                if (key !== 'loading' && key !== 'id') {
                    data[key] = this.state[key];
                }
            });
            this.setState({loading: true});
            const response = await service.updateCustomer({
                id: this.state.id,
                data: data,
            });
            if (!response.success) {
                throw Error(response.message);
            }
            this.setState(
                {
                    loading: false,
                },
                () => {
                    Alert.alert('Account Update', response.message);
                    this.props.navigation.navigate('Home');
                },
            );
        } catch (err) {
            this.setState(
                {
                    loading: false,
                },
                () => {
                    Alert.alert('Oops!', err.message);
                },
            );
        }
    };

    render() {
        return (
            <AuthConsumer>
                {({state}) => {
                    const {user} = state;
                    this.state.id = user._id;
                    return (
                        <ScrollView style={styles.scroll}>
                            {/* Header Component */}
                            <Header pressTo={() => { global.currentScreenIndex = 0;
                                this.props.navigation.goBack();}} />
    
                            <HeaderTxt1
                                txt1="Edit"
                                txt2=" Profile"
                                pressTo={() => this.props.navigation.goBack()}
                            />
                            <View style={styles.screen}>
                                <View style={styles.textFieldContainer}>
                                    <TxtInput
                                        placeholder="Full Name"
                                        IconName="user"
                                        IconType="feather"
                                        onChangeText={v =>
                                            (this.state.fullname = v.trim())
                                        }
                                        defaultValue={user.fullname}
                                    />
                                    <TxtInput
                                        placeholder="Email"
                                        IconName="mail"
                                        IconType="feather"
                                        onChangeText={v =>
                                            (this.state.email = v.trim())
                                        }
                                        keyboardType="email-address"
                                        defaultValue={user.email}
                                    />
                                    <TxtInput
                                        editable={false}
                                        placeholder="Phone number"
                                        IconName="phone"
                                        IconType="feather"
                                        defaultValue={user.phone}
                                    />
                                </View>

                                {/* button prop */}
                                <LoginPrimaryButton
                                    loading={this.state.loading}
                                    title="SAVE CHANGES"
                                    pressTo={this.updateUser.bind(this)}
                                />
                            </View>
                        </ScrollView>
                    );
                }}
            </AuthConsumer>
        );
    }
}

const styles = StyleSheet.create({
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    screen: {
        flex: 1,
        alignItems: 'center',
        paddingHorizontal: 30,
    },
    textFieldContainer: {
        paddingBottom: 30,
        paddingHorizontal: 10,
    },
});

export default EditProfileScreen;
