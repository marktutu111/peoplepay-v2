import React from 'react';
import {View, StyleSheet, ScrollView, Text} from 'react-native';
import ProfileAvatar from '../../components/Avatar';
import Link from '../../components/links';
import Colors from '../../constants/colors';
import DrawerHeader from '../../components/DrawerHeader';
import HeaderTxt1 from '../../components/HeaderTxt1';
// import DropDownItem from 'react-native-drop-down-item';

const AccountSettingsScreen = props => {
    return (
        <View style={styles.screen}>
            {/* Header */}
            <DrawerHeader pressTo={() => props.navigation.toggleDrawer()} />
            <ScrollView style={styles.scroll}>
                {/* Screen Title */}
                <HeaderTxt1 txt1="Account" txt2=" Settings" />
                {/* Screen Main Content */}
                <View style={styles.Body}>
                    {/* Profile Avatar */}
                    <View style={styles.profileContainer}>
                        <ProfileAvatar
                            name="Mark Tutu"
                            email="marktutu@gmail.com"
                        />
                    </View>
                    {/* Account Links */}
                    <View style={styles.sectionContainer}>
                        <View style={styles.bottomLink}>
                            <Link
                                name="Edit Profile"
                                pressTo={() =>
                                    props.navigation.navigate('EditProfile')
                                }
                            />
                        </View>
                        <View style={styles.bottomLink}>
                            <Link
                                name="Change Password"
                                pressTo={() =>
                                    props.navigation.navigate('Password')
                                }
                            />
                        </View>
                        <View style={styles.bottomLink}>
                            <Link
                                name="Support"
                                pressTo={() =>
                                    props.navigation.navigate('Help')
                                }
                            />
                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        // flex: 1,
        backgroundColor: 'white',
    },
    HeaderRow: {
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        alignItems: 'center',
        alignContent: 'center',
        width: '100%',
        height: 65,
        backgroundColor: Colors.primary,
        marginBottom: 15,
        paddingHorizontal: 30,
    },
    HeaderTextLogo: {
        textAlign: 'center',
        fontFamily: 'Roboto-Bold',
        fontSize: 18,
        textTransform: 'uppercase',
        color: Colors.accent,
    },
    navIcon: {
        padding: 20,
    },
    empty: {
        padding: 10,
    },
    HeaderTextMain: {
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingTop: 30,
        paddingBottom: 20,
        // flexDirection: 'row',
    },
    HeaderText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.secondary,
        // textAlign: 'center',
        opacity: 0.8,
    },
    HeaderText1: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.primary,
        // textAlign: 'center',
        opacity: 0.8,
        bottom: 20,
    },
    Body: {
        paddingHorizontal: 30,
        flex: 1,
        paddingTop: 20,
    },
    profileContainer: {
        // marginBottom: 10,
    },

    sectionContainer: {
        marginTop: 10,
    },
    bottomLink: {
        borderBottomWidth: 1,
        borderColor: '#ACACAC',
        width: '100%',
    },
});

export default AccountSettingsScreen;
