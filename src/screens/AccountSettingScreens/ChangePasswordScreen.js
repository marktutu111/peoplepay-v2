/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import {View, StyleSheet, ScrollView,Alert} from 'react-native';
// component imports
import HeaderTxt1 from '../../components/HeaderTxt1';
import TxtInput from '../../components/TxtInput';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import Header from '../../components/Header';
import { AuthConsumer } from '../../states/auth.state';
import service from "../../services/users.service";


class EditProfileScreen extends Component {
    constructor(props){
        super(props);
        this.state={
            loading:false,
            id:'',
            oldpassword:'',
            newpassword:'',
            confirm:''
        }
    }

    save=async()=>{
        try {
            const {
                id,
                oldpassword,
                newpassword,
                confirm
            }=this.state;
            const data={
                id:id,
                oldpassword:oldpassword,
                newpassword:newpassword
            }

            Object.keys(data).forEach(key=>{
                if (key !== 'id'){
                    if (data[key] === ''){
                        throw Error(
                            `You have not provided value ${key}`
                        )
                    }
                }
            });

            if (newpassword === oldpassword){
                throw Error('New password cannot be the same as old password');
            }

            if (newpassword !== confirm){
                throw Error('Password does not match');
            }

            this.setState({loading:true});
            const response=await service.updatePassword(data);
            if (!response.success){
                throw Error(response.message);
            }
            
            this.setState(
                {
                    loading:false
                }, ()=>{
                    Alert.alert(
                        'Update Password',
                        response.message
                    )
                    this.props.navigation.navigate('Home');
                }
            )

        } catch (err) {
            this.setState(
                {
                    loading:false
                }, ()=>{
                    Alert.alert(
                        'Oops!',
                        err.message
                    )
                }
            )
        }
    }


    render(){
        return (
            <ScrollView style={styles.scroll}>

                <AuthConsumer>
                    {
                        ({state})=>{
                            this.state.id=state.user._id;
                        }
                    }
                </AuthConsumer>
                <Header pressTo={() => { global.currentScreenIndex = 0;
                                this.props.navigation.goBack();}} />
    
                <HeaderTxt1
                    txt1="Change"
                    txt2=" Password"
                />
                <View style={styles.screen}>
                    <View style={styles.textFieldContainer}>
                        {/* Old Password */}
                        <TxtInput 
                            IconName="lock"
                            IconType="feather"
                            placeholder="Old Password"
                            password={true}
                            onChangeText={v=>this.state.oldpassword=v.trim()}
                        />
    
                        {/* New Password */}
                        <TxtInput 
                            IconName="lock"
                            IconType="feather"
                            placeholder="New Password" 
                            password={true}
                            onChangeText={v=>this.state.newpassword=v.trim()}
                        />
    
                        {/* Retype Password */}
                        <TxtInput 
                            IconName="lock"
                            IconType="feather"
                            placeholder="Retype Password"
                            password={true}
                            onChangeText={v=>this.state.confirm=v.trim()}
                        />
                    </View>
    
                    {/* button prop */}
                    <LoginPrimaryButton
                        loading={this.state.loading}
                        title="SAVE CHANGES"
                        pressTo={this.save.bind(this)}
                    />
                </View>
            </ScrollView>
        );
    }

};

const styles = StyleSheet.create({
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    screen: {
        flex: 1,
        alignItems: 'center',
        paddingTop: 20,
    },
    textFieldContainer: {
        paddingBottom: 30,
        paddingHorizontal: 30,
    },
});

export default EditProfileScreen;
