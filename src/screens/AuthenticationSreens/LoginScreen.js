import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image as ImageRn,
    Alert,
    ImageBackground,
    KeyboardAvoidingView
} from 'react-native';
import Colors from '../../constants/colors';
import {ScrollView} from 'react-native-gesture-handler';
import TxtInput from '../../components/TxtInput';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import AsyncStorage from '@react-native-community/async-storage';
import {AuthConsumer} from '../../states/auth.state';
import service from '../../services/users.service';
import {SCREEN_HEIGHT} from '../../constants/dimensions';
import colors from '../../constants/colors';
import FingerprintScanner from 'react-native-fingerprint-scanner';
import {Icon} from 'react-native-elements';

class LoginScreen extends Component {

    state={
        loading:false,
        email:'',
        password:'',
    }

    fingerAuth(){
        FingerprintScanner.authenticate({ description: 'Scan your fingerprint on the device scanner to continue' })
        .then(async(r) => {
            try {
                const result= await this.fingerPrintLogin();
                if (!result.email || !result.password){
                    throw Error(result);
                }
                this.login(result);
            } catch (err) {
                Alert.alert(err.message);
            }
        }).catch((error) => {
            Alert.alert(error.message);
        });
    }


    login=async _data=>{
        try {
            this.setState(
                {
                    loading: true,
                }
            );
            const response=await service.customerLogin(_data);
            if(!response.success){
                throw Error(
                    response[
                        'message'
                    ]
                )
            }
            const {data,token,card}=response;
            return this.setState(
                {
                    loading:false,
                },async()=>{
                    await Promise.all(
                        [
                            AsyncStorage.setItem('peoplepay-user-password',_data.password),
                            AsyncStorage.setItem('peoplepay-user',JSON.stringify(data))
                        ]
                    );
                    this.setHeaders(token);
                    this.updateState(
                        {
                            user:data,
                            token:token,
                            password:_data.password,
                            card:card
                        }
                    );
                },
            );
        } catch (err) {
            Alert.alert('Login failed',err.message);
            this.setState(
                {
                    loading: false,
                }
            );
        }
    };


    render() {
        return (
            <AuthConsumer>
                {({updateState, setHeaders,fingerPrintLogin}) => {
                    this.updateState=updateState;
                    this.setHeaders=setHeaders;
                    this.fingerPrintLogin=fingerPrintLogin;
                    return (
                        <ScrollView keyboardShouldPersistTaps='handled'>
                            <ImageBackground
                                source={require('../../assets/images/login_bg.jpg')}
                                style={{
                                    width: '100%',
                                    justifyContent: 'flex-end',
                                    backgroundColor: Colors.accent,
                                    height: SCREEN_HEIGHT,
                                    // paddingVertical: 10,
                                }}>
                                <View style={styles.linearGradient}>
                                    <ImageRn
                                        source={require('../../assets/images/peoplepay-icon-white.png')}
                                        style={styles.Logo}
                                        resizeMode="contain"
                                    />
                                </View>

                                <KeyboardAvoidingView style={styles.formContainer}>
                                    {/* Screen Title */}
                                    <View style={styles.HeaderTextMain}>
                                        <Text style={styles.HeaderText}>
                                            Welcome
                                        </Text>
                                        <Text style={styles.HeaderText1}>
                                            Sign in to continue
                                        </Text>
                                    </View>
                                    <View>
                                        <TxtInput
                                            placeholder="Phone number"
                                            IconName="user"
                                            IconType="antdesign"
                                            onChangeText={v =>
                                                this.setState({
                                                    email:v.trim(),
                                                })
                                            }
                                        />
                                        <TxtInput
                                            placeholder="Password"
                                            IconName="lock"
                                            IconType="feather"
                                            onChangeText={v =>
                                                this.setState({
                                                    password: v.trim(),
                                                })
                                            }
                                            password={true}
                                        />
                                        {/* <TxtInput placeholder="Repeat Password" /> */}
                                    </View>

                                    <View
                                        style={{
                                            flexDirection: 'column',
                                            justifyContent: 'space-evenly',
                                            // padding:10,
                                            alignItems: 'center',
                                        }}>
                                        <View style={styles.btn}>
                                            <LoginPrimaryButton
                                                title="Login"
                                                loading={this.state.loading}
                                                pressTo={() => {
                                                    const {
                                                        email,
                                                        password,
                                                    } = this.state;
                                                    if (
                                                        email === '' ||
                                                        password === ''
                                                    ) {
                                                        return Alert.alert(
                                                            'Oops!',
                                                            'Email or password is required',
                                                        );
                                                    }
                                                    return this.login({
                                                        email:email,
                                                        password:password,
                                                    });
                                                }}
                                            />
                                        </View>
                                        <TouchableOpacity onPress={this.fingerAuth.bind(this)}>
                                            <Icon
                                                name="ios-finger-print"
                                                type="ionicon"
                                                color={Colors.secondary}
                                                size={60}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                    <TouchableOpacity
                                        style={styles.textForgotContainer}
                                        onPress={()=>this.props.navigation.navigate(
                                                'ForgotPassword',
                                            )
                                        }
                                        activeOpacity={0.7}>
                                        <Text style={styles.textForgot}>
                                            Forgot password?
                                        </Text>
                                    </TouchableOpacity>
                                    <View style={styles.textSigninContainer}>
                                        <Text style={styles.textSignupLight}>
                                            Don't have an account?
                                        </Text>
                                        <TouchableOpacity
                                            style={{marginVertical:5}}
                                            hitSlop={{left:10,right:10,top:10,bottom:10}}
                                            activeOpacity={0.7}
                                            onPress={() =>
                                                this.props.navigation.replace(
                                                    'SignUp',
                                                )
                                            }>
                                            <Text style={styles.textSignup}>
                                                {' '}
                                                Create an account
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                    {/* End of container */}
                                </KeyboardAvoidingView>
                            </ImageBackground>
                        </ScrollView>
                    );
                }}
            </AuthConsumer>
        );
    }
}

const styles = StyleSheet.create({
    HeaderTextMain: {
        color: Colors.secondary,
        // textAlign: 'left',
        paddingBottom: 10,
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        // marginVertical: 5,
    },
    HeaderText: {
        // fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.secondary,
        textTransform: 'uppercase',
        // opacity: 0.7,
    },
    HeaderText1: {
        // fontFamily: 'Roboto-Regular',
        fontSize: 14,
        color: Colors.tertiary,
        // textAlign: 'center',
        opacity: 0.8,
        top: 3,
    },

    image: {
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    formContainer: {
        backgroundColor: Colors.accent,
        width: '90%',
        height:'auto',
        alignSelf: 'center',
        position: 'absolute',
        bottom: '8%',
        borderRadius: 20,
        paddingHorizontal: 30,
        paddingVertical:20,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.21,
        shadowRadius: 2,
        elevation: 4,
    },
    textForgotContainer: {
        marginVertical:10,
    },
    textForgot: {
        color: Colors.tertiary,
        // textAlign: 'center',
        fontSize: 14,
        opacity: 0.8,
        // marginBottom: 40,
        alignSelf: 'center',
        // paddingVertical: 10,
    },
    text: {
        fontSize: 16,
        color: 'black',
        textAlign: 'center',
        // padding: 30,
        // paddingBottom: 10,
        paddingHorizontal: 65,
        opacity: 0.7,
    },

    title: {
        fontSize: 25,
        color: 'black',
        textAlign: 'center',
        marginBottom: 16,
    },
    buttonCircle: {
        width: 40,
        height: 40,
        backgroundColor: 'rgba(0, 0, 0, .2)',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    LoginImage: {
        left: 200,
    },
    Logo: {
        width: 60,
        height: 60,
        alignSelf: 'center',
    },
    linearGradient: {
        position: 'absolute',
        top: '5%',
        zIndex: 1,
        width: '60%',
        height: 100,
        borderTopRightRadius: 45,
        borderBottomRightRadius: 45,
        alignContent: 'center',
        justifyContent: 'center',
        backgroundColor: colors.secondary,
        marginTop: 30,
    },
    textSigninContainer: {
        // paddingVertical: 20,
        flex: 1,
        // flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 20,
    },

    textSignupLight: {
        color: Colors.tertiary,
        // textAlign: 'center',
        fontSize: 16,
        opacity: 0.8,
    },
    textSignup: {
        color: Colors.secondary,
        // textAlign: 'center',
        fontSize: 16,
        // opacity: 0.85,
    },
    btn: {
        paddingVertical: 10,
        width: '100%',
    },
});

export default LoginScreen;
