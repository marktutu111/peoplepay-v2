import React,{ Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';


import Colors from '../../constants/colors';
import Header from '../../components/Header';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import {ScrollView} from 'react-native-gesture-handler';
import otpService from "../../services/otp.service";
import usersService from '../../services/users.service';
import { Alert } from 'react-native';
import TxtInput from '../../components/TxtInput';



// change cont declaration name and export default name
class ForgotPassword extends Component {

    state={
        phone:'',
        password:'',
        otp:'',
        confirm:'',
        loading:false,
        otpsent:false
    }

    resetpassword=async()=>{
        try {
            const {
                password,
                confirm,
                otp,
                phone
            }=this.state;
            if(password==='' || confirm==='' || otp==='' || phone===''){
                return;
            }
            if(password !== confirm){
                throw Error(
                    'Password does not match'
                )
            };
            this.setState(
                {
                    loading:true
                }
            )
            const response=await usersService.forgotPassword(
                {
                    phone:phone,
                    otp:otp,
                    password:password
                }
            );
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            this.setState(
                {
                    loading:false,
                },()=>{
                    Alert.alert(
                        'good!',
                        'Password reset successful, Kindly login.'
                    );
                    this.props.navigation.navigate(
                        'Login'
                    )
                }
            )
        } catch (err) {
            this.setState(
                {
                    loading:false,
                },()=>Alert.alert(
                    'Oops!',
                    err.message
                )
            )
        }
    }

    sendOtp=async()=>{
        try {
            const {phone}=this.state;
            if(phone===''){
                throw Error(
                    'Please enter a valid phone number'
                )
            }
            this.setState(
                {
                    otpsent:false,
                    password:'',
                    confirm:'',
                    otp:'',
                    loading:true
                }
            )
            const response=await otpService.sendOTP(phone);
            if (!response.success) {
                throw Error(
                    response.message
                )
            }
            this.setState(
                {
                    otpsent:true,
                    loading:false
                }
            )
        } catch (err) {
            this.setState(
                {
                    loading:false
                },()=>{
                    Alert.alert(
                        'OTP Not Sent', 
                        err.message
                    );
                }
            )
        }
    };

    render(){
        return (
            <View style={styles.screen}>
                {/* Header of the Screen */}
                <Header text="Forgot Password" pressTo={()=>{
                    switch (this.state.otpsent) {
                        case true:
                            this.setState(
                                {
                                    otpsent:false
                                }
                            )
                            break;
                        default:
                            this.props.navigation.goBack();
                            break;
                    }
                }} />
                <ScrollView
                    style={styles.scroll}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag">
                        <Text style={styles.infoText}>
                            Please enter the phone number that you used to sign up
                            your account.
                        </Text>
                    <View style={styles.Body}>
                        <TxtInput
                            placeholder="Phone number"
                            maxLength={10}
                            editable={!this.state.otpsent}
                            keyboardType={'phone-pad'}
                            onChangeText={
                                v=>{
                                    this.state.phone=v.trim()
                                }
                        }/>
                        {this.state.otpsent && <>
                            <TxtInput 
                                placeholder="OTP" 
                                maxLength={5}
                                onChangeText={
                                    v=>{
                                        this.state.otp=v.trim()
                                    }
                            }/>
                            <TxtInput
                                placeholder="Password"
                                secureTextEntry={true}
                                onChangeText={
                                    v=>{
                                        this.state.password=v.trim()
                                    }
                            }/>
                            <TxtInput
                                placeholder="Comfirm Password" 
                                secureTextEntry={true}
                                onChangeText={
                                    v=>{
                                        this.state.confirm=v.trim()
                                    }
                            }/>

                        </>}
                        <View style={styles.resendContainer}>
                            <TouchableOpacity 
                                activeOpacity={5}
                                onPress={this.sendOtp.bind(this)}
                            >
                                <Text style={styles.resendText}>RESEND</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.btn}>
                            <LoginPrimaryButton
                                loading={this.state.loading}
                                title="Continue"
                                pressTo={
                                    ()=>{
                                        if(this.state.otpsent){
                                            this.resetpassword().catch(err=>null);
                                        }else{
                                            this.sendOtp().catch(err=>null)
                                        }
                                    }
                                }
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }


};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor:'white',
    },
    HeaderTextMain: {
        color: Colors.secondary,
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingVertical: 10,
        // paddingBottom: 20,
    },
    HeaderText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.secondary,
        textAlign: 'left',
        opacity: 0.8,
        marginBottom: 0,
        paddingRight: 20,
    },
    HeaderText1: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.secondary,
        textAlign: 'left',
        opacity: 0.8,
        marginBottom: 0,
        paddingRight: 20,
        bottom: 20,
    },

    Body: {
        flex: 1,
        marginHorizontal:50,
        marginVertical:15
    },
    textfield: {
        borderBottomColor: '#262626',
        borderBottomWidth: 0.4,
        // width: '100%',
        marginHorizontal: 30,
        alignItems: 'center',
        fontFamily: 'Roboto-Regular',
        color: Colors.primary,
        padding:15,
    },
    infoText: {
        width:'80%',
        textAlign: 'left',
        marginVertical:40,
        paddingHorizontal:55,
        fontWeight:'300'
    },
    resendContainer: {
        width: '100%',
        alignItems: 'flex-end',
        paddingHorizontal: 30,
        paddingVertical: 15,
    },
    resendText: {
        fontFamily: 'Roboto-Bold',
        width: '100%',
        paddingBottom: 20,
        textAlign: 'right',
        opacity: 0.7,
        color: Colors.primary,
    },
    btn: {
        paddingHorizontal: 30,
    },
});

export default ForgotPassword;
