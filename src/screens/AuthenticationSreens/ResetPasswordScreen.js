import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    Dimensions,
    Alert,
} from 'react-native';
import Colors from '../../constants/colors';
import {ScrollView} from 'react-native-gesture-handler';
import TxtInput from '../../components/TxtInput';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';

export default class ResetPasswordScreen extends Component {
    
    state={
        password: '',
        confirmPassword: '',
    };

    render = () => {
        return (
            <ScrollView
                style={styles.scroll}
                keyboardShouldPersistTaps="always"
                keyboardDismissMode="on-drag">
                <View style={styles.screen}>
                    {/* Header of the Screen */}
                    {/* <Header pressTo={() => props.navigation.goBack()} /> */}

                    <View style={styles.LogoContainer}>
                        <Image
                            style={styles.Logo}
                            source={require('../../assets/images/PeoplePay1.png')}
                            resizeMode="contain"
                        />
                    </View>

                    {/* Screen Title */}
                    <View style={styles.HeaderTextMain}>
                        <Text style={styles.HeaderText}>ResetPassword</Text>
                        <Text style={styles.HeaderText1}>
                            create an account to get started
                        </Text>
                    </View>
                    {/* Screen Main Content */}
                    <View style={styles.Body}>
                        <View>
                            <TxtInput
                                password={true}
                                placeholder="Password"
                                IconName="lock"
                                IconType="feather"
                                onChangeText={v =>
                                    this.setState({
                                        password: v.trim(),
                                    })
                                }
                            />
                            <TxtInput
                                password={true}
                                placeholder="Confirm Password"
                                IconName="lock"
                                IconType="feather"
                                onChangeText={v =>
                                    this.setState({
                                        confirmPassword: v.trim(),
                                    })
                                }
                            />
                        </View>
                        {/* Terms and Conditons */}
                        <View style={styles.TnCsContainer}>
                            <Text style={styles.TnCsTextLight}>
                                By clicking on Register, you accept our{' '}
                            </Text>
                            <TouchableOpacity
                                activeOpacity={0.7}
                                // onPress={() => this.props.navigation.replace('')}
                            >
                                <Text style={styles.TnCsTextBold}>
                                    Terms and conditions
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.btn}>
                            <LoginPrimaryButton
                                title="Sign Up"
                                loading={this.state.loading}
                                pressTo={() => {
                                    const data = this.state;
                                    Object.keys(data).forEach(key => {
                                        if (data[key] === '') {
                                            empty = key;
                                        }
                                    });
                                    if (typeof empty === 'string') {
                                        return Alert.alert(
                                            'Oops!',
                                            `Please provide value for ${empty}`,
                                        );
                                    }
                                    if (
                                        data.password !== data.confirmPassword
                                    ) {
                                        return Alert.alert(
                                            'Oops!',
                                            'Passwords does match',
                                        );
                                    }

                                    this.props.navigation.navigate(
                                        'VerifyCustomer',
                                        {
                                            data: {
                                                password: data.password,
                                            },
                                        },
                                    );
                                }}
                            />
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    };
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
        paddingHorizontal: 30,
    },
    LogoContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 5,
    },
    Logo: {
        width: 90,
        height: 90,
    },
    HeaderTextMain: {
        color: Colors.secondary,
        textAlign: 'left',
        paddingBottom: 20,
    },
    HeaderText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.tetiary,
        // textAlign: 'center',
        opacity: 0.7,
    },
    HeaderText1: {
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
        color: Colors.tetiary,
        // textAlign: 'center',
        opacity: 0.8,
        // bottom: 10,
    },
    Body: {
        flex: 1,
        // alignItems: 'center',
        justifyContent: 'space-evenly',
        // paddingHorizontal: 30,
    },
    btn: {
        width: '100%',
        alignItems: 'center',
    },
    TnCsContainer: {
        flex: 1,
        // flexDirection: 'row',
        justifyContent: 'center',
        // paddingHorizontal: 30,
        paddingTop: 10,
        paddingBottom: 15,
        alignItems: 'center',
        alignContent: 'center',
    },

    TnCsTextLight: {
        fontFamily: 'Roboto-Regular',
        color: Colors.tertiary,
        // textAlign: 'center',
        fontSize: 12,
        opacity: 0.85,
    },
    TnCsTextBold: {
        fontFamily: 'Roboto-Bold',
        color: Colors.secondary,
        textAlign: 'center',
        fontSize: 12,
    },

    textSigninContainer: {
        marginTop: Dimensions.get('window').height > 700 ? 100 : 28,
        // paddingVertical: 20,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },

    textSignupLight: {
        fontFamily: 'Roboto-Regular',
        color: Colors.tertiary,
        // textAlign: 'center',
        fontSize: 14,
        opacity: 0.8,
    },
    textSignup: {
        fontFamily: 'Roboto-Bold',
        color: Colors.secondary,
        // textAlign: 'center',
        fontSize: 14,
        // opacity: 0.85,
    },
    gradient: {
        flex: 1,
    },
});
