import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    Dimensions,
    Alert,
    Platform,
    KeyboardAvoidingView,
    Linking
} from 'react-native';

import CheckBox from '@react-native-community/checkbox';
import Colors from '../../constants/colors';
import {ScrollView} from 'react-native-gesture-handler';
import TxtInput from '../../components/TxtInput';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import messages from '../../constants/errormessages';


// change cont declaration name and export default name
class SignUpScreen extends Component {

    state={
        lname:'',
        fname:'',
        email: '',
        phone: '',
        password: '',
        confirmPassword: '',
        code:null,
        checked:false
    };

    Validate_email() {
        const {email} = this.state;
        let rjx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (rjx.test(email) === false) {
            alert('Please enter a valid email');
        }
    }

    Validate_phone() {
        const {phone} = this.state;
        let rjx = /^\+?\(?(([2][3][3])|[0])?\)?[-.\s]?\d{2}[-.\s]?\d{3}[-.\s]?\d{4}?$/;
        if (rjx.test(phone) === false) {
            alert('Please enter a phone number');
        }
    }

    Validate_fullname() {
        const {fullname} = this.state;
        let rjx = /^(([A-Za-z]+[\-\']?)*([A-Za-z]+)?\s?)+([A-Za-z]+[\-\']?)*([A-Za-z]+)?$/;
        if (rjx.test(fullname) === false) {
            alert('Please enter a vaild name');
        }
    }

    render = () => {
        return (
            <ScrollView
                style={styles.scroll}
                keyboardShouldPersistTaps="always"
                keyboardDismissMode="on-drag"
            >
                <View style={styles.screen}>
                    {/* Header of the Screen */}
                    {/* <Header pressTo={() => props.navigation.goBack()} /> */}
                    <View style={styles.LogoContainer}>
                        <Image
                            style={styles.Logo}
                            source={require('../../assets/images/pp.png')}
                            resizeMode="contain"
                        />
                    </View>

                    {/* Screen Title */}
                    <View style={styles.HeaderTextMain}>
                        <Text style={styles.HeaderText}>Sign up</Text>
                        <Text style={styles.HeaderText1}>
                            create an account to get started
                        </Text>
                    </View>
                    {/* Screen Main Content */}
                    <KeyboardAvoidingView style={styles.Body}>
                        <View>
                            <TxtInput
                                placeholder="First Name"
                                IconName="user"
                                IconType="feather"
                                onChangeText={v=>
                                    this.setState({
                                        fname:v.trim(),
                                    })
                                }
                                onBlur={() => this.Validate_fullname()}
                            />
                            <TxtInput
                                placeholder="Last Name"
                                IconName="user"
                                IconType="feather"
                                onChangeText={v=>
                                    this.setState({
                                        lname: v.trim(),
                                    })
                                }
                                onBlur={() => this.Validate_fullname()}
                            />
                            <TxtInput
                                placeholder="Email"
                                IconName="mail"
                                IconType="feather"
                                onChangeText={v =>
                                    this.setState({email:v.trim()})
                                }
                                keyboardType="email-address"
                                onBlur={()=>this.Validate_email()}
                            />
                            <TxtInput
                                maxLength={10}
                                placeholder="Phone number"
                                IconName="phone"
                                IconType="feather"
                                onChangeText={v =>
                                    this.setState({
                                        phone:v.trim(),
                                    })
                                }
                                keyboardType="phone-pad"
                                onBlur={()=>this.Validate_phone()}
                            />
                            <TxtInput
                                password={true}
                                placeholder="Password"
                                IconName="lock"
                                IconType="feather"
                                onChangeText={v =>
                                    this.setState({
                                        password:v.trim(),
                                    })
                                }
                            />
                            <TxtInput
                                password={true}
                                placeholder="Confirm password "
                                IconName="lock"
                                IconType="feather"
                                onChangeText={v =>
                                    this.setState({
                                        confirmPassword:v.trim(),
                                    })
                                }
                            />
                        </View> 
                        <View style={{paddingVertical:20}}>
                            <Text style={{ marginVertical:5 }}>If you have a referal code,please enter it.</Text>
                            <TxtInput
                                hideIcon
                                style={{marginVertical:10}}
                                placeholder="Code"
                                IconName="magnet"
                                IconType="fontawesome"
                                onChangeText={v=>this.setState({
                                    code:v.trim(),
                                    })
                                }
                            />
                        </View>
                        {/* Terms and Conditons */}
                        <View style={styles.TnCsContainer}>
                            <View>
                                <Text style={styles.TnCsTextLight}>
                                    Check the box to proceed after reading our terms and conditions.
                                </Text>
                            </View>
                            <View
                                activeOpacity={0.7}
                                style={{flexDirection:'row',alignItems:'center',...Platform.select(
                                    {
                                        ios:{
                                            marginVertical:10
                                        }
                                    }
                                )}}
                            >
                                <CheckBox
                                    disabled={false}
                                    value={this.state.checked}
                                    onValueChange={v=>this.setState({checked:v})}
                                    style={{...Platform.select(
                                        {
                                            ios:{
                                                marginRight:10
                                            }
                                        }
                                    )}}
                                />
                                <TouchableOpacity
                                    onPress={()=>Linking.openURL(`https://peoplepay.com.gh/tnc`).catch(err=>null)}
                                    hitSlop={{
                                        top:10,
                                        bottom:10,
                                        left:10,
                                        right:10
                                    }}
                                >
                                    <Text style={styles.TnCsTextBold}>
                                        Terms and conditions
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.btn}>
                            <LoginPrimaryButton
                                title="Sign Up"
                                loading={this.state.loading}
                                pressTo={()=>{
                                    const data=this.state;
                                    let empty=null;
                                    Object.keys(data).forEach(key=>{
                                        if (data[key] === '') {
                                            empty=key;
                                        }
                                    });
                                    if (typeof empty === 'string') {
                                        return Alert.alert(
                                            'Oops!',
                                            'Please fill in all the required fields',
                                        );
                                    }
                                    if (data.password !== data.confirmPassword) {
                                        return Alert.alert(
                                            'Oops!',
                                            'Passwords does match',
                                        );
                                    };
                                    if(!this.state.checked){
                                        return Alert.alert(
                                            'Oops',
                                            'Please agree to our terms and conditions before signup'
                                        )
                                    }
                                    const _data={
                                        fullname:`${data['fname']} ${data['lname']}`,
                                        email: data.email,
                                        password: data.password,
                                        phone: data.phone,
                                        code:data['code']
                                    };
                                    this.props.navigation.navigate(
                                        'VerifyCustomer',
                                        {
                                            data:_data
                                        },
                                    );
                                }}
                            />
                        </View>
                    </KeyboardAvoidingView>

                    <View style={styles.textSigninContainer}>
                        <Text style={styles.textSignupLight}>Already have an account?</Text>
                        <TouchableOpacity
                            activeOpacity={0.7}
                            onPress={() =>
                                this.props.navigation.replace('Login')
                            }>
                            <Text style={styles.textSignup}> Login</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        );
    };
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
        marginVertical:40
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
        paddingHorizontal: 30,
    },
    LogoContainer: {
        ...Platform.select({
            ios:{
                margin:30,
            },
        }),
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 5,
    },
    Logo: {
        width: 150,
        height: 100,
    },
    HeaderTextMain: {
        color: Colors.secondary,
        textAlign: 'left',
        paddingBottom: 20,
    },
    HeaderText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.tetiary,
        // textAlign: 'center',
        opacity: 0.7,
    },
    HeaderText1: {
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
        color: Colors.tetiary,
        // textAlign: 'center',
        opacity: 0.8,
        // bottom: 10,
    },
    Body: {
        flex: 1,
        // alignItems: 'center',
        justifyContent: 'space-evenly',
        // paddingHorizontal: 30,
    },
    btn: {
        width: '100%',
        alignItems: 'center',
    },
    TnCsContainer: {
        flex: 1,
        // flexDirection: 'row',
        justifyContent: 'center',
        // paddingHorizontal: 30,
        paddingTop: 10,
        paddingBottom: 15,
        alignItems: 'center',
        alignContent: 'center',
    },

    TnCsTextLight: {
        fontFamily: 'Roboto-Regular',
        color: Colors.tertiary,
        // textAlign: 'center',
        fontSize: 15,
        opacity: 0.85,
    },
    TnCsTextBold: {
        fontFamily: 'Roboto-Bold',
        color: Colors.secondary,
        textAlign: 'center',
        fontSize: 12,
    },

    textSigninContainer: {
        marginTop: Dimensions.get('window').height > 700 ? 100 : 28,
        // paddingVertical: 20,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },

    textSignupLight: {
        fontFamily: 'Roboto-Regular',
        color: Colors.tertiary,
        // textAlign: 'center',
        fontSize: 14,
        opacity: 0.8,
    },
    textSignup: {
        fontFamily: 'Roboto-Bold',
        color: Colors.secondary,
        // textAlign: 'center',
        fontSize: 14,
        // opacity: 0.85,
    },
    gradient: {
        flex: 1,
    },
});

export default SignUpScreen;
