import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    Alert,
} from 'react-native';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import Colors from '../../constants/colors';
import {ScrollView} from 'react-native-gesture-handler';
import userService from '../../services/users.service';
import {AuthConsumer} from '../../states/auth.state';
import Header from '../../components/Header';
import WithOTP from '../../components/WithOtp.component';


class VerifyCustomerScreen extends Component {

    state={
        loading: false,
        otp: '',
    };

    componentDidMount=()=>{this.sendOtp()};


    sendOtp(resend=false){
        const {phone}=this.props.navigation.getParam('data');
        this.props.sendOtp(phone,resend);
    }


    confirmOTp=async()=>{
        try {
            const otp=this.state.otp;
            if (otp === '') {
                return;
            }
            if (otp.length !== 5) {
                return Alert.alert(
                    'Otp invalid', 'It should be 5 numbers'
                );
            }

            this.setState({loading:true});
            let data=this.props.navigation.getParam('data');
            let response=await userService.addCustomer({...data,otp:otp});
            if(!response.success){
                throw Error(
                    response.message
                )
            }
            response=await userService.customerLogin(
                {
                    email:data.phone,
                    password:data.password,
                }
            );
            if(!response.success){
                throw Error(
                    response.message
                )
            }
            this.setState(
                {
                    loading:false,
                },()=>{
                    Alert.alert(`Congratulations ${data.fullname}`,'Your account has been registered successfully,please login to continue.')
                    this.props.navigation.navigate(
                        'Login'
                    )
                }
            );
        } catch (err) {
            this.setState(
                {
                    loading:false,
                },()=>Alert.alert('Account failed',err.message),
            );
        }
    };

    render() {
        return (
            <AuthConsumer>
                {({updateState})=>{
                    this.updateState=updateState;

                    return (
                        <View style={styles.screen}>
                            {/* Header of the Screen */}
                            <Header pressTo={()=>this.props.navigation.goBack()} />
                            <ScrollView
                                style={styles.scroll}
                                keyboardShouldPersistTaps="always"
                                keyboardDismissMode="on-drag">
                                {/* Screen Title */}
                                <View style={styles.HeaderTextMain}>
                                    <Text style={styles.HeaderText}>
                                        Verify
                                    </Text>
                                    {/* <Text style={styles.HeaderText1}>Password</Text> */}
                                </View>
                                {/* Screen Main Content */}
                                <View style={styles.Body}>
                                    <Text style={styles.infoText}>
                                        Verification code has been sent to your
                                        phone via sms. Please check your mobile
                                        phone.
                                    </Text>

                                    <TextInput
                                        maxLength={5}
                                        style={styles.textfield}
                                        onChangeText={v=>
                                            this.setState({
                                                otp:v.trim(),
                                            })
                                        }
                                        keyboardType="number-pad"
                                    />

                                    <View style={styles.resendContainer}>
                                        <TouchableOpacity
                                            onPress={()=>this.sendOtp(true)}>
                                            <Text style={styles.resendText}>
                                                RESEND
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.btn}>
                                        <LoginPrimaryButton
                                            loading={this.state.loading}
                                            title="Confirm"
                                            pressTo={this.confirmOTp.bind(this)}
                                        />
                                    </View>
                                </View>
                            </ScrollView>
                        </View>
                    );
                }}
            </AuthConsumer>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    HeaderTextMain: {
        color: Colors.secondary,
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingVertical: 30,
        paddingBottom: 20,
    },
    HeaderText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.secondary,
        textAlign: 'left',
        opacity: 0.8,
        marginBottom: 50,
        paddingRight: 20,
    },
    HeaderText1: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.secondary,
        textAlign: 'left',
        opacity: 0.8,
        marginBottom: 0,
        paddingRight: 20,
        // bottom: 20,
    },

    Body: {
        // flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        paddingHorizontal: 30,
    },
    textfield: {
        borderBottomColor: 'rgba(0,0,0,0.3)',
        borderBottomWidth: 1,
        fontSize: 16,
        letterSpacing: 4,
        alignItems: 'center',
        fontFamily: 'Roboto-Medium',
        color: Colors.secondary,
        textAlign: 'center',
        width: '80%',
        paddingVertical:10
    },
    infoText: {
        fontFamily: 'Roboto-Medium',
        width: '80%',
        paddingVertical: 20,
        textAlign: 'center',
        opacity: 0.5,
        // paddingHorizontal: 10,
    },
    resendContainer: {
        width: '100%',
        alignItems: 'flex-end',
        paddingHorizontal: 30,
        paddingVertical: 15,
    },
    resendText: {
        fontFamily: 'Roboto-Bold',
        width: '100%',
        paddingBottom: 20,
        textAlign: 'right',
        opacity: 0.7,
        color: Colors.primary,
    },
    btn: {
        width: '100%',
        alignItems: 'center',
    },
});

export default WithOTP(VerifyCustomerScreen);
