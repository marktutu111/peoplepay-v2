import React, {Component} from 'react';
import {
    View,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    TextInput,
    Alert,
} from 'react-native';
import Colors from '../../constants/colors';
import Header from '../../components/Header';
import HeaderText from '../../components/HeaderText';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import service from "../../services/transactions.service";
import otpService from "../../services/otp.service";

export default class VerifyPaymentScreen extends Component {

    isLoaded=false;

    state = {
        otp:''
    };

    componentDidMount=()=>{
        this.isLoaded=true;
        this.sendOtp();
    }


    send=async()=>{
        try {
            if (this.state.otp === '') return;
            const transaction=this.props.navigation.getParam('transaction');
            this.setState({loading: true});
            const d = {...transaction, otp: this.state.otp};
            const response = await service.buycard(d);
            if (!response.success) {
                throw Error(response.message);
            }
            return this.isLoaded ? this.setState(
                {
                    loading: false,
                },
                ()=>{
                    Alert.alert(
                        'Transaction received', 
                        response.message
                    );
                    this.props.navigation.navigate('GiftCards');
                },
            ):null;
        } catch (err) {
            this.isLoaded && this.setState(
                {
                    loading: false
                },()=>{
                    Alert.alert('Oops!', err.message);
                }
            )
        }
    };


    sendOtp=async(resend=false)=>{
        try {
            let number='';
            const {
                payment_account_type,
                payment_account_number,
            } = this.props.navigation.getParam('transaction');
            switch (payment_account_type) {
                case 'momo':
                    number = payment_account_number;
                    break;
                default:
                    number = this.user.phone;
                    break;
            }
            if(resend)this.setState({loading:true});
            const response = await otpService.sendOTP(number);
            if (!response.success) {
                throw this.props.navigation.goBack();
            }
            if(resend){
                Alert.alert(
                    'OTP STATUS',
                    'your OTP has being sent to your phone'
                );
                this.setState(
                    {
                        loading:false
                    }
                )
            }
        } catch (err) {
            Alert.alert('OTP Not Sent', err.message);
        }
    };



    render() {
        return (
            <View style={styles.screen}>
                {/* Header of the Screen */}
                <Header pressTo={() => this.props.navigation.goBack()} />
                <ScrollView
                    style={styles.scroll}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag">
                    {/* Screen Title */}
                    <HeaderText title1="Verify" />
                    {/* Screen Main Content */}
                    <View style={styles.Body}>
                        <Text style={styles.infoText}>
                            Verification code has been sent to your phone via
                            sms. Please check your mobile phone.
                        </Text>

                        <TextInput 
                            style={styles.textfield} 
                            onChangeText={v=>this.state.otp=v.trim()}
                        />

                        <View style={styles.resendContainer}>
                            <TouchableOpacity onPress={()=>this.sendOtp(true)}>
                                <Text style={styles.resendText}>RESEND</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.btn}>
                            <LoginPrimaryButton
                                loading={this.state.loading}
                                title="Continue"
                                pressTo={this.send.bind(this)}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },

    Body: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 30,
    },
    textfield: {
        borderBottomColor: Colors.tetiary,
        borderBottomWidth: 1,
        // width: '100%',
        // marginHorizontal: 30,
        alignItems: 'center',
        fontFamily: 'Roboto-Regular',
        color: Colors.tetiary,
        textAlign: 'center',
        width: '80%',
        letterSpacing: 10,
    },
    infoText: {
        fontFamily: 'Roboto-Medium',
        width: '80%',
        paddingVertical: 20,
        textAlign: 'center',
        opacity: 0.5,
        // paddingHorizontal: 10,
    },
    resendContainer: {
        width: '100%',
        alignItems: 'flex-end',
        paddingHorizontal: 30,
        paddingVertical: 15,
    },
    resendText: {
        fontFamily: 'Roboto-Bold',
        width: '100%',
        paddingBottom: 20,
        textAlign: 'right',
        opacity: 0.7,
        color: Colors.secondary,
    },
    btn: {
        paddingHorizontal: 30,
        width: '100%',
    },
});
