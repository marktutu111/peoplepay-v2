/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    FlatList,
    ActivityIndicator,
} from 'react-native';
import Colors from '../../constants/colors';
import GiftCardTile from '../../components/GiftCardTiles';
import Header from '../../components/Header';
import HeaderTxt1 from '../../components/HeaderTxt1';
import service from '../../services/evoucher.service';
import AsyncStorage from '@react-native-community/async-storage';
import { TransactionContext } from "../../states/transactions.state";


class CardCategoryScreen extends Component {

    static contextType=TransactionContext;
    isLoaded=false;
    state={
        loading:false,
        cards:[],
    };


    async loadCache(){
        try {
          const cache=await AsyncStorage.getItem('ppay-cards');
          if(typeof cache==='string'){
            const data=JSON.parse(cache);
            this.isLoaded && this.setState(
              {
                cards:data
              }
            );
          }
        } catch (err) {}
      }


    getCards=async()=>{
        try {
            this.setState({loading: true, cards: []});
            const response=await service.getAllCategories();
            if(!response.success){
                throw Error(
                    'Oops'
                )
            }
            await AsyncStorage.setItem(
                'ppay-cards',
                JSON.stringify(response['data'])
            )
            return this.isLoaded && this.setState(
                {
                    loading:false
                },this.loadCache
            );
        } catch (err) {
            if(this.isLoaded){
                this.setState({loading: false});
            }
        }
    };

    componentDidMount() {
        this.isLoaded=true;
        this.loadCache();
        this.getCards();
    }


    componentWillUnmount(){
        this.isLoaded=false;
    }

    render() {

        const {updateState}=this.context;

        const renderItem=({item})=>{
            let cat=categories.find(
                (cat)=>cat.navOptionName === item.CatName,
            );
            return (
                <GiftCardTile
                    image={require('../../assets/images/giftcard.png')}
                    iconname={cat?.navOptionThumb || 'gift'}
                    icontype={cat?.navOptionThumbType || 'font-awesome-5'}
                    title={item?.CatName}
                    bg={cat?.navOptionColor || '#ffbbbd'}
                    pressTo={()=>{
                        updateState(
                            {
                                returnPage:'GiftCards',
                                transaction:{
                                    transaction_type:'ECARDS'
                                }
                            },()=>{
                                this.props.navigation.navigate('Product', {
                                    data:item,
                                    iconname:cat?.navOptionThumb || 'gift',
                                    icontype:cat?.navOptionThumbType || 'font-awesome-5',
                                    bg:cat?.navOptionColor || '#ffcccb',
                                })
                            }
                        )
                    }}
                />
            );
        };

        const categories=[
            {
                navOptionThumb: 'netflix',
                navOptionName: 'NetFlix',
                navOptionThumbType: 'material-community',
                navOptionColor: '#D91122',
            },
            {
                navOptionThumb: 'apple',
                navOptionName: 'Itunes Card',
                navOptionThumbType: 'material-community',
                navOptionColor: '#151515',
            },
            {
                navOptionThumb: 'amazon',
                navOptionName: 'Amazon',
                navOptionThumbType: 'material-community',
                navOptionColor: '#F28705',
            },
            {
                navOptionThumb: 'sony-playstation',
                navOptionName: 'Playstation',
                navOptionThumbType: 'material-community',
                navOptionColor: '#177FBF',
            },
            {
                navOptionThumb: 'microsoft-xbox',
                navOptionName: 'XBOX Card',
                navOptionThumbType: 'material-community',
                navOptionColor: '#1BA64B',
            },
            {
                navOptionThumb: 'steam',
                navOptionName: 'Steam',
                navOptionThumbType: 'material-community',
                navOptionColor: '#373D4A',
            },
            {
                navOptionThumb: 'ebay',
                navOptionName: 'Ebay',
                navOptionThumbType: 'font-awesome-5',
                navOptionColor: '#2F65B1',
            },
            {
                navOptionThumb: 'google-play',
                navOptionName: 'Google Play',
                navOptionThumbType: 'material-community',
                navOptionColor: '#1E869A',
            },
            {
                navOptionThumb: 'spotify',
                navOptionName: 'Spotify',
                navOptionThumbType: 'font-awesome-5',
                navOptionColor: '#81b71a',
            },
            {
                navOptionThumb: 'google',
                navOptionName: 'Google',
                navOptionThumbType: 'font-awesome-5',
                navOptionColor: '#4285F4',
            },
            {
                navOptionThumb: 'gamepad',
                navOptionName: 'Nintendo',
                navOptionThumbType: 'font-awesome-5',
                navOptionColor: '#fe0016',
            },
            {
                navOptionThumb: 'gamepad',
                navOptionName: 'Razer Gold',
                navOptionThumbType: 'font-awesome-5',
                navOptionColor: '#44d62c',
            },
        ];
        return (
            <View style={styles.screen}>
                <Header
                    text="Gift Cards"
                    pressTo={() => {
                        this.props.navigation.navigate('Home');
                    }}
                />
                {/* <HeaderTxt1 txt1="Gift" txt2=" Cards" /> */}
                <FlatList
                    data={this.state.cards}
                    numColumns={1}
                    renderItem={renderItem}
                    onResponderRelease={null}
                    keyExtractor={(item, index)=>index.toString()}
                    ListEmptyComponent={() => {
                        if (this.state.loading) {
                            return (
                                <ActivityIndicator
                                    color="red"
                                    size={'large'}
                                    style={{marginTop: 20}}
                                />
                            );
                        }
                        return null;
                    }}
                    showsVerticalScrollIndicator={false}
                    style={{
                        marginTop: 10,
                    }}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    HeaderRow: {
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        alignItems: 'center',
        alignContent: 'center',
        width: '100%',
        height: 65,
        backgroundColor: Colors.primary,
        marginBottom: 15,
        paddingHorizontal: 30,
    },
    HeaderTextLogo: {
        textAlign: 'center',
        fontFamily: 'Roboto-Bold',
        fontSize: 18,
        textTransform: 'uppercase',
        color: Colors.accent,
    },
    navIcon: {
        padding: 20,
    },
    empty: {
        padding: 10,
    },
    HeaderTextMain: {
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingTop: 30,
        paddingBottom: 20,
        flexDirection: 'row',
    },
    HeaderText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.secondary,
        // textAlign: 'center',
        opacity: 0.8,
    },
    HeaderText1: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.primary,
        // textAlign: 'center',
        opacity: 0.8,
        // bottom: 20,
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
    },
    tilesContainer: {
        // flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
    },
    tiles: {
        flexDirection: 'row',
        paddingVertical: 10,
        paddingHorizontal: 30,
        justifyContent: 'space-around',
    },
    mainContainer: {
        flex: 1,
        flexWrap: 'wrap',
        flexDirection: 'row',
    },
    cardItemlabel: {
        textAlign: 'center',
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
    },
    containerStyle: {
        padding: 10,
        borderWidth: 0,
        // marginBottom:5,
        marginTop: 10,
    },
    cardrow: {
        width: '100%',
        justifyContent: 'space-evenly',
        flexDirection: 'row',
    },
    QuickService: {
        fontFamily: 'Roboto-Bold',
        opacity: 0.7,
        fontSize: 16,
        paddingHorizontal: 20,
        color: Colors.tetiary,
    },
});
export default CardCategoryScreen;
