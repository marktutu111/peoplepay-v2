import React, {Component} from 'react';
import {View, Text, StyleSheet, Alert, TextInput} from 'react-native';
import Colors from '../../constants/colors';
import Header from '../../components/Header';
import HeaderText from '../../components/HeaderText';
import {ScrollView} from 'react-native-gesture-handler';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import {AuthConsumer} from '../../states/auth.state';
import TxtInput from '../../components/TxtInput';
import {CustomExample} from '../../components/customPicker';
import service from '../../services/wallet.service';
import otpservice from '../../services/otp.service';

export default class CardPaymentDetailScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            option: '',
            selected_account: '',
            payment_account_name: '',
            payment_account_number: '',
            payment_account_numberValid: true,
            payment_account_issuer: '',
            payment_account_type: 'momo',
            wallets: [],
        };
    }

    getWallets=async()=>{
        try {
            const {_id} = this.user;
            if (!_id) {
                return;
            }
            this.setState({loading: true});
            const response = await service.getWallets({
                customerId: _id,
                password: this.password,
            });
            if (!response.success) {
                throw Error(response.message);
            }
            this.setState({
                loading: false,
                wallets: response.data,
            });
        } catch (err) {
            this.setState({loading: false});
        }
    };
    validate = (text, type) => {
        const {payment_account_type} = this.state;

        if (payment_account_type === 'momo' && type === 'accountnumber') {
            if (!/^[0]?\d{9}$/.test(text)) {
                this.setState({
                    payment_account_numberValid: false,
                });
            } else {
                this.setState({
                    payment_account_numberValid: true,
                });
            }
        }
        if (payment_account_type === 'bank' && type === 'accountnumber') {
            if (!/^\d{10,17}$/.test(text)) {
                this.setState({
                    payment_account_numberValid: false,
                });
            } else {
                this.setState({
                    payment_account_numberValid: true,
                });
            }
        }
        if (type === 'accountname') {
            if (text === '' || !/^[a-zA-Z]+/.test(text)) {
                this.setState({
                    payment_account_nameValid: false,
                });
            } else {
                this.setState({
                    payment_account_nameValid: true,
                });
            }
        }
        if (type === 'issuer') {
            if (text === '') {
                this.setState({
                    payment_account_issuerValid: false,
                });
            } else {
                this.setState({
                    recipent_account_issuerValid: true,
                });
            }
        }
    };

    render() {
        return (
            <View style={styles.screen}>
                <AuthConsumer>
                    {({state}) => {
                        this.user = state.user;
                        this.password = state.password;
                    }}
                </AuthConsumer>
                {/* Header of the Screen */}
                <Header pressTo={() => this.props.navigation.goBack()} />
                <ScrollView
                    style={styles.scroll}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag">
                    {/* Screen Title */}
                    <HeaderText title1="Add your" title2="Money Source" />
                    {/* Screen Main Content */}
                    <View style={styles.Body}>
                        <Text style={styles.heading}>
                            Choose Source of Funds
                        </Text>
                        <View style={styles.dropdownContainer}>
                            <CustomExample
                                placeholder="Select Payment Account"
                                selectedValue={this.state.option}
                                onValueChange={({value}) =>
                                    this.setState(
                                        {
                                            option: value,
                                        },
                                        () => {
                                            if (this.state.option==='0') {
                                                this.state.payment_account_name = this.user.fullname;
                                            }
                                            if (this.state.option === '1') {
                                                this.setState({
                                                    selected_account: '',
                                                    payment_account_name: '',
                                                });
                                                this.getWallets();
                                            }
                                        },
                                    )
                                }
                                items={[
                                    {
                                        name: 'Select from my Accounts',
                                        value: '0',
                                    },
                                    {
                                        name: 'Enter Mobile Money Account Details',
                                        value: '1',
                                    }
                                ]}
                            />
                        </View>

                        <Text style={styles.heading} />

                        {this.state.option === '0' && (
                            <View style={styles.TxtInputContainer}>
                                {/* <Text style={styles.heading}>
                                Choose account type
                            </Text> */}

                                <CustomExample
                                    placeholder="Select Your Payment Type"
                                    selectedValue={
                                        this.state.payment_account_type
                                    }
                                    onValueChange={({value}) => {
                                        this.setState({
                                            payment_account_type: value,
                                            payment_account_issuer: '',
                                        });
                                    }}
                                    items={[
                                        {
                                            name: 'Mobile Money',
                                            value: 'momo',
                                        },
                                        {
                                            name: 'Bank Account',
                                            value: 'bank',
                                        },
                                    ]}
                                />

                                <Text style={styles.heading}>
                                    Choose Account
                                </Text>

                                <CustomExample
                                    placeholder="Select Account Issuer"
                                    selectedValue={
                                        this.state.payment_account_issuer
                                    }
                                    onValueChange={({value}) =>
                                        this.setState({
                                            payment_account_issuer: value,
                                        })
                                    }
                                    type={this.state.payment_account_type}
                                />
                                <Text style={styles.heading}>
                                    Enter Account Number
                                </Text>
                                <TxtInput
                                    style={
                                        !this.state.payment_account_numberValid
                                            ? {
                                                  borderColor: 'red',
                                                  borderWidth: 3,
                                              }
                                            : null
                                    }
                                    placeholder={
                                        this.state.payment_account_type ===
                                        'momo'
                                            ? 'Phone Number'
                                            : 'Account Number'
                                    }
                                    IconName="hash"
                                    IconType="feather"
                                    keyboardType="number-pad"
                                    onChangeText={v => {
                                        this.setState({
                                            payment_account_number: v.trim(),
                                        });
                                        this.validate(v, 'accountnumber');
                                    }}
                                />
                            </View>
                        )}

                        {this.state.option === '1' && (
                            <View style={styles.dropdownContainer}>
                                <CustomExample
                                    placeholder="Choose Account"
                                    selectedValue={this.state.selected_account}
                                    onValueChange={({value}) => {
                                        try {
                                            const wallet = this.state.wallets.find(
                                                wallet => wallet._id === value,
                                            );
                                            this.setState({
                                                payment_account_type:
                                                    wallet.account_type,
                                                payment_account_name:
                                                    wallet.account_name,
                                                payment_account_issuer:
                                                    wallet.account_issuer,
                                                payment_account_number:
                                                    wallet.account_number,
                                                selected_account: value,
                                            });
                                        } catch (err) {}
                                    }}
                                    items={this.state.wallets.map(
                                        ({
                                            account_name,
                                            _id,
                                            account_number,
                                        }) => {
                                            return {
                                                name: account_name,
                                                value: _id,
                                                account_number: account_number,
                                            };
                                        },
                                    )}
                                />
                            </View>
                        )}

                        <View style={styles.buttonContainer}>
                            <AuthConsumer>
                                {({state}) => {
                                    return (
                                        <LoginPrimaryButton
                                            loading={this.state.loading}
                                            title="Send"
                                            pressTo={()=>{
                                                let data = this.props.navigation.getParam(
                                                    'data',
                                                );

                                                const card=data.card;
                                                const {
                                                    payment_account_issuer,
                                                    payment_account_name,
                                                    payment_account_number,
                                                    payment_account_type,
                                                }=this.state;

                                                let transaction = {
                                                    amount:card.SellingPrice,
                                                    customerId:data.customerId,
                                                    payment_account_name: payment_account_name,
                                                    payment_account_number: payment_account_number,
                                                    payment_account_issuer: payment_account_issuer,
                                                    payment_account_type: payment_account_type,
                                                    gift_card:card,
                                                    description:card.ProductName
                                                };
                                                Alert.alert(
                                                    `Hello ${state.user.fullname}`,
                                                    `You have requested to pay an amount of GHS${transaction.amount} for ${card.ProductName}. Do you want to proceed?`,
                                                    [
                                                        {
                                                            text: 'YES',
                                                            onPress: async () => {
                                                                let invalid = null;
                                                                Object.keys(
                                                                    transaction,
                                                                ).forEach(
                                                                    key => {
                                                                        if (
                                                                            !key ||
                                                                            !transaction[
                                                                                key
                                                                            ] ||
                                                                            transaction[
                                                                                key
                                                                            ] ===
                                                                                ''
                                                                        ) {
                                                                            invalid = key;
                                                                        }
                                                                    },
                                                                );

                                                                if (
                                                                    typeof invalid ===
                                                                    'string'
                                                                ) {
                                                                    return alert(
                                                                        'Sorry Please provide value for ' +
                                                                            invalid,
                                                                    );
                                                                }

                                                                this.props.navigation.navigate(
                                                                    'VerifyPaymentCards',
                                                                    {
                                                                        transaction: transaction,
                                                                    }
                                                                );
                                                            },
                                                        },
                                                        {
                                                            text: 'NO',
                                                            onPress: null,
                                                            style: 'cancel',
                                                        },
                                                    ],
                                                    {cancelable: false},
                                                    //clicking out side of alert will not cancel
                                                );
                                            }}
                                        />
                                    );
                                }}
                            </AuthConsumer>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
    },
    heading: {
        marginTop: 10,
        textAlign: 'left',
        paddingBottom: 10,
        marginBottom: 10,
        borderBottomWidth: 0.8,
        borderColor: Colors.primary,
        marginHorizontal: 30,
        fontFamily: 'Roboto-Bold',
        color: Colors.tetiary,
        opacity: 0.7,
    },
    dropdownContainer: {
        paddingVertical: 10,
        marginHorizontal: 30,
    },

    TxtInputContainer: {
        paddingHorizontal: 30,
    },
    buttonContainer: {
        // alignItems: 'center',
        paddingVertical: 20,
        paddingHorizontal: 30,
    },
});