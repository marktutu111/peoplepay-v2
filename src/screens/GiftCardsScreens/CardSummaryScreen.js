import React, {Component} from 'react';
import {StyleSheet, Text, View, ScrollView} from 'react-native';
import {PricingCard} from 'react-native-elements';
import Header from '../../components/Header';
import HeaderText from '../../components/HeaderText';
import HeaderTxt1 from '../../components/HeaderTxt1';
import { AuthConsumer } from '../../states/auth.state';
import { TransactionContext } from '../../states/transactions.state';


class CardSummaryScreen extends Component {

    static contextType=TransactionContext;
    
    state={
        details: {},
    };

    render() {
        const {pushTransaction}=this.context;
        const {navigation} = this.props;
        return (
            <View style={styles.screen}>
                <Header pressTo={()=>navigation.goBack()} />
                <ScrollView
                    style={styles.scroll}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag">
                    <HeaderText title1="Confirm" title2="Card Details" />
                    <View style={styles.body}>
                        <AuthConsumer>
                            {
                                ({state})=>{
                                    const user=state.user;
                                    return (
                                    <View style={styles.recipientContainer}>
                                        <PricingCard
                                            color={navigation.getParam('color')}
                                            title= {navigation.getParam('card')['ProductName']}
                                            price={`GHS ${navigation.getParam('card')['SellingPrice']}`}
                                            info={[
                                                'Quantity: 1'
                                            ]}
                                            button={{
                                                title: 'BUY',
                                                icon: 'flight-takeoff',
                                            }}
                                            onButtonPress={()=>{
                                                const data=navigation.getParam('card');
                                                pushTransaction(
                                                    {
                                                        gift_card:data,
                                                        customerId:user._id,
                                                        amount:data.SellingPrice,
                                                        description:data.ProductName
                                                    },()=>{
                                                        navigation.navigate(
                                                            'ChooseSource',
                                                            {
                                                                page:'SenderDetailsMoMo',
                                                                data:{ page: 'ConfirmTransaction' }
                                                            }
                                                        )
                                                    }
                                                )
                                            }}
                                        />
                                    </View>
                                    )
                                }
                            }
                        </AuthConsumer>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
export default CardSummaryScreen;

const styles = StyleSheet.create({
    body:{
        justifyContent:'center',
        alignItems:'center'
    },
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    recipientContainer: {
        width: '90%',
        padding: 30,
    },
});
