/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, Text, StyleSheet, Image, FlatList,ActivityIndicator} from 'react-native';
import ProductsCard from '../../components/ProductsCard';
import Header from '../../components/Header';
import HeaderTxt1 from '../../components/HeaderTxt1';
import service from '../../services/evoucher.service';

class CardProductsScreen extends Component {

    isLoaded=false;

    state={
        products:[],
    };


    componentDidMount=()=>{
        this.isLoaded=true;
        const data=this.props.navigation.getParam('data');
        this.getproducts(data.Id);
    };


    async getproducts(id) {
        try {
            this.setState({loading:true});
            const response = await service.getCatergoryProducts(id);
            if (response.success) {
                return this.isLoaded ? this.setState({
                    products: response.data,
                    loading:false
                }):null;
            }
            throw Error();
        } catch (err) {
            if(this.isLoaded){
                this.setState({loading:false});
            }
        }
    }

    render() {
        const {navigation} = this.props;
        const renderItem=({item})=>{
            return (
                <ProductsCard
                    title={item.ProductName}
                    iconname={navigation.getParam('iconname')}
                    icontype={navigation.getParam('icontype')}
                    price={item.SellingPrice}
                    bg={navigation.getParam('bg')}
                    description={item.productdescription}
                    pressTo={() =>
                        navigation.navigate('CardSummary', {
                            card: item,
                            color: navigation.getParam('bg'),
                        })
                    }
                />
            );
        };
        return (
            <View style={styles.screen}>
                {/* Header */}
                <Header text="Choose your card" pressTo={()=>navigation.goBack()} />
                <View style={styles.scroll}>
                    {/* Screen Title */}
                    {/* <HeaderTxt1 txt2="Cards"/> */}
                    <View style={styles.Body}>
                        <FlatList
                            data={this.state.products}
                            extraData={this.state}
                            renderItem={renderItem}
                            keyExtractor={(item,index)=>index.toString()}
                            ListEmptyComponent={() => {
                                if (this.state.loading) {
                                    return (
                                        <ActivityIndicator
                                            color="red"
                                            size={'large'}
                                            style={{marginTop: 20}}
                                        />
                                    );
                                }
                                return null;
                            }}
                            style={{
                                width: '100%',
                                alignSelf: 'center',
                                paddingHorizontal: 10,
                            }}
                            showsVerticalScrollIndicator={false}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
    },
    Image: {
        height: 80,
        width: 80,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
export default CardProductsScreen;
