import React from 'react';
import {View, Image, StyleSheet, Text, StatusBar} from 'react-native';
import colors from '../../constants/colors';

class SplashScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {loading: false};
    }

    performTimeConsumingTask = async () => {
        return new Promise(resolve =>
            setTimeout(() => {
                resolve('result');
            }, 4000),
        );
    };
    async componentDidMount() {
        // Preload data from an external API
        // Preload data using AsyncStorage
        const data = await this.performTimeConsumingTask();
        if (data !== null) {
            this.props.navigation.navigate('Login');
        }
    }
    render() {
        return (
            <View style={styles.screen}>
                <StatusBar
                    backgroundColor={colors.secondary}
                    barStyle={'light-content'}
                />
                <View style={styles.gradient}>
                    <View style={styles.ImageContainer}>
                        <View style={styles.ImageContainer2}>
                            <View style={styles.ImageContainer1}>
                                <Image
                                    style={styles.Logo}
                                    source={require('../../assets/images/pp.png')}
                                    resizeMode="contain"
                                />
                            </View>
                        </View>
                    </View>
                </View>
                {/* <Text
                    style={{
                        color: '#fff',
                        textAlign: 'center',
                        fontSize: 12,
                        fontFamily: 'Roboto-Regular',
                        marginBottom: 70,
                    }}>
                    Powered By Bsystems
                </Text> */}
                {/* <ActivityIndicator
                    color={'#fff'}
                    size={'large'}
                    style={{marginBottom: 70}}
                /> */}
            </View>
        );
    }
}
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: colors.secondary,
    },
    ImageContainer: {
        alignSelf: 'center',
        marginTop: '35%',
        borderRadius: 150,
        height: 300,
        width: 300,
        backgroundColor: 'rgba(255,255,255,0.4)',
        padding: 20,
    },
    ImageContainer1: {
        alignSelf: 'center',
        borderRadius: 110,
        height: 220,
        width: 220,
        backgroundColor: '#ffffff',
        // padding: 10,
        opacity: 1,
        overflow: 'hidden',
    },
    ImageContainer2: {
        alignSelf: 'center',
        borderRadius: 130,
        height: 260,
        width: 260,
        // backgroundColor: colors.primary,
        backgroundColor: 'rgba(255,255,255,0.8)',
        padding: 20,
    },
    Logo: {
        width: 230,
        height: 230,
        alignSelf: 'center',
    },
    container: {
        flex: 1,
        borderRadius: 50,
    },
    gradient: {
        flex: 1,
    },
});
export default SplashScreen;
