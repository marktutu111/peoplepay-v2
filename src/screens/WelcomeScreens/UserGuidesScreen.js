import React from 'react';
import {StyleSheet, View, Image} from 'react-native';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import SecondaryButton from '../../components/SecondaryButton';
import AppIntro from '../../components/Appintro';



const UserGuidesScreen = props => {
    return (
        <View style={styles.screen}>
            <View
                // colors={['#cd8031', '#b2202b']}
                style={styles.gradient}>
                <AppIntro
                    Signup={() => {
                        props.navigation.navigate('SignUp');
                    }}
                    login={() => props.navigation.navigate('Login')}
                />
                {/* <View style={{flex:1, maxHeight:300, justifyContent: 'center',paddingHorizontal:30}}>
                    <LoginPrimaryButton
                        title="REGISTER"
                        pressTo={() => props.navigation.navigate('SignUp')}
                    />
                    <SecondaryButton
                        title="SIGN IN"
                        pressTo={() => props.navigation.navigate('Login')}
                    />
                </View> */}
            </View>
        </View>
    );
};
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        // paddingBottom: 20,
        // paddingHorizontal: 30,
    },
    appIntro: {
        alignSelf: 'center',
        // marginBottom: 120,
        // backgroundColor: '#ffffff',
    },
    gradient: {
        flex: 1,
        paddingTop: '40%',
        backgroundColor:'blue'
    },
    Logo: {
        width: 100,
        height: 100,
    },
    container: {
        flex: 1,
    },
    Button: {
        flex: 1,
        justifyContent: 'flex-end',
        paddingBottom: 36,
        backgroundColor: '#ffffff',
        paddingHorizontal: 60,
    },
});

export default UserGuidesScreen;
