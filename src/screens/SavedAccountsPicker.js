import * as React from 'react';
import {Text, StyleSheet, View, TouchableOpacity,Image,Dimensions,ScrollView} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Platform } from 'react-native';
import Header from '../components/Header';
import { TransactionContext } from "../states/transactions.state";
import _WService from "../services/wallet.service";
import { AuthConsumer } from '../states/auth.state';
import { ActivityIndicator } from 'react-native';
import colors from '../constants/colors';


const {width,height}=Dimensions.get('screen');

export class SavedAccountsPicker extends React.Component {

  static contextType=TransactionContext;
  isLoaded=false;


  state={
    loading:false,
    accounts:[],
    type:'',
    page:''
  };


  next(data){
    try {
      const page=this.props.navigation.getParam('page');
      const _data=this.props.navigation.getParam('data');
      const key=this.props.navigation.getParam('key');
      if(typeof page!=='string')return;
      const {pushTransaction}=this.context;
      pushTransaction(
        {
          [key[0]]:data.account_issuer,
          [key[1]]:data.account_issuer_name,
          [key[2]]:data.account_number
        },()=>this.props.navigation.navigate('ConfirmTransaction',_data || {})
      )
    } catch (err) {}
  }



  componentDidMount=()=>{
    try {
      this.isLoaded=true;
      const type=this.props.navigation.getParam('type');
      this.setState(
        {
          accounts:[],
          type:type
        },()=>{ this.getWallets() }
      );
    } catch (err) {}
  };


  componentWillUnmount(){
    this.isLoaded=false;
  }


  getWallets=async()=>{
    try {
        const {_id}=this.user;
        if (!_id) {
            return;
        }
        this.isLoaded && this.setState(
            {
                loading: true
            }
        );
        const response=await _WService.getWallets(
            {
                customerId:this.user._id,
                password:this.password,
            }
        );
        if (!response.success) {
            throw Error(
                response.message
            );
        }
        this.isLoaded && this.setState(
            {
                loading:false,
                accounts:response.data,
            }
        );
    } catch (err) {
        this.isLoaded && this.setState(
            {
                loading:false
            }
        );
    }
};



  render() {
    return (
      <View style={{height:height,backgroundColor:'#fff'}}>
          <AuthConsumer>
              {
                  ({state})=>{
                      this.user=state.user;
                      this.password=state.password;
                  }
              }
          </AuthConsumer>
        <ScrollView contentContainerStyle={{marginTop:5}}>
            <Header title="Choose Account" pressTo={()=>this.props.navigation.goBack()} />
            { this.state.loading && <ActivityIndicator size="large" color={colors.secondary}/> }
              {this.state.accounts.map((account,i)=>{
                return (
                  <TouchableOpacity
                    key={i.toString()}
                    onPress={()=>this.next(account)}
                    style={{
                      flexDirection:'row',
                      alignItems:'center',
                      marginHorizontal:20,
                      marginVertical:15
                    }}>
                        <Image 
                            style={{width:50,height:50}} 
                            source={{ uri:account.account_issuer_image }}
                        />
                        <View>
                            <Text 
                                style={{marginLeft:15,fontSize:20,fontWeight:'200',width:width/1.6}}>
                                    { account.account_name }
                            </Text>
                            <Text 
                                style={{marginLeft:15,fontSize:15,fontWeight:'200',width:width/1.6,marginVertical:5}}>
                                    { account.account_number }
                            </Text>
                            <Text 
                                style={{marginLeft:15,fontSize:15,fontWeight:'200',width:width/1.6,marginVertical:5}}>
                                    { account.account_issuer_name }
                            </Text>
                        </View>
                  </TouchableOpacity>
                )
              })}
        </ScrollView>
      </View>
    );
  }



}

const styles = StyleSheet.create({
  container: {
    borderColor: 'grey',
    borderWidth: 1,
    ...Platform.select(
      {
        ios:{
          paddingVertical:7,
          borderRadius: 20
        },
        android:{
          paddingVertical:1,
          borderRadius: 15,
        }
      }
    )
  },
  innerContainer: {
    flexDirection: 'row',
    alignItems: 'stretch',
    width: '100%',
    justifyContent: 'space-between',
  },
  text: {
    fontSize: 18,
    padding: 10,
    color: 'grey',
  },
  headerFooterContainer: {
    padding: 10,
    alignItems: 'center',
  },
  clearButton: {
    backgroundColor: 'white',
    borderRadius: 5,
    marginRight: 10,
    padding: 10,
    paddingLeft: 20,
  },
  optionContainer: {
    padding: 10,
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
    borderRadius: 0,
    fontFamily: 'Roboto-Regular',
  },
  optionInnerContainer: {
    flex: 1,
    flexDirection: 'row',
    fontFamily: 'Roboto-Bold',
  },
  box: {
    width: 20,
    height: 20,
    marginRight: 10,
  },
});
