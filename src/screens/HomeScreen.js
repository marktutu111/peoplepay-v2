/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    Platform,
    FlatList,
    TouchableOpacity,
    Text,
    ScrollView,
    Dimensions,
    Image,
    Alert,
    Linking
} from 'react-native';

import Fontawesome from "react-native-vector-icons/FontAwesome5";
import TransactionView from '../components/TransactionView';
import OtherButton from '../components/OtherButton';
import {DrawerActions} from 'react-navigation-drawer';
import HomeTile from '../components/HomeTile';
import service from "../services/transactions.service";
import { AuthConsumer } from '../states/auth.state';
import moment from "moment";
import colors from '../constants/colors';
import Header from '../components/Header';
import FormatCurreny from "../constants/currencyFormatter";
import { TransactionContext } from '../states/transactions.state';
import _JService from "../services/jwt.service";
import SwiperFlatList from 'react-native-swiper-flatlist';
import _AService from "../services/advert.service";
import { checkVersion } from "react-native-check-version";
import UpdateBeneficiary from './BeneficiaryScreens/UpdateBeneficiaryScreen';

const {width,height}=Dimensions.get('screen');




class HomeScreen extends Component {

    static contextType=TransactionContext;
    subscriptionValidateToken$=null;
    $subscription=null;
    viewLoaded=false;

    state={
        adverts:[],
        toggle:true,
        summary:{},
        balance:0,
    }

    componentDidMount(){
        this.checkVersion()
        this.viewLoaded=true;
        this.$subscription=this.props.navigation.addListener(
            'willFocus',()=>{
                this.getSummary();
                this.loadAdverts();
            }
        );

    }
   
   checkVersion = async()=>{
       try{
        let version=await checkVersion();
        if(version.needsUpdate){
            const {url}=version;
            return Alert.alert(
                "Please update the app",
                "Update to continue using the app",
                [
                  { text: "update", onPress: () => Linking.openURL(url).catch(e=>null)}
                ]
              );        
        }
       }catch(error){}
   }

    loadAdverts=async()=>{
        try {
            const response=await _AService();
            if(!response.success){
                throw Error(
                    response.message
                )
            }
            this.setState(
                {
                    adverts:response.data
                }
            )
        } catch (err) {}
    }

    componentWillUnmount(){
        try {
            this.$subscription.remove();
            this.viewLoaded=false;
        } catch (err) {}
    }



    getSummary(){
        service.getCustomerSummary(this.user._id).then(res=>{
            if(res.success){
                try {
                    this.viewLoaded && this.setState(
                        {
                            summary:res.data[0],
                            balance:res.data?.balance || 0
                        }
                    )
                } catch (err) {}
            }
        }).catch(err=>null);
    }

    render() {

        const {updateState}=this.context;

       const generateGreetings=()=>{
            var currentHour=moment().format("HH");
            if (currentHour >= 3 && currentHour < 12){
                return "Good Morning";
            } else if (currentHour >= 12 && currentHour < 15){
                return "Good Afternoon";
            }   else if (currentHour >= 15 && currentHour < 20){
                return "Good Evening";
            } else if (currentHour >= 20 && currentHour < 3){
                return "Good Night";
            } else {
                return "Hello"
            }
        }


        global.currentScreenIndex=0;
        const items=[
            {
                navOptionThumb: 'md-paper-plane',
                navOptionName: 'Send Money',
                screenToNavigate: 'SendMoney',
                navOptionThumbType: 'ionicon',
                // imageUri: require('../assets/menus/sendmoney.png'),
            },
            {
                navOptionThumb: 'qrcode-scan',
                navOptionName: 'GhQR Pay',
                screenToNavigate: 'QRP_Option',
                navOptionThumbType: 'material-community',
                // imageUri: require('../assets/menus/qrcode.png'),
            },
            {
                navOptionThumb:'wallet-giftcard',
                navOptionName:'Gift Cards',
                screenToNavigate:'GiftCards',
                navOptionThumbType:'material-community',
                // imageUri:require('../assets/menus/giftcard.png'),
            },
            {
                navOptionThumb: 'mobile',
                navOptionName: 'Airtime Topup',
                screenToNavigate: 'AccountRef',
                navOptionThumbType: 'font-awesome-5',
                // imageUri: require('../assets/menus/airtime.png'),
            },
            {
                navOptionThumb: 'clipboard-notes',
                navOptionName: 'Pay Bills',
                screenToNavigate: 'PayBills',
                navOptionThumbType: 'foundation',
                // imageUri:require('../assets/menus/paybills.png'),
            },
            // {
            //     navOptionThumb: 'users',
            //     navOptionName: 'Beneficiaries',
            //     screenToNavigate: 'BankBeneficiary',
            //     navOptionThumbType: 'font-awesome-5',
            //     // imageUri: require('../assets/menus/beneficiaries.png'),
            // },
            {
                navOptionThumb: 'wallet',
                navOptionName: 'Fund Account',
                screenToNavigate: 'WalletTopup',
                navOptionThumbType: 'material-community',
                // imageUri: require('../assets/menus/instant.png'),
            },
            {
                navOptionThumb: 'idcard',
                navOptionName: 'My Accounts',
                screenToNavigate: 'Wallets',
                navOptionThumbType: 'antdesign',
                // imageUri: require('../assets/menus/myaccounts.png'),
            },
            {
                navOptionThumb: 'hands',
                navOptionName: 'Referrals',
                screenToNavigate: 'Referals',
                navOptionThumbType: 'font-awesome-5',
                // imageUri:require('../assets/menus/referrals.png'),
            },
            {
                navOptionThumb: 'wifi',
                navOptionName: 'Busy Internet',
                screenToNavigate: 'BusyInternet',
                navOptionThumbType: 'font-awesome-5',
                // imageUri:require('../assets/menus/referrals.png'),
            },
        ];

        const renderItem=({item})=>{
            return (
                <HomeTile
                    imageUri={item.imageUri}
                    iconname={item.navOptionThumb}
                    icontype={item.navOptionThumbType}
                    title={item.navOptionName}
                    title2=""
                    pressTo={()=>{
                        switch (item.screenToNavigate) {
                            case 'AccountRef':
                                updateState(
                                    {
                                        returnPage:'Home',
                                        transaction:{
                                            transaction_type:'AT',
                                            description:'Airtime Topup'
                                        }
                                    },()=>{
                                        this.props.navigation.navigate(
                                            'AccountRef',
                                            { data:{
                                                "name": "Airtime",
                                                "merchantId":"1522",
                                                "account_type":"bank",
                                                "type":"ITC",
                                                "serviceCode":'TFUTLXX',
                                                "productId": "1",
                                                "_bill_type":"AIRTIME",
                                                "category":"AIRTIME",
                                                "page":"AIRTIME",
                                                "productDescription":"Airtime Topup"
                                            }}
                                        )
                                    }
                                )
                                break;
                            case 'VirtualCard':
                                Alert.alert(
                                    'Oops sorry',
                                    'Thank you for your interest in our Instant Visa Card. This feature will be available in our next update.'
                                )
                                break;
                            default:
                                this.props.navigation.navigate(item.screenToNavigate)
                                break;
                        }
                    }}
                />
            );
        };

        return (
            <View style={styles.screen}>
                <AuthConsumer>
                    {
                        ({state,logout})=>{
                            this.user=state['user'];
                            this.token=state['token'];
                            this.logout=logout;
                        }
                    }
                </AuthConsumer>
                <Header size={15} icon="ellipsis-v" text="PeoplesPay" pressTo={()=>{
                    this.props.navigation.dispatch(
                        DrawerActions.toggleDrawer(),
                    )
                }}/>
                <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{
                    width:width,
                    justifyContent:'center',
                    alignItems:'center'
                }}>
                    <View style={{
                        paddingVertical:10,
                        justifyContent:'center',
                        alignItems:'center'
                    }}>
                        <Text style={{
                                fontSize:15,
                                marginLeft:15,
                                fontWeight:'200',
                                color:colors.secondary,
                                textAlign:'center'
                            }}
                        >
                            {generateGreetings()}
                        </Text>
                        <Text style={{fontSize:20,textAlign:'center'}}>{this.user?.fullname}</Text>
                    </View>
                    <View style={styles.buttonContainerOut}>
                        <View style={styles.buttonContainer}>
                            <OtherButton
                                title="my beneficiary account"
                                pressTo={()=>this.props.navigation.navigate(
                                        'Beneficiary',
                                        {account:'bank'}
                                    )
                                }
                            />
                            <TouchableOpacity 
                                onPress={()=>this.setState({toggle:!this.state.toggle})}
                                hitSlop={{top:5,bottom:5,left:5,right:5}}
                                style={{
                                    width:30,
                                    height:30,
                                    zIndex:10000000000,
                                    justifyContent:'center',
                                    alignItems:'center',
                                    borderColor:'#000',
                                    borderWidth:1,
                                    borderRadius:100
                                }}>
                                <View>
                                    <Fontawesome 
                                        name={this.state.toggle?'eye':'eye-slash'}
                                        color={'#000'}
                                        size={15}
                                    />
                                </View>
                        </TouchableOpacity>
                            <OtherButton
                                title="my source account"
                                pressTo={() =>
                                    this.props.navigation.navigate(
                                        'ChooseWallet',
                                        {account:'momo'},
                                    )
                                }
                            />
                        </View>
                    </View>
                    <View style={styles.billboard}>
                        <View style={{
                            flexDirection:'row',
                            justifyContent:'space-between',
                            alignItems:'center',
                            paddingHorizontal:10
                        }}>
                            <View style={{
                                marginVertical:10
                            }}>
                                <Text style={{fontSize:10,marginBottom:5,fontWeight:'200'}}>MY PEOPLESPAY WALLET</Text>
                                <Text style={{fontSize:30,fontWeight:'200'}}>
                                    {this.state.toggle? 'GHSX.00':FormatCurreny(this.state.balance)}
                                </Text>
                                <Text style={{color:'#6d6d6d'}}>Current Balance</Text>
                            </View>
                            <TouchableOpacity
                                onPress={()=>this.props.navigation.navigate('History')}
                                style={{justifyContent:'flex-end',alignItems:'flex-end'}}>
                                <Text style={{fontSize:15,fontWeight:'200'}}>Payment History</Text>
                                <Fontawesome 
                                    style={{marginRight:10,marginTop:5,color:'#6d6d6d'}} 
                                    size={20} 
                                    name="long-arrow-alt-right"
                                />
                            </TouchableOpacity>
                        </View>
                        <TransactionView toggle={this.state.toggle} data={this.state.summary}/>
                    </View>
                    <View style={{
                        width:width/1.1
                    }}>
                        {this.state.adverts.length>0 && <SwiperFlatList autoplay autoplayDelay={5} autoplayLoop index={0}>
                            {this.state.adverts.map(advert=><View key={advert._id} style={{
                                justifyContent:'center',
                                alignItems:'center',
                                height:height/9,
                                width:width/1.1,
                                borderRadius:5,
                                overflow:'hidden',
                                borderColor:'#ddd',
                                borderWidth:1,
                                marginVertical:5
                            }}>
                                <Image 
                                        style={{width:'100%',height:'100%'}} 
                                        source={{uri:advert.image}}
                                    />
                            </View>)}
                        </SwiperFlatList>}
                    </View>
                    <View style={{
                        width:width,
                        height:height/1.4,
                        justifyContent:'center',
                        alignItems:'center'
                    }}>
                        <FlatList
                            scrollEnabled={false}
                            data={items}
                            numColumns={3}
                            renderItem={renderItem}
                            keyExtractor={(item, index)=>index.toString()}
                            showsVerticalScrollIndicator={false}
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }
}
const styles=StyleSheet.create({
    screen: {
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#fff'
    },
    HeaderRow: {
        ...Platform.select({
            ios: {
                height: 100,
                paddingTop: 25,
            },
            android: {
                height: 50,
            },
        }),
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        alignItems: 'center',
        alignContent: 'center',
        width: '100%',
        backgroundColor:colors.secondary,
        marginBottom:5,
    },
    TileContainer: {
        // paddingHorizontal: 20,
    },
    HeaderText: {
        textAlign: 'center',
        fontSize: 18,
        textTransform: 'uppercase',
        color: colors.accent,
        paddingHorizontal: 20,
    },
    navIcon: {
    },
    empty: {
        padding:10,
    },
    buttonContainer: {
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        transform:[{scale:0.8}]
    },
    buttonContainerOut:{
        marginTop:20,
        justifyContent:'center',
        alignItems:'center',
        width:width
    },
    QuickService: {
        opacity: 0.7,
        fontSize: 16,
        paddingHorizontal: 20,
        color: colors.tetiary,
    },
    LogoContainer: {
        paddingHorizontal: 30,
    },
    Logo: {
        width: 100,
        height: 100
    },

    tilesContainer: {
        paddingTop:10
    },
    transTextContainer: {
        paddingVertical: 10,
        paddingHorizontal: 10,
    },
    transText1: {
        fontSize: 12,
    },
    transText2: {
        fontSize: 30,
        paddingTop: 5,
    },
    billboard:{
        width:'auto',
        marginVertical:5,
        borderWidth:2,
        borderColor:'#ddd',
        borderRadius:10,
        padding:10,
        transform: [{ scale: 0.9 }],
        paddingVertical:10,
        height:'auto'
    }
});
export default HomeScreen;
