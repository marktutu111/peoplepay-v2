import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Colors from '../constants/colors';
import Header from '../components/Header';
import {ScrollView} from 'react-native-gesture-handler';
import {Icon} from 'react-native-elements';
import HeaderTxt1 from '../components/HeaderTxt1';

const PrivacyPolicyScreen = props => {
    return (
        <View style={styles.screen}>
            {/* Header of the Screen */}
            <Header pressTo={() => props.navigation.navigate('Home')} />
            <ScrollView
                style={styles.scroll}
                keyboardShouldPersistTaps="always"
                keyboardDismissMode="on-drag">
                {/* Screen Title */}
                <HeaderTxt1 txt1="Privacy" txt2=" Policy" />
                {/* Screen Main Content */}
                <View style={styles.Body}>
                    <View style={styles.TextContainer}>
                        <Text style={styles.InfoText}>
                            This page informs you of our policies regarding the
                            collection, use and disclosure of Personal
                            Information we receive from users of PeoplesPay
                        </Text>
                        <Text style={styles.HeaderTextMain}>
                            Information Technology Systems
                        </Text>
                        <Text style={styles.infoText}>
                            Our staff may need access to customer data in some
                            form to enable them perform their jobs and duties,
                            but this is limited to such staff having only
                            ‘relevant accesses”. By this we mean that staff
                            would not be able to access information that they do
                            not require to perform their roles and specific
                            tasks and only on a need to know basis. Systems
                            Rights/Access When and where a staff is required to
                            change his/her role, their IT rights will be
                            reviewed accordingly within the limitations of
                            his/her job requirement. Random Checks The
                            management of PeoplesPay is tasked with the
                            responsibility of conducting random audit and
                            periodic checks to ensure staff are accessing only
                            relevant information on customers as required by the
                            scope of their jobs. Deleting of Data It is against
                            company policy to delete customer data without
                            written approval from top management and backed up
                            with relevant written approval.
                        </Text>
                        <Text style={styles.HeaderTextMain}>
                            Storage, Management and Backing-up Customer Data
                        </Text>
                        <Text style={styles.infoText}>
                            At PeoplesPay a management personnel of the company
                            conducts routine security checks to ensure the
                            following actions are enforced physically or
                            technically;
                            {"\n"}
                            <Text>{'\u2022'}</Text> USB ports/Drives, CD ports on computers are disabled 
                            if staff do not need them to do their jobs.
                            {"\n"}
                            <Text>{'\u2022'}</Text> Records/information are cleared
                            when issuing used laptops to new staff.{"\n"}
                            <Text>{'\u2022'}</Text> Staff passwords are changed on a monthly basis.{"\n"}
                            <Text>{'\u2022'}</Text> Staff do not exchange passwords with colleagues.{"\n"} 
                            <Text>{'\u2022'}</Text> Staff do not write down passwords.{"\n"} 
                            <Text>{'\u2022'}</Text>Staff do not take computers home.{"\n"}  
                            <Text>{'\u2022'}</Text> A system log is in place to manage stolen computers. {"\n"}
                            <Text>{'\u2022'}</Text>Data encryption is effected at every data management point of storage
                            and movement within the system.{"\n"} 
                            1) DATA BACK-UP POLICY {"\n"}
                            Introduction PeoplesPay has created this data
                            back-up policy to ensure staff and management has
                            oversight in the process of backing-up/management of
                            data held by our company. Scope The service and
                            hence this policy has been designed and implemented
                            with disaster recovery/business continuity (i.e. the
                            ability to recover recent live data in the event of
                            a partial or total loss of data) as key deliverable
                            and is not therefore designed as a method of
                            archiving material for extended periods of time. The
                            ‘data’ back-ups cover all systems managed by the
                            PeoplesPay technology department. Data held and
                            managed locally within departments are excluded
                            unless departments have entered into specific
                            arrangements with technology department. Every staff
                            is reminded that they are individually responsible
                            for data held locally on their desktop or laptop
                            computer and all critical data must be stored on the
                            network drives provided or centralized e‐mail
                            services. {"\n"}
                            Backup Policy{"\n"}
                            <Text>{'\u2022'}</Text>Full backups of all
                            PeoplesPay data are performed daily. Full backups
                            are retained for 3 months before being overwritten.{"\n"}
                            <Text>{'\u2022'}</Text> Incremental backups of all PeoplesPay data are
                            performed daily. Incremental backups are retained
                            for 1 month before being overwritten.{"\n"} <Text>{'\u2022'}</Text> Where
                            possible backups are run overnight and are completed
                            before 8am on business days. Upon completion of
                            backups, media copies are moved automatically to a
                            secure remote site for disaster recovery purposes.{"\n"}
                            <Text>{'\u2022'}</Text>Backups are stored in secure locations. A limited
                            number of authorised personnel have access to the
                            backup application and media copies.{"\n"}
                            <Text>{'\u2022'}</Text> Requests
                            for backup data from 3rd parties must be approved
                            the company’s Head Of Technology.
                            Backup {"\n"}<Text>{'\u2022'}</Text> The IT Backup systems have been designed
                            to ensure that routine backup operations require no
                            manual intervention. {"\n"}<Text>{'\u2022'}</Text> The IT department monitors
                            backup operations and the status for backup job is
                            checked on a daily basis during the business week.{"\n"} <Text>{'\u2022'}</Text> Any failed 
                            backups are re‐run immediately on the
                            next business day. Restore {"\n"} <Text>{'\u2022'}</Text> Data is available for
                            restore within a few minutes of a backup job
                            completing on the daily schedule.{"\n"}<Text>{'\u2022'}</Text> Data will be
                            available during the retention policy of each backup
                            job – which is currently defined as 3 months.{"\n"} <Text>{'\u2022'}</Text>
                            Recent data is available from this system on
                            completion of the daily backup jobs, which means
                            that there is potential data loss during a business
                            day on some systems. The IT systems at PeoplesPay
                            have been specified to minimize data loss between
                            backup windows by having elements of system
                            redundancy.{"\n"} <Text>{'\u2022'}</Text> Requests for data recovery should be
                            submitted to the IT Service desk and will need to be
                            approved and signed off by the Head Of Technology
                            before being restored. Policy Review This policy
                            will be reviewed on an annual basis and be tabled
                            for approval with the written approval of our
                            Management. {"\n"}
                            2) DATA PROTECTION POLICY Introduction
                            PeoplesPay needs to keep certain information about
                            its employees, customers and other users to allow it
                            to monitor performance, achievements, and health and
                            safety, for example. It is also necessary to process
                            information so that staff can be recruited and paid,
                            customer transfers completed and legal obligations
                            to report. To comply with the law, information must
                            be collected and used fairly, stored safely and not
                            disclosed to any other person unlawfully. To do
                            this, PeoplesPay must comply with the Data Protection
                            Principles. In summary these state that personal
                            data shall: {"\n"} 
                            <Text>{'\u2022'}</Text> Be obtained and processed fairly and
                            lawfully and shall not be processed unless certain
                            conditions are met. {"\n"} <Text>{'\u2022'}</Text> Be obtained for a specified
                            and lawful purpose and shall not be processed in any
                            manner incompatible with that purpose. {"\n"} <Text>{'\u2022'}</Text>Be
                            adequate, relevant and not excessive for those
                            purposes. {"\n"} <Text>{'\u2022'}</Text> Be accurate and kept up to date. {"\n"} <Text>{'\u2022'}</Text>
                            Not be kept for longer than is necessary for that
                            purpose. {"\n"} <Text>{'\u2022'}</Text>Be processed in accordance with the
                            data subject’s rights. {"\n"} <Text>{'\u2022'}</Text>Be kept safe from
                            unauthorised access, accidental loss or destruction.
                            {"\n"} <Text>{'\u2022'}</Text> Not be transferred to a country outside the
                            area, unless that country has equivalent levels of
                            protection for personal data. PeoplesPay as a company
                            and individual staff or 3rd parties who process or
                            use any personal information must ensure that they
                            adopt these principles at all times. To ensure this
                            happens, PeoplesPay has developed the Data Protection
                            Policy. Status of the Policy This policy does not
                            form part of the formal contract of employment, but
                            it is a condition of employment that employees will
                            abide by the rules and policies made by PeoplesPay
                            from time to time. Any failures to follow the policy
                            can therefore result in disciplinary proceedings.
                            Any member of staff, who considers that the policy
                            has not been followed in respect of personal data
                            about themselves, should raise the matter with the
                            HR Department. If the matter is not resolved it
                            should be raised as a formal grievance. Notification
                            of Data held and Processed All staff and customers
                            and other users are entitled to; {"\n"} <Text>{'\u2022'}</Text> Know what
                            information PeoplesPay holds and processes about them
                            and why. {"\n"} <Text>{'\u2022'}</Text> Know how to gain access to it. {"\n"} <Text>{'\u2022'}</Text> 
                            Know
                            how to keep it up to date. {"\n"} <Text>{'\u2022'}</Text> 
                            Know what PeoplesPay
                            is doing to comply with its obligations PeoplesPay
                            will therefore provide all staff and customers and
                            other relevant users with a standard form of
                            notification. This will state all the types of data
                            PeoplesPay holds and processes about them, and the
                            reasons for which it is processed. PeoplesPay will
                            try to do this at least once every three years.
                            Responsibilities of Staff Every staff is responsible
                            for: {"\n"} <Text>{'\u2022'}</Text> Checking that any information that they
                            provide to PeoplesPay in connection with their
                            employment is accurate and up to date. {"\n"} <Text>{'\u2022'}</Text> Informing
                            PeoplesPay of any changes to information, which they
                            have provided i.e. changes of address {"\n"} <Text>{'\u2022'}</Text> Checking
                            the information that PeoplesPay will send out from
                            time to time, giving details of information kept and
                            processed about staff. {"\n"} <Text>{'\u2022'}</Text>Informing PeoplesPay of
                            any errors or changes. PeoplesPay cannot be held
                            responsible for any errors unless the staff member
                            has informed PeoplesPay of them. If and when, as part
                            of their responsibilities, staff collect information
                            about other people, (i.e. about customers course
                            work, opinions about ability, references to other
                            academic institutions, or details of personal
                            circumstances), they must comply with the guidelines
                            for staff. All staff will also complete relevant
                            training in Data Protection as component of their
                            Induction to employment at PeoplesPay. Data Security
                            {"\n"} <Text>{'\u2022'}</Text> Each staff is responsible for ensuring that: {"\n"} <Text>{'\u2022'}</Text>
                            Any personal data which they hold is kept securely.
                            {"\n"} <Text>{'\u2022'}</Text> Personal information is not disclosed either
                            orally or in writing or accidentally or otherwise to
                            any unauthorised third party. {"\n"} <Text>{'\u2022'}</Text> Staff are aware
                            that unauthorised disclosure and/or failure to
                            adhere to the requirements set out in this policy
                            will be a disciplinary matter, and may be considered
                            gross misconduct in some {"\n"}
                             Personal information
                            should be; {"\n"} <Text>{'\u2022'}</Text>kept in a locked filing cabinet; or
                            in a locked drawer; or if it is computerized , be
                            password protected; or when kept or in transit on
                            portable media the files themselves must be password
                            protected. {"\n"} Personal data should never be stored
                            at staff members’ homes, whether in manual or
                            electronic form, on laptop computers or other
                            personal portable devices or at other remote sites,
                            {"\n"} Ordinarily, personal data should not be
                            processed at staff members’ homes, whether in manual
                            or electronic form, on laptop computers or other
                            personal portable devices or at other remote sites.
                            In cases where such off-site processing is felt to
                            be necessary or appropriate, the agreement of the
                            Head Of Technology must be obtained, and all the
                            security guidelines given in this document must
                            still be followed. {"\n"} Data stored on portable
                            electronic devices or removable media is the
                            responsibility of the individual member of staff who
                            operates the equipment. It is the responsibility of
                            this individual to ensure that: {"\n"} Suitable backups
                            of the data exist.Sensitive data is appropriately
                            encrypted {"\n"} <Text>{'\u2022'}</Text> Sensitive data is not copied onto
                            portable storage devices without first consulting
                            the Head Of Technology, in regard to appropriate
                            encryption and protection measures. {"\n"} <Text>{'\u2022'}</Text> Electronic
                            devices such as laptops, mobile devices and computer
                            media (USB devices, CD’s etc.) that contain
                            sensitive data ARE not left unattended when offsite.
                            {"\n"}For some information the risks of failure to
                            provide adequate security may be so high that it
                            should never be taken home. This might include
                            payroll information, addresses of customers and
                            staff, disciplinary or appraisal records or bank
                            account details. Exceptions to this may only be with
                            the explicit agreement of the Principal. Customer
                            Obligations: {"\n"} <Text>{'\u2022'}</Text> Customers must ensure that all
                            personal data provided to PeoplesPay is accurate and
                            up to date. They must ensure that changes of
                            address, etc. are notified to our office. Rights to
                            Access Information {"\n"} <Text>{'\u2022'}</Text> Staff, customers and other
                            users of PeoplesPay have the right to access any
                            personal data thsat is being kept about them either
                            on computer or in certain files. {"\n"} In order to
                            gain access, an individual may wish to receive
                            notification of the information currently being
                            held. This request should be made in writing, in the
                            first instance to PeoplesPay data protection officer.
                            {"\n"}  PeoplesPay may request for a charge not more than
                            $10 on each occasion that access is requested,
                            although PeoplesPay have discretion to waive this. {"\n"} PeoplesPay aims to comply with requests for access
                            to personal information as quickly as possible, but
                            will ensure that it is provided within Subject
                            Consent. {"\n"} In many cases, PeoplesPay can only
                            process personal data with the consent of the
                            individual. In some cases, if the data is sensitive,
                            express consent must be obtained.{"\n"} Agreement for
                            PeoplesPay to process some specified classes of
                            personal data is a condition of acceptance for
                            customers and a condition of employment for staff.
                            This may include request for information about
                            previous criminal convictions.{"\n"} PeoplesPay will
                            also ask its staff members for information about
                            particular health needs, such as allergies to
                            particular forms of medication, or any conditions
                            such as asthma or diabetes.{"\n"} PeoplesPay will only
                            use the information in the protection of the health
                            and safety of the staff member but will need consent
                            to process in the event of a medical emergency, for
                            example. {"\n"}  All prospective staff and customers
                            will be asked to sign consent to process data,
                            regarding particular types of information when an
                            offer of employment or a course place is made. A
                            refusal to sign such a form can result in the offer
                            being withdrawn. Processing Sensitive Information
                            Sometimes it may be necessary to process information
                            about a person’s health, criminal convictions, race
                            and gender and family details. This may be to ensure
                            PeoplesPay is a safe place for everyone, or to
                            operate other Company policies, such as the sick pay
                            policy or equal opportunities policy. Because this
                            information is considered sensitive, and recognised
                            that the processing of it may cause particular
                            concern or distress to individuals. Staff will be
                            asked to give express consent for PeoplesPay to do
                            this. Offers of employment may be withdrawn if an
                            individual refuses to consent to this, without good
                            reason.
                        </Text>
                        <Text style={styles.infoText}>
                            Data Protection Act, 2012 (Act 843) Section 20 (1)
                            (e) grants us the right to collect your personal
                            data necessary to pursue the legitimate interest of
                            our business dealings with you.
                        </Text>
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    HeaderTextMain: {
        // color: Colors.secondary,
        // fontFamily: 'Roboto-ExtraBold',
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingVertical: 15,
        paddingBottom: 20,
        flexDirection: 'row',
        fontSize: 18,
    },
    HeaderText: {
        // fontFamily: 'Roboto-ExtraBold',
        fontSize: 35,
        color: Colors.secondary,
        textAlign: 'left',
        opacity: 0.8,
    },
    HeaderText1: {
        // fontFamily: 'Roboto-ExtraBold',
        fontSize: 35,
        color: Colors.primary,
        textAlign: 'left',
        opacity: 0.8,
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
        paddingTop: 30,
    },
    InfoText: {
        // fontFamily: 'Roboto-Medium',
        paddingVertical: 5,
        textAlign: 'left',
        opacity: 0.5,
        paddingHorizontal: 30,
        bottom: 30,
    },
    infoText:{
        // fontFamily:'Roboto-Mediium',
        paddingVertical: 20,
        textAlign:'left',
        paddingHorizontal: 20,
        bottom: 0,



    },
    ScanText: {
        // fontFamily: 'Roboto-Bold',
        width: '100%',
        paddingVertical: 15,
        textAlign: 'center',
        opacity: 0.5,
        fontSize: 15,
        // paddingHorizontal: 30,
    },
    container: {
        // borderRadius: 5,
        // height: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: Colors.accent,
        alignItems: 'center',
        // marginBottom: 30,
        bottom: 10,
    },
    iconsContainer: {
        borderRadius: 10,
        // borderWidth: 1,
        // justifyContent: 'space-between',
        borderColor: Colors.primary,
        paddingVertical: 40,
        paddingHorizontal: 70,
        backgroundColor: '#FAFAFA',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.21,
        shadowRadius: 10,
        elevation: 3,
    },
    navIcon2: {
        paddingVertical: 5,
        opacity: 0.4,
    },
    iconContainer2: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 20,
    },
    scanning: {
        // fontFamily: 'Roboto-Medium',
        opacity: 0.7,
        color: Colors.tertiary,
    },
});

export default PrivacyPolicyScreen;

// change cont declaration name and export default name
