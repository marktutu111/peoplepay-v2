/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import {View, Text, StyleSheet, Image, FlatList,TouchableOpacity} from 'react-native';
import Header from '../../components/Header';
import HeaderTxt1 from '../../components/HeaderTxt1';
import Merchants from "../../services/merchant.service";
import Icon from "react-native-vector-icons/FontAwesome";
import { ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';



class UtilityScreen extends Component {

    viewLoaded=false;

    state={
        list:[],
        loading:false
    }

    componentDidMount(){
        const category=this.props.navigation.getParam('category');
        this.viewLoaded=true;
        this.state.category=category;
        this.getList();
    }


    componentWillUnmount(){
        this.viewLoaded=false;
    }


    async loadCache(){
        try {
            const cache=await AsyncStorage.getItem(`${this.state.category}-ppay-utils`);
            if(typeof cache==='string'){
                const data=JSON.parse(cache);
                this.viewLoaded && this.setState(
                    {
                        list:data
                    }
                );
            }
        } catch (err) {}
    }


    async getList(){
        try {
            const category=this.state.category;
            this.setState({loading:true},this.loadCache);
            const response=await Merchants.getByCategory(category);
            if(!response.success){
                throw Error(
                    response[
                        'message'
                    ]
                )
            }
            await AsyncStorage.setItem(
                `${category}-ppay-utils`,
                JSON.stringify(
                    response[
                        'data'
                    ]
                )
            );
            this.viewLoaded && this.setState(
                {
                    loading:false
                },this.loadCache
            )
        } catch (err) {
            this.getList();
        }
    }


    render(){

        return (
            <View style={styles.screen}>
                {/* Header */}
                <Header pressTo={()=>this.props.navigation.goBack()} />
                <View style={styles.scroll}>
                    <HeaderTxt1 txt1={this.props.navigation.getParam('optionType')} />
                    <View style={styles.Body}>
                        <FlatList
                            extraData={this.state}
                            data={this.state.list}
                            ListEmptyComponent={()=>this.state.loading && <ActivityIndicator color="#000" size={'large'}/>}
                            keyExtractor={(v,i)=>i.toString()}
                            renderItem={({item})=>(
                                <TouchableOpacity 
                                    onPress={()=>{
                                        this.props.navigation.navigate(
                                            'ProductList', 
                                            {data:item}
                                        )
                                    }}
                                    style={{
                                        paddingVertical:20,
                                        paddingHorizontal:15,
                                        borderBottomColor:'#eee',
                                        borderBottomWidth:1,
                                        flexDirection:'row',
                                        alignItems:'center',
                                        justifyContent:'flex-start'
                                    }}
                                >
                                    <View style={{
                                        width:60,
                                        height:60,
                                        // borderColor:'#ddd',
                                        // borderWidth:1,
                                        marginRight:5,
                                        justifyContent:'center',
                                        alignItems:'center'
                                    }}>
                                        <Image 
                                            style={{width:'100%',height:'100%'}} 
                                            source={{uri:item.image}}
                                        />
                                    </View>
                                    <Text style={{
                                        fontSize:15,
                                        width:'68%',
                                        paddingHorizontal:10
                                    }}>{item.name}</Text>
                                    <Icon
                                        style={{marginTop:'10%'}}
                                        name="chevron-right"
                                        color="#eee"
                                    />
                                </TouchableOpacity>
                            )}
                        />
                    </View>
                </View>
            </View>
        );
    }

};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
    },
    Image: {
        height: 80,
        width: 80,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
export default UtilityScreen;
