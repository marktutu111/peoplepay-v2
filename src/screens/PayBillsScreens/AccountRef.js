import React, { Component } from 'react';
import {View, StyleSheet,Text,Image,Alert,BackHandler,Modal,TouchableOpacity, ActivityIndicator} from 'react-native';
import Header from '../../components/Header';
import HeaderTxt1 from '../../components/HeaderTxt1';
import TxtInput from '../../components/TxtInput';
import {ScrollView} from 'react-native-gesture-handler';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import merchantService from '../../services/merchant.service';
import { AuthConsumer } from '../../states/auth.state';
import LazyContactPicker from '../../components/contacts-picker.component';
import { TransactionContext } from '../../states/transactions.state';
import IconButton from '../../components/IconButton';
import Icon from "react-native-vector-icons/FontAwesome5";
import colors from '../../constants/colors';
import _TService from "../../services/transactions.service";



class AccountRef extends Component {

    static contextType=TransactionContext;

    viewLoaded=false;
    state={
        openModal:false,
        loading:false,
        openfavs:false,
        favs:[]
    }

    componentDidMount(){
        this.viewLoaded=true;
        this.setState(
            {
                ...this.state,...this.props.navigation.getParam('data')
            },()=>{
                this.handleBackPress();
            }
        );
    }

    handleBackPress(){
        BackHandler.addEventListener('hardwareBackPress',()=>{
            this.goBack();
            return true;
          });
    }


    goBack(){
        switch (this.state._bill_type) {
            case 'AIRTIME':
                return this.props.navigation.navigate('Home');
            default:
                return this.props.navigation.goBack();
        }
    }


    componentWillUnmount(){
        this.viewLoaded=false;
        this.getServiceCode=null;
        BackHandler.removeEventListener('hardwareBackPress',null);
    }


    async filterTransactions(){
        try {
            this.setState({loading:true});
            const response=await _TService.filterTransactions(
                {
                    customerId:this.user._id,
                    transaction_type:'AT',
                    status:'paid'
                }
            );
            if(!response.success){
                throw Error(
                    response.message
                )
            }
            const contacts=response.data.map(tr=>tr.recipient_account_number);
            const _filtered=Array.from(new Set(contacts));
            this.setState(
                {
                    favs:_filtered,
                    loading:false
                }
            )
        } catch (err) {
            this.setState(
                {
                    loading:false
                }
            )
        }
    }


    async lookup(){
        try {
            const {pushTransaction}=this.context;
            const{accountRef,serviceCode,productId,merchantId}=this.state;
            if(!accountRef || accountRef===''){
                return;
            };

            this.setState({loading:true});
            const data={
                serviceCode:serviceCode,
                terminalUser:this.user['_id'],
                accountRef:accountRef,
                productId:productId,
                merchantId:merchantId
            }
            const response=await merchantService.lookup(data);
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            this.viewLoaded && this.setState(
                {
                    loading:false
                },()=>{
                    pushTransaction(
                        {
                            billName:this.state.productDescription,
                            billMerchant:this.state.type,
                            bill_sessionId:response.data.sessionId,
                            recipient_account_number:accountRef,
                            recipient_account_issuer:response.data.name,
                            description:this.state.productDescription
                        },()=>{
                            this.props.navigation.navigate(
                                'CustomerDetail',
                                {data:response.data}
                            )
                        }
                    )
                }
            )
        } catch (err) {
            this.setState(
                {
                    loading:false
                },Alert.alert(
                    'Dear customer',
                    err.message
                )
            )
        }
    }

    render(){

        const {transaction}=this.context.state;

        const getName=()=>{
            switch (transaction.transaction_type) {
                case 'AT':
                    return `Enter phone number`;
                case 'SCH':
                    return `Enter Student number`;
                default:
                    return `Enter customer number`;
            }
        }

        return (
            <View style={styles.screen}>
                <Header pressTo={this.goBack.bind(this)} />
                <AuthConsumer>
                    {
                        ({state})=>{
                            this.user=state['user'];
                        }
                    }
                </AuthConsumer>
                <ScrollView
                    style={styles.scroll}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag">
                    <View style={styles.headerCont}>
                        <HeaderTxt1
                            txt1={transaction.transaction_type==='AT'?'Buy Airtime':this.state.productDescription} 
                            style={{margin:20}}
                        />
                        <IconButton  name="star" onPress={()=>this.setState({openfavs:!this.state.openfavs})}/>
                    </View>
                    { transaction.transaction_type !== 'AT' && <Text style={{
                        marginLeft:50,
                        fontSize:20
                    }}>{getName(this.state.category)}</Text>}
                    <View style={styles.Body}>
                        {transaction.transaction_type==='AT' && <Text style={{
                            paddingVertical:2,
                            paddingHorizontal:50,
                            fontSize:20
                        }}>Provide phone number to receive airtime</Text>}
                        <View style={styles.TxtInput}>
                            <TxtInput
                                defaultValue={this.state.accountRef}
                                IconName={transaction.transaction_type==='AT'?'address-book':"hash"}
                                // IconType="feather"
                                fa
                                placeholder={transaction.transaction_type==="AT"?"Phone number":"account ref"}
                                onChangeText={v=>this.state.accountRef=v.trim()}
                                contact={this.state._bill_type==="AIRTIME"?true:false}
                                onContact={()=>this.setState(
                                    {
                                        openModal:true
                                    }
                                )}
                            />
                            <View style={{paddingVertical:20}}>
                                <LoginPrimaryButton
                                    loading={this.state.loading}
                                    title="Next"
                                    pressTo={this.lookup.bind(this)}
                                />
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <LazyContactPicker 
                    openModal={this.state.openModal}
                    getContact={(v)=>{
                        try {
                            this.setState(
                                {
                                    openModal:false,
                                    accountRef:v.trim()
                                }
                            )
                        } catch (err) {}
                    }}
                    closeModal={
                        ()=>this.setState(
                            {
                                openModal:false
                            }
                        )
                    }
                />
                <Modal
                    animationType="slide"
                    onDismiss={()=>null}
                    presentationStyle="formSheet"
                    visible={this.state.openfavs}
                    onShow={this.filterTransactions.bind(this)}
                >
                    <View style={{
                        paddingHorizontal:30,
                        paddingVertical:20,
                        flexDirection:'row',
                        alignItems:'center',
                        borderRadius:5,
                        borderColor:'#ddd',
                        borderBottomWidth:1
                    }}>
                        <TouchableOpacity onPress={()=>this.setState({openfavs:false})}>
                            <Icon
                                name="times"
                                size={30}
                                color={colors.secondary}
                            />
                        </TouchableOpacity>
                        <Text style={{marginHorizontal:15}}>Choose from recents</Text>
                    </View>
                    <ScrollView>
                        {this.state.loading && <ActivityIndicator size={20} color={colors.secondary} style={{marginVertical:15}}/>}
                        {this.state.favs.map((fav,i)=><TouchableOpacity key={i.toString()} onPress={()=>this.setState({accountRef:fav.trim(),openfavs:false})} style={styles.contactList}>
                            <Text style={styles.contactText}>
                                {fav}
                            </Text>
                        </TouchableOpacity>)}
                    </ScrollView>
                </Modal>
            </View>
        );
    }

};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
    },
    Image: {
        height: 80,
        width: 80,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    TxtInput:{
        paddingHorizontal: '10%',
        paddingVertical: 10,
    },
    text:{
        paddingVertical:5,
        color:'#000',
        fontWeight:'bold'
    },
    background:{
        position:'absolute',
        height: 250,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        backgroundColor: 'white',
        opacity:0.3
    },
    image:{
        width:80,
        height:80,
        justifyContent:'center',
        alignItems:'center',
        borderColor:'#ddd',
        borderWidth:1,
        borderRadius:5,
        overflow:'hidden'
    },
    headerCont:{
        flexDirection:'row',
        alignItems:'center'
    },
    contactList:{
        padding:20,
        fontSize:25,
        borderBottomColor:'#ddd',
        borderBottomWidth:1
    },
    contactText:{
        fontSize:25
    }
});
export default AccountRef;
