import React, {Component} from 'react';
import {StyleSheet, Text, View, Alert, ScrollView} from 'react-native';
import HeaderText from '../../components/HeaderText';
import Header from '../../components/Header';

import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import Colors from '../../constants/colors';
import {CustomExample} from '../../components/customPicker';

class FundPaymentScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            option: '0',
            payment_account_name: '',
            payment_account_number: '',
            payment_account_issuer: '',
            payment_account_type: '',
            recipient_type: '',
        };
    }
    render() {
        return (
            <View style={styles.screen}>
                {/* Header of the Screen */}
                <Header pressTo={() => this.props.navigation.goBack()} />
                <ScrollView
                    style={styles.scroll}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag">
                    {/* Screen Title */}
                    <HeaderText title1="Select" title2="Money Source" />
                    {/* Screen Main Content */}
                    <View style={styles.Body}>
                        <Text style={styles.heading}>
                            Choose Source Of Funds
                        </Text>
                        <View style={styles.dropdownContainer}>
                            <CustomExample
                                placeholder="Payment Type"
                                selectedValue={this.state.option}
                                onValueChange={v =>
                                    this.setState({
                                        option: v,
                                    })
                                }
                                items={[
                                    {
                                        name: 'Enter Account',
                                        value: '0',
                                    },
                                    {
                                        name: 'Choose From My Accounts',
                                        value: '1',
                                    },
                                ]}
                            />
                        </View>

                        {this.state.option === '1' && (
                            <View style={styles.dropdownContainer}>
                                <CustomExample
                                    placeholder="Choose Account"
                                    selectedValue={this.state.option}
                                    onValueChange={v =>
                                        this.setState({
                                            payment_account_type: v,
                                        })
                                    }
                                    items={[
                                        {
                                            name: 'hell',
                                            value: '1',
                                        },
                                    ]}
                                />
                            </View>
                        )}

                        <Text style={styles.heading}>
                            Enter Account Details
                        </Text>
                        <View style={styles.buttonContainer}>
                            <LoginPrimaryButton
                                title="Send"
                                pressTo={() =>
                                    Alert.alert(
                                        'Hello Mark,',
                                        'Do you want to proceed ?',
                                        [
                                            {
                                                text: 'YES',
                                                onPress: () =>
                                                    this.props.navigation.navigate(
                                                        'VerifyPayment',
                                                    ),
                                            },
                                            {
                                                text: 'NO',
                                                onPress: () =>
                                                    console.log('No Pressed'),
                                                style: 'cancel',
                                            },
                                        ],
                                        {cancelable: false},
                                        //clicking out side of alert will not cancel
                                    )
                                }
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

export default FundPaymentScreen;

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
    },
    heading: {
        textAlign: 'left',
        paddingBottom: 10,
        marginBottom: 10,
        borderBottomWidth: 0.8,
        borderColor: Colors.primary,
        marginHorizontal: 30,
        fontFamily: 'Roboto-Bold',
        color: Colors.tetiary,
        opacity: 0.7,
    },
    dropdownContainer: {
        paddingVertical: 10,
        marginHorizontal: 30,
    },

    TxtInputContainer: {
        paddingHorizontal: 30,
    },
    buttonContainer: {
        // alignItems: 'center',
        paddingVertical: 20,
        paddingHorizontal: 30,
    },
});
