import React from 'react';
import {View, StyleSheet, Switch} from 'react-native';
import Header from '../../components/Header';
import HeaderTxt1 from '../../components/HeaderTxt1';
import TxtInput from '../../components/TxtInput';
import {ScrollView} from 'react-native-gesture-handler';


const CustomerDetailScreen = props => {
    const PayerDetails = [
        {
            catergory: 'Utilities',
            Institution: 'Pay WaterBill',
            imageUri: require(' ../../assets/images/utilities/ghana_water.png'),
            parameter: 'Customer Number',
        },
        {
            catergory: 'Utilities',
            Institution: 'Pay Electicity Bill',
            imageUri: require('../../assets/images/utilities/ecg.png'),
            parameter: 'Account Number',
        },
        {
            catergory: 'Pay TV',
            Institution: 'DStv Bill',
            imageUri: require('../../assets/images/Tv/DSTv.png'),
            parameter: 'Smart Card Number',
        },
        {
            catergory: 'Pay TV',
            Institution: 'DSTv BoxOffice',
            imageUri: require('../../assets/images/Tv/Dstv_BoxOffice.png'),
            parameter: 'Smart Card Number',
        },
        {
            catergory: 'Pay TV',
            Institution: 'Go TV',
            imageUri: require('../../assets/images/Tv/GOTv.png'),
            parameter: 'Smart Card Number',
        },
        {
            catergory: 'Schools',
            Institution: 'Ashesi University',
            imageUri: require('../../assets/images/Tv/GOTv.png'),
            parameter: 'Account Number',
        },
        {
            catergory: 'Schools',
            Institution: 'UG',
            imageUri: require('../../assets/images/Tv/GOTv.png'),
            parameter: 'Student ID',
        },
        {
            catergory: 'Schools',
            Institution: 'U.C.C',
            imageUri: require('../../assets/images/Tv/GOTv.png'),
            parameter: 'Student ID',
        },
        {
            catergory: 'Schools',
            Institution: 'KNUST',
            imageUri: require('../../assets/images/Tv/GOTv.png'),
            parameter: 'Student ID',
        },
        {
            catergory: 'Churches',
            Institution: 'Action Chapel',
            imageUri: require('../../assets/images/Tv/GOTv.png'),
            parameter: 'Phone Number',
        },
        {
            catergory: 'Churches',
            Institution: 'ICGC',
            imageUri: require('../../assets/images/Tv/GOTv.png'),
            parameter: 'Phone Number',
        },
        {
            catergory: 'Embassies',
            Institution: 'US Embassy',
            imageUri: require('../../assets/images/Tv/GOTv.png'),
        },
        {
            catergory: 'Embassies',
            Institution: 'Chinese Embassy',
            imageUri: require('../../assets/images/Tv/GOTv.png'),
        },
        {
            catergory: 'Airlines',
            Institution: 'British Airways',
            imageUri: require('../../assets/images/Tv/GOTv.png'),
            parameter: 'PNR',
        },
        {
            catergory: 'Airlines',
            Institution: 'KLM',
            imageUri: require('../../assets/images/Tv/GOTv.png'),
            parameter: 'PNR',
        },
        {
            catergory: 'Donations',
            Institution: 'Ghana COVID-19 fund',
            imageUri: require('../../assets/images/Tv/GOTv.png'),
            parameter: 'Phone Number',
        },
        {
            catergory: 'Donations',
            Institution: 'Child Right International',
            imageUri: require('../../assets/images/Tv/GOTv.png'),
            parameter: 'Phone Number',
        },
        {
            catergory: 'Insurance',
            Institution: 'Prudential Life',
            imageUri: require('../../assets/images/Tv/GOTv.png'),
            parameter: 'Policy Number',
        },
        {
            catergory: 'Insurance',
            Institution: 'Donewell',
            imageUri: require('../../assets/images/Tv/GOTv.png'),
            parameter: 'Policy Number',
        },
    ];
    const Instution = props.navigation.getParam('institution');
    const selectedInstitution = PayerDetails.filter(
        cat => cat.Institution === Instution,
    );
    return (
        <View style={styles.screen}>
            {/* Header of the Screen */}
            {/* <Header pressTo={() => props.navigation.goBack()} /> */}
            {/* Header */}
            <Header pressTo={()=>props.navigation.goBack()} />
            <ScrollView
                style={styles.scroll}
                keyboardShouldPersistTaps="always"
                keyboardDismissMode="on-drag">
                {/* Screen Institution */}
                <HeaderTxt1 txt1={`${Instution}`} />
                {/* Screen Main Content */}
                <View style={styles.Body}>
                    <View style={styles.TxtInput}>
                        <TxtInput />
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
    },
    Image: {
        height: 80,
        width: 80,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    TxtInput: {
        paddingHorizontal: '10%',
    },
});
export default CustomerDetailsScreen;
