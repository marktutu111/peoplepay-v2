import React, { Component } from 'react';
import {StyleSheet, Text, View, ScrollView} from 'react-native';
import Header from '../../components/Header';
import HeaderTxt1 from '../../components/HeaderTxt1';
import RecipientDetails from '../../components/RecipientDetails';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';

class ResponseScreen extends Component {

    render(){
        const data=this.props.navigation.getParam('data');
        return (
            <View style={styles.screen}>
                {/* Header of the Screen */}
                <Header pressTo={() => this.props.navigation.goBack()} />
                <ScrollView
                    style={styles.scroll}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag">
                    {/* Screen Title */}
                    <HeaderTxt1 txt1="Confirm" txt2=" Details" />
                    {/* Screen Main Content */}
                    <View style={styles.Body}>
                        <View style={styles.recipientContainer}>
                            <RecipientDetails
                                name={data?.description}
                                network=""
                                number="Premuim_Package"
                                amount="GHS360"
                                imageUri={require('../../assets/images/Tv/DSTv.png')}
                            />
                        </View>
                        <View style={styles.buttonContainer}>
                            <LoginPrimaryButton
                                title="PAY"
                                pressTo={() =>
                                    this.props.navigation.navigate('FundPayment')
                                }
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }


    
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
});



export default ResponseScreen;
