/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import {View, Text, StyleSheet, Image, FlatList,TouchableOpacity,ActivityIndicator} from 'react-native';
import Header from '../../components/Header';
import HeaderTxt1 from '../../components/HeaderTxt1';
import Icon from "react-native-vector-icons/FontAwesome";
import merchantService from '../../services/merchant.service';



class ProductListScreen extends Component {

    viewLoaded=false;

    state={
        loading:false,
        products:[]
    }

    componentDidMount(){
        this.viewLoaded=true;
        this.setState(
            {
                ...this.state,...this.props.navigation.getParam('data')
            },()=>{
                this.getServiceCode();
            }
        )
    }


    componentWillUnmount(){
        this.viewLoaded=false;
    }


    getServiceCode=()=>{
        this.setState({loading:true});
        merchantService.getMerchantCode(this.state.merchant_id).then(res=>{
            if(res.success){
                const data=res['data'];
                return this.viewLoaded && this.setState(
                    {
                        loading:false,
                        serviceCode:data['serviceCode'],
                        products:data['products'],
                        sessionId:data['sessionId']
                    }
                )
            }else{
                this.setState(
                    {
                        loading:false
                    }
                )
            }
        }).catch(err=>this.getServiceCode());
    }


    render(){
        return (
            <View style={styles.screen}>
                {/* Header */}
                <Header pressTo={()=>this.props.navigation.goBack()} />
                <View style={styles.scroll}>
                    <HeaderTxt1 txt1={'Products'} />
                    <Text style={{ marginLeft: 30,fontSize:17,color:'#6d6d6d'}}>Tap on product to proceed</Text>
                    <View style={styles.Body}>
                        <FlatList
                            ListEmptyComponent={()=>this.state.loading && <ActivityIndicator color={'#000'} size={'large'}/>}
                            data={this.state.products}
                            keyExtractor={(v,i)=>i.toString()}
                            renderItem={({item})=>(
                                <TouchableOpacity 
                                    onPress={()=>{
                                        this.props.navigation.navigate(
                                            'AccountRef', 
                                            {data:{...item,type:this.state.type,category:this.state.category,page:'PAYBILLS'}}
                                        )
                                    }}
                                    style={{
                                        paddingVertical:20,
                                        paddingHorizontal:15,
                                        borderBottomColor:'#eee',
                                        borderBottomWidth:1,
                                        flexDirection:'row',
                                        alignItems:'center',
                                        justifyContent:'space-between'
                                    }}
                                >
                                    <Text style={{
                                        fontSize:25
                                    }}>{item.productName}</Text>
                                    <Icon 
                                        name="chevron-right"
                                        color="#eee"
                                    />
                                </TouchableOpacity>
                            )}
                        />
                    </View>
                </View>
            </View>
        );
    }

};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
    },
    Image: {
        height: 80,
        width: 80,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
export default ProductListScreen;
