/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    FlatList,
} from 'react-native';
import Colors from '../../constants/colors';
import QuickserviceTile from '../../components/QuickserviceTile';
import Header from '../../components/Header';
import HeaderTxt1 from '../../components/HeaderTxt1';
import { TransactionContext } from '../../states/transactions.state';


class PayBills extends Component {

    static contextType=TransactionContext;

    state={
        loading:false,
    };


    getView(){
        const data=JSON.stringify(`
        <View style={styles.Body}>
            <View style={styles.tilesContainer}>
                <FlatList
                    data={categories}
                    numColumns={2}
                    renderItem={renderItem}
                    keyExtractor={(item, index) => index.toString()}
                    style={{
                        alignContent: 'center',
                        alignSelf: 'center',
                    }}
                    showsVerticalScrollIndicator={false}
                />
            </View>
        </View>
        `);

        const d=JSON.parse(data);
        return <Text>{d}</Text>;
        
    }

    render() {
        
        const {updateState}=this.context;

        const renderItem=({item})=>{
            return (
                <QuickserviceTile
                    iconname={item.navOptionThumb}
                    icontype={item.navOptionThumbType}
                    title={item.navOptionName}
                    pressTo={()=>{
                        updateState(
                            {
                                returnPage:'PayBills',
                                transaction:{
                                    transaction_type:'PB'
                                }
                            },()=>{
                                this.props.navigation.navigate(
                                    'Option',{
                                    optionType:item.navOptionName,
                                    category:item['category']
                                })
                            }
                        )
                    }}
                />
            );
        };

        const categories=[
            {
                navOptionThumb: 'lightbulb-on-outline',
                navOptionName: 'Utilities',
                category:'UTILS',
                navOptionThumbType: 'material-community',
            },
            {
                navOptionThumb: 'tv',
                navOptionName: 'Pay TV',
                category:'PAYTV',
                navOptionThumbType: 'material',
            },
            {
                navOptionThumb: 'school',
                navOptionName: 'Schools',
                category:'SCHOOLS',
                navOptionThumbType: 'material',
            },
            {
                navOptionThumb: 'church',
                navOptionName: 'Churches',
                category:'CHURCH',
                navOptionThumbType: 'font-awesome-5',
            },
            {
                navOptionThumb: 'flag',
                navOptionName: 'Embassies',
                category:'EMBASSIES',
                navOptionThumbType: 'material-community',
            },
            {
                navOptionThumb: 'aircraft',
                navOptionName: 'Airlines',
                category:'AIRLINES',
                navOptionThumbType: 'entypo',
            },
            {
                navOptionThumb: 'hand-holding-usd',
                navOptionName: 'Donations',
                category:'DONATIONS',
                navOptionThumbType: 'font-awesome-5',
            },
            {
                navOptionThumb: 'handshake',
                navOptionName: 'Insurance',
                category:'INSURANCE',
                navOptionThumbType: 'font-awesome-5',
            },
        ];

        return (
            <View style={styles.screen}>
                <Header text="Pay Bills" pressTo={()=>this.props.navigation.navigate('Home')} />
                <View style={styles.tilesContainer}>
                    <FlatList
                        data={categories}
                        numColumns={2}
                        renderItem={renderItem}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={()=>(
                            <View style={{
                                justifyContent:'center',
                                alignItems:'center'
                            }}>
                                <Text>No merchant has been registered yet</Text>
                            </View>
                        )}
                        style={{
                            alignContent: 'center',
                            alignSelf: 'center',
                        }}
                        showsVerticalScrollIndicator={false}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    HeaderRow: {
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        alignItems: 'center',
        alignContent: 'center',
        width: '100%',
        height: 65,
        backgroundColor: Colors.primary,
        marginBottom: 15,
        paddingHorizontal: 30,
    },
    HeaderTextLogo: {
        textAlign: 'center',
        fontFamily: 'Roboto-Bold',
        fontSize: 18,
        textTransform: 'uppercase',
        color: Colors.accent,
    },
    navIcon: {
        padding: 20,
    },
    empty: {
        padding: 10,
    },
    HeaderTextMain: {
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingTop: 30,
        paddingBottom: 20,
        flexDirection: 'row',
    },
    HeaderText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.secondary,
        // textAlign: 'center',
        opacity: 0.8,
    },
    HeaderText1: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.primary,
        // textAlign: 'center',
        opacity: 0.8,
        // bottom: 20,
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
    },
    tilesContainer: {
        marginVertical:20,
        justifyContent: 'center',
        alignContent: 'center',
    },
    tiles: {
        flexDirection: 'row',
        paddingVertical: 10,
        paddingHorizontal: 30,
        justifyContent: 'space-around',
    },
    mainContainer: {
        flex: 1,
        flexWrap: 'wrap',
        flexDirection: 'row',
    },
    cardItemlabel: {
        textAlign: 'center',
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
    },
    containerStyle: {
        padding: 10,
        borderWidth: 0,
        // marginBottom:5,
        marginTop: 10,
    },
    cardrow: {
        width: '100%',
        justifyContent: 'space-evenly',
        flexDirection: 'row',
    },
    QuickService: {
        fontFamily: 'Roboto-Bold',
        opacity: 0.7,
        fontSize: 16,
        paddingHorizontal: 20,
        color: Colors.tetiary,
    },
});
export default PayBills;
