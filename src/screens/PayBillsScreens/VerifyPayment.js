import React, {Component} from 'react';
import {
    View,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    Alert,
} from 'react-native';
import Colors from '../../constants/colors';
import Header from '../../components/Header';
import HeaderText from '../../components/HeaderText';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import service from "../../services/transactions.service";

export default class VerifyPaymentScreen extends Component {

    state={
        otp:''
    };

    render() {
        return (
            <View style={styles.screen}>
                {/* Header of the Screen */}
                <Header pressTo={() => this.props.navigation.goBack()} />
                <ScrollView
                    style={styles.scroll}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag">
                    {/* Screen Title */}
                    <HeaderText title1="Verify" />
                    {/* Screen Main Content */}
                    <View style={styles.Body}>
                        <Text style={styles.infoText}>
                            Verification code has been sent to your phone via
                            sms. Please check your mobile phone.
                        </Text>

                        <TextInput 
                            style={styles.textfield} 
                            onChangeText={v=>this.state.otp=v.trim()}
                        />

                        {/* <View style={styles.resendContainer}>
                            <TouchableOpacity>
                                <Text style={styles.resendText}>RESEND</Text>
                            </TouchableOpacity>
                        </View> */}
                        <View style={styles.btn}>
                            <LoginPrimaryButton
                                loading={this.state.loading}
                                title="Continue"
                                pressTo={async() =>{
                                    try {
                                        if (this.state.otp === ''){
                                            return;
                                        }
                                        const transaction=this.props.getParam('transaction');
                                        if (Object.keys(transaction).length <= 0){
                                            throw Error('')
                                        }
                                        const response=await service.createTransaction(
                                            {
                                                ...transaction,
                                                otp:this.state.otp
                                            }
                                        );
                                        if (!response.success){
                                            throw Error('');
                                        }
    
                                        this.props.navigation.navigate('Success');
                                    } catch (error) {
                                        this.setState(
                                            {
                                                loading:false
                                            }, ()=>{
                                                Alert.alert('Dear Customer','Dear customer we are sorry we cannot process your transaction please try again later');
                                                this.props.navigation.goBack()
                                            }
                                        )
                                    }

                                }}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },

    Body: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 30,
    },
    textfield: {
        borderBottomColor: Colors.tetiary,
        borderBottomWidth: 1,
        // width: '100%',
        // marginHorizontal: 30,
        alignItems: 'center',
        fontFamily: 'Roboto-Regular',
        color: Colors.tetiary,
        textAlign: 'center',
        width: '80%',
        letterSpacing: 10,
    },
    infoText: {
        fontFamily: 'Roboto-Medium',
        width: '80%',
        paddingVertical: 20,
        textAlign: 'center',
        opacity: 0.5,
        // paddingHorizontal: 10,
    },
    resendContainer: {
        width: '100%',
        alignItems: 'flex-end',
        paddingHorizontal: 30,
        paddingVertical: 15,
    },
    resendText: {
        fontFamily: 'Roboto-Bold',
        width: '100%',
        paddingBottom: 20,
        textAlign: 'right',
        opacity: 0.7,
        color: Colors.secondary,
    },
    btn: {
        paddingHorizontal: 30,
        width: '100%',
    },
});
