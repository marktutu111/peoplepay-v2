import React, { Component } from 'react';
import {View, StyleSheet, FlatList,Text,ImageBackground,Image,Alert} from 'react-native';
import Header from '../../components/Header';
import HeaderTxt1 from '../../components/HeaderTxt1';
import TxtInput from '../../components/TxtInput';
import {ScrollView} from 'react-native-gesture-handler';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import { AuthConsumer } from '../../states/auth.state';
import { TransactionContext } from '../../states/transactions.state';



class CustomerDetailScreen extends Component {

    static contextType=TransactionContext;
    viewLoaded=false;

    state={
        loading:false,
        amount:'',
    }

    componentDidMount(){
        this.viewLoaded=true;
        this.setState(
            {
                ...this.state,...this.props.navigation.getParam('data')
            }
        );
    }


    componentWillUnmount(){
        this.viewLoaded=false;
    }


    render(){

        const {transaction}=this.context.state;
        const pushTransaction=this.context.pushTransaction;

        const getImage=()=>{
            switch (this.state.name) {
                case 'VODAFONE':
                    return require(
                        '../../assets/images/airtime/vodafone.jpg'
                    );
                case 'MTN':
                    return require(
                        '../../assets/images/airtime/mtn.png'
                    );
                default:
                    return require(
                        '../../assets/images/airtime/airteltigo.jpg'
                    )
            }
        }

        return (
            <View style={styles.screen}>
                <Header pressTo={()=>this.props.navigation.goBack()} />
                <AuthConsumer>
                    {
                        ({state})=>{
                            this.user=state['user'];
                        }
                    }
                </AuthConsumer>
                <ScrollView
                    style={styles.scroll}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag">
                    <HeaderTxt1 txt1={''} style={{margin:20}}/>
                    <View style={styles.Body}>
                        {transaction.transaction_type !== 'AT' && <View 
                            style={{
                                borderRadius:20,
                                marginTop:20,
                                borderColor:'#ccc',
                                borderWidth:1,
                                marginHorizontal:45,
                                alignItems:'center',
                                padding:30,
                                overflow:'hidden'
                            }}
                        >
                            <Image
                                style={styles.background}
                                source={require('../../assets/images/bg1.png')}
                                resizeMode="contain"
                            />
                            <Text style={{paddingBottom:20}}>ACCOUNT DETAILS</Text>
                            <View style={{
                                flexDirection:'row',
                                justifyContent:'space-between',
                                alignItems:'center',
                                width:'100%'
                            }}>
                                <View>
                                    <Text style={styles.text}>NAME:</Text>
                                    <Text style={styles.text}>CHARGE:</Text>
                                </View>
                                <View>
                                    <Text style={styles.text}>{this.state.name}</Text>
                                    <Text style={styles.text}>{this.state.charge}</Text>
                                </View>
                            </View>
                        </View>}
                        {transaction.transaction_type==='AT' && <View style={{
                            justifyContent:'center',
                            alignItems:'center'
                        }}>
                            <View style={styles.image}>
                                    <Image
                                        source={getImage()}
                                        style={{
                                            width:'100%',
                                            height:'100%'
                                        }}
                                    />
                                </View>
                                <View style={{marginVertical:20}}>
                                    <Text style={{textAlign:'center'}}>Airtime Topup</Text>
                                    <Text style={{fontSize:30,paddingVertical:2,textAlign:'center'}}>
                                        {this.state.name}
                                    </Text>
                                </View>
                        </View>}
                        <View style={styles.TxtInput}>
                            <TxtInput
                                keyboardType="number-pad"
                                IconName="hash"
                                IconType="feather"
                                placeholder="amount"
                                onChangeText={v=>this.state.amount=v.trim()}
                            />
                            <View style={{paddingVertical: 20}}>
                                <LoginPrimaryButton
                                    loading={this.state.loading}
                                    title="Continue"
                                    pressTo={()=>{
                                        if(!this.state.amount || this.state.amount<=0){
                                            return;
                                        };
                                        pushTransaction(
                                            {
                                                amount:this.state.amount,
                                                customerId:this.user._id
                                            },()=>{
                                                this.props.navigation.navigate(
                                                    'ChooseSource',
                                                    { 
                                                        page:'SenderDetailsMoMo',
                                                        data:{ page: 'ConfirmTransaction' }
                                                    }
                                                )
                                            }
                                        )
                                    }}
                                />
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }

};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
    },
    Image: {
        height: 80,
        width: 80,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    TxtInput: {
        paddingHorizontal: '10%',
        paddingVertical: 10,
    },
    text:{
        paddingVertical:5,
        color:'#000',
        fontWeight:'bold'
    },
    background:{
        position:'absolute',
        height: 250,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        backgroundColor: 'white',
        opacity:0.3
    },
    image:{
        width:80,
        height:80,
        justifyContent:'center',
        alignItems:'center',
        borderColor:'#ddd',
        borderWidth:1,
        borderRadius:5,
        overflow:'hidden'
    }
});
export default CustomerDetailScreen;
