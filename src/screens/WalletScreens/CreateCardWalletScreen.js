import React, {Component} from 'react';
import {View, Text, StyleSheet, Alert} from 'react-native';
import Colors from '../../constants/colors';
import Header from '../../components/Header';

import {ScrollView} from 'react-native-gesture-handler';
import TxtInput from '../../components/TxtInput';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import ErrorText from '../../components/ErrorText';
import {AuthConsumer} from '../../states/auth.state';
import { TransactionContext } from '../../states/transactions.state';
import { LiteCreditCardInput } from 'react-native-credit-card-input';


class CreateCardWalletScreen extends Component {


    static contextType=TransactionContext;


    state={
        loading: false,
        account_name: '',
        account_type:'card',
        account_nameValid:false,
        card:{},
        status:{}
    };


    _onChange=formData=>{
        try {
            const {values,status}=formData;
            this.setState(
                {
                    card:values,
                    status:status
                }
            )
        } catch (err) {}
    }


    validate=(text,type)=>{
        if (type==='accountname') {
            if (text === '' || !/^[a-zA-Z]+/.test(text)) {
                this.setState({
                    account_nameValid:false,
                });
            } else {
                this.setState({
                    account_nameValid:true,
                });
            }
        }
    };


    send=async()=>{
        try {

            const {updateState}=this.context;
            const {_id}=this.user;
                if (!_id || !this.password) {
                    return;
                }
                const {
                    account_name,
                    card,
                    status
                }=this.state;
                Object.keys(status).forEach(key=>{
                    if(status[key]==='incomplete'){
                        throw Error(
                            `Please enter card ${key}`
                        )
                    }
                });
                let number=card['number'].replace(/\s+/g, '');
                let [month,year]=card['expiry'].split('/');
                let securityCode=card['cvc'];
                this.setState({loading:true});
                let data=Object.assign(
                    {
                        customerId:_id,
                        payment_account_type:'card',
                        payment_account_name:account_name,
                        payment_account_issuer:card.type,
                        payment_account_number:`0000 0000 0000 ${card.number.substring(card.number.length-4,card.number.length)}`,
                        card_transfer_data:{number,month,year,securityCode},
                        amount:'1',
                        transaction_type:'CA',
                        description:'Card Authorization',
                        temp_card_encription_key:this.password
                    }
                );
                updateState(
                    {
                        returnPage:'Wallets',
                        transaction:data
                    },()=>this.props.navigation.navigate('ConfirmTransaction')
                )
            } catch (err) {
                this.setState(
                    {
                        loading: false,
                    },
                    () => Alert.alert('Oops!','Something went wrong,we could not process your request.'),
                );
            }
        };


    render=()=>{

        return (
            <AuthConsumer>
                {({state}) => {
                    this.user=state.user;
                    this.password=state.password;
                    return (
                        <View style={styles.screen}>
                            <Header
                                pressTo={()=>this.props.navigation.goBack()}
                            />
                            <ScrollView style={styles.scroll}>
                            <View style={{
                                justifyContent:'flex-start',
                                alignItems:'center',
                                marginVertical:50
                            }}>
                                <Text style={{fontSize:25,fontWeight:'200'}}>Please enter your Card Details</Text>
                            </View>
                                <View style={styles.Body}>
                                    <TxtInput
                                        style={!this.state.account_nameValid? {borderColor:'red',borderWidth:0.2}: null}
                                        IconName="user"
                                        IconType="antdesign"
                                        placeholder="Wallet  Name"
                                        onChangeText={v=>{
                                            this.setState(
                                                {
                                                    account_name:v.trim(),
                                                }
                                            );
                                            this.validate(v,'accountname');
                                        }}
                                    />

                                    {this.state.account_numberValid && (
                                        <ErrorText
                                            message={
                                                this.state.account_numberValid
                                            }
                                        />
                                    ) }

                                    <View style={{marginVertical:20}}>
                                        <Text style={{padding:10,color:'#6d6d6d'}}>Enter your card details</Text>
                                        <LiteCreditCardInput
                                            autoFocus
                                            inputStyle={styles.input}
                                            validColor={'black'}
                                            invalidColor={'red'}
                                            placeholderColor={'darkgray'}
                                            onFocus={this._onFocus}
                                            onChange={this._onChange}
                                        />
                                    </View>

                                    <View style={styles.btn}>
                                        <LoginPrimaryButton
                                            title="Create"
                                            loading={this.state.loading}
                                            pressTo={this.send.bind(this)}
                                        />
                                    </View>
                                </View>
                            </ScrollView>
                        </View>
                    );
                }}
            </AuthConsumer>
        );
    };
}


const styles=StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
        paddingHorizontal: 30,
    },
    ToSPP: {
        fontFamily: 'Roboto-Bold',
        color: Colors.tertiary,
        fontSize: 12,
        opacity: 0.6,
        paddingTop: 20,
    },
    ToSPP1: {
        fontFamily: 'Roboto-Bold',
        color: Colors.tertiary,
        fontSize: 12,
        opacity: 0.6,
        paddingVertical: 5,
    },
    btn: {
        paddingVertical: 50,
        paddingHorizontal: 30,
    },
    switch: {
        alignSelf: 'center',
        marginTop: 20,
        marginBottom: 20,
    },
    container: {
        backgroundColor: '#F5F5F5',
        marginTop:60,
        marginHorizontal:20,
        marginVertical:50,
    backgroundColor:'#fff'
    },
    label: {
        color: 'black',
        fontSize: 12,
    },
    input: {
        fontSize: 16,
        color: 'black',
    },
});

export default CreateCardWalletScreen;
