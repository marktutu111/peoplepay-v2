import React, { Component } from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Colors from '../../constants/colors';
import CardThumbnail2 from '../../components/CardThumbnail2';
import {ScrollView} from 'react-native-gesture-handler';
import Header from '../../components/Header';
import { TransactionContext } from '../../states/transactions.state';
import HeaderText from '../../components/HeaderText';


class ChooseWalletScreen extends Component {

    static contextType=TransactionContext;


    render(){

        const {updateState}=this.context;

        return (
            <View style={styles.screen}>
                <Header pressTo={()=>this.props.navigation.goBack()} />
                <ScrollView style={styles.scroll}>
                    <HeaderText
                        title1="Choose Your"
                        title2="Account Type"
                        pressTo={()=>props.navigation.goBack()}
                    />
                    <View style={styles.Body}>
                        <View>
                            <CardThumbnail2
                                iconname="mobile"
                                icontype="entypo"
                                title="Mobile Money Account"
                                subtitle="Add your mobile money account"
                                pressTo={()=>{
                                    updateState(
                                        {
                                            returnPage:'ChooseWallet'
                                        },()=>{
                                            this.props.navigation.navigate(
                                                'CustomPicker',{
                                                    page:'CreateBankWallet',
                                                    type:'momo',
                                                    key:['account_issuer','account_issuer_name','account_issuer_image']
                                                }
                                            )
                                        }
                                    )
                                }}
                            />
                            <CardThumbnail2
                                iconname="credit-card"
                                icontype="entypo"
                                title="Debit Card"
                                subtitle="Add your debit card"
                                pressTo={()=>{
                                    this.props.navigation.navigate(
                                        'CreateCardWalletScreen'
                                    )
                                }}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }



};

const styles=StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    HeaderTextMain: {
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingTop: 30,
        paddingBottom: 20,
        // flexDirection: 'row',
    },
    HeaderText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.secondary,
        // textAlign: 'center',
        opacity: 0.8,
    },
    HeaderText1: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.primary,
        // textAlign: 'center',
        opacity: 0.8,
        bottom: 20,
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
        paddingHorizontal: '5%',
        marginTop: 50,
    },
    mainContainer: {
        flex: 1,
        flexWrap: 'wrap',
        flexDirection: 'row',
    },
    ViewAllContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginBottom: 10,
        alignSelf: 'flex-end',
        marginHorizontal: 20,
    },
    ViewAllText: {
        textAlign: 'center',
        // backgroundColor: Colors.primary,,
        paddingHorizontal: 15,
        paddingVertical: 10,
        color: Colors.tetiary,

        fontSize: 12,
        fontFamily: 'Roboto-Medium',
    },
    cardItemlabel: {
        textAlign: 'center',
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
    },
    containerStyle: {
        padding: 10,
        borderWidth: 0,
        // marginBottom:5,
        marginTop: 10,
    },
    cardrow: {
        width: '100%',
        justifyContent: 'space-evenly',
        flexDirection: 'row',
    },
});
export default ChooseWalletScreen;
