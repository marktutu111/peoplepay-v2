import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator,
    FlatList,
    Alert,
    Dimensions,
    Platform
} from 'react-native';
import Colors from '../../constants/colors';
import Header from '../../components/Header';
import {Icon} from 'react-native-elements';
import {AuthConsumer} from '../../states/auth.state';
import service from '../../services/wallet.service';
import colors from '../../constants/colors';
import HeaderText from '../../components/HeaderText';
import BeneficiaryTiles from '../../components/BeneficiaryTiles';
import { WalletConsumer } from '../../states/wallets.state';
const {height}=Dimensions.get('screen');



// change cont declaration name and export default name
class CreateWalletScreen extends Component {

    subscription=null;
    isLoaded=false;

    state={
        loading: false,
        wallets:[],
        issuers:[],
    };

    componentDidMount=()=>{
        this.isLoaded=true;
        this.subscription=this.props.navigation.addListener('willFocus',this.getWallets);
    };


    componentWillUnmount(){
        this.subscription.remove();
        this.isLoaded=false;
    }

    
    deleteAccount=async(id)=>{
        try {
            this.isLoaded && this.setState({loading:true});
            const response = await service.deleteWallet(id);
            if (!response.success) {
                throw Error(response.message);
            };
            this.isLoaded && this.setState(
                {
                    loading:false
                },()=>{
                    Alert.alert(
                        'Delete Successful', 
                        'Your wallet was deleted successfully'
                    );
                    this.getWallets();
                }
            )
        } catch (err) {
            Alert.alert(
                'Delete Failed', 
                'Sorry we could not delete account'
            );
            this.isLoaded && this.setState({loading: false});
        }
    };

    render=()=>{
        return (
            <View style={styles.screen}>
                <AuthConsumer>
                    {({state}) => {
                        this.user = state.user;
                        this.password = state.password;
                    }}
                </AuthConsumer>
                <Header text="Accounts" pressTo={()=>this.props.navigation.goBack()} />
                <HeaderText title1="My Saved Accounts" />
                <View style={styles.Body}>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={styles.main}
                        onPress={() => {
                            this.props.navigation.navigate('ChooseWallet');
                        }}>
                        <View style={styles.ViewAllContainer}>
                            <Text style={styles.ViewAllText}>
                                Add My Account
                            </Text>
                            <Icon
                                name="arrow-right"
                                type="feather"
                                size={16}
                                color={Colors.tetiary}
                                containerStyle={styles.BottomIcon}
                            />
                        </View>
                    </TouchableOpacity>
                    <View style={styles.Container}>
                        <WalletConsumer>
                            {
                                ({state,getWallets})=>{
                                    this.getWallets=getWallets;
                                    return(
                                        <FlatList
                                            ListEmptyComponent={()=>{
                                                return state.loading ? (
                                                    <ActivityIndicator
                                                        color={colors.secondary}
                                                        size={'large'}
                                                    />
                                                ) : (
                                                    <Text
                                                        style={{
                                                            margin: 50,
                                                            fontSize: 20,
                                                            fontFamily: 'Roboto-Bold',
                                                        }}>
                                                        You have not saved any account,
                                                    </Text>
                                                );
                                            }}
                                            data={state.wallets}
                                            extraData={state}
                                            keyExtractor={(k,i)=>i.toString()}
                                            renderItem={({item})=>{
                                                return (
                                                    item && <BeneficiaryTiles
                                                        title={item.name ?? item.account_name}
                                                        number={item.account_number}
                                                        issuer={item.account_issuer_name}
                                                        imageUri={{ uri:item.account_issuer_image }}
                                                        showDelete
                                                        disabled
                                                        {...item}
                                                        delete={()=>{
                                                            Alert.alert(
                                                                'Delete Account',
                                                                `You have requested to delete your account with name ${item.account_name}`,
                                                                [
                                                                    {
                                                                        text: 'Yes, Delete',
                                                                        onPress: () =>
                                                                            this.deleteAccount(
                                                                                item._id,
                                                                            ),
                                                                    },
                                                                    {
                                                                        text: 'No, Cancel',
                                                                        onPress: null,
                                                                    },
                                                                ],
                                                            );
                                                        }}
                                                    />
                                                );
                                            }}
                                        />
                                    )
                                }
                            }
                        </WalletConsumer>
                    </View>
                </View>
            </View>
        );
    };
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    HeaderTextMain: {
        color: Colors.secondary,
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingVertical: 30,
        paddingBottom: 20,
    },
    HeaderText: {
        fontFamily: 'Roboto-ExtraBold',
        fontSize: 30,
        color: Colors.secondary,
        textAlign: 'left',
        opacity: 0.8,
    },
    Body: {
        justifyContent: 'center',
    },
    Container: {
        paddingBottom:height/4,
        ...Platform.select(
            {
                android:{
                    paddingBottom:height/3.4
                }
            }
        )
    },
    mainContainer: {
        flex: 1,
        flexWrap: 'wrap',
        flexDirection: 'row',
    },
    ViewAllContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        paddingHorizontal: 10,
        marginBottom: 10,
        // width: '40%',
        alignSelf: 'flex-end',
        marginHorizontal: 20,
    },
    ViewAllText: {
        textAlign: 'center',
        // backgroundColor: Colors.primary,,
        paddingHorizontal: 5,
        paddingVertical: 10,
        color: Colors.tetiary,
        fontSize: 12,
        fontFamily: 'Roboto-Medium',
    },
    cardItemlabel: {
        textAlign: 'center',
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
    },
    containerStyle: {
        padding: 10,
        borderWidth: 0,
        // marginBottom:5,
        marginTop: 10,
    },
    cardrow: {
        width: '100%',
        justifyContent: 'space-evenly',
        flexDirection: 'row',
    },
});

export default CreateWalletScreen;
