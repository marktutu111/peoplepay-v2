import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    ScrollView,
    Alert,
} from 'react-native';

import Colors from '../../constants/colors';
import Header from '../../components/Header';
import WithOTP from '../../components/WithOtp.component';
import HeaderText from '../../components/HeaderText';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import otpService from '../../services/otp.service';
import {AuthConsumer} from '../../states/auth.state';
import { Platform } from 'react-native';
import _WService from "../../services/wallet.service";
import { TransactionContext } from '../../states/transactions.state';



class VerifyWalletAccount extends Component {

    isViewLoaded=false;
    static contextType=TransactionContext;

    state={
        loading:false,
        otp:'',
    };

    componentDidMount=()=>{
        this.isViewLoaded=true;
        const data=this.props.navigation.state.params;
        this.setState(
            {...data},()=>{
                this.props.sendOtp(
                    data.account_number
                )
            }
        )
    }


    send=async()=>{
        try {
            const {transaction}=this.context.state;
            const {_id}=this.user;
                if (!_id) {
                    return;
                }
                const {
                    account_name,
                    account_number,
                    account_type,
                    password,
                    otp
                }=this.state;
                if(!otp || otp.length<=0){
                    throw Error(
                        'Invalid OTP'
                    )
                }
                this.setState({loading:true});
                let data=Object.assign(
                    {
                        customerId: _id,
                        account_type:account_type,
                        account_name:account_name,
                        account_number:account_number,
                        account_issuer:transaction.account_issuer,
                        account_issuer_name:transaction.account_issuer_name,
                        account_issuer_image:transaction.account_issuer_image,
                        password:password,
                        otp:otp
                    }
                );
                const response=await _WService.addWallet(data);
                if(!response.success){
                    throw Error(
                        response[
                            'message'
                        ]
                    )
                }
                this.setState(
                    {
                        loading:false,
                    },
                    ()=>{
                        this.props.navigation.navigate('Wallets');
                        Alert.alert(
                            'success',
                            'Your account has been successfully added',
                        );
                    },
                );
            } catch (err) {
                this.setState(
                    {
                        loading: false,
                    },
                    () => Alert.alert('Oops!',err.message),
                );
            }
        };


    render() {
        return (
            <View style={styles.screen}>
                <AuthConsumer>
                    {({state}) => {
                        this.user=state.user;
                    }}
                </AuthConsumer>
                <Header pressTo={()=>this.props.navigation.goBack()} />
                <ScrollView
                    style={styles.scroll}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag">
                    <HeaderText title1="Verify" />
                    <View style={styles.Body}>
                        <Text style={styles.infoText}>
                            Verification code has been sent to your phone via
                            sms. Please check your mobile phone.
                        </Text>

                        <TextInput
                            style={styles.textfield}
                            onChangeText={v=>this.state.otp=v.trim()}
                            maxLength={5}
                        />

                        <View style={styles.resendContainer}>
                            <TouchableOpacity
                                onPress={()=>this.props.sendOtp(this.state.account_number,true)}>
                                <Text style={styles.resendText}>RESEND</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.btn}>
                            <LoginPrimaryButton
                                loading={this.state.loading || this.props.loading}
                                title="Continue"
                                pressTo={this.send.bind(this)}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },

    Body: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 30,
    },
    textfield: {
        borderBottomColor: Colors.tetiary,
        borderBottomWidth: 1,
        // width: '100%',
        // marginHorizontal: 30,
        alignItems: 'center',
        fontFamily: 'Roboto-Regular',
        color: Colors.tetiary,
        textAlign: 'center',
        width: '80%',
        ...Platform.select(
            {
                ios:{
                    height:50
                }
            }
        ),
        letterSpacing:10,
    },
    infoText: {
        width: '80%',
        paddingVertical: 20,
        textAlign: 'center',
        opacity: 0.5,
    },
    resendContainer: {
        width: '100%',
        alignItems: 'flex-end',
        paddingHorizontal: 30,
        paddingVertical: 15,
    },
    resendText: {
        fontFamily: 'Roboto-Bold',
        width: '100%',
        paddingBottom: 20,
        textAlign: 'right',
        opacity: 0.7,
        color: Colors.secondary,
    },
    btn: {
        paddingHorizontal: 30,
        width: '100%',
    },
});


export default WithOTP(VerifyWalletAccount);