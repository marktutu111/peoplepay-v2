import React, {Component} from 'react';
import {View, Text, StyleSheet, Alert} from 'react-native';
import Colors from '../../constants/colors';
import Header from '../../components/Header';
import {ScrollView} from 'react-native-gesture-handler';
import TxtInput from '../../components/TxtInput';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import {CustomExample} from '../../components/customPicker';
import ErrorText from '../../components/ErrorText';
import HeaderTxt1 from '../../components/HeaderTxt1';
import HeaderText from '../../components/HeaderText';
import { TransactionContext } from '../../states/transactions.state';
import { AuthConsumer } from '../../states/auth.state';



// change cont declaration name and export default name
class WalletTopupScreen extends Component {

    static contextType=TransactionContext;


    state={
        amt: '',
        amtErr: ''
    };



    Validate = type => {
        const {amt,phone}=this.state;
        let rjx = /^\+?\(?(([2][3][3])|[0])?\)?[-.\s]?\d{2}[-.\s]?\d{3}[-.\s]?\d{4}?$/,
            amtrjx = /\d{1,5}/;
        switch (type) {
            case 'number':
                if (rjx.test(phone) === false) {
                    this.setState({
                        phoneErr: 'Please enter a valid mobile number',
                    });
                } else {
                    this.setState({
                        phoneErr: '',
                    });
                }
                break;
            case 'amt':
                if (amtrjx.test(amt) === false || amt <= 0) {
                    this.setState({
                        amtErr: 'Please enter valid amount',
                    });
                } else {
                    this.setState({
                        amtErr: '',
                    });
                }
                break;
            default:
                return;
        }
    };

    render() {


        const {updateState}=this.context;

        return (
            <View style={styles.screen}>
                <AuthConsumer>
                    {
                        ({state})=>{
                            this.user=state.user;
                        }
                    }
                </AuthConsumer>
                <Header pressTo={() => this.props.navigation.goBack()} />
                <ScrollView style={styles.scroll}>
                    <HeaderText title1="Enter amount" title2="You want to deposit" />
                    <View style={styles.Body}>
                        <View style={styles.TxtInputContainer}>
                            <TxtInput
                                placeholder="Amount"
                                IconName="money"
                                IconType="font-awesome"
                                keyboardType="number-pad"
                                onChangeText={v=>this.setState({amt:v.trim()})}
                                onBlur={()=>this.Validate('amt')}
                            />
                            {this.state.amtErr ? (
                                <ErrorText message={this.state.amtErr} />
                            ) : null}
                        </View>
                        <View style={styles.buttonContainer}>
                            <LoginPrimaryButton
                                title={`PAY GHS${this.state.amt?this.state.amt + '.00':'0.00'}`}
                                pressTo={()=>{
                                    try {
                                        if(this.state.amt==='' || this.state.amt<=0){
                                            throw Error(
                                                'Invalid amount provided'
                                            )
                                        }
                                        updateState(
                                            {
                                                returnPage:'Home',
                                                transaction:{
                                                    transaction_type:'WF',
                                                    description:'PeoplesPay Wallet Topup',
                                                    amount:this.state.amt,
                                                    customerId:this.user._id
                                                }
                                            },()=>{
                                                this.props.navigation.navigate(
                                                    'ChooseSource',
                                                    {
                                                        page:'SenderDetailsMoMo',
                                                        showWallet:false,
                                                        data:{ page: 'ConfirmTransaction' }
                                                    }
                                                )
                                            }
                                        )
                                    } catch (err) {
                                        Alert.alert(
                                            'Topup Error',
                                            err.message
                                        )
                                    }
                                }}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}



const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    HeaderTextMain: {
        textAlign: 'left',
        paddingHorizontal: 30,
        // paddingTop: 40,
        paddingBottom: 20,
        paddingTop: 20,
    },
    HeaderText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 30,
        color: Colors.secondary,
        // textAlign: 'center',
        opacity: 0.8,
    },
    HeaderText1: {
        fontFamily: 'Roboto-Bold',
        fontSize: 30,
        color: Colors.primary,
        // textAlign: 'center',
        opacity: 0.8,
        bottom: 15,
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
    },
    dropdwonContainer: {
        paddingVertical: 10,
    },
    dropdwonContainerText: {
        justifyContent: 'center',
        paddingHorizontal: 30,
        flex: 1,
        fontFamily: 'Roboto-Bold',
        opacity: 0.5,
        color: '#262626',
    },
    TxtInputContainer: {
        paddingHorizontal:30,
        marginVertical:15
    },
    buttonContainer: {
        alignItems: 'center',
        paddingVertical: 20,
        paddingHorizontal: 20,
    },

    lineContainer: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        paddingHorizontal: 30,
    },
    line: {
        borderColor: Colors.secondary,
        borderBottomWidth: 1,
    },
    textOr: {
        fontFamily: 'Roboto-Bold',
        color: Colors.secondary,
    },
});

export default WalletTopupScreen;
