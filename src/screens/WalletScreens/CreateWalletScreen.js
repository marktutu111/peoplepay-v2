import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Colors from '../../constants/colors';
import Header from '../../components/Header';
// import BoldCard from '../../components/BoldCard';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import {ScrollView} from 'react-native-gesture-handler';

// change cont declaration name and export default name
const CreateWalletScreen = props => {
    return (
        <View style={styles.screen}>
            {/* Header of the Screen */}
            <Header pressTo={() => props.navigation.goBack()} />
            <ScrollView
                style={styles.scroll}
                keyboardShouldPersistTaps="always"
                keyboardDismissMode="on-drag">
                {/* Screen Title */}
                <View style={styles.HeaderTextMain}>
                    <Text style={styles.HeaderText}>Create</Text>
                    <Text style={styles.HeaderText}>Wallet </Text>
                </View>
                {/* Screen Main Content */}
                <View style={styles.Body}>
                    <View style={styles.row1}>
                        {/* <BoldCard
                            title="Bank Wallet"
                            IconName="bank"
                            IconType="font-awesome"
                            pressTo={() =>
                                props.navigation.navigate('CreateBankWallet')
                            }
                        />
                        <BoldCard
                            title="Mobile Wallet"
                            IconName="mobile1"
                            IconType="antdesign"
                            pressTo={() =>
                                props.navigation.navigate('CreateMobileWallet')
                            }
                        /> */}
                    </View>
                    <Text style={styles.ToSPP}>
                        By tapping on 'CREATE', you agree to the Terms of
                    </Text>
                    <Text style={styles.ToSPP1}>use and Privacy Policy</Text>
                    <View style={styles.btn}>
                        <LoginPrimaryButton
                            title="Create"
                            pressTo={() =>
                                props.navigation.navigate('CreateBankWallet')
                            }
                        />
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    HeaderTextMain: {
        color: Colors.secondary,
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingVertical: 15,
        paddingBottom: 20,
    },
    HeaderText: {
        fontFamily: 'Roboto-ExtraBold',
        fontSize: 30,
        color: Colors.secondary,
        textAlign: 'left',
        opacity: 0.8,
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
    },
    row1: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 70,
        paddingVertical: 20,
    },
    ChooseWallet: {
        paddingHorizontal: 30,
        fontFamily: 'Roboto-Bold',
        color: Colors.secondary,
        fontSize: 16,
        opacity: 0.7,
        paddingVertical: 10,
    },
    ToSPP: {
        paddingLeft: 30,
        paddingRight: 60,
        fontFamily: 'Roboto-Bold',
        color: Colors.secondary,
        fontSize: 12,
        opacity: 0.6,
        paddingTop: 20,
    },
    ToSPP1: {
        paddingLeft: 30,
        paddingRight: 60,
        fontFamily: 'Roboto-Bold',
        color: Colors.secondary,
        fontSize: 12,
        opacity: 0.6,
        paddingVertical: 5,
    },
    btn: {
        // alignItems: 'center',
        paddingVertical: 50,
        paddingHorizontal: 30,
    },
});

export default CreateWalletScreen;
