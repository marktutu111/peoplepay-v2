import React, {Component} from 'react';
import {View,StyleSheet} from 'react-native';
import Colors from '../../constants/colors';
import Header from '../../components/Header';
import HeaderText from '../../components/HeaderText';

import {ScrollView} from 'react-native-gesture-handler';
import TxtInput from '../../components/TxtInput';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
// change cont declaration name and export default name
import ErrorText from '../../components/ErrorText';
import {AuthConsumer} from '../../states/auth.state';
import { TransactionContext } from '../../states/transactions.state';

const placeHolders={
    momo:'Select network',
    bank:'Select bank',
};


class CreateBankWalletScreen extends Component {


    static contextType=TransactionContext;


    state={
        loading: false,
        account_name: '',
        account_nameValid:true,
        account_number: '',
        account_type:'momo',
        account_numberValid:true,
        password:''
    };


    validate=(text,type)=>{
        const {account_type}=this.state;
        if (account_type === 'momo' && type === 'accountnumber') {
            if (!/^[0]?\d{9}$/.test(text)) {
                this.setState({
                    account_numberValid: false,
                });
            } else {
                this.setState({
                    account_numberValid: true,
                });
            }
        }
        if (account_type === 'bank' && type === 'accountnumber') {
            if (!/^\d{10,17}$/.test(text)) {
                this.setState({
                    account_numberValid: false,
                });
            } else {
                this.setState({
                    account_numberValid: true,
                });
            }
        }
        if (type === 'accountname') {
            if (text === '' || !/^[a-zA-Z]+/.test(text)) {
                this.setState({
                    account_nameValid: false,
                });
            } else {
                this.setState({
                    account_nameValid: true,
                });
            }
        }
        if (type === 'issuer') {
            if (text === '') {
                this.setState({
                    account_issuerValid: false,
                });
            } else {
                this.setState({
                    account_issuerValid: true,
                });
            }
        }
    };

    addWallet=()=>{
        const {
            account_number,
            account_name,
        }=this.state;
        if(account_number==='' || account_name===''){
            return;
        }
        this.props.navigation.navigate(
            'VerifyWalletAccount',this.state
        )
    }



    render=()=>{

        const {transaction}=this.context.state;

        return (
            <AuthConsumer>
                {({state}) => {
                    this.user=state.user;
                    this.state.password=state.password;
                    return (
                        <View style={styles.screen}>
                            <Header pressTo={()=>this.props.navigation.goBack()}/>
                            <ScrollView style={styles.scroll}>
                                <HeaderText title1="Add my" title2={`${ transaction.account_issuer_name } Account`}/>
                                <View style={styles.Body}>
                                    <TxtInput
                                        style={
                                            !this.state.account_nameValid
                                                ? {
                                                      borderColor: 'red',
                                                      borderWidth: 3,
                                                  }
                                                : null
                                        }
                                        IconName="user"
                                        IconType="antdesign"
                                        placeholder="Account  Name"
                                        onChangeText={v=>{
                                            this.setState({
                                                account_name:v.trim(),
                                            });
                                            this.validate(v, 'accountname');
                                        }}
                                    />

                                    <TxtInput
                                        style={
                                            !this.state.account_numberValid
                                                ? {
                                                      borderColor:'red',
                                                      borderWidth: 3,
                                                  }
                                                : null
                                        }
                                        IconName="hash"
                                        IconType="feather"
                                        placeholder="Mobile Money Number"
                                        onChangeText={v => {
                                            this.setState({account_number:v.trim()});
                                            this.validate(v,'accountnumber');
                                        }}
                                        keyboardType="number-pad"
                                    />

                                    {this.state.account_numberValid && (
                                        <ErrorText
                                            message={
                                                this.state.account_numberValid
                                            }
                                        />
                                    ) }
                                    <View style={styles.btn}>
                                        <LoginPrimaryButton
                                            title="Create"
                                            loading={this.state.loading}
                                            pressTo={this.addWallet.bind(this)}
                                        />
                                    </View>
                                </View>
                            </ScrollView>
                        </View>
                    );
                }}
            </AuthConsumer>
        );
    };
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },

    Body: {
        justifyContent: 'center',
        flex: 1,
        paddingHorizontal: 30,
    },
    ToSPP: {
        fontFamily: 'Roboto-Bold',
        color: Colors.tertiary,
        fontSize: 12,
        opacity: 0.6,
        paddingTop: 20,
    },
    ToSPP1: {
        fontFamily: 'Roboto-Bold',
        color: Colors.tertiary,
        fontSize: 12,
        opacity: 0.6,
        paddingVertical: 5,
    },
    btn: {
        // alignItems: 'center',
        paddingVertical: 50,
        paddingHorizontal: 30,
    },
});

export default CreateBankWalletScreen;
