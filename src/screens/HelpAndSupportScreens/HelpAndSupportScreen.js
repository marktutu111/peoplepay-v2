import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    Linking
} from 'react-native';

import Colors from '../../constants/colors';
import Header from '../../components/Header';
import SupportButtons from '../../components/SupportButtons';
import HeaderText from '../../components/HeaderText';

const HelpAndSupportScreen=props=>{
    return (
        <View style={styles.screen}>
            <Header text="Help And Support" pressTo={()=>props.navigation.navigate('Home')} />
            <ScrollView
                style={styles.scroll}
                keyboardShouldPersistTaps="always"
                keyboardDismissMode="on-drag">

                <View style={styles.Body}>
                    <View>

                            <Text style={styles.menuTitle}>Support</Text>

                            <SupportButtons 
                                text="+233 244 255 909" 
                                icon="phone-in-talk"
                            />

                            <SupportButtons 
                                text="info@peoplespay.com.gh" 
                                icon="email"
                                onPress={()=>Linking.openURL('mailto:admin@peoplepay.com.gh').catch(err=>null)}
                            />
                            
                            <SupportButtons 
                                text="Facebook" 
                                icon="facebook-with-circle"
                                type="entypo"
                                onPress={()=>Linking.openURL('https://www.facebook.com/profile.php?id=100063399302025').catch(err=>null)}
                            />

                            <SupportButtons 
                                text="Twitter" 
                                icon="twitter-with-circle"
                                type="entypo"
                                onPress={()=>Linking.openURL('https://twitter.com/PeoplesPay1?t=hPOaPb4nc9ll6OjuBZHH3g&s=09').catch(err=>null)}
                            />

                            <SupportButtons 
                                text="Instagram" 
                                icon="instagram"
                                type="entypo"
                                onPress={()=>Linking.openURL('https://www.instagram.com/peoplespay').catch(err=>null)}
                            />

                            <Text style={styles.menuTitle}>Help</Text>

                            <SupportButtons 
                                text="Frequently Asked Questions" 
                                icon="questioncircleo"
                                type="antdesign"
                                onPress={()=>props.navigation.navigate('FAQ')}
                            />

                            <SupportButtons 
                                text="Terms and Conditions" 
                                icon="ios-newspaper-sharp"
                                type="ionicon"
                                onPress={()=>Linking.openURL(`https://peoplespay.com.gh/terms.html`).catch(err=>null)}
                            />

                            <SupportButtons 
                                text="Privacy Policy" 
                                icon="block"
                                type="entypo"
                                onPress={()=>props.navigation.navigate('Privacy')}
                            />

                    </View>
                </View>
            </ScrollView>
        </View>
    );
};
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: Colors.accent,
    },
    scroll: {
        // flex: 1,
        backgroundColor: 'white',
    },
    HeaderTextMain: {
        color: Colors.secondary,
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingVertical: 15,
        paddingBottom: 20,
    },
    HeaderText: {
        fontSize: 30,
        color: Colors.secondary,
        textAlign: 'left',
        opacity: 0.8,
    },
    Body: {
        paddingHorizontal: 30,
        flex: 1,
    },
    menuTitle: {
        marginVertical: 20,
        color:'#000',
        opacity: 0.8,
    },
    sectionContainer: {
        marginTop: 10,
    },
    topLink: {
        borderBottomWidth: 1,
        borderColor: '#ACACAC',
        width: '100%',
        flexDirection: 'row',
    },
    bottomLink: {
        borderBottomWidth: 1,
        borderColor: '#ACACAC',
        width: '100%',
        flexDirection: 'row',
    },
    menuContainer: {
        paddingVertical: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        // borderColor: '#262626',
        borderBottomWidth: 1,
        borderColor: '#ccc',
        width: '100%',
    },
    menuContainerbottom: {
        paddingVertical: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        // borderColor: '#262626',
        borderColor: '#ACACAC',
        width: '100%',
        alignItems:'center'
    },
    menuleftItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderColor: '#262626',
    },
    menuText: {
        fontSize: 16,
        fontFamily: 'Roboto-Medium',
        color: '#262626',
        paddingLeft: 20,
    },
    helpContainer: {
        paddingVertical: 30,
    },
});
export default HelpAndSupportScreen;
