import React, {Component} from 'react';
import {Text, StyleSheet, View,ScrollView} from 'react-native';
import Accordion from 'react-native-collapsible/Accordion';
import Header from '../../components/Header';
import HeaderTxt1 from '../../components/HeaderTxt1';
import Colors from '../../constants/colors';
import {Icon} from 'react-native-elements';

const SECTIONS = [
    {
        title: 'What is PeoplesPay?',
        content:
            'PeoplesPay is a mobile application built with to support multiple types of payments such as; transfer of money to any bank account and mobile money wallet in Ghana, payment of ECG, DSTV, Ghana Water etc, including various fees for various schools and church collections. In addition, with PeoplesPay app, users can purchase various gift cards such as iTunes, Netflix, Xbox, PlayStation etc and also make payment for purchases of goods and service at shops and supermarkets with the GhQR-Code. Payment for all transactions on PeoplesPay app can be made from the user’s mobile money wallet (all networks) or bank payment card such as Visa and MasterCard (all bank cards).',
    },
    {
        title: 'How do I Sign-Up?',
        content:
            'Download the PeoplesPay app from the Google Play store or AppStore on your device and follow the steps . Firstly,select the “create an account” option. Secondly,create an account by entering your details and creating your password.Thirdly,tap the signup button.Thats it and you are done. You can now login using your email and password.',
    },
    {
        title: 'How do I start transactions?',
        content:
            'On PeoplesPay, you can pay for your transactions primarily in two ways; with mobile money wallet or your bank account. Either way you can choose to save the account details under the “my accounts” for ease of access next time otherwise enter it while making a transaction. This implies that one can process a transaction on behalf of a third party while the third party would be required to enter the transaction password that will be sent to his own device.',
    },
    {
        title: 'Why will my transaction fail?',
        content:
            'Payments can fail for multiple reasons. There are common issues that could cause a payment to fail. These include; - invalid account number or mobile money number, insufficient funds. In other scenarios, the transaction time-out due to poor data/internetconnection and many others. Users are encouraged to call our help line on 0302254340 for further assistance.',
    },
    {
title:"How safe is PeoplesPay?",
content:"PeoplesPay is very secured as we have implemented multiple layers of authorizations before transactions can be completed. This called “two-factor” authentication method which has been put in place to protect the user’s transaction against unauthorized access by unknown persons. The “two-factor” authentication requires an OTP (One-Time-Pin) to be sent to the users registered phone number and the user will be required to enter this one-time-pin before the transaction can be completed. Note that this one-time-pin will become invalid if not utilized within 60 seconds.In addition to the OTP security, PeoplesPay also supports the use of finger print and facial scan for the purpose of accessing the app and also for transaction authorization. Note that the user’s finger print and facial scan must have been enrolled during the user’s registration process. These additional security options are available for ONLY Android and IOS devices that support biometric (finger print) enrollment and facial scan"
    },
    {
title:"I am  able to see my transaction history?",
content:"Yes. Users will be able to view all their transactions by tapping on the “Transaction History” icon located on the home screen."
    },
    {
        title:"When I have issues who do I contact to get it resolved?",
        content:"When issues are encountered by users, they can either contact our help line on +233302254340 / +233244255909 or by clicking on the “Online Support” icon from within the PeoplesPay app to chat with our support team."
            },
];

export class FAQsScreen extends Component {
    state = {
        activeSections: [],
    };

    // _renderSectionTitle = section => {
    //     return (
    //         <View style={styles.content}>
    //             <Text>{section.content}</Text>
    //         </View>
    //     );
    // };

    _renderHeader=section=>{
        return (
            <View style={styles.header}>
                <Text style={styles.headerText}>{section.title}</Text>
                <Icon
                    name="plus"
                    type="feather"
                    size={25}
                    color="#262626"
                    style={styles.iconLeft}
                />
            </View>
        );
    };

    _renderContent=section=>{
        return (
            <View style={styles.content}>
                <Text style={styles.contentText}>{section.content}</Text>
            </View>
        );
    };

    _updateSections = activeSections => {
        this.setState({activeSections});
    };

    render() {
        return (
            <ScrollView contentContainerStyle={styles.screen}>
                <Header pressTo={() => this.props.navigation.navigate("Home")} />
                <HeaderTxt1 txt1="FA" txt2="Qs" />
                <View style={styles.Body}>
                    <Accordion
                            sections={SECTIONS}
                            activeSections={this.state.activeSections}
                            renderSectionTitle={this._renderSectionTitle}
                            renderHeader={this._renderHeader}
                            renderContent={this._renderContent}
                            onChange={this._updateSections}
                        />
                </View>
            </ScrollView>
        );
    }
}

export default FAQsScreen;

const styles = StyleSheet.create({
    screen: {
        // backgroundColor:Colors.accent,
    },

    Body: {
        paddingHorizontal:10,
        paddingVertical:20
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20,
        marginBottom: 10,
        backgroundColor: Colors.accent,
        shadowColor: Colors.tetiary,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.21,
        shadowRadius: 2,
        elevation: 4,
        width: '100%',
        height: 60,
        borderRadius: 5,
    },
    headerText: {
        fontSize: 16,
        fontFamily: 'Roboto-Medium',
        paddingVertical: 5,
        color: 'rgba(0,0,0,0.7)',
    },
    contentText: {
        paddingBottom: 20,
    },
    contentText: {
        fontSize: 16,
        fontFamily: 'Roboto-regular',
        lineHeight: 25,
        color: 'rgba(0,0,0,0.7)',
        paddingHorizontal: 5,
    },
});
