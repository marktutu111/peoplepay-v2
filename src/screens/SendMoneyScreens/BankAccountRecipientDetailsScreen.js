import React, {Component} from 'react';
import {View, Text, StyleSheet, Alert} from 'react-native';
import Colors from '../../constants/colors';
import Header from '../../components/Header';
import HeaderText from '../../components/HeaderText';

import {ScrollView} from 'react-native-gesture-handler';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import service from '../../services/transactions.service';
import { TransactionContext } from "../../states/transactions.state";


// change cont declaration name and export default name
class RecipientDetails extends Component {

    static contextType=TransactionContext;

    state={
        loading:false,
    };

    componentDidMount=()=>{
        this.getRecipient();
    };


    getRecipient=async()=>{
        try {
            const {transaction}=this.context.state;
            this.setState({loading:true});
            const data={
                DestBank:transaction.recipient_account_issuer,
                AccountToCredit:transaction.recipient_account_number,
                Narration:transaction.description,
            };
            const response=await service.getNec(data);
            if (response.success) {
                const name=response.data.NameToCredit;
                return this.setState({
                    loading:false,
                    recipient_account_name:name,
                });
            }
            throw Error(response.message);
        } catch (err) {
            this.setState(
                {
                    loading: false,
                },
                ()=>{
                    Alert.alert('Dear Customer',err.message);
                    this.props.navigation.goBack();
                },
            );
        }
    };

    render=()=>{

        const {state,pushTransaction}=this.context;

        return (
            <View style={styles.screen}>
                <Header pressTo={()=>this.props.navigation.goBack()} />
                <ScrollView
                    style={styles.scroll}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag">
                    <HeaderText title1="Confirm" title2="Recipient Details" />
                    <View style={styles.Body}>
                        <View style={styles.recipientContainer}>
                            <View style={styles.textContainer1}>
                                <Text style={styles.text1}>Account Number</Text>
                                <Text style={styles.text1}>Account Name</Text>
                                <Text style={styles.text1}>Amount</Text>
                                <Text style={styles.text1}>Description</Text>
                            </View>
                            <View style={styles.textContainer2}>
                                <Text style={styles.text2}>
                                    {state.transaction.recipient_account_number}
                                </Text>
                                <Text style={styles.text2}>
                                    {this.state.recipient_account_name ||
                                        'finding account...'}
                                </Text>
                                <Text style={styles.text2}>
                                    {state.transaction.amount}
                                </Text>
                                <Text style={styles.text2}>
                                    {state.transaction.description}
                                </Text>
                            </View>
                        </View>
                        <View style={styles.buttonContainer}>
                            <LoginPrimaryButton
                                loading={this.state.loading}
                                title="Proceed"
                                pressTo={() => {
                                    if (this.state.recipient_account_name && this.state.recipient_account_name !== '') {
                                        pushTransaction(
                                            {
                                                recipient_account_name:this.state.recipient_account_name
                                            },()=>{
                                                this.props.navigation.navigate(
                                                    'ChooseSource',
                                                    {
                                                        page:'SenderDetailsMoMo',
                                                        data:{ page:'ConfirmTransaction' }
                                                    }
                                                );
                                            }
                                        )
                                    }
                                }}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    };
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
    },
    recipientContainer: {
        flexDirection: 'row',
        backgroundColor: Colors.accent,
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        marginTop: 10,
        marginBottom: 30,
        opacity: 0.7,
    },
    textContainer: {
        flexDirection: 'row',
        paddingVertical: 3,
        backgroundColor: Colors.accent,
    },
    text1: {
        paddingHorizontal: 20,
        backgroundColor: Colors.accent,
        fontFamily: 'Roboto-Regular',
        textAlign: 'left',
        paddingVertical: 3,
        fontSize: 16,
        opacity: 0.7,
    },
    text2: {
        paddingHorizontal: 20,
        color: Colors.secondary,
        opacity: 0.7,
        fontFamily: 'Roboto-Medium',
        paddingVertical: 3,
        fontSize: 16,
    },
    heading: {
        textAlign: 'left',
        paddingBottom: 10,
        marginBottom: 10,
        borderBottomWidth: 0.8,
        borderColor: Colors.primary,
        marginHorizontal: 30,
        fontFamily: 'Roboto-Bold',
        color: Colors.tetiary,
        opacity: 0.7,
    },
    dropdownContainer: {
        paddingVertical: 10,
        marginHorizontal: 30,
    },
    TxtInputContainer: {
        paddingHorizontal: 30,
    },
    buttonContainer: {
        // alignItems: 'center',
        paddingVertical: 20,
        paddingHorizontal: 30,
    },
});

export default RecipientDetails;
