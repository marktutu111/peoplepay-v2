import React, {Component} from 'react';
import {View, Text, StyleSheet,Dimensions} from 'react-native';
import Colors from '../../constants/colors';
import Header from '../../components/Header';
import HeaderText from '../../components/HeaderText';
import {ScrollView} from 'react-native-gesture-handler';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import {AuthConsumer} from '../../states/auth.state';
import TxtInput from '../../components/TxtInput';
import service from '../../services/wallet.service';
import { TransactionContext } from '../../states/transactions.state';



const {width}=Dimensions.get('screen');



export default class SenderDetailsMoMoScreen extends Component {

    static contextType=TransactionContext;

    returnpage=null;
    isViewLoaded=false;

    state={
        loading: false,
        option: '',
        selected_account: '',
        payment_account_name: '',
        payment_account_number: '',
        payment_account_numberValid: true,
        payment_account_issuer: '',
        payment_account_issuer_name:'',
        payment_account_type: '',
        cardtype:'',
        wallets: [],
        page:'SM',
        openModal:false
    };


    componentDidMount(){
        this.isViewLoaded=true;
        this.getPage();
    }


    componentWillUnmount(){
        this.isViewLoaded=false;
    }


    getPage(){
        const page=this.props.navigation.getParam('page');
        switch (page) {
            case 'ECARDS':
                this.returnpage='GiftCards';
                break;
            case 'QRP':
                this.returnpage='QRPay';
                break;
            case 'PAYBILLS':
                this.returnpage='PayBills';
                break;
            case 'AIRTIME':
                this.returnpage='AccountRef';
                break;
            case 'VCI':
                this.returnpage='VirtualCard';
                break;
            case 'VCT':
                this.returnpage='VirtualCard';
                break;
            case 'VCTR':
                this.returnpage='VirtualCard';
                break;
            case 'WF':
                this.returnpage='WalletTopup';
                break;
            default:
                this.returnpage='SendMoneyScreen';
                break;
        }
    }


    getIssuer(){
        switch (this.state.payment_account_issuer) {
            case '300591':
                return 'Enter your momo number';
            case '300594':
                return 'Enter vodafone cash number';
            case '300592':
                return 'Enter airteltigo cash number';
            default:
                return 'Enter account number';
        }
    }


    getWallets=async()=>{
        try {
            const {_id}=this.user;
            if (!_id) {
                return;
            }
            this.setState({loading: true});
            const response=await service.getWallets({
                customerId:_id,
                password:this.password,
            });
            if (!response.success) {
                throw Error(
                    response.message
                );
            }
            this.setState({
                loading:false,
                wallets:response.data,
            });
        } catch (err) {this.setState({loading:false})}
    };


    validate=(text,type)=>{
        const {payment_account_type} = this.state;
        if (payment_account_type === 'momo' && type === 'accountnumber') {
            if (!/^[0]?\d{9}$/.test(text)) {
                this.setState({
                    payment_account_numberValid: false,
                });
            } else {
                this.setState({
                    payment_account_numberValid: true,
                });
            }
        }
        if (payment_account_type === 'bank' && type === 'accountnumber') {
            if (!/^\d{10,17}$/.test(text)) {
                this.setState({
                    payment_account_numberValid: false,
                });
            } else {
                this.setState({
                    payment_account_numberValid: true,
                });
            }
        }
        if (type === 'accountname') {
            if (text === '' || !/^[a-zA-Z]+/.test(text)) {
                this.setState({
                    payment_account_nameValid: false,
                });
            } else {
                this.setState({
                    payment_account_nameValid: true,
                });
            }
        }
        if (type === 'issuer') {
            if (text === '') {
                this.setState({
                    payment_account_issuerValid: false,
                });
            } else {
                this.setState({
                    recipent_account_issuerValid: true,
                });
            }
        }
    };



    validateTransaction=()=>{
        try {

            const {pushTransaction}=this.context;
            const { payment_account_number}=this.state;
            if(payment_account_number==='')return;
            pushTransaction(
                {
                    payment_account_number:payment_account_number
                },()=>{
                    this.props.navigation.navigate(
                        'Verify'
                    )
                }
            )
        } catch (err) {}
    }



    render() {

        const {state}=this.context;

        return (
            <View style={styles.screen}>
                <AuthConsumer>
                    {({state}) => {
                        this.user=state.user;
                        this.password=state.password;
                    }}
                </AuthConsumer>
                {/* Header of the Screen */}
                <Header text="Transaction Details" pressTo={()=>this.props.navigation.goBack()} />
                <ScrollView
                    style={styles.scroll}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag">
                    <HeaderText 
                        title1={`Enter your ${state.transaction.payment_account_issuer_name}`} 
                        title2="Mobile Money Number" 
                    />
                    <View style={styles.Body}>
                        <TxtInput
                            style={
                                !this.state.payment_account_numberValid
                                    ? {
                                        borderColor: 'red',
                                        borderWidth: 3,
                                    }
                                    : null
                            }
                            placeholder={this.getIssuer()}
                            IconName="hash"
                            IconType="feather"
                            keyboardType="number-pad"
                            contact={true}
                            defaultValue={this.state.payment_account_number}
                            onChangeText={v=>{
                                this.setState(
                                    {
                                        payment_account_number:v.trim(),
                                    },()=>{
                                        this.validate(v,'accountnumber')
                                    }
                                );
                            }}
                            onContact={()=>this.setState(
                                {
                                    openModal:true
                                }
                            )}
                        />                        

                        <View style={styles.buttonContainer}>
                            <LoginPrimaryButton
                                loading={this.state.loading}
                                title="Send"
                                pressTo={this.validateTransaction.bind(this)}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor:'white'
    },
    scroll: {
        flex: 1,
        backgroundColor:'white',
    },
    Body: {
        justifyContent:'center',
        paddingHorizontal:width/14,
        paddingVertical:15,
        flex: 1,
    },
    heading: {
        marginTop:10,
        textAlign:'left',
        paddingBottom: 10,
        marginBottom: 10,
        borderBottomWidth: 0.8,
        borderColor: Colors.primary,
        marginHorizontal: 30,
        fontFamily: 'Roboto-Bold',
        color: Colors.tetiary,
        opacity: 0.7,
    },
    dropdownContainer: {
        paddingVertical: 10,
        marginHorizontal: 30,
    },

    TxtInputContainer: {
        paddingHorizontal: 30,
    },
    buttonContainer: {
        paddingVertical:20
    },
});
