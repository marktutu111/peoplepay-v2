import React, { Component, useContext } from 'react';
import {View, Text, StyleSheet, Dimensions,ActivityIndicator,TouchableOpacity} from 'react-native';
import Colors from '../../constants/colors';
import { TransactionContext } from "../../states/transactions.state";

import Header from '../../components/Header';
import TertiaryButton from '../../components/TertiaryButton';
import {ScrollView} from 'react-native-gesture-handler';
import HeaderText from '../../components/HeaderText';
import { AuthConsumer } from '../../states/auth.state';
import { WalletConsumer } from '../../states/wallets.state';
import BeneficiaryTiles from '../../components/BeneficiaryTiles';
import colors from '../../constants/colors';
import CardThumbnail2 from '../../components/CardThumbnail2';
import {Icon} from 'react-native-elements';



class ChooseSource extends Component {


    static contextType=TransactionContext;
    
     state={
         showbtn:true
     }

    componentDidMount(){
        this.getWallets().then(()=>{
             if(this.wallets && this.wallets.length>0){
                this.setState({showbtn:false})
             } 
        })
        
    }

    render(){

        const {pushTransaction}=this.context;
        const data=this.props.navigation.state.params;
        const {showWallet}=data
        
        
         
        return (
            <View style={styles.screen}>
                <Header pressTo={()=>this.props.navigation.navigate('Home')} />
                <ScrollView
                    style={styles.scroll}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag">
                    <HeaderText title1="Choose Account" title2="You want to Pay from" />
                    {  this.state.showbtn &&
                        <TouchableOpacity
                        activeOpacity={0.7}
                        style={styles.main}
                        onPress={() => {
                            this.props.navigation.navigate('ChooseWallet');
                        }}>
                        <View style={styles.ViewAllContainer}>
                            <Text style={styles.ViewAllText}>
                                Add My Account
                            </Text>
                            <Icon
                                name="arrow-right"
                                type="feather"
                                size={16}
                                color={Colors.tetiary}
                                containerStyle={styles.BottomIcon}
                            />
                        </View>
                    </TouchableOpacity>
                    }
                    <AuthConsumer>
                        {
                            ({state})=>{
                                const {fullname,phone}=state.user;
                                return (                
                                    <View style={styles.Body}>
                                        {/* <View style={styles.cardrow}>
                                            <CardThumbnail2
                                                iconname="mobile"
                                                icontype="entypo"
                                                title="Mobile Money Account"
                                                subtitle="new mobile money account"
                                                pressTo={()=>{
                                                    pushTransaction(
                                                        {
                                                            payment_account_type:'momo',
                                                            payment_account_name:fullname
                                                        },()=>{
                                                            this.props.navigation.navigate(
                                                                'CustomPicker',
                                                                {
                                                                    type:'momo',...data,
                                                                    key:['payment_account_issuer','payment_account_issuer_name'] 
                                                                }
                                                            )
                                                        }
                                                    )
                                                }}
                                            />
                                        </View> */}
                                        {showWallet===false? null:
                                           <View style={styles.cardrow}>
                                            {/* <CardThumbnail2
                                                iconname="credit-card"
                                                IconType="antdesign"
                                                title="Debit Card"
                                                subtitle="Pay from a new card"
                                                pressTo={()=>{
                                                    pushTransaction(
                                                        { 
                                                            payment_account_type:'card',
                                                            payment_account_name:fullname
                                                        },()=>{
                                                            this.props.navigation.navigate(
                                                                'DebitCard'
                                                            )
                                                        }
                                                    )
                                                }}
                                            /> */}
                                            <CardThumbnail2
                                                iconname="wallet"
                                                icontype="font-awesome-5"
                                                title="PeoplesPay Wallet"
                                                subtitle="Pay from your peoplespay account"
                                                pressTo={()=>{
                                                    pushTransaction(
                                                        {
                                                            payment_account_name:fullname,
                                                            payment_account_type:'wallet',
                                                            payment_account_number:phone,
                                                            payment_account_issuer:"PPAY"
                                                        },()=>{
                                                            this.props.navigation.navigate(
                                                                'ConfirmTransaction'
                                                            )
                                                        }
                                                    )
                                                }}
                                            />
                                        </View>}

                                        <WalletConsumer>
                                            {
                                                ({state,getWallets})=>{
                                                    this.getWallets=getWallets;
                                                    this.wallets=state.wallets;
                                                    switch (state.loading) {
                                                        case true:
                                                            return (
                                                                <ActivityIndicator 
                                                                    size="large"
                                                                    color={colors.secondary}
                                                                />
                                                            )
                                                        default:
                                                            return state.wallets.map((wallet,i)=>{
                                                                return(
                                                                    <BeneficiaryTiles
                                                                        key={i.toString()}
                                                                        title={wallet.name || wallet.account_name}
                                                                        number={wallet.account_number}
                                                                        issuer={wallet.account_issuer_name}
                                                                        {...wallet}
                                                                        imageUri={{ uri:wallet.account_issuer_image }}
                                                                        pressTo={()=>{
                                                                            let _payload={};
                                                                            switch (wallet.account_type) {
                                                                                case 'card':
                                                                                    _payload={
                                                                                        payment_account_type:wallet.account_type,
                                                                                        payment_account_name:wallet.name ?? wallet.account_name,
                                                                                        payment_account_issuer:wallet.account_issuer,
                                                                                        payment_account_number:wallet.account_number,
                                                                                        card_transfer_data:wallet.card_encrypted
                                                                                    }
                                                                                    break;
                                                                                default:
                                                                                    _payload={
                                                                                        payment_account_type:wallet.account_type,
                                                                                        payment_account_name:wallet.name ?? wallet.account_name,
                                                                                        payment_account_issuer:wallet.account_issuer,
                                                                                        payment_account_number:wallet.account_number
                                                                                    }
                                                                                    break;
                                                                            }
                                                                            pushTransaction(_payload,()=>{
                                                                                    this.props.navigation.navigate(
                                                                                        'ConfirmTransaction'
                                                                                    )
                                                                                }
                                                                            )
                                                                        }}
                                                                    />
                                                                )
                                                            })
                                                    }
                                                }
                                            }
                                        </WalletConsumer>
                                    </View>
                                )
                            }
                        }
                    </AuthConsumer>
                </ScrollView>
            </View>
        );
    }
};


const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },

    Body: {
        marginTop:20,
        justifyContent: 'center',
        flex: 1,
    },
    cardrow: {
        paddingHorizontal:30,
        // paddingVertical: SCREEN_HEIGHT > 700 ? 5 : 0,
        justifyContent: 'space-evenly',
    },
    heading: {
        textAlign: 'left',
        color: 'white',
        opacity: 1,
        fontFamily: 'Roboto-Bold',
        fontSize: 18,
        textAlign: 'center',
        textAlignVertical: 'center',
        padding: 5,
    },
    HeaderTextMain: {
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingTop: 30,
        paddingBottom: 20,
        flexDirection: 'row',
    },
    HeaderText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.secondary,
        // textAlign: 'center',
        opacity: 0.8,
    },
    HeaderText1: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.primary,
        // textAlign: 'center',
        opacity: 0.8,
        // bottom: 20,
    },
    ViewAllText: {
        textAlign: 'center',
        // backgroundColor: Colors.primary,,
        paddingHorizontal: 5,
        paddingVertical: 10,
        color: Colors.tetiary,
        fontSize: 12,
        fontFamily: 'Roboto-Medium',
    },
    ViewAllContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        paddingHorizontal: 10,
        marginBottom: 10,
        // width: '40%',
        alignSelf: 'flex-end',
        marginHorizontal: 20,
    },
});

export default ChooseSource;
