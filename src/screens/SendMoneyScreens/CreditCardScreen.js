import React, {Component} from 'react';
import {StyleSheet,View,Text,Dimensions,Image,Alert} from 'react-native';
import {LiteCreditCardInput} from 'react-native-credit-card-input';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
const {height,width}=Dimensions.get('screen');
import Header from '../../components/Header';
import { TransactionContext } from '../../states/transactions.state';
import _TService from "../../services/transactions.service";


export default class CreditCard extends Component {

    static contextType=TransactionContext;

    state={
        card:{},
        status:{}
    };
    
    _onChange=formData=>{
        try {
            const {values,status}=formData;
            this.setState(
                {
                    card:values,
                    status:status
                }            
            )
        } catch (err) {}
    }


    pay(){
        try {
            const {pushTransaction}=this.context;
            let {status,card}=this.state;
            Object.keys(status).forEach(key=>{
                if(status[key]==='incomplete'){
                    throw Error(
                        `Please enter card ${key}`
                    )
                }
            });
            let number=card['number'].replace(/\s+/g, '');
            let [month,year]=card['expiry'].split('/');
            let securityCode=card['cvc'];
            const data={
                card_transfer_data:{number,month,year,securityCode},
                payment_account_number:`${card.number.substring(0,4)} 0000 0000 ${card.number.substring(card.number.length-4,card.number.length)}`,
                payment_account_issuer:card.type
            }
            pushTransaction(data,()=>{
                    this.props.navigation.navigate(
                        'ConfirmTransaction'
                    )
                }
            )
        } catch (err) {
            Alert.alert(
                'Invalid Card',
                'Sorry we could not process your request,kindly try again after sometime.'
            )
        }
    }

    render() {
        return (
            <View style={{flex:1,backgroundColor:'#fff'}}>
                <Header text="Card Payment" pressTo={()=>this.props.navigation.goBack()} />
                <View style={s.container}>
                    <Image style={{  width:150,height:50}} source={require('../../assets/images/visa-mastercard.png')}/>
                    <Text style={{padding:10,fontSize:17,fontWeight:'200'}}>Enter Card Details</Text>
                    <View style={{marginTop:20}}>
                        <LiteCreditCardInput
                            autoFocus
                            inputStyle={s.input}
                            validColor={'black'}
                            invalidColor={'red'}
                            placeholderColor={'darkgray'}
                            onFocus={this._onFocus}
                            onChange={this._onChange}
                        />
                    </View>
                    <LoginPrimaryButton 
                        title="PAY"
                        pressTo={this.pay.bind(this)}
                        style={{
                            marginVertical:50
                        }}
                    />
                </View>
            </View>
        );
    }
}

const s = StyleSheet.create({
    switch: {
        alignSelf: 'center',
        marginTop: 20,
        marginBottom: 20,
    },
    container: {
        backgroundColor: '#F5F5F5',
        marginTop:60,
        marginHorizontal:20,
        marginVertical:50,
    backgroundColor:'#fff'
    },
    label: {
        color: 'black',
        fontSize: 12,
    },
    input: {
        fontSize: 16,
        color: 'black',
    },
});
