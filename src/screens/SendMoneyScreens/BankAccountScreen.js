import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Header from '../../components/Header';
import {ScrollView} from 'react-native-gesture-handler';
import TxtInput from '../../components/TxtInput';
import HeaderText from '../../components/HeaderText';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import {AuthConsumer} from '../../states/auth.state';
import {CustomExample} from '../../components/customPicker';
import LazyContactPicker from '../../components/contacts-picker.component';
import { TransactionContext } from '../../states/transactions.state';




// change cont declaration name and export default name
class BankAccount extends Component {

    static contextType=TransactionContext;

    state={
        customerId: '',
        amount: '',
        amountValid: true,
        recipient_account_number: '',
        recipient_account_numberValid: true,
        recipient_account_issuer: '',
        recipient_account_issuerValid: true,
        recipient_account_type: '',
        recipient_account_typeValid: true,
        description:'',
        descriptionValid: true,
        recipient_type: '',
        openModal:false
    };

    validate=(text,type)=>{
        const {recipient_account_type} = this.state;
        if (recipient_account_type === 'momo' && type === 'accountnumber') {
            if (!/^[0]?\d{9}$/.test(text)) {
                this.setState({
                    recipient_account_numberValid: false,
                });
            } else {
                this.setState({
                    recipient_account_numberValid: true,
                });
            }
        }
        if (recipient_account_type === 'bank' && type === 'accountnumber') {
            if (!/^\d{10,17}$/.test(text)) {
                this.setState({
                    recipient_account_numberValid: false,
                });
            } else {
                this.setState({
                    recipient_account_numberValid: true,
                });
            }
        }
        if (type === 'accountname') {
            if (text === '' || !/^[a-zA-Z]+/.test(text)) {
                this.setState({
                    recipient_account_nameValid: false,
                });
            } else {
                this.setState({
                    recipient_account_nameValid: true,
                });
            }
        }
        if (type === 'desc') {
            if (!/^[a-zA-Z]+/.test(text)) {
                this.setState({
                    descriptionValid: false,
                });
            } else {
                this.setState({
                    descriptionValid: true,
                });
            }
        }
        if (type === 'amount') {
            if (text === '' || !/^[0-9]+/.test(text)) {
                this.setState({
                    amount_Valid: false,
                });
            } else {
                this.setState({
                    amount_Valid: true,
                });
            }
        }
        if (type === 'issuer') {
            if (text === '') {
                this.setState({
                    recipient_account_issuerValid: false,
                });
            } else {
                this.setState({
                    recipent_account_issuerValid: true,
                });
            }
        }
    };


    componentDidMount=()=>{};



    render=()=>{

        const {state,pushTransaction}=this.context;
        const {transaction}=state;

        return (
            <View style={styles.screen}>
                <AuthConsumer>
                    {({state})=>{
                        const {user}=state;
                        this.state.customerId=user._id;
                    }}
                </AuthConsumer>
                <Header text="Transaction Details" pressTo={()=>this.props.navigation.goBack()} />
                <ScrollView
                    style={styles.scroll}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag">
                    <HeaderText
                        title1="Send money"
                        title2={`to ${transaction.recipient_account_issuer_name} Account`}
                    />
                    <View style={styles.Body}>
                        <View style={styles.TxtInputContainer}>
                            {transaction.recipient_type === 'onetime' && (
                                <TxtInput
                                    style={
                                        !this.state
                                            .recipient_account_numberValid
                                            ? {
                                                  borderColor: 'red',
                                                  borderWidth: 3,
                                              }
                                            : null
                                    }
                                    placeholder={
                                        this.state.recipient_account_type ===
                                        'momo'
                                            ? 'Phone Number'
                                            : 'Account number'
                                    }
                                    keyboardType="number-pad"
                                    IconName="hash"
                                    IconType="feather"
                                    contact={true}
                                    defaultValue={this.state.recipient_account_number}
                                    onChangeText={v=>{
                                        this.setState({
                                            recipient_account_number:v.trim(),
                                        });
                                        this.validate(v,'accountnumber');
                                    }}
                                    onContact={()=>this.setState(
                                        {
                                            openModal:true
                                        }
                                    )}
                                />
                            )}
                            <TxtInput
                                style={
                                    !this.state.amountValid
                                        ? {
                                              borderColor: 'red',
                                              borderWidth: 3,
                                          }
                                        : null
                                }
                                keyboardType="number-pad"
                                placeholder="Amount"
                                IconName="money"
                                IconType="font-awesome"
                                onChangeText={v => {
                                    this.setState({amount:v.trim()});
                                    this.validate(v, 'amount');
                                }}
                            />
                            <TxtInput
                                style={
                                    !this.state.descriptionValid
                                        ? {
                                              borderColor: 'red',
                                              borderWidth: 3,
                                          }
                                        : null
                                }
                                placeholder="Description"
                                IconName="description"
                                IconType="material"
                                onChangeText={v => {
                                    this.setState({description: v});
                                    this.validate(v, 'desc');
                                }}
                            />
                        </View>
                        <View style={styles.buttonContainer}>
                            <LoginPrimaryButton
                                title="Verify Details"
                                pressTo={()=>{
                                    try {
                                        let data={};
                                        switch (transaction.recipient_type) {
                                            case 'beneficiary':
                                                data={
                                                    customerId:this.state.customerId,
                                                    amount:this.state.amount,
                                                    description:this.state.description,
                                                }
                                                break;
                                            default:
                                                data={
                                                    customerId:this.state.customerId,
                                                    amount:this.state.amount,
                                                    description:this.state.description,
                                                    recipient_account_number:this.state.recipient_account_number || ""
                                                };
                                                break;
                                        }
                                        Object.values(data).forEach(v=>{
                                            if(!v || v===''){
                                                throw Error(
                                                    'INVALID DATA'
                                                )
                                            }
                                        });
                                        pushTransaction(data,()=>{
                                                this.props.navigation.navigate(
                                                    'BankRecipientDetails'
                                                );
                                            }
                                        )
                                    } catch (err) {
                                        
                                    }
                                }}
                            />
                        </View>
                    </View>
                </ScrollView>
                <LazyContactPicker 
                    openModal={this.state.openModal}
                    getContact={(v)=>{
                        try {
                            this.setState(
                                {
                                    openModal:false,
                                    recipient_account_number:v.trim()
                                },()=>{
                                    this.validate(v,'accountnumber');
                                }
                            )
                        } catch (err) {}
                    }}
                    closeModal={
                        ()=>this.setState(
                            {
                                openModal:false
                            }
                        )
                    }
                />
            </View>
        );
    };
}
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
    },
    heading: {
        textAlign: 'left',
        paddingVertical:5,
        // marginBottom: 10,
        // borderBottomWidth: 0.8,
        // borderColor: Colors.primary,
        color: 'white',
        opacity: 1,
        fontFamily: 'Roboto-Bold',
        fontSize:18,
        textAlign: 'center',
        textAlignVertical: 'center',
    },
    dropdownContainer: {
        paddingVertical: 10,
        marginHorizontal: 30,
    },
    TxtInputContainer: {
        paddingHorizontal: 30,
    },
    buttonContainer: {
        // alignItems: 'center',
        paddingVertical: 20,
        paddingHorizontal: 30,
    },
});

export default BankAccount;
