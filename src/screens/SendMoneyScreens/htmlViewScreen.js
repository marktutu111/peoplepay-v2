import React, { Component } from 'react'
import { Text, View,ActivityIndicator,Alert } from 'react-native';
import { WebView } from 'react-native-webview';
import Header from '../../components/Header';
import { TransactionContext } from '../../states/transactions.state';
import _TService from "../../services/transactions.service";


export default class htmlViewScreen extends Component {

    static contextType=TransactionContext;
    $subscription=null;


    state={
        html:'',
        loading:false
    }

    componentDidMount(){
        this.$subscription=setInterval(()=>{this.checkTransactionStatus()},1000*3);
    }


    LoadingIndicatorView() {
        return (
          <ActivityIndicator
            color='#e30e1c'
            size='large'
            style={{
              flex: 1,
              justifyContent:'center'
            }}
          />
        )
      }



      componentWillUnmount(){clearInterval(this.$subscription)};



      checkTransactionStatus=async()=>{
          try {
            const {id,returnPage}=this.props.navigation.state.params;
            if(!id)return this.props.navigation.goBack();
            const response=await _TService.getTransactionStatus(id);
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            switch (response.data.status) {
                case 'reversed':
                case 'paid':
                case 'pc':
                    Alert.alert(
                        'Transaction Processed',
                        'your transaction has been processed successfully'
                    )
                    return this.props.navigation.navigate(
                        returnPage
                    )
                case 'failed':
                    Alert.alert(
                        'Transaction Failed',
                        'Sorry we could not process your card transaction'
                    );
                    return this.props.navigation.navigate(
                        returnPage
                    )
                default:
                    break;
            }
          } catch (err) {

          }
      }


      goBack(){
          try {
            const {returnPage}=this.props.navigation.state.params;
              this.props.navigation.navigate(returnPage);
              clearInterval(this.$subscription);
          } catch (err) {
              
          }
      }



    render() {

        const html=this.props.navigation.getParam('html');

        return (
            <View style={{
                flex: 1,
                backgroundColor:'#fff',
            }}>
                <Header pressTo={this.goBack.bind(this)} />
                { this.state.loading && <ActivityIndicator 
                    size={'large'}
                    color="#000"
                    style={{
                        marginTop:'20%'
                    }}
                />}
                <WebView
                    style={{
                        flex:1,
                        backgroundColor:'#fff',
                        width:'100%'
                    }}
                    onLoadEnd={()=>{this.setState({loading:false});}}
                    onError={e=>this.props.navigation.goBack()}
                    originWhitelist={['*']}
                    renderLoading={this.LoadingIndicatorView}
                    source={{html:html}}
                    ref={ref=>this.ref=ref}
                />
            </View>
        )
    }
}
