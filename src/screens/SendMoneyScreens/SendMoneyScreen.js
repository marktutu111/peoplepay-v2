import React, { Component, useContext } from 'react';
import {View, Text, StyleSheet, Dimensions,TouchableOpacity,Image} from 'react-native';
import Colors from '../../constants/colors';
import { TransactionContext } from "../../states/transactions.state";

import Header from '../../components/Header';
import TertiaryButton from '../../components/TertiaryButton';
import {ScrollView} from 'react-native-gesture-handler';
import HeaderText from '../../components/HeaderText';
import _WService from "../../services/wallet.service";
import { AuthConsumer } from '../../states/auth.state';
import CardThumbnail2 from '../../components/CardThumbnail2';
import BeneficiaryTiles from '../../components/BeneficiaryTiles';
import { BeneficiariesConsumer } from '../../states/beneficiaries.state';

const {width}=Dimensions.get('screen');



class AirtimeAndDataScreen extends Component {

    static contextType=TransactionContext;

    state={
        loading:false
    }

    componentDidMount(){this.getBeneficiaries()};


    render(){
        const {updateState}=this.context;
        return (
            <View style={styles.screen}>
                <AuthConsumer>
                    {
                        ({state})=>{
                            this.user=state.user;
                            this.password=state.password;
                        }
                    }
                </AuthConsumer>
                <Header text="Send Money" pressTo={()=>this.props.navigation.navigate('Home')} />
                <ScrollView
                    style={styles.scroll}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag">
                    <HeaderText 
                        title1='Choose Account' 
                        title2="You want to send money to" 
                    />
                    <View style={styles.Body}>
                        <View style={styles.cardrow}>
                            <CardThumbnail2
                                iconname="mobile"
                                icontype="entypo"
                                title="Mobile Money"
                                subtitle="new mobile money account"
                                pressTo={()=>updateState(
                                        {
                                            returnPage:'SendMoney',
                                            transaction:{
                                                recipient_type:'onetime',
                                                recipient_account_type:'momo',
                                                transaction_type:'SM'
                                            }
                                        },()=>{
                                            this.props.navigation.navigate(
                                                'CustomPicker',{
                                                    page:'BankAccount',
                                                    type:'momo',
                                                    key:['recipient_account_issuer','recipient_account_issuer_name']
                                                }
                                            )
                                        }
                                    )
                                }
                            />
                        </View>
                        <View style={styles.cardrow}>
                        <CardThumbnail2
                            iconname="bank"
                            icontype="material-community"
                            title="Bank Account"
                            subtitle="New bank account"
                            pressTo={()=>updateState(
                                    {
                                        returnPage:'SendMoney',
                                        transaction:{
                                            recipient_type:'onetime',
                                            recipient_account_type:'bank',
                                            transaction_type:'SM'
                                        }
                                    },()=>{
                                        this.props.navigation.navigate(
                                            'CustomPicker',{
                                                page:'BankAccount',
                                                type:'bank',
                                                key:['recipient_account_issuer','recipient_account_issuer_name']
                                            }
                                        )
                                    }
                                )
                            }
                        />
                        </View>
                        <BeneficiariesConsumer>
                            {
                                ({state,getBeneficiaries})=>{
                                    this.getBeneficiaries=getBeneficiaries;
                                    return state.beneficiaries.map((ben,i)=>(
                                        <BeneficiaryTiles
                                            key={i.toString()}
                                            title={ben.account_name}
                                            number={ben.account_number}
                                            issuer={ben.account_issuer_name}
                                            {...ben}
                                            imageUri={{ uri:ben.account_issuer_image }}
                                            pressTo={()=>{
                                                updateState(
                                                    {
                                                        returnPage:'SendMoney',
                                                        transaction:{
                                                            recipient_type:'beneficiary',
                                                            beneficiary:ben._id,
                                                            recipient_account_issuer:ben.account_issuer,
                                                            recipient_account_issuer_name:ben.account_issuer_name,
                                                            recipient_account_number:ben.account_number,
                                                            recipient_account_type:ben.account_type,
                                                            transaction_type:'SM'
                                                        }
                                                    },()=>{
                                                        this.props.navigation.navigate(
                                                            'BankAccount',{
                                                                page:'ChooseSource',
                                                                data:{ page:'SenderDetailsMoMo' }
                                                            }
                                                        )
                                                    }
                                                )
                                            }}
                                        />
                                    ))
                                }
                            }
                        </BeneficiariesConsumer>
                    </View>
                </ScrollView>
            </View>
        );
    }
};


const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },

    Body: {
        marginTop:20,
        justifyContent:'center',
        flex:1,
        paddingBottom:20
    },
    cardrow: {
        paddingHorizontal:30,
        // paddingVertical: SCREEN_HEIGHT > 700 ? 5 : 0,
        justifyContent: 'space-evenly',
    },
    heading: {
        textAlign: 'left',
        // paddingVertical: 10,
        // marginBottom: 10,
        // borderBottomWidth: 0.8,
        // borderColor: Colors.primary,
        color: 'white',
        opacity: 1,
        fontFamily: 'Roboto-Bold',
        fontSize: 18,
        textAlign: 'center',
        textAlignVertical: 'center',
        padding: 5,
    },
    // HeaderText: {
    //     textAlign: 'center',
    //     fontFamily: 'Roboto-Bold',
    //     fontSize: 18,
    //     textTransform: 'uppercase',
    //     color: Colors.accent,
    //     paddingHorizontal: 20,
    // },
    HeaderTextMain: {
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingTop: 30,
        paddingBottom: 20,
        flexDirection: 'row',
    },
    HeaderText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.secondary,
        // textAlign: 'center',
        opacity: 0.8,
    },
    HeaderText1: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.primary,
        // textAlign: 'center',
        opacity: 0.8,
        // bottom: 20,
    },
});

export default AirtimeAndDataScreen;
