import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    ScrollView,
} from 'react-native';

import Colors from '../../constants/colors';
import Header from '../../components/Header';
import HeaderText from '../../components/HeaderText';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';

import {AuthConsumer} from '../../states/auth.state';
import { Platform } from 'react-native';
import { TransactionContext } from '../../states/transactions.state';
import WithOTP from '../../components/WithOtp.component';



class MobileVerificationScreen extends Component {

    isViewLoaded=false;
    static contextType=TransactionContext;

    state={
        loading:false,
        otp:'',
    };

    componentDidMount=()=>{
        this.isViewLoaded=true;
        this.returnpage=this.props.navigation.getParam('page');
        this.sendOtp();
    }


    send=async()=>{
        try {
            if (this.state.otp==='')return;
            const {pushTransaction}=this.context;
            pushTransaction(
                {
                    otp:this.state.otp
                },()=>{
                    this.props.navigation.navigate(
                        'ConfirmTransaction'
                    )
                }
            )
        } catch (err) {

        }
    };


    sendOtp=(resend)=>{
        const {transaction}=this.context.state;
        let number='';
        const {
            payment_account_type,
            payment_account_number,
        }=transaction;
        switch (payment_account_type) {
            case 'momo':
                number=payment_account_number;
                break;
            default:
                number=this.user.phone;
                break;
        }
        this.props.sendOtp(
            number,
            resend
        )
    };


    render() {
        return (
            <View style={styles.screen}>
                <AuthConsumer>
                    {({state}) => {
                        this.user = state.user;
                    }}
                </AuthConsumer>
                <Header pressTo={()=>this.props.navigation.goBack()} />
                <ScrollView
                    style={styles.scroll}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag">
                    <HeaderText title1="Verify" />
                    <View style={styles.Body}>
                        <Text style={styles.infoText}>
                            Verification code has been sent to your phone via
                            sms. Please check your mobile phone.
                        </Text>

                        <TextInput
                            style={styles.textfield}
                            onChangeText={v=>this.state.otp=v.trim()}
                            maxLength={5}
                        />

                        <View style={styles.resendContainer}>
                            <TouchableOpacity
                                onPress={()=>this.sendOtp(true)}>
                                <Text style={styles.resendText}>RESEND</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.btn}>
                            <LoginPrimaryButton
                                loading={this.state.loading}
                                title="Continue"
                                pressTo={this.send.bind(this)}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },

    Body: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 30,
    },
    textfield: {
        borderBottomColor: Colors.tetiary,
        borderBottomWidth: 1,
        alignItems: 'center',
        fontFamily: 'Roboto-Regular',
        color: Colors.tetiary,
        textAlign: 'center',
        width: '80%',
        ...Platform.select(
            {
                ios:{
                    height:50
                }
            }
        ),
        letterSpacing:10,
    },
    infoText: {
        fontFamily: 'Roboto-Medium',
        width: '80%',
        paddingVertical: 20,
        textAlign: 'center',
        opacity: 0.5,
    },
    resendContainer: {
        width: '100%',
        alignItems: 'flex-end',
        paddingHorizontal: 30,
        paddingVertical: 15,
    },
    resendText: {
        fontFamily: 'Roboto-Bold',
        width: '100%',
        paddingBottom: 20,
        textAlign: 'right',
        opacity: 0.7,
        color: Colors.secondary,
    },
    btn: {
        paddingHorizontal: 30,
        width: '100%',
    },
});


export default WithOTP(MobileVerificationScreen);