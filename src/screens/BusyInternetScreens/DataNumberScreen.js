import React, { Component } from 'react'
import {View, StyleSheet,Text,Image} from 'react-native';
import Header from '../../components/Header';
import HeaderTxt1 from '../../components/HeaderTxt1';
import TxtInput from '../../components/TxtInput';
import {ScrollView} from 'react-native-gesture-handler';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import { AuthConsumer } from '../../states/auth.state';
import { TransactionContext } from '../../states/transactions.state';
import IconButton from '../../components/IconButton';


export default class DataNumberScreen extends Component {


   static contextType=TransactionContext;

  state={
      bundleDataNumber:'',
  }


   render() {
    const bundleData=this.props.navigation.getParam('bundleData',{});
    const {updateState} = this.context;
    return (
        <View style={styles.screen}>
            <Header pressTo={()=>this.props.navigation.goBack()} text="Data number" />
            <AuthConsumer>
                {
                    ({state})=>{
                        this.user=state['user'];
                    }
                }
            </AuthConsumer>
            <ScrollView
                style={styles.scroll}
                keyboardShouldPersistTaps="always"
                keyboardDismissMode="on-drag">
                <View style={styles.Body}>
                    <View style={{
                        height:100,
                        width:100,
                        justifyContent:'center',
                        alignItems:'center',
                        marginVertical:15
                    }}>
                    <Image style={{height:'100%',width:'100%'}} source={require('../../assets/images/busy.jpg')}/>
                    </View>
                    <Text style={{
                        paddingVertical:2,
                        fontSize:20
                    }}>
                        Provide data number to receive bundle
                    </Text>
                    <View style={styles.TxtInput}>
                        <TxtInput
                            defaultValue={this.state.bundleDataNumber}
                            IconName="hashtag"
                            fa
                            onChangeText={e=>this.state.bundleDataNumber=e.trim()}
                            placeholder="Data number"
                        
                        />
                        <View style={{paddingVertical:20}}>
                            <LoginPrimaryButton
                                title="Next"
                                pressTo={()=>{
                                    const {bundleDataNumber}=this.state;
                                    if(bundleDataNumber === "")return alert('Data number field cannot be empty');
                                    updateState(
                                        {
                                            returnPage:'BusyInternet',
                                            transaction:{
                                                transaction_type:'BI',
                                                customerId : this.user._id,
                                                amount:bundleData.SalesPrice,
                                                description:bundleData.PricePlanRemarks,
                                                bundleDataNumber,
                                                otp:'',
                                                bundleData
                                            },
                                        },()=>{
                                            this.props.navigation.navigate(
                                                'ChooseSource',
                                                { 
                                                    page:'SenderDetailsMoMo',
                                                    data:{ page: 'ConfirmTransaction' }
                                                }
                                            )
                                        }
                                    ) 
                                }}
                            />
                        </View>
                    </View>
                </View>
            </ScrollView>            
        </View>
    );
}

};

const styles = StyleSheet.create({
screen: {
    flex: 1,
    backgroundColor: 'white',
},
scroll: {
    flex: 1,
    backgroundColor: 'white',
},
Body: {
    alignItems:'center',
    justifyContent: 'center',
    flex: 1,
},
TxtInput:{
    paddingVertical: 10,
},
text:{
    paddingVertical:5,
    color:'#000',
    fontWeight:'bold'
},
background:{
    position:'absolute',
    height: 250,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    backgroundColor: 'white',
    opacity:0.3
},
image:{
    width:80,
    height:80,
    justifyContent:'center',
    alignItems:'center',
    borderColor:'#ddd',
    borderWidth:1,
    borderRadius:5,
    overflow:'hidden'
},
headerCont:{
    flexDirection:'row',
    alignItems:'center'
},
contactList:{
    padding:20,
    fontSize:25,
    borderBottomColor:'#ddd',
    borderBottomWidth:1
},
contactText:{
    fontSize:25
}
});

