import React,{Component} from 'react';
import {View,Alert,FlatList,Text,TouchableOpacity,ActivityIndicator,StyleSheet,Image, Dimensions} from 'react-native';
import busyinternetService from '../../services/busyinternet.service';
import Card from '../../components/Card';
import Header from '../../components/Header';
import colors from '../../constants/colors';
import FIcons from "react-native-vector-icons/FontAwesome5";
const {height}=Dimensions.get('screen');



class DataPackagesScreen extends Component{

  state={
    loading:false,
    data:[]
  }
  
    getDataPackages=async()=>{
        try{
          this.setState({loading:true})
           const res = await busyinternetService.getDataPackges();
           if(!res.success){
             this.setState({loading:false})
             return Alert(res.message)
           }
           this.setState({loading:false})
           return this.setState({data:res.data[0]?.Bundle?.BundleList})
        }
        catch(err){
          this.setState({loading:false})
          Alert(err);
        }
    }


    componentDidMount() {
      this.getDataPackages();
  }

  render() {
    
    const renderItem=({item})=>(
      <TouchableOpacity key={item.PricePlanCode} 
      onPress={()=>this.props.navigation.navigate('EnterBundleDataNumber',
       {
        bundleData:{
          PricePlanName:item.PricePlanName,
        PricePlanCode:item.PricePlanCode,
        SalesPrice:item.SalesPrice,
        PricePlanRemarks:item.PricePlanRemarks
        }
        })}>
       <Card style={{ marginVertical:5 }}>
        <View style={{
            flexDirection:'row',
            marginVertical:5
         }}>
          <Text style={{
            color:colors.secondary,
            fontSize:30,
          }}>
           {item.PricePlanName}
          </Text>
        </View>
        <View>
            <Text style={{fontSize:20}}>
              {item.PricePlanRemarks}
            </Text>
         </View>
         <View style={styles.priceContainer}>
          <View style={styles.imageContainer}>
            <Image style={{width:'100%',height:'100%'}} source={require('../../assets/images/busy.jpg')}/>
          </View>
            <View style={styles.amount}>
              <Text style={styles.amountText}>
                GHS{item.SalesPrice}
              </Text>
              <FIcons name='arrow-right' color='green'/>
            </View> 
         </View>
      </Card>
      </TouchableOpacity>
      )

    return (
      <View style={{backgroundColor:'white',height:height,paddingBottom:10}}>
        <Header text="Package Offers" pressTo={() => this.props.navigation.pop()} />
          {this.state.loading ? 
            (
              <ActivityIndicator
                style={{
                      margin: 10
                      }}
                size={'large'}
                color={colors.secondary}
                />
            ):
          <FlatList
            data={this.state.data}
            initialNumToRender={5}
            renderItem={renderItem}
            keyExtractor={item=>item.id}
          />
          }
      </View>
    );
  }
}


const styles = StyleSheet.create({
  amount:{
    width:'auto',
    flexDirection:'row',
    borderRadius:10,
    justifyContent:'flex-end',
    alignItems:'center'
  },
  amountText:{
    fontSize:30,
    color:colors.tetiary,
    padding:5
  },
  imageContainer:{
    justifyContent:'center',
    alignItems:'center',
    width:50,
    height:50
  },
  priceContainer:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between',
    marginVertical:10
  }
})


export default DataPackagesScreen;