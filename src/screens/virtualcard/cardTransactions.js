import React, {Component} from 'react';
import {View, Text, StyleSheet, FlatList,ActivityIndicator} from 'react-native';
import Colors from '../../constants/colors';
import Header from '../../components/Header'
import HeaderTxt1 from '../../components/HeaderTxt1';
import {AuthContext} from '../../states/auth.state';
import HistoryDetails from "../../components/HistoryDetails"
import colors from '../../constants/colors';
import moment from "moment";
import AsyncStorage from '@react-native-community/async-storage';
import service from "../../services/bcard.service";




class CardTransactions extends Component {

    static contextType=AuthContext;

    isLoaded=false; // note this flag denote mount status
    state={
        loading:false,
        transactions:[],
        lastDate:''
    };


    componentDidMount=()=>{
        this.isLoaded=true;
        this.getHistory();
    };


    componentWillUnmount(){
        this.isLoaded=false;
        this.getHistory=null;
    }


    getHistory=async()=>{
        try {
            this.setState(
                {
                    loading:true
                },this.loadCache
            );
            const {mobileNumber,customerId} =this.props.navigation.getParam('data');
            const response=await service.cardTransactions(
                mobileNumber,
                {
                    "customerId":customerId,
                    "startDate":"08/08/1999",
                    "endDate":moment().format('DD/MM/YYYY'),
                    "numberOfTransactions":"10",
                    "country":"Ghana"
                }
            );
            if (!response.success) {
                throw Error(
                    response.message
                );
            }
            return this.isLoaded && this.setState(
                {
                    loading:false,
                    transactions:response[
                        'transactions'
                    ]
                }
            );
        } catch (err) {
            this.isLoaded && this.getHistory();
        }
    };

    getDate(date){
        const{lastDate}=this.state;
        const _date=moment(date).format('DD/MM/YYYY');
        if(lastDate===_date){
            return {
                show:false,
                date:_date
            }
        }
        this.state.lastDate=_date;
        return {
            show:true,
            date:_date
        }
    }


    render() {

        return (
            <View style={styles.screen}>
                <Header text='Transactions' pressTo={()=>this.props.navigation.goBack()}/>
                    <FlatList
                        data={this.state.transactions}
                        keyExtractor={(v,i)=>i.toString()}
                        ListEmptyComponent={() => {
                            return this.state.loading ? (
                                <ActivityIndicator
                                    color={colors.secondary}
                                    size={'large'}
                                />
                            ) : (
                                <Text
                                    style={{
                                        margin: 50,
                                        fontSize: 20,
                                        fontFamily: 'Roboto-Bold',
                                    }}>
                                    You have not sent any money
                                </Text>
                            );
                        }}
                        renderItem={({item})=>{
                            return (
                                <>
                                    {this.getDate(item.createdAt).show && <View style={{
                                        padding:15
                                    }}>
                                        <Text style={styles.date}>{this.getDate(item.createdAt).date}</Text>
                                    </View>}
                                    <HistoryDetails
                                        onPress={null}
                                        title={item.referenceInformation ?? item.ReferenceInformation}
                                        amount={`GHS${item.totalAmount ?? item.TotalAmount}`}
                                        from={item['transactionDesc'] ?? item['TransactionDesc']}
                                        date={item['transactionDate'] ?? item['TransactionDate']}
                                        description={item['transactionDesc'] ?? item['TransactionDesc']}
                                        data={{...item,status:'paid'}}
                                    />
                                </>
                            );
                        }}
                    />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        height:'100%',
        backgroundColor:'#fff',
        width:'100%',
        justifyContent:'center',
        alignItems:'center'
    },
    container: {
        // flexDirection: 'row',
        backgroundColor: Colors.accent,
        padding: 20,
        borderRadius: 2,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.21,
        shadowRadius: 10,
        elevation: 3,
        justifyContent: 'center',
    },
    TotalCostContainer: {
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.08)',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 5,
    },
    TotalCost: {
        fontSize: 25,
        color: Colors.tertiary,
        textTransform: 'uppercase',
    },
    NameContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 5,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.08)',
    },
    name: {
        fontSize: 16,
        paddingBottom: 5,
    },
    clear: {
        fontSize: 14,
        color: Colors.primary,
        textTransform: 'uppercase',
    },
    date: {
        fontSize: 14,
        color: Colors.primary,
        textTransform: 'uppercase',
        opacity: 0.7,
    },
    Amount: {
        fontSize: 16,
        paddingBottom: 5,
    },
    LogoContainer: {
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.08)',
    },
    logo: {
        borderRadius: 200,
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10,
    },
    Logo: {
        borderRadius: 200,
        padding: 20,
        height: 50,
    },
    LogoText: {
        fontSize: 14,
    },
    navIcon: {},
});

export default CardTransactions;
