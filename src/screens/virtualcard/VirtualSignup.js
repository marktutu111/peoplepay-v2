import React, { Component } from 'react'
import { Text, View,ScrollView,KeyboardAvoidingView,StyleSheet,Dimensions } from 'react-native'
import Header from '../../components/Header';
import LazyDatePicker from '../../components/LazyDatePicker';
import TxtInput from '../../components/TxtInput';
import colors from '../../constants/colors';
import LazyArrowButton from '../../components/LazyArrowButton';
import CardService from "../../services/bcard.service";
import moment from "moment";
import { Alert } from 'react-native';
import { CustomPicker } from 'react-native-custom-picker';


const messages={
    idNumber:'Please provide your ID Number',
    idType:'Please choose your ID Type',
    address:'Please enter your house address',
    city:'Please choose your City',
    dateOfBirth:'Please provide your Date Of Birth'
}


const {height}=Dimensions.get('screen');
const ids=[
    'passport',
    'Voters ID',
    'Drivers License',
    'National ID',
    'Certificate of Identity'
]



class VirtualSignup extends Component {

    isLoaded=false;

    state={
        idNumber:'',
        idType:'',
        address:'',
        city:'',
        idExpiryDate:'',
        dateOfBirth:'',
        cities:[]
    }


    componentDidMount(){
        this.isLoaded=true;
        this.getCities();
    }


    componentWillUnmount(){
        this.isLoaded=false;
    }


    getCities=()=>{
        CardService.getCities().then(response=>{
            try {
                if(typeof response==='object'){
                    if(response.message && response.message instanceof Error){
                        throw Error(
                            'SERVER ERROR'
                        )
                    }
                }
                this.isLoaded && this.setState(
                    {
                        cities:response
                    }
                )
            } catch (err) {
                throw Error(
                    err
                )
            }
        }).catch(()=>this.getCities());
    }


    next=()=>{
        try {
            const state=this.state;
            const data={
                idNumber:state['idNumber'],
                idType:state['idType'],
                address:state['address'],
                city:state['city'],
                dateOfBirth:moment(state['dateOfBirth']).format('MM/DD/YYYY'),
            };
            Object.keys(data).forEach(key=>{
                if(data[key]===''){
                    throw Error(
                        messages[key]
                    )
                }
            })
            this.props.navigation.navigate(
                'VerifyId',
                {data:data}
            )
        } catch (err) {
            Alert.alert(
                'Invalid Data',
                err.message
            )
        }
    }

    render() {
        return (
            <View style={{height:height,backgroundColor:'#fff'}}>
                <Header pressTo={()=>this.props.navigation.navigate('Home')} />
                <ScrollView style={{
                    padding:25
                }}>

                    <Text style={{
                        fontSize:20,
                        textAlign:'center',
                        padding:25
                    }}>Provide your personal details to get your instant card</Text>
                    <KeyboardAvoidingView>
                        <LazyDatePicker 
                            placeholder="Date of Birth"
                            onDateChange={v=>this.setState(
                                {
                                    dateOfBirth:v
                                }
                            )}
                        />
                        <CustomPicker
                                placeholder="Choose City"
                                style={styles.picker}
                                onValueChange={(value)=>{
                                    this.setState(
                                        {
                                            city:value
                                        }
                                    )
                                }}
                                options={this.state.cities.map(city=>city['regionName'])}
                            />
                        <CustomPicker
                                style={styles.picker}
                                placeholder="Choose ID"
                                onValueChange={(value)=>{
                                    this.setState(
                                        {
                                            idType:value
                                        }
                                    )
                                }}
                                options={ids}
                            />
                        <TxtInput
                            placeholder="ID Number"
                            IconName="id-card"
                            fa={true}
                            onChangeText={v =>
                                this.setState({
                                    idNumber:v.trim(),
                                })
                            }
                        />
                        <TxtInput
                            placeholder="Address"
                            IconName="home"
                            IconType="fontawesome"
                            onChangeText={v =>
                                this.setState({
                                    address:v.trim(),
                                })
                            }
                        />
                    </KeyboardAvoidingView>
                    <LazyArrowButton 
                            onPress={this.next.bind(this)}
                            style={{
                                marginVertical:'30%'
                            }}
                        />
                </ScrollView>
            </View>
        )
    }
}



const styles=StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
        marginVertical:40
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
        paddingHorizontal: 30
    },
    button:{
        width:'50%',
        borderRadius:0,
        backgroundColor:'transparent'
    },
    textStyle:{
        color:colors.secondary
    },
    picker:{
        // borderColor:'#ddd',
        // borderWidth:0.5,
        marginVertical:5,
        borderRadius:10,
        backgroundColor:'#fff'
    }
});

export default VirtualSignup;
