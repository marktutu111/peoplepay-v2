import React, { Component } from 'react'
import { Text, View,StyleSheet,ScrollView } from 'react-native'
import Header from '../../components/Header'
import LazyArrowButton from '../../components/LazyArrowButton'
import TxtInput from '../../components/TxtInput'
import { TransactionContext } from '../../states/transactions.state'

export default class FundCard extends Component {

    static contextType=TransactionContext;

    state={
        amount:''
    };


    send=()=>{
        const updateState=this.context.updateState;
        const {amount}=this.state;
        if(amount===''){
            return;
        };
        const data=this.props.navigation.getParam('data');
        updateState(
            {
                returnPage:'',
                transaction:{...data,amount:amount,transaction_type:'VCT'}
            },()=>{
                this.props.navigation.navigate(
                    'ChooseSource',
                    {
                        page:'SenderDetailsMoMo',
                        data:{ page:'ConfirmTransaction' }
                    }
                )
            }
        )
    }

    render() {
        return (
            <>
                <Header text="Fund Card" pressTo={()=>this.props.navigation.goBack()}/>
                <View style={{ backgroundColor:'#fff', flex:1 }}>
                    <ScrollView contentContainerStyle={{padding:20,justifyContent:'center',alignItems:'center'}}>
                        <Text style={{
                            fontSize:30
                        }}>Virtual Card Topup</Text>
                        <Text style={{
                            fontSize:20,
                            textAlign:'center',
                            padding:25,
                            fontWeight:'200'
                        }}>Please enter an amount you want to load onto your Instant Visa Card.</Text>
                        <TxtInput
                            placeholder="Amount"
                            // IconName="lock"
                            // IconType="feather"
                            onChangeText={v=>
                                this.setState({
                                    amount:v.trim(),
                                })
                            }
                        />
                        <LazyArrowButton
                            loading={this.state.loading}
                            onPress={this.send.bind(this)}
                            style={{
                                marginVertical:60,
                                width:40,
                                height:40
                            }}
                        />
                    </ScrollView>
                </View>
            </>
        )
    }
}





const styles = StyleSheet.create({
    
});