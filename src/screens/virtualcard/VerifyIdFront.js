import React, { Component } from 'react'
import { Text, View,ScrollView,Image,StyleSheet } from 'react-native'
import Header from '../../components/Header'
import LazyArrowButton from '../../components/LazyArrowButton'
import LoginPrimaryButton from '../../components/LoginPrimaryButton'
import { launchImageLibrary } from "react-native-image-picker";


const options={
    includeBase64:true,
    storageOptions:{
        skipBackup:true,
        path:'images'
    }
}


export default class VerifyId extends Component {

    state={
        idImage:'',
        dataUri:''
    }

    openGallery=()=>{
        launchImageLibrary(options,(response)=>{
            try {
                if(!response.assets){
                    throw Error(
                        'Oops'
                    )
                }
                const data=response['assets'][0]['base64'];
                this.setState(
                    {
                        idImage:`data:image/png;base64,${data}`,
                        dataUri:data
                    }
                )
            } catch (err) {}
          });
    }


    next=()=>{
        try {
            let data=this.props.navigation.getParam('data');
            if(this.state.idImage.length<=0)return;
            this.props.navigation.navigate(
                'VerifyIdSelfie',
                {
                    data:{...data,idImage:this.state.dataUri}
                }
            )
        } catch (err) {}
    }


    render() {
        return (
            <>
                <Header pressTo={()=>this.props.navigation.goBack()}/>
                <ScrollView contentContainerStyle={{padding:20,justifyContent:'center',alignItems:'center'}}>
                    <Text style={{
                        fontSize:20,
                        textAlign:'center',
                        padding:25
                    }}>Please Take a Photo of the <Text style={{fontWeight:'bold'}}>Front</Text> of your ID Card</Text>
                    <View style={styles.imageCont}>
                        <Image
                            style={{
                                width:'100%',
                                height:'100%',
                                resizeMode:'contain'
                            }} 
                            source={this.state.idImage.length>0?{ uri:this.state.idImage }:require('../../assets/images/verifyID2.png')}
                        />
                    </View>
                    <LoginPrimaryButton
                        icon="camera"
                        title="Take Photo of Front ID"
                        pressTo={this.openGallery.bind(this)}
                        style={{
                            marginVertical:10,
                            width:'70%'
                        }}
                    />
                    <LazyArrowButton
                        onPress={this.next.bind(this)}
                        style={{
                            marginVertical:60
                        }}
                    />
                </ScrollView>
            </>
        )
    }
}


const styles=StyleSheet.create({
    imageCont:{
        justifyContent:'center',
        alignItems:'center',
        overflow:'hidden',
        width:250,
        height:250,
        marginVertical:20,
        // borderColor:'#6d6d6d',
        // borderWidth:1,
        // borderStyle:'dotted',
        borderRadius:5,
        marginHorizontal:5
    }
});