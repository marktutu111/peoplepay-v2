import React, { Component } from 'react'
import { Text, View,StyleSheet,ScrollView,Dimensions } from 'react-native'
import Header from '../../components/Header'
import LazyArrowButton from '../../components/LazyArrowButton'
import TxtInput from '../../components/TxtInput'
import colors from '../../constants/colors'
import { Alert } from 'react-native'
import CardService from "../../services/bcard.service";
import SwiperFlatList from 'react-native-swiper-flatlist';
import CardContainer from '../../components/CardContainer';
const {height,width}=Dimensions.get('screen');



export default class TransferMoneyToCard extends Component {

    viewLoaded=false;

    state={
        amount:'',
        user:{},
        cards:[],
        toCard:{},
        loading:false
    };

    componentDidMount(){
        try {
            this.viewLoaded=true;
            const data=this.props.navigation.getParam('data');
            this.setState(
                {
                    ...data
                },this.getInitialCard.bind(this)
            )
        } catch (err) {};
    }


    getInitialCard=()=>{
        if(this.state.cards.length>0){
            const firstcard=this.state.cards[0];
            this.viewLoaded && this.setState(
                {
                    toCard:firstcard
                },this.viewBalance.bind(this)
            )
        }
    }

    
    componentWillUnmount(){
        this.viewLoaded=false;
    }


    send=async()=>{
        try {
            const {amount,fromCard,toCard,user}=this.state;
            if(Object.keys(toCard).length<=0){
                throw Error(
                    'Choose your Virtual Card to transfer funds'
                )
            }
            if(amount===''){
                throw Error(
                    'Please enter a valid amount'
                )
            };
            this.setState({loading:true});
            const data={
                "mobileNumber":fromCard['mobileNumber'],
                "fromCustomerId":fromCard['customerId'],
                "fromCardId":fromCard['cardId'],
                "amount":amount,
                "toCustomerId":toCard['customerId'],
                "toCardId":toCard['cardId'],
                "fromReferenceMemo":`${fromCard['preferredName']} sent ${amount}ghc to ${toCard['preferredName']}`,
                "toReferenceMemo":`${toCard['preferredName']} received ${amount}ghc from ${fromCard['preferredName']}`,
                "country":fromCard['country'],
                "bank":fromCard['bank']
            };
            const response=await CardService.transferFunds(data);
            if(!response.success){
                throw Error(
                    response[
                        'message'
                    ]
                )
            };
            this.setState(
                {
                    sending:false
                },()=>{
                    Alert.alert(
                        'Transaction Successful',
                        `You have successfuly transfered an amount ${data['amount']} to Your Instant Visa Card`
                    )
                    this.props.navigation.navigate(
                        'VirtualCard'
                    )
                }
            )
        } catch (err) {
            this.setState({loading:false});
            Alert.alert(
                'Transfer Failed',
                err.message
            )
        }
    }


    onCardChange=({index})=>{
        try {
            const card=this.state.cards.find((card,i)=>i===index);
            card && this.setState(
                {
                    toCard:card
                },this.viewBalance.bind(this)
            )
        } catch (err) {}
    }


    viewBalance=async()=>{
        try {
            this.setState(
                {
                    loading_balance:true
                }
            );
            const {customerId,cardId,mobileNumber}=this.state.toCard;
            const response=await CardService.viewBalance(
                {
                    "mobileNumber":mobileNumber,
                    "customerId":customerId,
                    "cardId":cardId,
                    "country":"Ghana"
                }
            );
            if(!response.success){
                throw Error(
                    response[
                        'message'
                    ]
                )
            };
            const {recordId}=response;
            this.setState(
                {
                    balance:recordId,
                    loading_balance:false
                }
            )
        } catch (err) {
            this.setState(
                {
                    loading_balance:false
                }
            ) 
        }
    }



    render() {
        return (
            <>
                <Header text="Tranfer Funds" pressTo={()=>this.props.navigation.goBack()}/>
                <ScrollView>
                    <View style={styles.screen}>
                        <Text style={{
                            fontSize:20,
                            textAlign:'center',
                            padding:25,
                            fontWeight:'200'
                        }}>Please choose a Virtual card from the list and enter an amount you want to load onto your Instant Visa Card.</Text>
                        <View>
                            <SwiperFlatList onChangeIndex={this.onCardChange.bind(this)}>
                                {
                                    this.state.cards.map((k,i)=>(
                                        <CardContainer
                                            key={i.toString()}
                                            _key={i.toString()}
                                            style={{transform: [{ scale: 0.96 }]}}
                                            date={this.state.toCard?.validFor}
                                            name={this.state.toCard?.preferredName?.length>0?this.state.toCard?.preferredName:this.state.user?.fullname}
                                            status={this.state.toCard?.status}
                                            balance={this.state.balance}
                                            cardId={this.state.toCard?.cardId}
                                        />
                                    ))
                                }
                            </SwiperFlatList>
                        </View>
                        <View style={{
                            width:width,
                            paddingHorizontal:50
                        }}>
                            <TxtInput
                                placeholder="Amount"
                                keyboardType="number-pad"
                                onChangeText={v=>this.setState(
                                        {
                                            amount:v.trim(),
                                        }
                                    )
                                }
                            />
                        </View>
                        <LazyArrowButton
                            loading={this.state.loading}
                            disabled={!this.state.toCard?.cardId || this.state.amount.length<=0}
                            onPress={()=>{
                                Alert.alert(
                                    'Confirm Transaction',
                                    `You have requested to transfer an amount of GHS${this.state.amount} to your Instant Visa Card,Kindly confirm your request`,
                                    [
                                        {
                                            text:'Yes,Continue',
                                            onPress:this.send.bind(this)
                                        },
                                        {
                                            text:'No,Cancel',
                                            onPress:null
                                        }
                                    ]
                                )
                            }}
                            style={{
                                marginVertical:60,
                                width:40,
                                height:40
                            }}
                        />
                    </View>
                </ScrollView>
            </>
        )
    }
}





const styles = StyleSheet.create({
    screen: {
        height:height,
        width:width,
        backgroundColor:'#fff'
    },
    cardrow: {
        paddingHorizontal:30,
        justifyContent: 'space-evenly',
    },
    heading: {
        textAlign: 'left',
        color: 'white',
        opacity: 1,
        fontFamily: 'Roboto-Bold',
        fontSize: 18,
        textAlign: 'center',
        textAlignVertical: 'center',
        padding: 5,
    },
    HeaderTextMain: {
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingTop: 30,
        paddingBottom: 20,
        flexDirection: 'row',
    },
    HeaderText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: colors.secondary,
        // textAlign: 'center',
        opacity: 0.8,
    },
    HeaderText1: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: colors.primary,
        opacity: 0.8
    },
    card:{
        justifyContent:'center',
        alignItems:'center',
        marginVertical:15
    },
    dropdownContainer:{
        width:'80%'
    }
});