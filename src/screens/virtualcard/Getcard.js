import React, { Component } from 'react'
import { Text, View,ScrollView,Alert } from 'react-native'
import Header from '../../components/Header';
import LazyArrowButton from '../../components/LazyArrowButton';
import colors from '../../constants/colors';
import {  AuthContext } from "../../states/auth.state";
import { TransactionConsumer } from '../../states/transactions.state';



 class Getcard extends Component {

    static contextType=AuthContext;

    state={}


    componentDidMount(){
        try {
            const fees=this.props.navigation.getParam('fees');
            this.setState(
                {
                    fees:fees
                }
            )
        } catch (err) {}
    }



    getCard=()=>{
        try {
            const {_id,fullname}=this.context.state?.user || {};
            Alert.alert(
                `Hi, ${fullname}`,
                'You have requested to get an instant Visa card,kindly confirm your request',
                [
                    {
                        text:'Yes,Proceed',
                        onPress:()=>{
                            const {fees}=this.state;
                            this.updateState(
                                {
                                    returnPage:'',
                                    transaction:{
                                        transaction_type:'VCI',
                                        amount:fees['amount'],
                                        customerId:_id,
                                        description:fees['description']
                                    }
                                },()=>{
                                    this.props.navigation.navigate('ChooseSource',{
                                            page:'SenderDetailsMoMo',
                                            data:{ page:'ConfirmTransaction' }
                                        }
                                    )
                                }
                            )
                        }
                    },
                    {
                        text:'No,Cancel',
                        onPress:null
                    }
                ]
            )
        } catch (err) {}
    }

    render() {
        return (
            <View>
                <TransactionConsumer>
                    {
                        ({updateState})=>{
                            this.updateState=updateState;
                        }
                    }
                </TransactionConsumer>
                <Header pressTo={()=>this.props.navigation.navigate('Home')} />
                <ScrollView contentContainerStyle={{marginVertical:'15%'}}>
                    <View style={{
                            marginVertical:80,
                            justifyContent:'center',
                            alignItems:'center'
                        }}>
                            <Text style={{fontSize:30}}>Hi, {this.context.state?.user?.fullname}</Text>
                            <Text 
                                style={{textAlign:'center',marginVertical:6,fontSize:20,color:'#6d6d6d'}}
                                >Tap to get your first Virtual Card
                            </Text>
                            <Text 
                                style={{textAlign:'center',fontSize:15,color:'#6d6d6d'}}
                                >It will only cost you GHS{this.state.fees?.amount}
                            </Text>
                            <LazyArrowButton
                                loading={this.state.loading}
                                icon="thumbs-up"
                                size={40}
                                style={{
                                    width:160,
                                    height:160,
                                    backgroundColor:colors.secondary,
                                    marginVertical:50
                                }}
                                onPress={this.getCard.bind(this)}
                            />
                        </View>
                </ScrollView>
            </View>
        )
    }
}


export default Getcard;