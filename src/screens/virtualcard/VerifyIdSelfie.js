import React, { Component } from 'react'
import { Text, View,ScrollView,Image,StyleSheet,Alert } from 'react-native';
import Header from '../../components/Header'
import LazyArrowButton from '../../components/LazyArrowButton'
import LoginPrimaryButton from '../../components/LoginPrimaryButton'
import { launchImageLibrary } from "react-native-image-picker";
import { AuthContext } from "../../states/auth.state";
import CardService from "../../services/bcard.service";
import UserService from "../../services/users.service";


const options={
    includeBase64:true,
    storageOptions:{
        skipBackup:true,
        path:'images'
    }
}


export default class VerifyIdSelfie extends Component {

    static contextType=AuthContext;

    state={
        selfieImage:'',
        dataUri:'',
        loading:false
    }


    openGallery=()=>launchImageLibrary(options,(response)=>{
        try {
            if(!response.assets){
                throw Error(
                    'Oops'
                )
            }
            const data=response['assets'][0]['base64'];
            this.setState(
                {
                    selfieImage:`data:image/png;base64,${data}`,
                    dataUri:data
                }
            )
        } catch (err) {}
    });


    next=async()=>{
        try {
            const {user}=this.context['state'];
            const [fistname,lastname]=user.fullname.split(' ');
            let data=this.props.navigation.getParam('data');
            if(this.state.dataUri.length<=0){
                throw Error(
                    'Please upload a selfie of your ID Card'
                )
            }
            const _payload={
                ...data,
                selfieImage:this.state.dataUri,
                email:user['email'],
                mobileNumber:user['phone'],
                firstname:fistname,
                surname:lastname,
                password:'0000',
                country:'Ghana',
                othernames:'',
                bank:'ecobank'
            };
            this.setState({loading:true});
            let response=await CardService.createAccount(
                _payload
            );
            if(response instanceof Error || (typeof response==='string' && response.indexOf('text/html')>-1)){
                throw Error(
                    'Something went wrong we could not process your request,kindly try again later'
                )
            }
            if(!response.success){
                throw Error(
                    response[
                        'message'
                    ]
                )
            };
            await UserService.updateCustomer(
                {
                    id:user['_id'],
                    data:{
                        subscribed_on_bvirtual:true
                    }
                }
            );
            this.setState(
                {
                    loading:false
                },()=>{
                    this.props.navigation.navigate(
                        'VirtualCard'
                    )
                    Alert.alert(
                        'Congratulations',
                        'Your account has been created successfuly, you may proceed to load your card'
                    )
                }
            )
        } catch (err) {
            this.setState(
                {
                    loading:false
                },()=>Alert.alert(
                    'Account failed',
                    err.message
                )
            )
        }
    }


    render() {
        return (
            <>
                <Header pressTo={()=>this.props.navigation.goBack()}/>
                <ScrollView contentContainerStyle={{padding:20,justifyContent:'center',alignItems:'center'}}>
                    <Text style={{
                        fontSize:20,
                        textAlign:'center',
                        padding:25
                    }}>Please Take a Selfie of the <Text style={{fontWeight:'bold'}}>Front</Text> of your ID Card and your face</Text>
                    <View style={styles.imageCont}>
                        <Image
                            style={{
                                width:'60%',
                                height:'60%',
                                resizeMode:'contain'
                            }} 
                            source={this.state.selfieImage.length>0?{ uri:this.state.selfieImage }:require('../../assets/images/thumbnail_verifyID.png')}
                        />
                    </View>
                    <LoginPrimaryButton
                        icon="camera"
                        title="Take Selfie of Front ID"
                        pressTo={this.openGallery.bind(this)}
                        style={{
                            marginVertical:10,
                            width:'70%'
                        }}
                    />
                    <LazyArrowButton
                        icon="thumbs-up"
                        loading={this.state.loading}
                        onPress={this.next.bind(this)}
                        style={{
                            marginVertical:60
                        }}
                    />
                </ScrollView>
            </>
        )
    }
}


const styles=StyleSheet.create({
    imageCont:{
        justifyContent:'center',
        alignItems:'center',
        overflow:'hidden',
        width:250,
        height:250,
        marginVertical:20,
        borderRadius:5,
        marginHorizontal:5
    }
});