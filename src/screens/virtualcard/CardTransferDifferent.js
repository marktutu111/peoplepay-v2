import React, { Component } from 'react'
import { Text, View,StyleSheet,ScrollView,Dimensions } from 'react-native'
import Header from '../../components/Header'
import LazyArrowButton from '../../components/LazyArrowButton'
import TxtInput from '../../components/TxtInput'
import colors from '../../constants/colors'
import { Alert } from 'react-native'
import CardService from "../../services/bcard.service";
import LoginPrimaryButton from '../../components/LoginPrimaryButton'

import SwiperFlatList from 'react-native-swiper-flatlist';
import CardContainer from '../../components/CardContainer';

const {width,height}=Dimensions.get('screen');


export default class TransferMoneyToDifferentCard extends Component {

    state={
        amount:'',
        user:{},
        cards:[],
        toCard:{},
        recipient:'',
        mobilenumber:''
    };

    componentDidMount(){
        try {
            const data=this.props.navigation.getParam('data');
            this.setState(
                {
                    ...data
                }
            )
        } catch (err) {};
    }


    getCards=async()=>{
        try {
            const {mobilenumber}=this.state;
            if(mobilenumber.length<=0){
                return;
            }
            this.setState(
                {
                    loading:true
                }
            )
            const res=await CardService.loadCards(mobilenumber);
            if(Array.isArray(res) && res.length>0){
                this.setState(
                    {
                        loading:false,
                        cards:res
                    }
                )
            }else throw Error(
                'Customer does not have any Cards'
            )
        } catch (err) {
            this.setState(
                {
                    loading:false
                },()=>{
                    Alert.alert(
                        'Oops!',
                        err.message
                    )
                }
            )
        }
    }


    send=async()=>{
        try {
            const {amount,fromCard,toCard,user}=this.state;
            if(Object.keys(toCard).length<=0){
                throw Error(
                    'Choose Virtual Card to transfer funds'
                )
            }
            if(amount===''){
                throw Error(
                    'Please enter a valid amount'
                )
            };
            this.setState(
                {
                    sending:true
                }
            )
            const data={
                "mobileNumber":fromCard['mobileNumber'],
                "fromCustomerId":fromCard['customerId'],
                "fromCardId":fromCard['cardId'],
                "amount":amount,
                "toCustomerId":toCard['customerId'],
                "toCardId":toCard['cardId'],
                "fromReferenceMemo":`${fromCard['preferredName']} sent ${amount}ghc to ${toCard['preferredName']}`,
                "toReferenceMemo":`${toCard['preferredName']} received ${amount}ghc from ${fromCard['preferredName']}`,
                "country":fromCard['country'],
                "bank":fromCard['bank']
            };
            const response=await CardService.transferFunds(data);
            if(!response.success){
                throw Error(
                    response[
                        'message'
                    ]
                )
            };
            this.setState(
                {
                    sending:false
                },()=>{
                    Alert.alert(
                        'Transaction Successful',
                        `You have successfuly transfered an amount ${data['amount']} to Customer`
                    )
                    this.props.navigation.navigate(
                        'VirtualCard'
                    )
                }
            )
        } catch (err) {
            this.setState(
                {
                    sending:false
                }
            )
            Alert.alert(
                'Transfer Failed',
                err.message
            )
        }
    }


    viewBalance=async()=>{
        try {
            this.setState(
                {
                    loading_balance:true
                }
            );
            const {customerId,cardId,mobileNumber}=this.state.toCard;
            const response=await CardService.viewBalance(
                {
                    "mobileNumber":mobileNumber,
                    "customerId":customerId,
                    "cardId":cardId,
                    "country":"Ghana"
                }
            );
            if(!response.success){
                throw Error(
                    response[
                        'message'
                    ]
                )
            };
            const {recordId}=response;
            this.setState(
                {
                    balance:recordId,
                    loading_balance:false
                }
            )
        } catch (err) {
            this.setState(
                {
                    loading_balance:false
                }
            ) 
        }
    }


    onCardChange=({index})=>{
        try {
            const card=this.state.cards.find((card,i)=>i===index);
            card && this.setState(
                {
                    toCard:card
                },this.viewBalance.bind(this)
            )
        } catch (err) {}
    }


    render() {
        return (
            <>
                <Header text="Funds Transfer" pressTo={()=>this.props.navigation.goBack()}/>
                <ScrollView>
                    <View style={styles.screen}>
                        <Text style={{
                            fontSize:20,
                            textAlign:'center',
                            padding:25,
                            fontWeight:'200',
                            marginVertical:20
                        }}>Enter customer phone number to display the Visa cards. Select the card from the list  and amount to load onto the card.</Text>
                        <View style={styles.input}>
                            <TxtInput
                                placeholder="Phone Number"
                                IconName="phone"
                                IconType="feather"
                                keyboardType="phone-pad"
                                maxLength={10}
                                onChangeText={v=>
                                    this.setState({
                                        mobilenumber:v.trim(),
                                    })
                                }
                            />
                        </View>
                        {this.state.cards.length>0 && <>
                            <View>
                                <SwiperFlatList onChangeIndex={this.onCardChange.bind(this)}>
                                    {
                                        this.state.cards.map((k,i)=>(
                                            <CardContainer
                                                key={i.toString()}
                                                style={{transform: [{ scale: 0.96 }]}}
                                                _key={i.toString()}
                                                date={this.state.toCard?.validFor}
                                                name={this.state.toCard?.preferredName?.length>0?this.state.toCard?.preferredName:this.state.user?.fullname}
                                                status={this.state.toCard?.status}
                                                balance={this.state.balance}
                                                cardId={this.state.toCard?.cardId}
                                            />
                                        ))
                                    }
                                </SwiperFlatList>
                            </View>
                            <View style={styles.input}>
                                <TxtInput
                                    placeholder="Enter Amount"
                                    keyboardType="number-pad"
                                    hideIcon={true}
                                    onChangeText={v=>
                                        this.setState({
                                            amount:v.trim(),
                                        })
                                    }
                                />
                            </View>
                        </>}
                        {this.state.amount<=0 && <View
                            style={{width:width,paddingHorizontal:100}}
                        >
                            <LoginPrimaryButton
                                icon="credit-card"
                                title="load cards"
                                pressTo={this.getCards.bind(this)}
                                loading={this.state.loading}
                                style={{
                                    marginVertical:10,
                                    width:'70%'
                                }}
                            />
                            </View>
                        }
                        {this.state.cards.length>0 && this.state.amount.length>0 && <LazyArrowButton
                            loading={this.state.sending}
                            onPress={()=>{
                                Alert.alert(
                                    'Confirm Transaction',
                                    `You have requested to transfer an amount of GHS${this.state.amount} to a Customer,Kindly confirm your request`,
                                    [
                                        {
                                            text:'Yes,Continue',
                                            onPress:this.send.bind(this)
                                        },
                                        {
                                            text:'No,Cancel',
                                            onPress:null
                                        }
                                    ]
                                )
                            }}
                            style={{
                                marginVertical:60,
                                width:40,
                                height:40
                            }}
                        />}
                    </View>
                </ScrollView>
            </>
        )
    }
}





const styles = StyleSheet.create({
    screen: {
        height:height,
        width:width,
        backgroundColor:'#fff'
    },
    cardrow: {
        paddingHorizontal:30,
        justifyContent: 'space-evenly',
    },
    heading: {
        textAlign: 'left',
        color: 'white',
        opacity: 1,
        fontFamily: 'Roboto-Bold',
        fontSize: 18,
        textAlign: 'center',
        textAlignVertical: 'center',
        padding: 5,
    },
    HeaderTextMain: {
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingTop: 30,
        paddingBottom: 20,
        flexDirection: 'row',
    },
    HeaderText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: colors.secondary,
        // textAlign: 'center',
        opacity: 0.8,
    },
    HeaderText1: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: colors.primary,
        opacity: 0.8
    },
    card:{
        justifyContent:'center',
        alignItems:'center',
        marginVertical:15
    },
    dropdownContainer:{
        width:'82%'
    },
    input:{
        width:width,
        paddingHorizontal:50,
        justifyContent:'center',
        alignItems:'center'
    }
});