import React, { Component } from 'react'
import { View,StyleSheet,ScrollView,Alert,Switch,Text } from 'react-native'
import Colors from '../../constants/colors';
import Header from '../../components/Header';
import TertiaryButton from '../../components/TertiaryButton';
import CardService from "../../services/bcard.service";
import {  AuthContext } from "../../states/auth.state";
import CardContainer from '../../components/CardContainer';
import SwiperFlatList from 'react-native-swiper-flatlist';
import { ActivityIndicator } from 'react-native';
import colors from '../../constants/colors';
import { TransactionConsumer } from '../../states/transactions.state';




export default class VirtualCardScreen extends Component {


    $subscription=null;
    static contextType=AuthContext;

    state={
        loading_fees:false,
        blocking:false,
        loading_cards:false,
        loading_balance:false,
        loading:false,
        fees:[],
        cards:[],
        cardIssueFee:'0',
        selected:{},
        balance:'',
        toggle:false
    }

    componentDidMount(){
        this.$subscription=this.props.navigation.addListener(
            'willFocus',()=>this.verifyAccount()
        );
    }


    componentWillUnmount(){
        this.$subscription.remove();
    }



    verifyAccount=async()=>{
        try {
            this.setState(
                {
                    loading_cards:true
                }
            )
            const {user}=this.context['state'];
            const response=await CardService.verifyAccount(
                {
                    "mobileNumber":user['phone'],
                    "country":"Ghana"
                }
            );
            if(typeof response==='object'){
                if(typeof response.success !== 'boolean'){
                    throw Error(
                        'ERROR'
                    )
                }
            }
            this.setState(
                {
                    loading_cards:false
                },()=>{
                    switch (response.success) {
                        case true:
                            this.loadCards();
                            break;
                        default:
                            this.props.navigation.navigate(
                                'VirtualSignup'
                            )
                            break;
                    }
                }
            )
        } catch (err) {this.verifyAccount()}
    }



    getFees=()=>new Promise(async(resolve,reject)=>{
        try {
            this.setState({ loading_fees:true });
            const response=await CardService.getFees();
            this.setState(
                {
                    loading_fees:false
                },()=>{
                    switch (Array.isArray(response) && response.length>0) {
                        case true:
                            response.find(fee=>{
                                if(fee['feeId']==='23'){
                                    return resolve(
                                        fee
                                    )
                                }
                            });
                            break;
                        default:
                            throw Error(
                                'Oops'
                            )
                    }
                }
            )
        } catch (err) {
            reject(null)
        }
    })


    
    loadCards=async()=>{
        try {
            this.setState(
                {
                    loading_cards:true
                }
            );
            const {user}=this.context['state'];
            const res=await CardService.loadCards(user['phone']);
            switch (Array.isArray(res) && res.length>0) {
                case true:
                    this.setState(
                        {
                            loading_cards:false,
                            cards:res
                        },this.getInitialCard.bind(this)
                    );
                    break;
                default:
                    const fees=await this.getFees();
                    this.props.navigation.navigate(
                        'GetCard', { fees:fees }
                    );
                    break;
            }
        } catch (err) {this.loadCards()}
    }



    getInitialCard=()=>{
        if(this.state.cards.length>0){
            const firstcard=this.state.cards[0];
            this.setState(
                {
                    selected:firstcard,
                    toggle:firstcard.status==='activated'?true:false
                },this.viewBalance.bind(this)
            )
        }
    }


    getUser=(key)=>{
        const user=this.context['state']['user'];
        if(key){
            return user[key]
        };
        return user;
    }



    block=async()=>{
        try {
            let type='';
            let _status='';
            const {customerId,cardId,status}=this.state.selected;
            if(!customerId || !cardId){
                return;
            }
            this.setState(
                {
                    blocking:true
                }
            );
            switch (status) {
                case 'activated':
                    type='deactivate';
                    _status='deactivated'
                    break;
                default:
                    type='activate';
                    _status='activated';
                    break;
            }
            const response=await CardService.blockCard(
                type,
                {
                    "mobileNumber":this.getUser('phone'),
                    "customerId":customerId,
                    "cardId":cardId,
                    "country":"Ghana"
                }
            );
            if(!response.success){
                throw Error(
                    'Sorry, we could not complete your request'
                )
            };
            this.setState(
                {
                    blocking:false,
                    selected:{...this.state.selected,status:_status}
                },()=>{
                    switch (type) {
                        case 'activate':
                            Alert.alert(
                                'Request successful',
                                'Your card is now active,you may transfer or load money onto your card'
                            )
                            break;
                        default:
                            Alert.alert(
                                'Request successful',
                                'Your card is now blocked,you cannot transfer or load money onto your card or use for any transaction'
                            )
                            break;
                    }
                }
            )
        } catch (err) {
            this.setState(
                {
                    blocking:false,
                    toggle:!this.state.toggle
                },()=>Alert.alert(
                    'Request Failed',
                    err.message
                )
            )
        }
    }


    viewBalance=async()=>{
        try {
            this.setState(
                {
                    loading_balance:true
                }
            );
            const {customerId,cardId}=this.state.selected;
            const response=await CardService.viewBalance(
                {
                    "mobileNumber":this.getUser('phone'),
                    "customerId":customerId,
                    "cardId":cardId,
                    "country":"Ghana"
                }
            );
            if(!response.success){
                throw Error(
                    response[
                        'message'
                    ]
                )
            };
            const {recordId}=response;
            this.setState(
                {
                    balance:recordId,
                    loading_balance:false
                }
            )
        } catch (err) {
            this.setState(
                {
                    loading_balance:false
                }
            ) 
        }
    }



    getCard=()=>{
        Alert.alert(
            `Hi, ${this.getUser('fullname')}`,
            'You have requested to get an instant Visa card,kindly confirm your request',
            [
                {
                    text:'Yes,Proceed',
                    onPress:async()=>{
                        try {
                            const fees=await this.getFees();
                            if(!fees)return;
                            this.updateState(
                                {
                                    returnPage:"VirtualCard",
                                    transaction:{
                                        transaction_type:'VCI',
                                        amount:fees['amount'],
                                        customerId:this.getUser('_id'),
                                        description:fees['feeName']
                                    }
                                },()=>{
                                    this.props.navigation.navigate(
                                        'ChooseSource',
                                        {
                                            page:'SenderDetailsMoMo',
                                            data:{ page:'ConfirmTransaction' }
                                        }
                                    )
                                }
                            )
                        } catch (err) {console.log(err)}
                    }
                },
                {
                    text:'No,Cancel',
                    onPress:null
                }
            ]
        )
    }


    onCardChange=({index})=>{
        try {
            const card=this.state.cards.find((card,i)=>i===index);
            card && this.setState(
                {
                    selected:card
                },this.viewBalance.bind(this)
            )
        } catch (err) {}
    }


    disable(){return this.state.cards.length<=0};


    render() {
        return (
            <View style={styles.screen}>
                <TransactionConsumer>
                    {
                        ({updateState})=>{
                            this.updateState=updateState;
                        }
                    }
                </TransactionConsumer>
            <Header text="Instant Visa Card" pressTo={()=>this.props.navigation.navigate('Home')} />
            <ScrollView
                style={styles.scroll}
                keyboardShouldPersistTaps="always"
                keyboardDismissMode="on-drag">
                <View style={styles.Body}>

                    {this.state.loading_cards && this.state.cards<=0 ? <View
                        style={{justifyContent:'center',alignItems:'center',marginVertical:10}}
                    >
                        <ActivityIndicator style={{ marginVertical:15 }} size="large" color={colors.secondary}/>
                        <Text style={{fontWeight:'200'}}>loading your visa cards..</Text>
                    </View>:<SwiperFlatList onChangeIndex={this.onCardChange.bind(this)}>
                        {
                            this.state.cards.map((k,i)=>(
                                <CardContainer
                                    key={i.toString()}
                                    style={{transform: [{ scale: 0.96 }]}}
                                    reload={this.viewBalance.bind(this)}
                                    _key={i.toString()}
                                    loading_balance={this.state.loading_balance}
                                    date={this.state.selected?.validFor}
                                    name={this.state.selected?.preferredName?.length>0?this.state.selected?.preferredName:this.getUser('fullname')}
                                    status={this.state.selected?.status}
                                    balance={this.state.balance}
                                    cardId={this.state.selected?.cardId}
                                />
                            ))
                        }
                    </SwiperFlatList>}

                <View style={styles.cardrow}>
                    <TertiaryButton
                        IconName="wallet"
                        fas
                        disable={this.disable()}
                        title="Add money to card"
                        pressTo={()=>{
                            const card=this.state.selected;
                            if(!card.cardId || !card.customerId){
                                return;
                            }
                            this.props.navigation.navigate(
                                'FundCard',
                                {
                                    data:{
                                        customerId:this.getUser('_id'),
                                        description:'Virtual Card Topup',
                                        vcard:card
                                    }
                                }
                            )
                        }}
                    />
                    <TertiaryButton
                        IconName="history"
                        IconType="fontawesome"
                        title="View Transactions"
                        disabled={this.disable()}
                        pressTo={()=>{
                            this.props.navigation.navigate(
                                'CardTransactions',
                                {
                                    data:this.state.selected
                                }
                            )
                        }}
                    />
                    <TertiaryButton
                        IconName={this.state.selected?.status==='activated'?'lock':'lock-open'}
                        IconType="fontawesome"
                        title={this.state.selected?.status==='activated'?'Deactivate Card':'Activate Card'}
                        loading={this.state.blocking}
                        disabled={this.disable()}
                    > 
                        <Switch
                            trackColor={{ false: "#767577", true: "#ddd" }}
                            thumbColor={this.state.selected?.status==='activated' ? "green" : "red"}
                            ios_backgroundColor="#eee"
                            disabled={this.disable()}
                            onValueChange={(v)=>{
                                try {
                                    this.setState(
                                        {
                                            toggle:v
                                        }
                                    )
                                    let message='';
                                    switch (v) {
                                        case true:
                                            message='Do you want to Activate your Virtual Card'
                                            break;
                                        default:
                                            message='Do you want to Deactivate your Virtual Card'
                                            break;
                                    };
                                    Alert.alert(
                                        'Confirm Request',
                                        message,
                                        [
                                            {
                                                text:'Yes,Continue',
                                                onPress:this.block.bind(this)
                                            },
                                            {
                                                text:'No,Cancel',
                                                onPress:()=>{
                                                    this.setState(
                                                        {
                                                            toggle:!v
                                                        }
                                                    )
                                                }
                                            }
                                        ]
                                    )
                                } catch (err) {}
                            }}
                            value={this.state.toggle}
                        />
                     </TertiaryButton>
                    <TertiaryButton
                        IconName="paper-plane"
                        fas
                        title="Transfer money to your Card"
                        disabled={this.disable()}
                        pressTo={()=>{
                            try {
                                const filter=this.state.cards.filter(card=>card['cardId']!==this.state.selected['cardId']);
                                this.props.navigation.navigate(
                                    'CardTranfer',
                                    {
                                        data:{
                                            cards:filter,
                                            user:this.getUser(),
                                            fromCard:this.state.selected
                                        }
                                    }
                                )
                            } catch (err) {}
                        }}
                    />
                    <TertiaryButton
                        IconName="user"
                        fas
                        title="Transfer to a Customer"
                        disabled={this.disable()}
                        pressTo={()=>{
                            try {
                                this.props.navigation.navigate(
                                    'CardTranferCustomer',
                                    {
                                        data:{
                                            user:this.getUser(),
                                            fromCard:this.state.selected
                                        }
                                    }
                                )
                            } catch (err) {}
                        }}
                    />
                    <TertiaryButton
                        loading={this.state.loading_fees}
                        IconName="credit-card"
                        IconType="fontawesome"
                        title="Get Another Card"
                        pressTo={this.getCard.bind(this)}
                        disabled={this.disable()}
                    />
                </View>
                </View>
            </ScrollView>
        </View>
        )
    }
}


const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
        marginBottom:20
    },

    Body: {
        marginTop:20,
        justifyContent: 'center',
        flex: 1,
    },
    cardrow: {
        paddingHorizontal:30,
        justifyContent: 'space-evenly',
    },
    heading: {
        textAlign: 'left',
        color: 'white',
        opacity: 1,
        fontFamily: 'Roboto-Bold',
        fontSize: 18,
        textAlign: 'center',
        textAlignVertical: 'center',
        padding: 5,
    },
    HeaderTextMain: {
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingTop: 30,
        paddingBottom: 20,
        flexDirection: 'row',
    },
    HeaderText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.secondary,
        // textAlign: 'center',
        opacity: 0.8,
    },
    HeaderText1: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.primary,
        opacity: 0.8
    },
    card:{
        justifyContent:'center',
        alignItems:'center',
        marginVertical:15
    },
    cardCont:{
        width:'85%',
        borderRadius:10,
        justifyContent:'center',
        borderWidth:2,
        borderColor:'#ddd',
        padding:20,
        height:180
    }
});