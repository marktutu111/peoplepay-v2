import React, { useState,useEffect } from 'react';
import { View,StyleSheet,Dimensions,ScrollView,Text,Modal, Alert } from 'react-native';
import Header from '../../components/Header'
import IconButton from '../../components/IconButton';
import LazyArrowButton from '../../components/LazyArrowButton';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import TxtInput from '../../components/TxtInput';
import { ReferralConsumer } from '../../states/referrals.state';
import Clipboard from "@react-native-community/clipboard";
const {height,width}=Dimensions.get('window');

const ReferalsComponent=(props)=>{

    let _generateCode=null;
    let _loadReferal=null;
    let _deleteCode=null;
    let code='';

    const [onShow,showModal]=useState(false);

    useEffect(()=>{
        _loadReferal();
    })


    const share=(code)=>{
        try {
            Clipboard.setString(code);
            Alert.alert(code,'Your referral code is copied to clipboard,you can paste and share any where with your friends');
        } catch (err) {
            console.log(err);
        }
    }


    const removeReferalCode=()=>Alert.alert(
        'Confirm Delete',
        'Do you want to delete your referral code? you will no longer receive rewards from this code by friends',
        [
            {
                text:'Yes,Delete',
                onPress:_deleteCode
            },
            {
                text:'No,Cancel',
                onPress:null
            }
        ]
    )

    const sendRequest=()=>{
        if(code.length<=0){
            return;
        }
        _generateCode(code).then(()=>showModal(false)).catch(err=>code='');
    }

    return(
        <ReferralConsumer>
            {
                ({state,generateCode,loadReferal,deleteCode})=>{
                    _generateCode=generateCode;
                    _loadReferal=loadReferal;
                    _deleteCode=deleteCode;
                    return(
                        <>
                            <Header text="Referals" pressTo={()=>props.navigation.goBack()}/>
                            <View style={styles.container}>
                                <ScrollView contentContainerStyle={styles.scroll}>
                                    <Text style={styles.mainText}>Share your referral code and get GHS1 every time a friend Sign's up and make a total transaction of GHS100.</Text>
                                    <View style={styles.code}>
                                        <IconButton onPress={()=>share(state.referal?.code)} name="share"/>
                                        <Text style={{fontWeight:'bold',marginLeft:10}}>{state.referal?.code || 'NO CODE'}</Text>
                                    </View>
                                    <View style={styles.summaryCont}>
                                        <Text style={styles.summayText}>Friends Registered: <Text>{state.summary?.count ?? 0}</Text></Text>
                                        <Text style={styles.summayText}>Rewards Earned: <Text>GHS{state.summary?.total ?? 0}</Text></Text>
                                    </View>
                                    {state.referal?._id?<LoginPrimaryButton
                                        loading={state.loading}
                                        title="Delete Code"
                                        pressTo={removeReferalCode}
                                    />:<LoginPrimaryButton
                                        title="Generate Code"
                                        pressTo={()=>showModal(true)}
                                    />}
                                </ScrollView>
                            </View>
                            <Modal 
                                presentationStyle="formSheet"
                                animationType="slide"
                                onRequestClose={null}
                                visible={onShow}
                            >
                                <View style={styles.modal}>
                                    <View style={styles.closeContainer}>
                                        <IconButton onPress={()=>showModal(false)} name="times"/>
                                    </View>
                                    <View style={styles.modalContent}>
                                        <Text style={{
                                            marginVertical:15,
                                            fontSize:20,
                                            fontWeight:'200'
                                        }}>Enter a Referral code</Text>
                                        <TxtInput onChangeText={(v)=>code=v.trim()} placeholder="type a name"/>
                                        <LazyArrowButton
                                            onPress={sendRequest}
                                            loading={state.loading}
                                            style={{
                                                marginVertical:20
                                            }}
                                        />
                                    </View>
                                </View>
                            </Modal>
                        </>
                    )
                }
            }
        </ReferralConsumer>
    )
}

const styles=StyleSheet.create({
    container:{
        height:height,
        backgroundColor:'#fff'
    },
    scroll:{
        paddingHorizontal:50,
    },
    mainText:{
        paddingVertical:50,
        fontSize:20,
        fontWeight:'200'
    },
    code:{
        backgroundColor:'#eee',
        padding:20,
        borderRadius:5,
        marginVertical:25,
        flexDirection:'row',
        alignItems:'center'
    },
    modalContainer:{
        backgroundColor:'yellow'
    },
    modal:{
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#fff'
    },
    closeContainer:{
        position:'absolute',
        top:height/20,
        left:0,
        margin:50
    },
    modalContent:{
        marginVertical:height/4,
        justifyContent:'center',
        alignItems:'center'
    },
    summaryCont:{
        marginVertical:20
    },
    summayText:{
        fontWeight:'200',
        marginVertical:2
    }
});

export default ReferalsComponent;