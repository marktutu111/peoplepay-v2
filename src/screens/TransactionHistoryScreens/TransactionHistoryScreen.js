import React, {Component} from 'react';
import {View, Text, StyleSheet, FlatList,ActivityIndicator} from 'react-native';
import Colors from '../../constants/colors';
import Header from '../../components/Header'
import HeaderTxt1 from '../../components/HeaderTxt1';
import service from '../../services/transactions.service';
import {AuthConsumer} from '../../states/auth.state';
import HistoryDetails from "../../components/HistoryDetails"
import colors from '../../constants/colors';
import moment from "moment";
import AsyncStorage from '@react-native-community/async-storage';




class TransactionHistoryScreen extends Component {

    isLoaded=false; // note this flag denote mount status
    state={
        loading:false,
        transactions:[],
        lastDate:''
    };


    componentDidMount=()=>{
        this.isLoaded=true;
        this.getHistory();
    };


    componentWillUnmount(){
        this.isLoaded=false;
        this.getHistory=null;
    }


    async loadCache(){
        try {
            const cache=await AsyncStorage.getItem('ppay-trans-history');
            if(typeof cache==='string'){
                const data=JSON.parse(cache);
                this.isLoaded && this.setState(
                    {
                        transactions:data
                    }
                );
            }
        } catch (err) {}
    }


    getHistory=async()=>{
        try {
            this.setState(
                {
                    loading:true
                },this.loadCache
            );
            const _id = this.user._id;
            if (!_id) {
                return;
            }
            const response=await service.getCustomerTransactions(_id);
            if (!response.success) {
                throw Error(
                    response.message
                );
            }
            await AsyncStorage.setItem(
                'ppay-trans-history',
                JSON.stringify(
                    response[
                        'data'
                    ]
                )
            )
            return this.isLoaded && this.setState(
                {
                    loading: false
                },this.loadCache
            );
        } catch (err) {
            this.isLoaded && this.getHistory();
        }
    };


    getDate(date){
        const{lastDate}=this.state;
        const _date=moment(date).format('DD/MM/YYYY');
        if(lastDate===_date){
            return {
                show:false,
                date:_date
            }
        }
        this.state.lastDate=_date;
        return {
            show:true,
            date:_date
        }
    }


    render() {

        const getTitle=(type)=>{
            switch (type) {
                case 'RM':
                    return 'RECEIVED MONEY';
                case 'SM':
                    return 'SENT MONEY';
                case 'ECARDS':
                    return 'GIFT CARD';
                case 'QRP':
                    return 'GHQR PAY';
                case 'AT':
                    return 'AIRTIME TOPUP';
                case 'VCI':
                    return 'VISA CARD ISSUANCE';
                case 'VCT':
                    return 'VISA CARD LOAD';
                case 'CA':
                    return 'CARD AUTHORIZATION';
                case 'WF':
                    return 'ACCOUNT FUNDING';
                case 'RF':
                    return 'TRANSACTION REVERSAL';
                default:
                    return 'BILL PAY';
            }
        }

        return (
            <>
                <AuthConsumer>
                    {({state}) => {
                        this.user=state.user;
                    }}
                </AuthConsumer>
                <Header
                    text="Transactions"
                    pressTo={()=>this.props.navigation.goBack()}
                />
                <View style={styles.screen}>
                    <FlatList
                            data={this.state.transactions}
                            keyExtractor={(v,i)=>i.toString()}
                            ListEmptyComponent={() => {
                                return this.state.loading ? (
                                    <ActivityIndicator
                                        color={colors.secondary}
                                        size={'large'}
                                        style={{marginVertical:20}}
                                    />
                                ) : (
                                    <Text
                                        style={{
                                            margin: 50,
                                            fontSize: 20,
                                            fontFamily: 'Roboto-Bold',
                                        }}>
                                        You don't have any transaction yet
                                    </Text>
                                );
                            }}
                            renderItem={({item})=>{
                                return (
                                    <>
                                    {this.getDate(item.createdAt).show && <View style={{
                                        padding:15
                                    }}>
                                        <Text style={styles.date}>{this.getDate(item.createdAt).date}</Text>
                                    </View>}
                                    <HistoryDetails
                                        onPress={()=>this.props.navigation.navigate('HistoryDetails', { transaction:item })}
                                        title={getTitle(item.transaction_type)}
                                        amount={`GHS${item.actualAmount}`}
                                        from={item.recipient_account_name}
                                        date={new Date(item.createdAt).toDateString()}
                                        showArrow={true}
                                        data={item}
                                    />
                                    </>
                                );
                            }}
                        />
                </View>
            </>
        );
    }
}

const styles = StyleSheet.create({
    screen:{
        height:'100%',
        backgroundColor:'#fff',
        paddingHorizontal:10,
        justifyContent:'center',
        alignItems:'center',
    },
    container:{
        backgroundColor:Colors.accent,
        padding: 20,
        borderRadius: 2,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.21,
        shadowRadius: 10,
        elevation: 3,
        justifyContent: 'center',
    },
    TotalCostContainer:{
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.08)',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 5,
    },
    TotalCost:{
        fontSize: 25,
        color: Colors.tertiary,
        textTransform: 'uppercase',
    },
    NameContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        padding:5,
        borderBottomWidth:1,
        borderBottomColor:'rgba(0,0,0,0.08)',
    },
    name:{
        fontSize:16,
        paddingBottom:5,
    },
    clear: {
        fontSize:14,
        color:Colors.primary,
        textTransform:'uppercase',
    },
    date: {
        fontSize: 14,
        color: Colors.secondary,
        textTransform: 'uppercase',
        opacity: 0.7,
    },
    Amount: {
        fontSize: 16,
        paddingBottom: 5,
    },
    LogoContainer: {
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.08)',
    },
    logo: {
        borderRadius: 200,
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10,
    },
    Logo: {
        borderRadius: 200,
        padding: 20,
        height: 50,
    },
    LogoText: {
        fontSize: 14,
    },
    navIcon: {},
});

export default TransactionHistoryScreen;
