import React, {Component} from 'react';
import {Text, View, StyleSheet, Image, ScrollView,Dimensions} from 'react-native';
import Colors from '../../constants/colors';
import Header from '../../components/Header';
import issuer from '../../services/issuers.service';
import moment from 'moment';
import colors from '../../constants/colors';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import { Alert } from 'react-native';
import TransactionService from "../../services/transactions.service";


const {height,width}=Dimensions.get('screen');

class HistoryFilterScreen extends Component {

    state={
        issuers:[],
        loading:false
    };

    getStatus(status){
        switch (status) {
            case 'pp':
                return 'AUTHORIZATION PENDING'
            default:
                return status.toUpperCase();
        }
    }
    

    // getIssuerName=(issuerid)=>{
    //     const issuerSet=[...this.state.issuers];
    //     const specificIssuer=issuerSet.map((d)=>d.value === issuerid ? d.name : null);
    //     return specificIssuer;
    // };


    getIssuers=async()=>{
        try {
            const response=await issuer.getIssuers();
            if (response.success) {
                return this.setState({
                    issuers:response.data.map((d)=>{
                        return {
                            name:d.name,
                            value: d.id
                        };
                    }),
                });
            }
            throw Error(response.message);
        } catch (err) {
            this.setState({loading: false});
        }
    };


    componentDidMount() {
        this.getIssuers();
    }



    reversal=(id)=>{
        if(typeof id !== 'string' || id==='')return;
        Alert.alert(
            'Confirm Reversal',
            'You have requested to reverse this transaction amount from your PeoplesPay Wallet into your source account. There is no charge for this reversal, Confirm to proceed?',
            [
                {
                    text:'Yes,Proceed',
                    onPress:async()=>{
                        try {
                            this.setState(
                                {
                                    loading:true
                                }
                            )
                            const response=await TransactionService.reversal(
                                {
                                    id:id
                                }
                            );
                            if(!response.success){
                                throw Error(
                                    response[
                                        'message'
                                    ]
                                )
                            };
                            this.setState(
                                {
                                    loading:false
                                },()=>{
                                    Alert.alert(
                                        'Transaction Successful',
                                        response[
                                            'message'
                                        ]
                                    )
                                }
                            )
                        } catch (err) {
                            this.setState(
                                {
                                    loading:false
                                },()=>{
                                    Alert.alert(
                                        'Transaction Failed',
                                        err[
                                            'message'
                                        ]
                                    )
                                }
                            )
                        }
                    }
                },
                {
                    text:'No,Cancel',
                    onPress:null
                }
            ]
        )
    }


    render() {

        const getTitle=(type)=>{
            switch (type) {
                case 'SM':
                    return 'SENT MONEY';
                case 'ECARDS':
                    return 'GIFT CARD';
                case 'QRP':
                    return 'GHQR PAY';
                case 'AT':
                    return 'AIRTIME TOPUP';
                case 'VCI':
                    return 'VISA CARD ISSUANCE';
                case 'VCT':
                    return 'VISA CARD LOAD';
                case 'CA':
                    return 'CARD AUTHORIZATION'
                case 'WF':
                    return 'ACCOUNT FUNDING'
                default:
                    return 'BILL PAY';
            }
        }

        const {
            _id,
            amount,
            createdAt,
            status,
            recipient_account_number,
            payment_account_number,
            recipient_account_issuer,
            payment_account_issuer,
            reference,
            recipient_account_name,
            transaction_type,
            payment_account_type,
            refund_payload,
            gift_card,
            actualAmount,
            billName,
            debit_status,
            refund_status,
            payment_account_name,
            recipient_account_issuer_name
        }=this.props.navigation.getParam('transaction');

        const stats={
            failed:'red',
            pending:'blue',
            paid:'green',
            pp:'orange'
        };

        
        return (
            <ScrollView>
                    <Header text='Transaction Details' pressTo={()=>this.props.navigation.goBack()} />
                <View style={styles.screen}>
                    <View style={styles.container}>
                        {status==='paid' && <View style={styles.TotalCostContainer}>
                            <Text style={styles.TotalCost}>GHS{amount}</Text>
                            <Text>amount with charges</Text>
                        </View>}
                        <View style={styles.NameContainer}>
                            <View>
                                <Text style={styles.name}>Amount</Text>
                                <Text style={styles.name}>Date</Text>
                                <Text style={styles.name}>Reference</Text>
                                <Text style={styles.name}>Status</Text>
                                <Text style={styles.name}>Payment Account</Text>
                                <Text style={styles.name}>Transaction</Text>
                            </View>
                            <View style={{alignItems:'flex-end'}}>
                                <Text style={styles.Amount}>GHS{actualAmount}</Text>
                                <Text style={styles.Amount}>
                                    {moment(createdAt).format('MMM Do YY')} -{' '}
                                    {moment(createdAt).format('LT')}
                                </Text>
                                <Text style={styles.Amount}>{reference}</Text>
                                <Text
                                    style={{
                                        ...styles.Amount,
                                        color:stats[status],
                                    }}>
                                    {this.getStatus(status)}
                                </Text>
                                <Text
                                    style={{
                                        ...styles.Amount,
                                        color:colors.primary,
                                    }}>
                                    {payment_account_type}
                                </Text>
                                <Text
                                    style={{
                                        ...styles.Amount,
                                        color:colors.primary,
                                    }}>
                                    {getTitle(transaction_type)}
                                </Text>
                            </View>
                        </View>
                        {transaction_type !== 'ECARDS' && transaction_type !== 'CA' && transaction_type !== 'PB'  && <View style={styles.LogoContainer}>
                            <Text style={styles.LogoText}>
                                Recipient Account Details
                            </Text>
                            <View style={styles.logo}>
                                <View style={{paddingHorizontal: 10}}>
                                    <Text style={styles.LogoText}>
                                        {recipient_account_number || recipient_account_name}
                                    </Text>
                                    <Text style={styles.LogoText}>
                                        {recipient_account_issuer_name || 'UNKNOWN'}
                                    </Text>
                                </View>
                            </View>
                        </View>}
                        {transaction_type==='PB' && <View style={styles.LogoContainer}>
                            <Text style={styles.LogoText}>
                                BILL Details
                            </Text>
                            <View style={styles.logo}>
                                <View style={{paddingHorizontal: 10}}>
                                    <Text style={styles.LogoText}>
                                        {billName}
                                    </Text>
                                </View>
                            </View>
                        </View>}
                        {transaction_type==='ECARDS' && <View style={styles.LogoContainer}>
                            <Text style={styles.LogoText}>
                                CARD Details
                            </Text>
                            <View style={styles.logo}>
                                <View style={{paddingHorizontal: 10}}>
                                    <Text style={styles.LogoText}>
                                        {gift_card?.ProductName}
                                    </Text>
                                </View>
                            </View>
                        </View>}
                        {(payment_account_type === 'momo' || payment_account_type === 'card' || payment_account_type === 'token')  && <View style={styles.LogoContainer}>
                            <Text style={styles.LogoText}>
                                Payment Details
                            </Text>
                            <View style={styles.logo}>
                                <View style={{paddingHorizontal: 10}}>
                                    <Text style={styles.LogoText}>
                                        {payment_account_number}
                                    </Text>
                                    <Text style={styles.LogoText}>
                                        {payment_account_name}
                                    </Text>
                                </View>
                            </View>
                        </View>}
                    </View>
                </View>
                {/* <Icon
                    name="trash"
                    type="feather"
                    color="#888"
                    size={30}
                    containerStyle={styles.navIcon}
                /> */}
                {status==='failed' && debit_status==='paid' && payment_account_type==='momo' && <LoginPrimaryButton
                    pressTo={()=>this.reversal(_id)}
                    loading={this.state.loading}
                    title="Self Reversal"
                    style={{
                        width:200,
                        marginVertical:15
                    }}
                />}
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        width:width,
        height:height,
        backgroundColor:'#fff',
        justifyContent:'flex-start',
        alignItems:'center'
    },
    container: {
        backgroundColor:'#fff',
        borderRadius:2,
        shadowColor: '#000',
        justifyContent: 'center',
    },
    TotalCostContainer: {
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.08)',
        width: '100%',
        justifyContent: 'center',
        paddingVertical:30,
    },
    TotalCost: {
        fontSize: 45,
        fontFamily: 'Roboto-Regular',
        color: Colors.tertiary,
        textTransform: 'uppercase',
    },
    NameContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical:5,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.08)',
    },
    name: {
        fontSize: 16,
        fontFamily: 'Roboto-Bold',
        padding: 10,
    },
    clear: {
        fontSize: 14,
        color: Colors.primary,
        textTransform: 'uppercase',
    },
    date: {
        fontSize: 14,
        color: Colors.primary,
        textTransform: 'uppercase',
        opacity: 0.7,
    },
    Amount: {
        fontSize: 16,
        fontFamily: 'Roboto-Regular',
        padding: 10,
    },
    LogoContainer: {
        // padding: 15,
        paddingVertical:15,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.08)',
    },
    logo: {
        borderRadius: 200,
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10,
    },
    Logo: {
        borderRadius: 200,
        padding: 20,
        height: 50,
    },
    LogoText: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        paddingBottom: 5,
    },
    navIcon: {},
});

export default HistoryFilterScreen;
