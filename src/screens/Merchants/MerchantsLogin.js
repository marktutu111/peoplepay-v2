import React, { Component } from 'react'
import { Text, View,ScrollView,StyleSheet,Dimensions,ActivityIndicator } from 'react-native'
import Header from '../../components/Header';
import TxtInput from '../../components/TxtInput';
import colors from '../../constants/colors';
import LazyArrowButton from '../../components/LazyArrowButton';
import { Alert } from 'react-native';
import service from "../../services/merchant.service";
import AsyncStorage from "@react-native-community/async-storage";
import FormatAmount from '../../constants/currencyFormatter';
import HistoryDetails from '../../components/HistoryDetails';
import moment from "moment";


const {width,height}=Dimensions.get('screen');



class MerchantLogin extends Component {

    isLoaded=false;

    state={
        loading:false,
        merchant:{},
        summary:{},
        transactions:[]
    }


    componentDidMount(){
        this.isLoaded=true;
        this.getMerchant();
    }


    componentWillUnmount(){
        this.isLoaded=false;
    }


    getTransactions=async()=>{
        try {
            this.setState(
                {
                    loading:true
                }
            )
            const {_id}=this.state.merchant;
            const response=await service.getMerchantTransactions(_id);
            if(!response.success){
                throw Error(
                    response[
                        'message'
                    ]
                )
            };
            const [summary,transactions]=response['data'];
            this.setState(
                {
                    loading:false,
                    transactions:transactions,
                    summary:summary && summary[0]
                }
            )
        } catch (err) {
            this.setState(
                {
                    loading:false
                }
            )
        }
    }


    getMerchant=async()=>{
        try {
            const data=await AsyncStorage.getItem('ppay-merchant-account');
            if(typeof data!=='string'){
                throw Error(
                    'No account'
                )
            }
            const merchant=JSON.parse(data);
            if(!merchant || !merchant._id){
                throw Error(
                    'Invalid Account'
                )
            }
            this.setState(
                {
                    merchant:merchant
                },this.getTransactions.bind(this)
            )
        } catch (err) {
            this.setState(
                {
                    merchant:{}
                }
            )
        }
    }


    next=async()=>{
        try {
            const {id}=this.state;
            if(!id || id===''){
                return;
            }
            this.setState(
                {
                    loading:true
                }
            );
            const response=await service.checkMerchant(id);
            if(!response || !response.success){
                throw Error(
                    response[
                        'message'
                    ]
                )
            };
            const merchant=response['data'];
            this.setState(
                {
                    loading:false,
                    merchant:merchant
                },()=>{
                    AsyncStorage.setItem('ppay-merchant-account',JSON.stringify(merchant));
                    this.getTransactions();
                }
            )
        } catch (err) {
            Alert.alert(
                'Invalid ID or Phone number',
                err.message
            )
            this.setState(
                {
                    loading:false
                }
            )
        }
    }

    getDate(date){
        const{lastDate}=this.state;
        const _date=moment(date).fromNow();
        if(lastDate===_date){
            return {
                show:false,
                date:_date
            }
        }
        this.state.lastDate=_date;
        return {
            show:true,
            date:_date
        }
    }

    logout=async()=>{
        try {
            await AsyncStorage.removeItem('ppay-merchant-account');
            this.getMerchant();
        } catch (err) {}
    }

    render() {
        return (
            <View style={styles.container}>
                <Header 
                    iconRight 
                    iconRName="sign-out" 
                    text='Merchant' 
                    pressTo={()=>this.props.navigation.navigate('Home')}
                    iconRPress={this.logout.bind(this)}
                />
                <ScrollView>
                    {
                        this.state.merchant && this.state.merchant._id ? (
                            <View style={styles.screen}>
                                <View style={{
                                    marginVertical:50,
                                    alignItems:'center'
                                }}>
                                    <Text style={{fontSize:20,fontWeight:'700'}}>{this.state.merchant?.name}</Text>
                                    <Text style={{
                                        fontSize:30,
                                        marginVertical:10
                                    }}>{FormatAmount(this.state.summary?.amount || 0)}</Text>
                                    <Text style={{fontWeight:'100'}}>Available</Text>
                                </View>
                                <View style={{marginBottom:30}}>
                                    { this.state.transactions.length <=0 && !this.state.loading && <Text
                                            style={{
                                                margin: 50,
                                                fontSize: 20,
                                                fontFamily: 'Roboto-Bold',
                                            }}>
                                            You don't have any transaction yet
                                        </Text> 
                                    }
                                    {
                                        this.state.loading && <ActivityIndicator
                                            color={colors.secondary}
                                            size={'large'}
                                            style={{marginVertical:20}}
                                        />
                                    }
                                    {
                                        this.state.transactions.map((item,i)=>{
                                            return (
                                                <>
                                                {this.getDate(item.createdAt).show && <View style={{
                                                    padding:15
                                                }}>
                                                    <Text style={styles.date}>{this.getDate(item.createdAt).date}</Text>
                                                </View>}
                                                <HistoryDetails
                                                    key={i.toString()}
                                                    title={item.receiverAccount}
                                                    amount={FormatAmount(item.transactionAmount)}
                                                    from={item.receiverAccount}
                                                    date={new Date(item.createdAt).toDateString()}
                                                    data={item}
                                                    status={item.status}
                                                />
                                                </>
                                            );
                                        })
                                    }
                                </View>
                            </View>
                        ):(
                            <View style={styles.screen}>
                                <Text style={{marginTop:50}}>Merchant Login</Text>
                                <Text style={{
                                    fontSize:20,
                                    textAlign:'center',
                                    padding:25,
                                    fontWeight:'200'
                                }}>Enter your merchant Account Number or Phone Number</Text>
                                <View style={{
                                    justifyContent:'center',
                                    alignItems:'center'
                                }}>
                                    <TxtInput
                                        style={{width:300}}
                                        placeholder="Merchant ID"
                                        fa={true}
                                        onChangeText={v=>{
                                            this.setState(
                                                {
                                                    id:v.trim()
                                                }
                                            )
                                        }}
                                    />
                                </View>
                                <LazyArrowButton
                                    loading={this.state.loading}
                                    onPress={this.next.bind(this)}
                                    style={{
                                        marginVertical:'30%'
                                    }}
                                />
                            </View>
                        )
                    }
                </ScrollView>
            </View>
        )
    }
}



const styles=StyleSheet.create({
    screen: {
        alignItems:'center'
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
        paddingHorizontal: 30
    },
    button:{
        width:'50%',
        borderRadius:0,
        backgroundColor:'transparent'
    },
    textStyle:{
        color:colors.secondary
    },
    container:{
        height:height,
        backgroundColor:'#fff'
    }
});

export default MerchantLogin;
