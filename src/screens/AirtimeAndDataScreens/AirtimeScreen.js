import React, {Component} from 'react';
import {View, Text, StyleSheet, Alert} from 'react-native';
import Colors from '../../constants/colors';
import Header from '../../components/Header';
import {ScrollView} from 'react-native-gesture-handler';
import TxtInput from '../../components/TxtInput';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import {CustomExample} from '../../components/customPicker';
import ErrorText from '../../components/ErrorText';

import HeaderTxt1 from '../../components/HeaderTxt1';
// change cont declaration name and export default name
class AirtimeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            amt: '',
            amtErr: '',
            product: '',
            network: '',
            phone: '',
            phoneErr: '',
        };
    }
    Validate = type => {
        const {amt, phone} = this.state;
        let rjx = /^\+?\(?(([2][3][3])|[0])?\)?[-.\s]?\d{2}[-.\s]?\d{3}[-.\s]?\d{4}?$/,
            amtrjx = /\d{1,5}/;
        switch (type) {
            case 'number':
                if (rjx.test(phone) === false) {
                    this.setState({
                        phoneErr: 'Please enter a valid mobile number',
                    });
                } else {
                    this.setState({
                        phoneErr: '',
                    });
                }
                break;
            case 'amt':
                if (amtrjx.test(amt) === false || amt <= 0) {
                    this.setState({
                        amtErr: 'Please enter valid amount',
                    });
                } else {
                    this.setState({
                        amtErr: '',
                    });
                }
                break;
            default:
                return;
        }
    };

    render() {
        return (
            <View style={styles.screen}>
                {/* Header of the Screen */}
                <Header pressTo={() => this.props.navigation.goBack()} />
                <ScrollView style={styles.scroll}>
                    {/* Screen Title */}
                    <HeaderTxt1
                        txt1="Airtime & "
                        txt2="Data"
                        pressTo={() => this.props.navigation.goBack()}
                    />
                    <View style={styles.Body}>
                        <View style={styles.dropdwonContainer}>
                            <Text style={styles.dropdwonContainerText}>
                                Enter Recipient Details
                            </Text>
                        </View>
                        <View style={styles.TxtInputContainer}>
                            <CustomExample
                                placeholder="Select Network"
                                items={[
                                    {
                                        name: 'Mtn',
                                        value: 'Mtn',
                                    },
                                    {
                                        name: 'Vodafone',
                                        value: 'Vodafone',
                                    },
                                    {
                                        name: 'AirtelTigo',
                                        value: 'Airtel',
                                    },
                                ]}
                                onValueChange={v=>this.setState({
                                        network:v,
                                    })
                                }
                            />
                            {/* </View> */}
                            <TxtInput
                                placeholder="Mobile Number"
                                IconName="idcard"
                                IconType="antdesign"
                                keyboardType="numeric"
                                onBlur={()=>this.Validate('number')}
                                onChangeText={v=>this.setState({phone:v.trim()})}
                            />
                            {this.state.phoneErr ? (
                                <ErrorText message={this.state.phoneErr} />
                            ) : null}
                            <CustomExample
                                placeholder="Select Product Type"
                                items={[
                                    {
                                        name: 'Airtime',
                                        value: 'Airtime',
                                    },
                                    {
                                        name: 'Data',
                                        value: 'Data',
                                    },
                                ]}
                                onValueChange={v=>this.setState({product:v})}
                            />
                            <TxtInput
                                placeholder="Amount"
                                IconName="money"
                                IconType="font-awesome"
                                keyboardType="number-pad"
                                onChangeText={v=>this.setState({amt:v.trim()})}
                                onBlur={()=>this.Validate('amt')}
                            />
                            {this.state.amtErr ? (
                                <ErrorText message={this.state.amtErr} />
                            ) : null}
                        </View>
                        <View style={styles.buttonContainer}>
                            <LoginPrimaryButton
                                title={`PAY GHS ${
                                    this.state.amt
                                        ? this.state.amt + '.00'
                                        : '0.00'
                                }`}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}



const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    HeaderTextMain: {
        textAlign: 'left',
        paddingHorizontal: 30,
        // paddingTop: 40,
        paddingBottom: 20,
        paddingTop: 20,
    },
    HeaderText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 30,
        color: Colors.secondary,
        // textAlign: 'center',
        opacity: 0.8,
    },
    HeaderText1: {
        fontFamily: 'Roboto-Bold',
        fontSize: 30,
        color: Colors.primary,
        // textAlign: 'center',
        opacity: 0.8,
        bottom: 15,
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
    },
    dropdwonContainer: {
        paddingVertical: 10,
    },
    dropdwonContainerText: {
        justifyContent: 'center',
        paddingHorizontal: 30,
        flex: 1,
        fontFamily: 'Roboto-Bold',
        opacity: 0.5,
        color: '#262626',
    },
    TxtInputContainer: {
        paddingHorizontal: 30,
    },
    buttonContainer: {
        alignItems: 'center',
        paddingVertical: 20,
        paddingHorizontal: 20,
    },

    lineContainer: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        paddingHorizontal: 30,
    },
    line: {
        borderColor: Colors.secondary,
        borderBottomWidth: 1,
    },
    textOr: {
        fontFamily: 'Roboto-Bold',
        color: Colors.secondary,
    },
});

export default AirtimeScreen;
