import React, {Component} from 'react';
import {View, Text, StyleSheet, Alert} from 'react-native';
import Colors from '../../constants/colors';
import Header from '../../components/Header';
import {ScrollView} from 'react-native-gesture-handler';

import ErrorText from '../../components/ErrorText';
import TxtInput from '../../components/TxtInput';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import {AuthConsumer} from '../../states/auth.state';
import {CustomExample} from '../../components/customPicker';
import service from '../../services/beneficiary.service';
import HeaderText from '../../components/HeaderText';
import { TransactionContext } from "../../states/transactions.state";



// change cont declaration name and export default name
class AddBankBeneficiary extends Component {

    static contextType=TransactionContext;

    state={
        loading: false,
        account_type: '',
        account_name: '',
        account_nameValid:true,
        account_number: '',
        account_numberValid:true,
        account_issuer: '',
        account_issuerValid:true,
    }

    validate = (text, type) => {
        const {account_type}=this.state;
        if (account_type === 'momo' && type === 'accountnumber') {
            if (!/^[0]?\d{9}$/.test(text)) {
                this.setState(
                    {
                        account_numberValid: false,
                    }
                );
            } else {
                this.setState(
                    {
                        account_numberValid: true,
                    }
                );
            }
        }
        if (account_type === 'bank' && type === 'accountnumber') {
            if (!/^\d{10,17}$/.test(text)) {
                this.setState({
                    account_numberValid: false,
                });
            } else {
                this.setState({
                    account_numberValid: true,
                });
            }
        }
        if (type === 'accountname') {
            if (text === '' || !/^[a-zA-Z]+/.test(text)) {
                this.setState({
                    account_nameValid: false,
                });
            } else {
                this.setState({
                    account_nameValid: true,
                });
            }
        }
        if (type === 'issuer') {
            if (text === '') {
                this.setState({
                    account_issuerValid: false,
                });
            } else {
                this.setState({
                    account_issuerValid: true,
                });
            }
        }
    };

    addBeneficiary=async()=>{
        try {

            const {transaction,returnPage}=this.context.state;
            const {_id} = this.user;
            if (!_id) {
                return;
            }

            const {
                account_name,
                account_number,
            } = this.state;
            if (account_name === '' || account_number === '') {
                return;
            }

            this.setState({loading: true})
            let data={
                customerId: _id,
                account_type:transaction.account_type,
                account_name:account_name,
                account_number:account_number,
                account_issuer:transaction.account_issuer,
                account_issuer_name:transaction.account_issuer_name,
                account_issuer_image:transaction.account_issuer_image
            };

            const response=await service.addBeneficiary(data);
            if (response.success) {
                return this.setState(
                    {
                        loading: false,
                    },
                    () => {
                        this.props.navigation.navigate(returnPage);
                        Alert.alert(
                            'success',
                            'Your beneficiary account has been created successfully',
                        );
                    },
                );
            }
            throw Error(response.message);
        } catch (err) {
            this.setState(
                {
                    loading: false,
                },
                () => Alert.alert('Oops!', err.message),
            );
        }
    };

    render=()=>{

        const {transaction}=this.context.state;
        const account=this.props.navigation.getParam('account');
        this.state.account_type = account;

        return (
            <View style={styles.screen}>
                <AuthConsumer>
                    {({state}) => {
                        this.user = state.user;
                    }}
                </AuthConsumer>
                {/* Header of the Screen */}
                <Header pressTo={() => this.props.navigation.goBack()} />
                <ScrollView
                    style={styles.scroll}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag">
                    
                    <HeaderText
                        title1={`Enter your ${transaction.account_issuer_name}`} 
                        title2="Account Details" 
                    />

                    <View style={styles.Body}>
                        <View>
                            <TxtInput
                                style={
                                    !this.state.account_nameValid
                                        ? {
                                              borderColor: 'red',
                                              borderWidth: 3,
                                          }
                                        : null
                                }
                                placeholder="Name"
                                onChangeText={(v) => {
                                    this.setState({account_name:v});
                                    this.validate(v,'accountname');
                                }}
                            />
                            <TxtInput
                                style={
                                    !this.state.account_numberValid
                                        ? {
                                              borderColor: 'red',
                                              borderWidth: 3,
                                          }
                                        : null
                                }
                                placeholder="number"
                                keyboardType="number-pad"
                                onChangeText={(v) => {
                                    this.setState({account_number: v.trim()});
                                    this.validate(v, 'accountnumber');
                                }}
                            />
                        </View>
                        <View style={styles.buttonContainer}>
                            <LoginPrimaryButton
                                loading={this.state.loading}
                                title="Save"
                                pressTo={this.addBeneficiary.bind(this)}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    };
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    HeaderTextMain: {
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingTop: 10,
        paddingBottom: 20,
        // flexDirection: 'row',
    },
    HeaderText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 30,
        color: Colors.tetiary,
        // textAlign: 'left',
        opacity: 0.8,
    },
    HeaderText1: {
        fontFamily: 'Roboto-Regular',
        fontSize: 30,
        color: Colors.secondary,
        // textAlign: 'center',
        opacity: 0.8,
        bottom: 10,
        marginTop: 5,
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
        // width:'90%',
        // marginRight:'aut',
        // marginLeft:'auto'
        paddingHorizontal: 30,
        paddingTop: 10,
    },
    textFieldContainer: {
        paddingHorizontal: 30,
    },
    dropdownContainer: {
        paddingVertical: 5,
        paddingHorizontal: 30,
    },
    dropdownContainerText: {
        justifyContent: 'center',
        paddingHorizontal: 30,
        flex: 1,
        fontFamily: 'Roboto-Bold',
        opacity: 0.7,
    },
    buttonContainer: {
        // alignItems: 'center',
        paddingVertical: 20,
        paddingHorizontal: 30,
    },
    ViewAllContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginBottom: 10,
        alignSelf: 'flex-end',
        marginHorizontal: 20,
    },
    ViewAllText: {
        textAlign: 'center',
        // backgroundColor: Colors.primary,,
        paddingHorizontal: 15,
        paddingVertical: 10,
        color: Colors.tetiary,

        fontSize: 12,
        fontFamily: 'Roboto-Medium',
    },
    cardItemlabel: {
        textAlign: 'center',
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
    },
    containerStyle: {
        padding: 10,
        borderWidth: 0,
        marginTop: 10,
    },
});

export default AddBankBeneficiary;
