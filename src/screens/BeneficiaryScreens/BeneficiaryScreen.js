import React, { Component } from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Colors from '../../constants/colors';
import CardThumbnail2 from '../../components/CardThumbnail2';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import Header from '../../components/Header';
import HeaderText from '../../components/HeaderText';
import { TransactionContext } from '../../states/transactions.state';

class AddBeneficiary extends Component {

    static contextType=TransactionContext;

    state={}


    render(){
        const {updateState}=this.context;
        return (
            <View style={styles.screen}>
                <Header text="Accounts" pressTo={()=>this.props.navigation.navigate("Home")} />
                <ScrollView
                    style={styles.scroll}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag">
                    <HeaderText
                        title1="Choose"
                        title2="Beneficiary Account Type"
                        pressTo={()=>props.navigation.goBack()}
                    />
                    <View style={styles.Body}>
                        <View>
                            <CardThumbnail2
                                iconname="bank"
                                icontype="material-community"
                                title="Bank Account"
                                subtitle="Add beneficiary bank account"
                                pressTo={()=>{
                                    updateState(
                                        {
                                            returnPage:'BankBeneficiary',
                                            transaction:{
                                                account_type:'bank'
                                            }
                                        },()=>{
                                            this.props.navigation.navigate(
                                                'CustomPicker',
                                                {
                                                    type:'bank',
                                                    page:'AddBeneficiaryBankAccount',
                                                    key:['account_issuer','account_issuer_name','account_issuer_image']
                                                }
                                            )
                                        }
                                    )
                                }}
                            />
                        </View>
    
                        <View>
                            <CardThumbnail2
                                iconname="mobile"
                                icontype="entypo"
                                title="Mobile Money Account"
                                subtitle="Add beneficiary mobile money account"
                                pressTo={() =>{
                                    updateState(
                                        {
                                            returnPage:'BankBeneficiary',
                                            transaction:{
                                                account_type:'momo'
                                            }
                                        },()=>{
                                            this.props.navigation.navigate(
                                                'CustomPicker',
                                                {
                                                    type:'momo',
                                                    page:'AddBeneficiaryBankAccount',
                                                    key:['account_issuer','account_issuer_name','account_issuer_image']
                                                }
                                            )
                                        }
                                    )
                                }}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }



};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    HeaderTextMain: {
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingTop: 30,
        paddingBottom: 20,
        // flexDirection: 'row',
    },
    HeaderText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.secondary,
        // textAlign: 'center',
        opacity: 0.8,
    },
    HeaderText1: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.primary,
        // textAlign: 'center',
        opacity: 0.8,
        bottom: 20,
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
        paddingHorizontal: '5%',
    },
    mainContainer: {
        flex: 1,
        flexWrap: 'wrap',
        flexDirection: 'row',
    },
    ViewAllContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginBottom: 10,
        alignSelf: 'flex-end',
        marginHorizontal: 20,
    },
    ViewAllText: {
        textAlign: 'center',
        // backgroundColor: Colors.primary,,
        paddingHorizontal: 15,
        paddingVertical: 10,
        color: Colors.tetiary,

        fontSize: 12,
        fontFamily: 'Roboto-Medium',
    },
    cardItemlabel: {
        textAlign: 'center',
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
    },
    containerStyle: {
        padding: 10,
        borderWidth: 0,
        // marginBottom:5,
        marginTop: 10,
    },
    cardrow: {
        width: '100%',
        justifyContent: 'space-evenly',
        flexDirection: 'row',
    },
});
export default AddBeneficiary;
