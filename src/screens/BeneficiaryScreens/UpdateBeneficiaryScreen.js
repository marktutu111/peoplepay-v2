import React, {Component} from 'react';
import {View, Text, StyleSheet, Alert} from 'react-native';
import Colors from '../../constants/colors';
import Header from '../../components/Header';
import {ScrollView} from 'react-native-gesture-handler';

import selectors from '../../constants/Selectables';
// import Dropdown from '../components/Dropdown';
import TxtInput from '../../components/TxtInput';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import {AuthConsumer} from '../../states/auth.state';

import service from '../../services/beneficiary.service';
import {CustomExample} from '../../components/customPicker';

// change cont declaration name and export default name
class UpdateBeneficiary extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            account_type: 'bank',
            account_name: '',
            account_number: '',
            account_issuer: '',
            account_numberValid: true,
            account_nameValid: true,
            account_issuerValid: true,
        };
    }

    componentDidMount = () => {
        const account = this.props.navigation.getParam('account');
        this.setState({account_type: account});
    };

    validate = (text, type) => {
        const {account_type} = this.state;

        if (account_type === 'momo' && type === 'accountnumber') {
            if (!/^[0]?\d{9}$/.test(text)) {
                this.setState({
                    account_numberValid: false,
                });
            } else {
                this.setState({
                    account_numberValid: true,
                });
            }
        }
        if (account_type === 'bank' && type === 'accountnumber') {
            if (!/^\d{10,17}$/.test(text)) {
                this.setState({
                    account_numberValid: false,
                });
            } else {
                this.setState({
                    account_numberValid: true,
                });
            }
        }
        if (type === 'accountname') {
            if (text === '' || !/^[a-zA-Z]+/.test(text)) {
                this.setState({
                    account_nameValid: false,
                });
            } else {
                this.setState({
                    account_nameValid: true,
                });
            }
        }
        if (type === 'issuer') {
            if (text === '') {
                this.setState({
                    account_issuerValid: false,
                });
            } else {
                this.setState({
                    account_issuerValid: true,
                });
            }
        }
    };

    addBeneficiary = async () => {
        try {
            const {_id} = this.user;
            if (!_id) {
                return;
            }
            const {account_issuer, account_name, account_number} = this.state;
            if (
                account_issuer === '' ||
                account_name === '' ||
                account_number === ''
            ) {
                return;
            }
            this.setState({loading: true});
            let data = Object.assign({customerId: _id}, this.state);
            delete data.loading;
            const response = await service.addBeneficiary(data);
            if (response.success) {
                return this.setState(
                    {
                        loading: false,
                    },
                    () => {
                        this.props.navigation.goBack();
                        Alert.alert(
                            'success',
                            'Your beneficiary account has been created successful',
                        );
                    },
                );
            }
            throw Error(response.message);
        } catch (err) {
            this.setState(
                {
                    loading: false,
                },
                () => Alert.alert('Oops!', err.message),
            );
        }
    };

    render = () => {
        const setTitle = () => {
            let string = this.state.account_type;
            if (string === 'bank') {
                return 'bank';
            }
            if (string === 'momo') {
                return 'mobile money';
            }
            if (string === 'proxy') {
                return 'Proxy Pay';
            }
        };
        return (
            <View style={styles.screen}>
                <AuthConsumer>
                    {({state}) => {
                        this.user = state.user;
                    }}
                </AuthConsumer>
                {/* Header of the Screen */}
                <Header pressTo={() => this.props.navigation.goBack()} />
                <ScrollView
                    style={styles.scroll}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag">
                    {/* Screen Title */}
                    <View style={styles.HeaderTextMain}>
                        <Text style={styles.HeaderText}>
                            Update beneficiary{' '}
                        </Text>
                        <Text style={styles.HeaderText1}>
                            {setTitle()} account{' '}
                        </Text>
                    </View>
                    {/* Screen Main Content */}
                    <View style={styles.Body}>
                        <View>
                            <TxtInput
                                style={
                                    !this.state.account_nameValid
                                        ? {
                                              borderColor: 'red',
                                              borderWidth: 3,
                                          }
                                        : null
                                }
                                placeholder={
                                    this.state.account_type === 'momo'
                                        ? 'Name'
                                        : 'Account Name'
                                }
                                onChangeText={v =>
                                    this.setState({
                                        account_name: v,
                                    })
                                }
                            />
                            <TxtInput
                                placeholder={
                                    this.state.account_type === 'momo'
                                        ? 'Mobile Number'
                                        : this.state.account_type === 'bank'
                                        ? 'Account Number'
                                        : 'Proxy Id'
                                }
                                keyboardType="number-pad"
                                onChangeText={v => {
                                    this.setState({
                                        account_number: v,
                                    });
                                    this.validate(v, 'accountnumber');
                                }}
                            />
                            {this.state.account_type === 'proxy' ? null : (
                                <CustomExample
                                    placeholder={`Select ${
                                        this.state.account_type === 'momo'
                                            ? 'mobile money type'
                                            : 'bank'
                                    }`}
                                    items={
                                        this.state.account_type === 'momo'
                                            ? selectors.NetworkProviders
                                            : selectors.BankNames
                                    }
                                    selectedValue={this.state.account_issuer}
                                    onValueChange={v => {
                                        this.setState({
                                            account_issuer: v,
                                        });
                                        this.validate(v, 'issuer');
                                    }}
                                />
                            )}
                        </View>
                        <View style={styles.buttonContainer}>
                            <LoginPrimaryButton
                                loading={this.state.loading}
                                title="Save"
                                pressTo={this.addBeneficiary.bind(this)}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    };
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    HeaderTextMain: {
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingTop: 10,
        paddingBottom: 20,
        // flexDirection: 'row',
    },
    HeaderText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 30,
        color: Colors.tetiary,
        // textAlign: 'left',
        opacity: 0.8,
    },
    HeaderText1: {
        fontFamily: 'Roboto-Regular',
        fontSize: 30,
        color: Colors.secondary,
        // textAlign: 'center',
        opacity: 0.8,
        bottom: 10,
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
        // width:'90%',
        // marginRight:'aut',
        // marginLeft:'auto'
        paddingHorizontal: 30,
        paddingTop: 10,
    },
    textFieldContainer: {
        paddingHorizontal: 30,
    },
    dropdownContainer: {
        paddingVertical: 5,
        paddingHorizontal: 30,
    },
    dropdownContainerText: {
        justifyContent: 'center',
        paddingHorizontal: 30,
        flex: 1,
        fontFamily: 'Roboto-Bold',
        opacity: 0.7,
    },
    buttonContainer: {
        // alignItems: 'center',
        paddingVertical: 20,
        paddingHorizontal: 30,
    },
    ViewAllContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginBottom: 10,
        alignSelf: 'flex-end',
        marginHorizontal: 20,
    },
    ViewAllText: {
        textAlign: 'center',
        // backgroundColor: Colors.primary,,
        paddingHorizontal: 15,
        paddingVertical: 10,
        color: Colors.tetiary,

        fontSize: 12,
        fontFamily: 'Roboto-Medium',
    },
    cardItemlabel: {
        textAlign: 'center',
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
    },
    containerStyle: {
        padding: 10,
        borderWidth: 0,
        // marginBottom:5,
        marginTop: 10,
    },
});

export default UpdateBeneficiary;
