import React, { Component } from 'react';
import {View, Text, StyleSheet,Alert} from 'react-native';
import Colors from '../constants/colors';
import { TransactionContext } from "../states/transactions.state";

import Header from '../components/Header';
import LoginPrimaryButton from '../components/LoginPrimaryButton';
import _TService from "../services/transactions.service";





class ConfirmTransaction extends Component {

    static contextType=TransactionContext;
    isViewLoaded=false;

    state={
        loading:false
    }

    componentDidMount(){
        this.isViewLoaded=true;
    }


    componentWillUnmount(){
        this.isViewLoaded=false;
    }


    pay=async()=>{
        const {
            transaction,
            returnPage
        }=this.context.state;
        const clearTransaction=this.context.clearTransaction;
        try {
            this.setState(
                {
                    loading:true
                }
            );
            const response=await _TService.createTransaction(transaction);
            if (!response.success) {
                throw Error(
                    response.message
                );
            };
            switch (transaction.payment_account_type) {
                case 'card':
                    this.isViewLoaded && this.setState(
                        {
                            loading:false
                        },()=>{
                            this.props.navigation.navigate(
                                'WebView',
                                {
                                    html:response.data,
                                    id:response.id,
                                    returnPage:returnPage
                                }
                            )
                        }
                    )
                    break;
                default:
                    this.isViewLoaded && this.setState(
                        { loading:false },()=>{
                            clearTransaction(()=>{
                                    Alert.alert('Transaction received and processed',response.message);
                                    this.props.navigation.navigate(
                                        returnPage
                                    );
                                }
                            )
                        }
                    );
                    break;
            }
        } catch (err) {
            this.setState(
                {
                    loading:false
                },()=>{
                    this.props.navigation.navigate(returnPage);
                    Alert.alert(
                        'Something Went Wrong',
                        err.message
                    )
                }
            )
        }
    }

    render(){

        const {transaction}=this.context.state;
        let message='';
        switch (transaction.transaction_type) {
            case 'PB':
                message=`You have requested to pay an amount of GHS${transaction.amount} for ${transaction.description}. Do you want to proceed?`;
                break;
            case 'AT':
                message=`You have requested to buy airtime of GHS${transaction.amount} for ${transaction.recipient_account_number}. Do you want to proceed?`;
                break;
            case 'ECARDS':
                message=`You have requested to pay an amount of GHS${transaction.amount} for ${transaction.gift_card.ProductName}. Do you want to proceed?`;
                break;
            case 'QRP':
                message=`You have requested to pay an amount of GHS${transaction.amount} to ${transaction.recipient_account_name}. Do you want to proceed?`;
                break;
            case 'VCI':
                message=`You have requested to pay an amount of GHS${transaction.amount} for your Virtual Card. Do you want to proceed?`;
                break;
            case 'VCT':
                message=`You have requested to pay an amount of GHS${transaction.amount} to Topup your Virtual Card. Do you want to proceed?`;
                break;
            case 'VCTR':
                message=`You have requested to Transfer an amount of GHS${transaction.amount} for your Virtual Card to card number ${transaction.card_transfer_data?.toCardId}. Do you want to proceed?`;
                break;
            case 'CA':
                message=`You will be debited an amount of GHS1 to authorize your card. The transaction will be reversed into your PoeplesPay Account when the authorization process is successful`;
                break;
            case 'WF':
                message=`You will be debited an amount of GHS${transaction.amount} to be deposited in your PeoplesPay Account. Please comfirm your transaction.`;
                break;
            case 'BI':
                message=`You have requested to pay an amount of GHS${transaction.amount} for ${transaction.bundleData.PricePlanName}. Do you want to proceed?`;
                break;
            default:
                message=`You have requested to send an amount of GHS${transaction.amount} to ${transaction.recipient_account_name}. Do you want to proceed?`;
                break;
        }


        return (
            <View style={styles.screen}>
                <Header text="Confirm Transaction" pressTo={()=>this.props.navigation.goBack()} />
                <View style={{
                    justifyContent:'center',
                    alignItems:'center',
                    marginVertical:40,
                    paddingHorizontal:30
                }}>
                    <Text style={{fontSize:20,fontWeight:'200'}}>{message}</Text>
                </View>
                <View style={{
                    marginVertical:'20%',
                    justifyContent:'center',
                    alignItems:'center',
                    width:'100%',
                }}>
                    <View style={{ width:'50%' }}>
                        <LoginPrimaryButton 
                            title="SEND NOW"
                            loading={this.state.loading}
                            pressTo={this.pay.bind(this)}
                        />
                    </View>
                </View>
            </View>
        );
    }
};


const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },

    Body: {
        marginTop:50,
        justifyContent: 'center',
        flex: 1,
    },
    cardrow: {
        paddingHorizontal:30,
        justifyContent: 'space-evenly',
    },
    heading: {
        textAlign: 'left',
        color: 'white',
        opacity: 1,
        fontFamily: 'Roboto-Bold',
        fontSize: 18,
        textAlign: 'center',
        textAlignVertical: 'center',
        padding: 5,
    },
    HeaderTextMain: {
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingTop: 30,
        paddingBottom: 20,
        flexDirection: 'row',
    },
    HeaderText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.secondary,
        opacity: 0.8,
    },
    HeaderText1: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.primary,
        opacity: 0.8,
    },
    txt2:{
        fontWeight:'300',
        color:'#000'
    }
});

export default ConfirmTransaction;
