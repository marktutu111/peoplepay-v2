import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import Colors from '../../constants/colors';
import TxtInput from '../../components/TxtInput';
import {ScrollView} from 'react-native-gesture-handler';
import Header from '../../components/Header';
import HeaderText from '../../components/HeaderText';

const ProxyTransferScreen = props => {
    return (
        <View style={styles.screen}>
            {/* Header of the Screen */}
            <Header pressTo={() => props.navigation.goBack()} />
            <ScrollView
                style={styles.scroll}
                keyboardShouldPersistTaps="always"
                keyboardDismissMode="on-drag">
                {/* Screen Title */}
                <HeaderText title1="Transfer" title2="money to" />

                {/* Screen Main Content */}
                <View style={styles.Body}>
                    <View style={styles.textFieldContainer}>
                        <TxtInput
                            placeholder="Recipient Name"
                            IconName="user"
                            IconType="antdesign"
                        />
                        <TxtInput
                            placeholder="Proxy ID"
                            IconName="idcard"
                            IconType="antdesign"
                        />
                        <TxtInput
                            placeholder="Amount"
                            IconName="money"
                            IconType="font-awesome"
                        />
                        {/* <TxtInput
                            placeholder="Account Number"
                            IconName="bank"
                            IconType="material-community"
                        /> */}
                        <TxtInput
                            placeholder="Description"
                            IconName="description"
                            IconType="material"
                        />
                    </View>

                    {/* Button */}
                    <View style={styles.buttonContainer}>
                        <LoginPrimaryButton
                            title="send"
                            pressTo={() =>
                                props.navigation.navigate('ProxyResponse')
                            }
                        />
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    HeaderTextMain: {
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingTop: 10,
        // paddingBottom: 20,
        // flexDirection: 'row',
    },
    HeaderText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.secondary,
        // textAlign: 'center',
        opacity: 0.8,
    },
    HeaderText1: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.primary,
        // textAlign: 'center',
        opacity: 0.8,
        bottom: 20,
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
        paddingHorizontal: 30,
    },

    buttonContainer: {
        // alignItems: 'center',
        paddingHorizontal: 30,
        paddingVertical: 20,
    },
});

export default ProxyTransferScreen;
