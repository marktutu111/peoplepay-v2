import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import CardThumbnail2 from '../../components/CardThumbnail2';
import HeaderTxt1 from '../../components/HeaderTxt1';
import {ScrollView} from 'react-native-gesture-handler';

import Header from '../../components/Header';

const ProxyPayScreen = props => {
    return (
        <View style={styles.screen}>
            {/* Header of the Screen */}
            {/* <Header pressTo={() => props.navigation.goBack()} /> */}
            {/* Header */}
            <Header pressTo={() => props.navigation.navigate("Home")} />
            <ScrollView
                style={styles.scroll}
                keyboardShouldPersistTaps="always"
                keyboardDismissMode="on-drag">
                {/* Screen Title */}
                <HeaderTxt1 txt1="Proxy" txt2="Pay" />
                {/* Screen Main Content */}
                <View style={styles.Body}>
                    <View style={styles.container}>
                        <View>
                            <CardThumbnail2
                                iconname="bell"
                                icontype="feather"
                                title="Subscribe"
                                subtitle="Create an account with Proxy Pay"
                                navTitle="Get Started"
                                x
                                pressTo={() =>
                                    props.navigation.navigate('Subscribe')
                                }
                            />
                        </View>
                        <View>
                            <CardThumbnail2
                                iconname="bank-transfer"
                                icontype="material-community"
                                title="Transfer to Proxy"
                                subtitle="Transfer money to any proxy pay user."
                                navTitle="Get in"
                                pressTo={() =>
                                    props.navigation.navigate('ProxyTransfer')
                                }
                            />
                        </View>
                        <View>
                            <CardThumbnail2
                                iconname="update"
                                icontype="material-community"
                                title="Update"
                                subtitle="Update your Proxy Pay details."
                                navTitle="Get in"
                                pressTo={() =>
                                    props.navigation.navigate('ProxyUpdate')
                                }
                            />
                        </View>
                        <View>
                            <CardThumbnail2
                                iconname="trash-2"
                                icontype="feather"
                                title="Unsubscribe"
                                subtitle="Cancel your Proxy Pay subscription."
                                navTitle="Cancel"
                                pressTo={() =>
                                    props.navigation.navigate('ProxyDelete')
                                }
                            />
                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
        paddingHorizontal: 30,
        paddingTop: 30,
    },
    container: {
        bottom: 20,
    },
});
export default ProxyPayScreen;
