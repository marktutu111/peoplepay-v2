import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    TextInput,
} from 'react-native';
import Colors from '../../constants/colors';
import Header from '../../components/Header';
import HeaderText from '../../components/HeaderText';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import {ScrollView} from 'react-native-gesture-handler';

// change cont declaration name and export default name
const Verify = props => {
    return (
        <View style={styles.screen}>
            {/* Header of the Screen */}
            <Header pressTo={() => props.navigation.goBack()} />
            <ScrollView
                style={styles.scroll}
                keyboardShouldPersistTaps="always"
                keyboardDismissMode="on-drag">
                {/* Screen Title */}
                <HeaderText title1="Verify" />
                {/* Screen Main Content */}
                <View style={styles.Body}>
                    <Text style={styles.infoText}>
                        Verification code has been sent to your phone via sms.
                        Please check your mobile phone.
                    </Text>

                    <TextInput style={styles.textfield} />

                    <View style={styles.resendContainer}>
                        <TouchableOpacity>
                            <Text style={styles.resendText}>RESEND</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.btn}>
                        <LoginPrimaryButton
                            title="Continue"
                            pressTo={() => props.navigation.navigate('Succes')}
                        />
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },

    Body: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 30,
    },
    textfield: {
        borderBottomColor: Colors.tetiary,
        borderBottomWidth: 1,
        // width: '100%',
        // marginHorizontal: 30,
        alignItems: 'center',
        fontFamily: 'Roboto-Regular',
        color: Colors.tetiary,
        textAlign: 'center',
        width: '80%',
        letterSpacing: 10,
    },
    infoText: {
        fontFamily: 'Roboto-Medium',
        width: '80%',
        paddingVertical: 20,
        textAlign: 'center',
        opacity: 0.5,
        // paddingHorizontal: 10,
    },
    resendContainer: {
        width: '100%',
        alignItems: 'flex-end',
        paddingHorizontal: 30,
        paddingVertical: 15,
    },
    resendText: {
        fontFamily: 'Roboto-Bold',
        width: '100%',
        paddingBottom: 20,
        textAlign: 'right',
        opacity: 0.7,
        color: Colors.secondary,
    },
    btn: {
        paddingHorizontal: 30,
    },
});

export default Verify;
