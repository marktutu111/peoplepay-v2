import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import Colors from '../../constants/colors';
import TxtInput from '../../components/TxtInput';
import {ScrollView} from 'react-native-gesture-handler';
import Header from '../../components/Header';
import HeaderText from '../../components/HeaderText';

const ProxyTransferScreen = props => {
    return (
        <View style={styles.screen}>
            {/* Header of the Screen */}
            <Header pressTo={() => props.navigation.goBack()} />
            <ScrollView
                style={styles.scroll}
                keyboardShouldPersistTaps="always"
                keyboardDismissMode="on-drag">
                {/* Screen Title */}
                <HeaderText title1="Transfer" title2="money to" />

                {/* Screen Main Content */}
                <View style={styles.Body}>
                    <Text style={styles.heading}>
                        Please confirm 'Name of Beneficiary' and 'Bank'.
                    </Text>
                    <View style={styles.textFieldContainer}>
                        <TxtInput
                            placeholder="Name of Beneficiary"
                            IconName="user"
                            IconType="antdesign"
                        />
                        <TxtInput
                            placeholder="Bank"
                            IconName="bank"
                            IconType="material-community"
                        />
                    </View>

                    {/* Button */}
                    <View style={styles.buttonContainer}>
                        <LoginPrimaryButton
                            title="Confirm"
                            pressTo={() =>
                                props.navigation.navigate('ProxyVerify')
                            }
                        />
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    heading: {
        textAlign: 'left',
        paddingBottom: 10,
        marginBottom: 10,
        borderBottomWidth: 0.8,
        borderColor: Colors.primary,
        // marginHorizontal: 30,
        fontFamily: 'Roboto-Regular',
        color: Colors.tetiary,
        opacity: 0.7,
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
        paddingHorizontal: 30,
    },
    textFieldContainer: {
        marginVertical: 20,
    },

    buttonContainer: {
        // alignItems: 'center',
        paddingVertical: 20,
        paddingHorizontal: 30,
    },
});

export default ProxyTransferScreen;
