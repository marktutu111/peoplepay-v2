import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Colors from '../../constants/colors';
import QuickserviceTile from '../../components/QuickserviceTile';
import {ScrollView} from 'react-native-gesture-handler';
import Header from '../../components/Header';
import HeaderText from '../../components/HeaderText';

const ProxySendScreen = props => {
    return (
        <ScrollView
            style={styles.scroll}
            keyboardShouldPersistTaps="always"
            keyboardDismissMode="on-drag">
            <View style={styles.screen}>
                <Header pressTo={() => props.navigation.goBack()} />
                <HeaderText title1="Transfer" title2="money to" />

                <View style={styles.body}>
                    <View style={styles.TopRow}>
                        <View>
                            <Text style={styles.LargeText}>Transfer Type</Text>
                        </View>
                    </View>
                    <View style={styles.cardrow}>
                        <View style={styles.cardItem}>
                            <QuickserviceTile
                                iconname="bank"
                                iconsize={70}
                                icontype="material-community"
                                pressTo={() =>
                                    props.navigation.navigate('ProxyTransfer')
                                }
                            />
                            <Text style={styles.cardItemlabel}>
                                Bank Transfer
                            </Text>
                        </View>
                        <View style={styles.cardItem}>
                            <QuickserviceTile
                                iconname="cellphone"
                                iconsize={70}
                                icontype="material-community"
                                pressTo={() =>
                                    props.navigation.navigate('ProxyTransfer')
                                }
                            />
                            <Text style={styles.cardItemlabel}>
                                Mobile Money
                            </Text>
                        </View>
                    </View>
                </View>
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    scroll: {
        backgroundColor: Colors.accent,
    },
    body: {
        flex: 1,
    },
    mainContainer: {
        flex: 1,
        flexWrap: 'wrap',
        flexDirection: 'row',
    },
    cardItemlabel: {
        textAlign: 'center',
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
    },
    containerStyle: {
        padding: 10,
        borderWidth: 0,
        // marginBottom:5,
        marginTop: 10,
    },
    cardrow: {
        width: '100%',
        justifyContent: 'space-evenly',
        flexDirection: 'row',
    },
    TopRow: {
        paddingVertical: 20,
        width: '80%',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    LargeText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 25,
        color: Colors.tetiary,
    },
});
export default ProxySendScreen;
