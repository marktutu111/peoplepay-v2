import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import Colors from '../../constants/colors';
import Header from '../../components/Header';
import {ScrollView} from 'react-native-gesture-handler';

const ProxyPaySuccessScreen = props => {
    return (
        <ScrollView
            keyboardShouldPersistTaps="always"
            keyboardDismissMode="on-drag">
            <View style={styles.screen}>
                {/* Header Component */}
                <Header
                    label1="Success"
                    // label2="password"
                    pressTo={() => props.navigation.goBack()}
                />
                <View style={styles.Body}>
                    <View style={styles.TopRow}>
                        <View>
                            <Text style={styles.LargeText}>Success</Text>
                        </View>
                    </View>
                    <Text
                        style={{
                            fontFamily: 'Roboto-Regular',
                            width: '80%',
                            paddingVertical: 20,
                            textAlign: 'center',
                        }}>
                        Operation has being Successful
                    </Text>
                </View>

                <View style={styles.buttonContainer}>
                    <LoginPrimaryButton
                        title="Ok"
                        pressTo={() => props.navigation.navigate('ProxyPay')}
                    />
                </View>
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    HeaderRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignContent: 'space-between',
        alignItems: 'baseline',
        width: '100%',
        borderBottomColor: Colors.primary,
        borderBottomWidth: 1,
        paddingRight: '36%',
    },
    HeaderText: {
        textAlign: 'center',
        fontFamily: 'Roboto-ExtraBold',
        fontSize: 18,
    },
    navIcon: {
        padding: 10,
    },
    Body: {
        // paddingHorizontal: '10%',
        width: '100%',
        alignItems: 'center',
    },
    TopRow: {
        paddingVertical: 20,
        width: '80%',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    LargeText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 25,
        color: Colors.tetiary,
    },
    buttonContainer: {
        // alignItems: 'center',
        paddingVertical: 50,
        paddingHorizontal: 30,
    },
});
export default ProxyPaySuccessScreen;
