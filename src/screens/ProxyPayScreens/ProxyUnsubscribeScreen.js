import React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import Colors from '../../constants/colors';
import Header from '../../components/Header';
import HeaderText from '../../components/HeaderText';
import {ScrollView} from 'react-native-gesture-handler';

const ProxyUnsubscribeScreen = props => {
    return (
        <View style={styles.screen}>
            {/* Header of the Screen */}
            <Header pressTo={() => props.navigation.goBack()} />
            <ScrollView
                style={styles.scroll}
                keyboardShouldPersistTaps="always"
                keyboardDismissMode="on-drag">
                {/* Screen Title */}
                <HeaderText title1="Delete" />
                {/* Screen Main Content */}
                <View style={styles.Body}>
                    <Text style={styles.textContainer}>
                        Please note you are about to deactivate subcription to
                        this service.Please tap "0k" to proceed or "Cancel" to
                        back to the mainpage
                    </Text>
                    <Image
                        source={require('../../assets/images/undraw_feeling_blue_4b7q.png')}
                        resizeMode="contain"
                        style={styles.imageContainer}
                    />
                    <View style={styles.btnContainer}>
                        <LoginPrimaryButton
                            title="ok"
                            pressTo={() =>
                                props.navigation.navigate('ProxyVerify')
                            }
                        />
                    </View>
                    <TouchableOpacity
                        onPress={() => props.navigation.goBack()}
                        activeOpacity={0.8}
                        style={styles.ButtonContainer}>
                        <Text style={styles.buttonText}>Cancel</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    HeaderTextMain: {
        color: Colors.secondary,
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingTop: 30,
        paddingBottom: 20,
    },
    HeaderText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.secondary,
        textAlign: 'left',
        opacity: 0.8,
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
        alignItems: 'center',
    },
    textContainer: {
        fontFamily: 'Roboto-Medium',
        paddingVertical: 20,
        width: '80%',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        opacity: 0.6,
        fontSize: 14,
        bottom: 20,
    },
    imageContainer: {
        height: 250,
        width: 250,
        bottom: 50,
    },
    btnContainer: {
        paddingVertical: 20,
        width: '100%',
        paddingHorizontal: 30,
        bottom: 50,
    },
    ButtonContainer: {
        width: '85%',
        height: 60,
        // paddingHorizontal: 30,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        borderRadius: 50,
        // paddingHorizontal: 30,
        backgroundColor: '#B6202B',
        bottom: 60,
    },

    buttonText: {
        fontSize: 18,
        fontFamily: 'Roboto-SemiBold',
        textTransform: 'uppercase',
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        backgroundColor: 'transparent',
        letterSpacing: 0.2,
    },
});

export default ProxyUnsubscribeScreen;
