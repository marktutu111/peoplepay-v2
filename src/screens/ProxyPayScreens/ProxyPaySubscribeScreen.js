import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import Colors from '../../constants/colors';
import TxtInput from '../../components/TxtInput';
import {ScrollView} from 'react-native-gesture-handler';
import Header from '../../components/Header';
import HeaderText from '../../components/HeaderText';
import  { CustomExample } from '../../components/customPicker';

const ProxyPaySubscribeScreen = props => {
    return (
        <View style={styles.screen}>
            {/* Header of the Screen */}
            <Header pressTo={() => props.navigation.goBack()} />
            <ScrollView
                style={styles.scroll}
                keyboardShouldPersistTaps="always"
                keyboardDismissMode="on-drag">
                {/* Screen Title */}
                <HeaderText title1="Register" />
                {/* Screen Main Content */}
                <View style={styles.Body}>
                    <View style={styles.textFieldContainer}>
                        <TxtInput
                            placeholder="Proxy ID"
                            IconName="idcard"
                            IconType="antdesign"
                        />
                    </View>
                    <View style={styles.dropdownContainer}>
                        <CustomExample placeholder="Select account" />
                    </View>
                    <View style={styles.textFieldContainer}>
                        <TxtInput
                            keyboardType="number-pad"
                            placeholder="Account Number"
                            IconName="account-card-details-outline"
                            IconType="material-community"
                        />
                    </View>
                    <View style={styles.buttonContainer}>
                        <LoginPrimaryButton
                            title="Subscribe"
                            pressTo={() =>
                                props.navigation.navigate('ProxyVerify')
                            }
                        />
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    HeaderTextMain: {
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingTop: 10,
        paddingBottom: 20,
        // flexDirection: 'row',
    },
    HeaderText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.secondary,
        // textAlign: 'left',
        opacity: 0.8,
    },
    HeaderText1: {
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        color: Colors.primary,
        // textAlign: 'center',
        opacity: 0.8,
        bottom: 10,
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
    },
    dropdownContainer: {
        paddingVertical: 10,
        marginHorizontal: 30,
    },
    textFieldContainer: {
        paddingHorizontal: 30,
    },
    buttonContainer: {
        // alignItems: 'center',
        paddingVertical: 50,
        paddingHorizontal: 30,
    },
});

export default ProxyPaySubscribeScreen;
