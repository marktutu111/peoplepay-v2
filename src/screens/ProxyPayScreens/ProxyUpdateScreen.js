import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import Colors from '../../constants/colors';
import TxtInput from '../../components/TxtInput';
import Header from '../../components/Header';
import HeaderText from '../../components/HeaderText';

import {ScrollView} from 'react-native-gesture-handler';
import { CustomExample } from '../../components/customPicker';

const ProxyUpdateScreen = props => {
    return (
        <View style={styles.screen}>
            {/* Header of the Screen */}
            <Header pressTo={() => props.navigation.goBack()} />
            <ScrollView
                style={styles.scroll}
                keyboardShouldPersistTaps="always"
                keyboardDismissMode="on-drag">
                {/* Screen Title */}
                <HeaderText title1="Update" />
                {/* Screen Main Content */}
                <View style={styles.Body}>
                    <View style={styles.dropdownContainer}>
                        <CustomExample placeholder="Select bank type" />
                    </View>
                    <View style={styles.textFieldContainer}>
                        <TxtInput
                            placeholder="Account Number"
                            keyboardType="number-pad"
                            IconName="account-card-details-outline"
                            IconType="material-community"
                        />
                    </View>
                    <View style={styles.buttonContainer}>
                        <LoginPrimaryButton
                            title="Update"
                            pressTo={() =>
                                props.navigation.navigate('ProxyVerify')
                            }
                        />
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
    },
    dropdwonContainer: {
        paddingVertical: 10,
    },
    itemContainer: {
        paddingVertical: 20,
    },
    dropdownContainer: {
        paddingVertical: 10,
        marginHorizontal: 30,
    },
    textFieldContainer: {
        paddingHorizontal: 30,
    },
    buttonContainer: {
        // alignItems: 'center',
        paddingVertical: 20,
        paddingHorizontal: 30,
    },
});

export default ProxyUpdateScreen;
