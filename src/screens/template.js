import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Colors from '../constants/colors';
import Header from '../components/Header';
import {ScrollView} from 'react-native-gesture-handler';

// change cont declaration name and export default name
const NameHere = props => {
    return (
        <View style={styles.screen}>
            {/* Header of the Screen */}
            <Header pressTo={() => props.navigation.goBack()} />
            <ScrollView style={styles.scroll}>
                {/* Screen Title */}
                <View style={styles.HeaderTextMain}>
                    <Text style={styles.HeaderText}> Header label 1 </Text>
                    <Text style={styles.HeaderText}> Header label 2 </Text>
                </View>
                {/* Screen Main Content */}
                <View style={styles.Body}>{/* Content Here */}</View>
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    HeaderTextMain: {
        color: Colors.secondary,
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingVertical: 15,
        paddingBottom: 20,
    },
    HeaderText: {
        fontFamily: 'Roboto-ExtraBold',
        fontSize: 30,
        color: Colors.secondary,
        textAlign: 'left',
        opacity: 0.8,
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
    },
});

export default NameHere;
