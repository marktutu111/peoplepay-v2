import React, {Component} from 'react';
import {StatusBar,BackHandler,AppState,Platform} from 'react-native';
import Navigator from './src/navigation/MainNavigator';
import AuthState, {AuthConsumer} from './src/states/auth.state';
import TransactionState from './src/states/transactions.state';
import WalletState from './src/states/wallets.state';
import BeneficiariesState from './src/states/beneficiaries.state';
import colors from './src/constants/colors';
import ReferralProvider from './src/states/referrals.state';



export default class App extends Component {

  render() {
    const root=()=>(
      <AuthState>
        <AuthConsumer>
          {({state})=>{
            const {user,password}=state;
            if (user && user._id) {
              return (
                <WalletState user={user} password={password}>
                  <TransactionState>
                    <BeneficiariesState user={user}>
                      <ReferralProvider {...state}>
                        <StatusBar
                          backgroundColor={colors.secondary}
                          barStyle={'dark-content'}
                        />
                          <Navigator.MainNavigator />
                      </ReferralProvider>
                    </BeneficiariesState>
                  </TransactionState>
                </WalletState>
              );
            }
            return <Navigator.AuthNavigator />;
          }}
        </AuthConsumer>
      </AuthState>
    );
    return root();
  }
}
